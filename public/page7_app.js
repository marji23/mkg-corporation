(function (){

  angular
      .module('page7App', ['ngLoader' , 'ui.bootstrap','ui-notification'])
      .controller('page7Controller', Page7Controller)
      .config(function(NotificationProvider) {
          NotificationProvider.setOptions({
              delay: 10000,
              startTop: 20,
              startRight: 10,
              verticalSpacing: 20,
              horizontalSpacing: 20,
              positionX: 'right',
              positionY: 'top'
          });
        });



    function Page7Controller(Notification ,$scope, $http, $window) {

      $scope.logout_func = logout_func;
      $scope.findTomember = findTomember;
      $scope.day_filter = day_filter;
      $scope.reset_filter = reset_filter;
      $scope.updateVisitor = updateVisitor;
      $scope.myinfo = myinfo;
      $scope.onChangeNum_perPage = onChangeNum_perPage;
      $scope.pageChanged = pageChanged;

      var s_lvl_name = ["본사" , "부본사" , "총판" , "대리"];
     

      var whole_notice;
      $scope.notify_clicked = notify_clicked;

      $scope.closeNav = closeNav;

      var acdata = [];

      var audio = new Audio('./sounds/Maramba.mp3');

      function init() {
        console.log("page7-hey initer");

        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 1)
                $window.location.href = '/index.html';
              else
              {
                console.log("aya~~")
                $http.get('/api/get_global_acdata')
                      .success(function(ggadata){
                        acdata = ggadata.acdata;
                        $scope.mycid = ggadata.mycid;
                        $scope.mygroup = ggadata.mygroup;
                        $scope.mydeeper = ggadata.mydeeper;

                        if(($scope.mydeeper == 1)&&(acdata[0].ac_visitor <2))
                          logout_func();

                        imp_allow();

                        load_sidebar();
                        $scope.topnav = {url : "TopNav.html"};
                        showAllStatus();
                        loadShopList();
                      });

              }
          
            });

      }

      init();

      function imp_allow()
      {
          var mystat = {};
          var groupstat = {};
          var finalclc = {}; 
          console.log("$scope.mygroup : " + $scope.mygroup);

          acdata.forEach(function(raudata){
                          if(raudata.uinfo[0] != undefined)
                          {
                            if(raudata.uinfo[0].u_id == $scope.mycid)
                              mystat = raudata;
                          }
                          else if(raudata.ginfo[0] != undefined)
                          {
                            if(raudata.ginfo[0].g_id == $scope.mycid)
                              mystat = raudata;
                            if(raudata.ginfo[0].g_id == $scope.mygroup)
                              groupstat = raudata
                          }
                        });
          if($scope.mygroup < 100) // not manager grouop
              finalclc = mystat;
          else
              {
                
                finalclc = mystat; // init

                var counter = 0;
                for (var key in mystat) {
                    if(counter < 23)
                    {
                      if(mystat[key] == 0)
                        finalclc[key] = groupstat[key];
                      else
                        finalclc[key] = mystat[key];

                      //console.log("final : " + finalclc[key] + " <= group : " + groupstat[key] + " + mine : " + mystat[key]);
                    }  //
                    counter++;
                }
              }
              //console.log("final : " + finalclc.ac_zhong);
            $scope.fang = finalclc;

      }

      function load_sidebar()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                    switch(rsys.result.deep)
                    {
                      case 1:
                        $scope.template = {url : "sidebar.html"};
                        $scope.top = {url : "topbar.html"};
                       break;
                      case 2:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 3:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 4:
                        $scope.template = {url : "sidebar3.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                    }

                    setStatusboxWidth();
                  });
      }

      $scope.num_per_page = 10;
      $scope.maxSize = 3;
      $scope.bigTotalItems = 10;
      $scope.bigCurrentPage = 1;

      function pageChanged() {

          console.log('Page changed to: ' + $scope.bigCurrentPage);
          console.log($scope.bigCurrentPage);
          detail_filter();
      }

      function onChangeNum_perPage(){

        detail_filterWhenPageCalc();
      }

      function myinfo()
      {

        $http.get('/api/set_myinfo')
            .success(function(cbdata){
              
              console.log("_kekeke : " + cbdata[0].savepay);

                var fnum = parseInt(cbdata[0].orig_gid);
                if( fnum == 2 )
                  $window.location.href = '/server-page8(edit&detail).html';
                if( fnum > 2 )
                  $window.location.href = '/server-page15(edit&detail).html';
            });
      }

      function logout_func()
      {
        console.log("logouted");
        $http.get('/api/logout_func')
            .success(function(cbdata){
              if(cbdata.logresult == "success")
                $window.location.href = '/index.html';
            });
      }

      function checkFromAnotherpage()
      {
        $http.get('/api/get_updated_mem')
                .success(function(cbdata){
                  console.log(cbdata);
                      if(cbdata != null){
                        if((cbdata.u_nickname != "") && (cbdata.u_nickname != undefined))
                          { 
                            console.log("nicknamessss : " + cbdata.u_nickname );
                            console.log("ongap : " + $('#uppername option:contains('+ cbdata.u_nickname +')').val());
                            $('#uppername').val( $('#uppername option:contains('+ cbdata.u_nickname +')').val() );
                            CheckFromTopNav();
                          }
                        else
                          {
                            console.log("2");
                            CheckFromTopNav();
                          }
                      }
                      else
                      {
                        console.log("3");
                         CheckFromTopNav();
                      }
                });
      }

      function CheckFromTopNav()
      {
        console.log("check from top nav..........");
        $http.get("/api/GetTopNavVal")
              .success(function(mgtnvdata){
                console.log("gettopnaval-1 : " + JSON.stringify(mgtnvdata));
                if(mgtnvdata.rouflag != 1000)
                {
                  
                  $http.post("/api/SetTopNavVal" , {rouflag : 1000})
                    .success(function(gtnndata){

                      if(gtnndata.result == "success")
                        day_filter(mgtnvdata.dfilter);

                    });
                }
                else
                  detail_filterWhenPageCalc();
              });
      }

// - start make awesome tree structured upper shop list part

      function eachtext(nums , txt , sindex)
      {
        nums = 4*nums;
        var prefix = "|";
        for(i=0;i<nums;i++)
          prefix += "-";
        var suffix = " (" + s_lvl_name[sindex] + ") ";
        return prefix + ">" + txt + suffix;
      }

      function make_items_title(maxlvl , global_list)
      {
        console.log("txt_res : " + s_lvl_name.length);
        var inc = s_lvl_name.length - maxlvl -1;
        global_list.forEach(function(item){

          item['su_text'] = eachtext( item['su_lvlnum'] , item.u_nickname , item.u_deep-1 );
          //item['su_text'] = eachtext( item['su_lvlnum'] , item.u_nickname , (item['su_lvlnum'] + inc) );
          console.log('su : ' + item.su_text);
        });

        $scope.shoplists = global_list;
        checkFromAnotherpage();

      }

      function make_Structured_menutitle( lvl , starter , arr , global_list , lvl_gijun )
      {
        $scope.cout++;
        
        if( (arr.length) == $scope.cout )
        {
            console.log("really finished~~" + lvl_gijun);
            make_items_title(lvl_gijun , global_list);
        }

        lvl++;

        arr.forEach(function(item){
          //console.log("item : " + item.u_uppercode + "starter" + starter.u_uppercode);
            if(item.u_uppercode == starter.u_id)
            {
              item['su_lvlnum'] = lvl;
              //console.log("l + :"+lvl + " - " + lvl_gijun);
              global_list.push(item);

              if(lvl_gijun < lvl)
                lvl_gijun = lvl;
              make_Structured_menutitle( lvl , item , arr , global_list , lvl_gijun );

            }
            else{

                
            }
        });
      }

      function get_Structured_shoplist(sflag , arr)
      {
        var start_v;
        var global_list = [];

        if(sflag == 0)
        {
          start_v = arr[0];
          arr[0]['su_lvlnum'] = 0;
          global_list.push(arr[0]);
        }
        else if(sflag == 1)
        {
          arr.forEach(function(item){
            if(item.u_uppercode == "1")
            {
              start_v = item;
              item['su_lvlnum'] = 0;
              global_list.push(item);
            }
          });
        }

        console.log("startv : " + start_v.u_nickname);

        $scope.cout = 0;
        var lvl_gijun = 0;
        
        make_Structured_menutitle( 0 , start_v , arr , global_list , lvl_gijun );

      }

// - end make awesome tree structured upper shop list part

      function loadAllShoplist()
      {
          console.log("loadshoplist~~~~");
          $http.post('/api/getRealAllChildshop')
            .success(function(cbdata){
              console.log('rara : '+cbdata);
              //$scope.shoplists = cbdata;
              get_Structured_shoplist(1 , cbdata);
            });
      }

      function loadShopList()
      {
        console.log("lodshoplist");
        var forshopcase = [];
        $http.get('/api/getDocofLogined')
            .success(function(cbdata1){
              if(cbdata1[0].u_groupid == '2') {
                console.log("shop");

                $http.post('/api/getRealChildshop')
                    .success(function(cbdata){

                      //forshopcase.push(cbdata1[0]);
                      cbdata.obj.splice(0, 0, cbdata1[0]);
                      //cbdata.obj.push(cbdata1[0]);
                      //console.log("mememememe :     "+ cbdata1[0].u_nickname);
                      forshopcase = cbdata.obj;
                      //console.log("come : "+forshopcase.length);

                      //$scope.shoplists = forshopcase;

                      get_Structured_shoplist(0 , forshopcase);

                    });

              }
              else{
                console.log("manager");
                loadAllShoplist();
              }
            });
      }

      function detail_filterWhenPageCalc()
      {

        var obj = {};

        if($('#fromdate').val()!="")
          obj['fromdate'] = $('#fromdate').val();

        if($('#todate').val()!="")
          obj['todate'] = $('#todate').val();

        if($('#uu_id').val()!="")
          obj['uu_id'] = {'$regex': '.*'+$('#uu_id').val()+'.*'};

        if($('#uu_nickname').val()!="")
          obj['uu_nickname'] = {'$regex': '.*'+$('#uu_nickname').val()+'.*'};

        if($('#uu_mail').val()!="")
          obj['uu_mail'] = {'$regex': '.*'+$('#uu_mail').val()+'.*'};

        if($('#uu_ip').val()!="")
          obj['uu_ip'] = {'$regex': '.*'+$('#uu_ip').val()+'.*'};

        if( $('#uppertype').val() != '1' )
          obj['u_uppertype'] = $('#uppertype').val();

        $scope.working = true;
        $scope.message = 'Loading...'

        //check part
        var checkflag = {};
        if ($('#fflag').is(":checked"))
          checkflag['opt'] = 0; // all members
        else
          checkflag['opt'] = 1;
        checkflag['targetid'] = $('#uppername').val();
        
        console.log("for matched : " + JSON.stringify(obj));

        $http.post('/api/getMatchedMemData' , {"main" : obj , "other" : checkflag})
              .success(function(gmmdata){
                $scope.bigTotalItems = (10*Math.ceil(gmmdata.matched_len/$scope.num_per_page));   
                
                
                 //pagination
                var pageval = { "currentpage" : parseInt($scope.bigCurrentPage) , "numperpage" : parseInt($scope.num_per_page) };

                $http.post('/api/find_member_result_by_detail' , {"main" : obj , "other" : checkflag , "pv" : pageval})
                      .success(function(cbdata){

                        console.log("arrived!!! : " + cbdata.length);
                        getProfits(cbdata);

                      });

              });

      }

      function detail_filter()
      {


        var obj = {};

        if($('#fromdate').val()!="")
          obj['fromdate'] = $('#fromdate').val();

        if($('#todate').val()!="")
          obj['todate'] = $('#todate').val();

        if($('#uu_id').val()!="")
          obj['uu_id'] = {'$regex': '.*'+$('#uu_id').val()+'.*'};

        if($('#uu_nickname').val()!="")
          obj['uu_nickname'] = {'$regex': '.*'+$('#uu_nickname').val()+'.*'};

        if($('#uu_mail').val()!="")
          obj['uu_mail'] = {'$regex': '.*'+$('#uu_mail').val()+'.*'};

        if($('#uu_ip').val()!="")
          obj['uu_ip'] = {'$regex': '.*'+$('#uu_ip').val()+'.*'};

        if( $('#uppertype').val() != '1' )
          obj['u_uppertype'] = $('#uppertype').val();


        $scope.working = true;
        $scope.message = 'Loading...'

        //check part
        var checkflag = {};
        if ($('#fflag').is(":checked"))
          checkflag['opt'] = 0; // all members
        else
          checkflag['opt'] = 1;
        checkflag['targetid'] = $('#uppername').val();

        //pagination
        var pageval = { "currentpage" : parseInt($scope.bigCurrentPage) , "numperpage" : parseInt($scope.num_per_page) };

        console.log("for matched : " + JSON.stringify(obj));

        $http.post('/api/find_member_result_by_detail' , {"main" : obj , "other" : checkflag , "pv" : pageval})
            .success(function(cbdata){

              console.log("arrived!!! : " + cbdata.length);
              getProfits(cbdata);

            });

      }

      function get_other_detail(ff_obj) // for money , shop earned , visitor earned
      {
        console.log("getother : " + ff_obj.length);
        $http.post('/api/get_other_detail' , ff_obj)
            .success(function(cbdata){

              console.log(' yeah , great!!!  :  ' + cbdata.length);
              
              $scope.working = false;
              $scope.detailers = cbdata;
            });
      }

      function getProfits(f_obj)
      {
        $http.post('/api/startForVisitor_profit', f_obj)
              .success(function(cbdata){
                get_other_detail( cbdata.obj );
              });
      }

      /* Define new prototype methods on Date object. */
      // Returns Date as a String in YYYY-MM-DD format.
      Date.prototype.toISODateString = function () {
        return this.toISOString().substr(0,10);
      };

      // Returns new Date object offset `n` days from current Date object.
      Date.prototype.toDateFromDays = function (n) {
        n = parseInt(n) || 0;
        var newDate = new Date(this.getTime());
        newDate.setDate(this.getDate() + n);
        return newDate;
      };

      function day_filter(types)
      {
        console.log(types);
        
        switch(types)
        {
          case 31:
            
            var dto = new Date();
            var dfrom  = new Date();
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            detail_filterWhenPageCalc();
            break;
          case 32:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-7);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            detail_filterWhenPageCalc();
            break;
          case 33:
            var dto = new Date();
            var dfrom  = new Date();
            dfrom.setHours(0,0,0,0);
            dfrom.setDate(1);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            detail_filterWhenPageCalc();
            break;
          case 34:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-90);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            detail_filterWhenPageCalc();
            break;
          case 35:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-180);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            detail_filterWhenPageCalc();
            break;
          case 36:
            var dto = new Date();
            var dfrom = "";
            console.log( dto.toISODateString() + " : " +  dfrom );
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            detail_filterWhenPageCalc();
            break;
        }
      }

      function reset_filter()
      {
        $('#fromdate').val("");
        $('#todate').val("");
        $('#uppername').val($("#uppername option:first").val());
        $('#uppertype').val($("#uppertype option:first").val());
        //$('#fflag').attr('checked', false);
        $('#uu_id').val("");
        $('#uu_nickname').val("");
        $('#uu_mail').val("");
        $('#uu_ip').val("");

        //console.log("reset_filter");
      }

      function findTomember()
      {

            console.log("exist");

            detail_filterWhenPageCalc();

        
      }

      function updateVisitor(selected)
      {
        $http.post('/api/set_selected_visitor' , selected)
            .success(function(cbdata){
              $window.location.href = "/server-page7(edit&detail).html";
              
            });
      }

      //start for notice on the top bar

      function checkForSounds(typesound)
      {
        $http.get('/api/getSoundsjson')
            .success(function(sdata)
            {
              if(typesound == 1)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) || (sdata.obj.notify < $scope.len ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
              else if(typesound == 2)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
            });
      }

      function load_notice()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    if(rsys.result.deep == 1){
                      console.log("dfdf");
                     $http.get('/api/get_all_notice')
                        .success(function(cbdata){
                            console.log("rusuky : " + cbdata.obj);
                            whole_notice = cbdata.obj;
                            whole_notice = whole_notice.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

                            var unreads = [];
                            whole_notice.forEach(function(item){
                              if(item.am_status == 0)
                                {
                                  unreads.push(item);
                                  console.log(JSON.stringify(item));
                                }
                            });

                            $scope.ur = unreads;
                            $scope.len = unreads.length;

                            checkForSounds(1);

                        });
                    }
                    else
                      checkForSounds(2);

                  });
      }

      function notify_clicked(iobj)
      {
        whole_notice.forEach(function(item){
          if(item == iobj)
            item.am_status = 1;
        }); 
        $http.post('/api/update_all_notice' , { obj : whole_notice } )
                .success(function(cbdata){
                  if(cbdata.result == true){

                      $http.get('/api/getAllalarm' )
                        .success(function(cbdata1){
                            var atypes = cbdata1.obj;
                            var contents = "";
                            var titles = "";
                            for(var i=0;i<atypes.length;i++)
                              if(atypes[i].a_id == iobj.am_content)
                              {
                                contents = atypes[i].a_content.a_en;
                                titles = atypes[i].a_title.a_en;
                              }

                               var mbody = "<p> 위반대상 : "+ iobj.am_target +" </p>"+
                               "<p> 위반대상 식별자 : "+ iobj.am_id +" </p>"+
                              "<p> 위반날자 : "+ iobj.am_date.toLocaleString().split("T")[0] + " " + iobj.am_date.toLocaleString().split("T")[1] +"</p>"+
                              "<p> 위반내용 : "+ contents +" </p> ";
          Notification.success({title : '<h3 style="text-align: center;"><i style=" -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: orange; color: rgba(187, 48, 23, 0.63); font-size : 42px;" class="fa fa-bell"></i></h3>' ,  message: mbody , delay: 10000});
                              
                              load_notice();
                          });
                  }
                });
      }

      //end for notice on the top bar

      //start show all status on the top bar
      function showAllStatus()
      {
        console.log("yayaya~~~hahaha");
        $http.get("/api/setAFlagZero")
                .success(function(afz){
                  if(afz.result == true)
                  {
                    $http.get("/api/getAllStatus")
                          .success(function(cbdata)
                          {

                            console.log("rat : " + JSON.stringify(cbdata));
                            $scope.as = cbdata;

                            load_notice();

                          });
                  }
                });
        
      }

      function closeNav(snd)
      {
        console.log("heyheyhey");
        var x = document.getElementById("myTopnav");
        x.className = "topnav";

        var flagf;
        if( ((3<=snd)&&(snd<=6)) || (snd == 13) || (snd == 11) )
        {
          flagf = 1;
        }
        else
        {
          flagf = 2;
        }
        $http.post("/api/setSoundsflag" , {obj : flagf})
                .success(function(cbdata)
                {
                  if((3<=snd)&&(snd<=10))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 3:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 4:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 5:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 6:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 7:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 8:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                            case 9:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 10:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page2.html';
                                });
                          
                        }
                        else if((snd == 11) || (snd == 12))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 11:
                              goParam = { cstatus : '3' , rouflag : 999};
                              break;
                            case 12:
                              goParam = { cstatus : '2' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page10.html';
                                });
                          
                        }
                        else if(snd == 1)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page7.html';
                                });
                          
                        }
                        else if(snd == 2)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page8.html';
                                });
                          
                        }

                });

      }

      function setStatusboxWidth()
      {
        console.log("wow excellent.. : "+( parseInt($(".page-container").width()) - 390 ));
        var gijuns = parseInt($(".page-container").width());
        var d_amount;
        if(gijuns<768)
          d_amount = 200;
        else
          d_amount = 400;

        var mywidth = gijuns - d_amount;

        $scope.statusbox={'width':mywidth+'px'};
      }

      $(window).resize(function(){
          
        setStatusboxWidth();

      });

      //end show all status on the top bar

      //start test for transfer

      // handles the callback from the received event
        var handleCallback = function (msg) {
            $scope.$apply(function () {
                //$scope.msg = JSON.parse(msg.data)
                var r_obj = JSON.parse(msg.data);
                console.log("receiver : " + r_obj.flag);
                
                if(r_obj.flag != 0)
                {
                  if(r_obj.flag != 100)
                  {
                    console.log("will chekc for target~");
                    $http.post("/api/CheckForEmitTarget" , { cfeparam : r_obj.shcdata }) // check if this is logined id which is correct target to listen the emit socket signal
                          .success(function(cfedata)
                          {
                            if(cfedata.result == true) // this is correct target
                            {
                                  if(cfedata.flag == 1)
                                  {
                                    console.log("will find....");
                                    showAllStatus();
                                    
                                  }
                                  else if(cfedata.flag == 2){
                                    $http.get("/api/showShopRWmsm")
                                          .success(function(ssrw){
                                            alert(ssrw.msmbody);
                                          });
                                    //console.log("listening for change.....");
                                  }
                            }

                          });
                  }
                  else if(r_obj.flag == 100) // its self signal
                  {
                    console.log("will find....");
                    showAllStatus();
                  }
                  
                }
            });
        }
 
        var source = new EventSource('/stats');
        source.addEventListener('message', handleCallback, false);

      //end test for transfer


    }

})();
