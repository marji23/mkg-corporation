(function (){

  angular
      .module('page3_App', ['ngLoader' , 'ui.bootstrap', 'ui-notification' , 'chart.js'])
      .controller('page3_Controller', page3_Controller)
      .config(function(NotificationProvider) {
          NotificationProvider.setOptions({
              delay: 10000,
              startTop: 20,
              startRight: 10,
              verticalSpacing: 20,
              horizontalSpacing: 20,
              positionX: 'right',
              positionY: 'top'
          });
        });



    function page3_Controller(Notification ,$scope, $http, $window) {

      var gameParams = [{gameid : 1 , winflag : 103 , failflag : 3 , gametype : 1}, // Holdem Poker
                        {gameid : 2 , winflag : 104 , failflag : 4 , gametype : 1}, // Majang
                        {gameid : 3 , winflag : 105 , failflag : 5 , gametype : 1}, // Deou Di zhu
                        {gameid : 4 , winflag : 106 , failflag : 6 , gametype : 1}] // Sadari

          //if game id is changed , you can only change the gameid on above list to maintain the site. !!!            

      $scope.logout_func = logout_func;
      $scope.myinfo = myinfo;

      $scope.reset = reset;
      $scope.analyseForGame = analyseForGame;
      $scope.analyseForGameWhenBtnclicked = analyseForGameWhenBtnclicked;
      $scope.day_filter = day_filter;

      var whole_notice;
      $scope.notify_clicked = notify_clicked;

      $scope.onChangeNum_perPage = onChangeNum_perPage;
      $scope.pageChanged = pageChanged;
      var wholeitems;

      $scope.closeNav = closeNav;

      var acdata = [];

     var audio = new Audio('./sounds/Maramba.mp3');

      function init() {
        console.log("page3 initer");

        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 1)
                $window.location.href = '/index.html';
              else
              {
                console.log("aya~~")
                $http.get('/api/get_global_acdata')
                      .success(function(ggadata){
                        acdata = ggadata.acdata;
                        $scope.mycid = ggadata.mycid;
                        $scope.mygroup = ggadata.mygroup;
                        $scope.mydeeper = ggadata.mydeeper;

                        if(($scope.mydeeper == 1)&&(acdata[0].ac_allgame <2))
                          logout_func();

                        imp_allow();

                        load_sidebar();
                        $scope.topnav = {url : "TopNav.html"};
                        showAllStatus();

                          $scope.num_per_page = '10';
                          $scope.maxSize = 3;
                          $scope.bigTotalItems = 10;
                          $scope.bigCurrentPage = 1;

                         load_games();

                      });
              }

            });
      }

      init();

      function imp_allow()
      {
          var mystat = {};
          var groupstat = {};
          var finalclc = {}; 
          console.log("$scope.mygroup : " + $scope.mygroup);

          acdata.forEach(function(raudata){
                          if(raudata.uinfo[0] != undefined)
                          {
                            if(raudata.uinfo[0].u_id == $scope.mycid)
                              mystat = raudata;
                          }
                          else if(raudata.ginfo[0] != undefined)
                          {
                            if(raudata.ginfo[0].g_id == $scope.mycid)
                              mystat = raudata;
                            if(raudata.ginfo[0].g_id == $scope.mygroup)
                              groupstat = raudata
                          }
                        });
          if($scope.mygroup < 100) // not manager grouop
              finalclc = mystat;
          else
              {
                
                finalclc = mystat; // init

                var counter = 0;
                for (var key in mystat) {
                    if(counter < 23)
                    {
                      if(mystat[key] == 0)
                        finalclc[key] = groupstat[key];
                      else
                        finalclc[key] = mystat[key];

                      //console.log("final : " + finalclc[key] + " <= group : " + groupstat[key] + " + mine : " + mystat[key]);
                    }  //
                    counter++;
                }
              }
              //console.log("final : " + finalclc.ac_zhong);
            $scope.fang = finalclc;

      }

      function pageChanged() {

          console.log('Page changed to: ' + $scope.bigCurrentPage);
          console.log($scope.bigCurrentPage);
          analyseForGame();
      }

      function onChangeNum_perPage(){
          analyseForGameWhenBtnclicked();
      }

      function load_sidebar()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                    switch(rsys.result.deep)
                    {
                      case 1:
                        $scope.template = {url : "sidebar.html"};
                        $scope.top = {url : "topbar.html"};
                       break;
                      case 2:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 3:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 4:
                        $scope.template = {url : "sidebar3.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                    }

                    setStatusboxWidth();
                  });
      }

      function myinfo()
      {

        $http.get('/api/set_myinfo')
            .success(function(cbdata){
              
              console.log("_kekeke : " + cbdata[0].savepay);

                var fnum = parseInt(cbdata[0].orig_gid);
                if( fnum == 2 )
                  $window.location.href = '/server-page8(edit&detail).html';
                if( fnum > 2 )
                  $window.location.href = '/server-page15(edit&detail).html';
            });
      }

      function logout_func()
      {
        console.log("logouted");
        $http.get('/api/logout_func')
            .success(function(cbdata){
              if(cbdata.logresult == "success")
                $window.location.href = '/index.html';
            });
      }

      function load_games()
      {
        $http.get('/api/findAllGame')
              .success(function(gdata){
                  
                 $scope.mygdata = gdata;

                 $http.get('/api/get_visitor_for_game')
                    .success(function(gvfg){
                      console.log("gvfg-user : " + JSON.stringify(gvfg));

                      if(gvfg.user == "")
                          analyseForGameWhenBtnclicked();
                      else if(gvfg.user != "")
                      {
                        $("#playername").val(gvfg.user);
                          analyseForGameWhenBtnclicked();
                      }

                    });

              });
      }

      function reset()
      {
        $('#dto').val('');
        $('#dfrom').val('');
        $('#games').val('1');
        $('#playername').val('');
      }

      function analyseForGameWhenBtnclicked() {

          var dsparam = {};
          var final_param = {};
          if($('#dto').val() != "")
              dsparam['$lt'] = $('#dto').val();
          if($('#dfrom').val() != "")
              dsparam['$gte'] = $('#dfrom').val();
          if( ($('#dto').val() != "") || ($('#dfrom').val() != "") )
          {
              final_param['ep_datetime'] = dsparam;
          }

          if($('#playername').val() != "")
              final_param['uinfo.u_nickname'] = {'$regex': '.*'+$('#playername').val()+'.*'};


          gameParams.forEach(function(item){
              if( item.gameid == $('#games').val() )
              {
                  final_param['$or'] = [{'ep_type' : item.winflag} , {'ep_type' : item.failflag}];
              }
          });

          console.log("huh~ : " + JSON.stringify(final_param));

          var ggname;
          var gametype;
          var specified_gid = 10000; // this is game id what is the gametype == 2

          var local_gnames = $scope.mygdata;
          local_gnames.forEach(function(gnitem){
              if( gnitem.ga_id == $('#games').val() )
              {
                  ggname = gnitem.ga_title.ga_en;
                  gametype = gnitem.ga_earning_typeid;
                  if(gametype == 2)
                      specified_gid = gnitem.ga_id;
              }
          });

          var failedFilter = 10000; // this is used to show the item what if only( result is failed & gametype ==2)
          gameParams.forEach(function (ffitem) {
              if( ffitem.gameid == specified_gid )
                  failedFilter = ffitem.failflag;
          });
          var additional_filter;
          if(failedFilter != 10000)
              additional_filter = { $or: [  { "ep_type" : {"$gte" : 103 , "$lt" : 124} } , { "ep_type": parseInt(failedFilter) } ] };
          else
              additional_filter = { "ep_type" : {"$gte" : 103 , "$lt" : 124} };


          console.log("final param : " + JSON.stringify(final_param));

          $scope.working = true;
          $scope.message = 'Loading...'

          $http.post('/api/getWholeMatchedGHData' , { "fp" : final_param , "fpr" : additional_filter })
              .success(function (numdata) {
                  console.log("my num data : " + numdata.wghnum);

                  console.log("numperpage : " + $scope.num_per_page);

                  $scope.bigTotalItems = (10*Math.ceil(numdata.wghnum/$scope.num_per_page));

                  var pageval = { "currentpage" : parseInt($scope.bigCurrentPage) , "numperpage" : parseInt($scope.num_per_page) };

                  $http.post('/api/getWholeGameHistory' , { "fp" : final_param , "pv" : pageval , "adfil" : additional_filter })
                      .success(function(wghdata){

                          console.log("wgh data : " + wghdata.result.length);
                          var wghrdata = wghdata.result;
                          var tempwghr = [];
                          console.log("wgh data2 : " + wghrdata.length);
                          wghrdata.forEach(function(wghitem){
                              if((wghitem.ep_type <= 123) && (wghitem.ep_type >= 103))
                              {
                                  wghitem['str_gresult'] = "Win";
                              }
                              if((wghitem.ep_type <= 23) && (wghitem.ep_type >= 3))
                              {
                                  wghitem['str_gresult'] = "Failed";
                              }

                              wghitem['str_gname'] = ggname;
                              wghitem['str_date'] = (new Date(wghitem.ep_datetime)).toLocaleString();

                               tempwghr.push(wghitem);
                          });

                          $scope.wghresults = tempwghr;

                          $scope.working = false;

                      });


              });

      }

      function analyseForGame()
      {
        var dsparam = {};
        var final_param = {};
        if($('#dto').val() != "")
          dsparam['$lt'] = $('#dto').val();
        if($('#dfrom').val() != "")
          dsparam['$gte'] = $('#dfrom').val();
        if( ($('#dto').val() != "") || ($('#dfrom').val() != "") )
        {
          final_param['ep_datetime'] = dsparam;
        }

        if($('#playername').val() != "")
          final_param['uinfo.u_nickname'] = {'$regex': '.*'+$('#playername').val()+'.*'};

        
        gameParams.forEach(function(item){
          if( item.gameid == $('#games').val() )
            {
              final_param['$or'] = [{'ep_type' : item.winflag} , {'ep_type' : item.failflag}];
            }
        });

        console.log("huh~ : " + JSON.stringify(final_param));

          var ggname;
          var gametype;
          var specified_gid = 10000; // this is game id what is the gametype == 2

          var local_gnames = $scope.mygdata;
          local_gnames.forEach(function(gnitem){
              if( gnitem.ga_id == $('#games').val() )
              {
                  ggname = gnitem.ga_title.ga_en;
                  gametype = gnitem.ga_earning_typeid;
                  if(gametype == 2)
                      specified_gid = gnitem.ga_id;
              }
          });

          var failedFilter = 10000; // this is used to show the item what if only( result is failed & gametype ==2)
          gameParams.forEach(function (ffitem) {
              if( ffitem.gameid == specified_gid )
                  failedFilter = ffitem.failflag;
          });
          var additional_filter;
          if(failedFilter != 10000)
              additional_filter = { $or: [  { "ep_type" : {"$gte" : 103 , "$lt" : 124} } , { "ep_type": parseInt(failedFilter) } ] };
          else
              additional_filter = { "ep_type" : {"$gte" : 103 , "$lt" : 124} };

        console.log("final param : " + JSON.stringify(final_param));

        $scope.working = true;
        $scope.message = 'Loading...'


        var pageval = { "currentpage" : parseInt($scope.bigCurrentPage) , "numperpage" : parseInt($scope.num_per_page) };

                  $http.post('/api/getWholeGameHistory' , { "fp" : final_param , "pv" : pageval, "adfil" : additional_filter })
                      .success(function(wghdata){

                          console.log("wgh data : " + wghdata.result.length);
                          var wghrdata = wghdata.result;
                          var tempwghr = [];
                          wghrdata.forEach(function(wghitem){
                              if((wghitem.ep_type <= 123) && (wghitem.ep_type >= 103))
                              {
                                  wghitem['str_gresult'] = "Win";
                              }
                              if((wghitem.ep_type <= 23) && (wghitem.ep_type >= 3))
                              {
                                  wghitem['str_gresult'] = "Failed";
                              }

                              wghitem['str_gname'] = ggname;
                              wghitem['str_date'] = (new Date(wghitem.ep_datetime)).toLocaleString();

                              tempwghr.push(wghitem);
                          });

                          $scope.wghresults = tempwghr;

                          $scope.working = false;

                      });


      }

      /* Define new prototype methods on Date object. */
      // Returns Date as a String in YYYY-MM-DD format.
      Date.prototype.toISODateString = function () {
        return this.toISOString().substr(0,10);
      };

      // Returns new Date object offset `n` days from current Date object.
      Date.prototype.toDateFromDays = function (n) {
        n = parseInt(n) || 0;
        var newDate = new Date(this.getTime());
        newDate.setDate(this.getDate() + n);
        return newDate;
      };

      function day_filter(types)
      {
        console.log(types);
        
        switch(types)
        {
          case 31:
            
            var dto = new Date();
            var dfrom  = new Date();
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#dto').val(dto);
            $('#dfrom').val(dfrom);
              analyseForGameWhenBtnclicked();
            break;
          case 32:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-7);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#dto').val(dto);
            $('#dfrom').val(dfrom);
              analyseForGameWhenBtnclicked();
            break;
          case 33:
            var dto = new Date();
            var dfrom  = new Date();
            dfrom.setHours(0,0,0,0);
            dfrom.setDate(1);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#dto').val(dto);
            $('#dfrom').val(dfrom);
              analyseForGameWhenBtnclicked();
            break;
          case 34:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-90);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#dto').val(dto);
            $('#dfrom').val(dfrom);
              analyseForGameWhenBtnclicked();
            break;
          case 35:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-180);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#dto').val(dto);
            $('#dfrom').val(dfrom);
              analyseForGameWhenBtnclicked();
            break;
          case 36:
            var dto = new Date();
            var dfrom = "";
            console.log( dto.toISODateString() + " : " +  dfrom );
            $('#dto').val(dto);
            $('#dfrom').val(dfrom);
              analyseForGameWhenBtnclicked();
            break;
        }
      }


      //start for notice on the top bar

      function checkForSounds(typesound)
      {
        $http.get('/api/getSoundsjson')
            .success(function(sdata)
            {
              if(typesound == 1)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) || (sdata.obj.notify < $scope.len ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
              else if(typesound == 2)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
            });
      }

      function load_notice()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    if(rsys.result.deep == 1){
                      console.log("dfdf");
                     $http.get('/api/get_all_notice')
                        .success(function(cbdata){
                            console.log("rusuky : " + cbdata.obj);
                            whole_notice = cbdata.obj;
                            whole_notice = whole_notice.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

                            var unreads = [];
                            whole_notice.forEach(function(item){
                              if(item.am_status == 0)
                                {
                                  unreads.push(item);
                                  console.log(JSON.stringify(item));
                                }
                            });

                            $scope.ur = unreads;
                            $scope.len = unreads.length;

                            checkForSounds(1);
                             

                        });
                    }
                    else
                      checkForSounds(2);

                  });
      }

      function notify_clicked(iobj)
      {
        whole_notice.forEach(function(item){
          if(item == iobj)
            item.am_status = 1;
        }); 
        $http.post('/api/update_all_notice' , { obj : whole_notice } )
                .success(function(cbdata){
                  if(cbdata.result == true){

                      $http.get('/api/getAllalarm' )
                        .success(function(cbdata1){
                            var atypes = cbdata1.obj;
                            var contents = "";
                            var titles = "";
                            for(var i=0;i<atypes.length;i++)
                              if(atypes[i].a_id == iobj.am_content)
                              {
                                contents = atypes[i].a_content.a_en;
                                titles = atypes[i].a_title.a_en;
                              }

                               var mbody = "<p> 위반대상 : "+ iobj.am_target +" </p>"+
                               "<p> 위반대상 식별자 : "+ iobj.am_id +" </p>"+
                              "<p> 위반날자 : "+ iobj.am_date.toLocaleString().split("T")[0] + " " + iobj.am_date.toLocaleString().split("T")[1] +"</p>"+
                              "<p> 위반내용 : "+ contents +" </p> ";
          Notification.success({title : '<h3 style="text-align: center;"><i style=" -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: orange; color: rgba(187, 48, 23, 0.63); font-size : 42px;" class="fa fa-bell"></i></h3>' ,  message: mbody , delay: 10000});
                              
                              load_notice();
                          });
                  }
                });
      }

      //end for notice on the top bar

      //start show all status on the top bar
      function showAllStatus()
      {
        console.log("yayaya~~~hahaha");
        $http.get("/api/setAFlagZero")
                .success(function(afz){
                  if(afz.result == true)
                  {
                    $http.get("/api/getAllStatus")
                          .success(function(cbdata)
                          {

                            console.log("rat : " + JSON.stringify(cbdata));
                            $scope.as = cbdata;

                            load_notice();

                          });
                  }
                });
        
      }

      function closeNav(snd)
      {
        console.log("heyheyhey");
        var x = document.getElementById("myTopnav");
        x.className = "topnav";

        var flagf;
        if( ((3<=snd)&&(snd<=6)) || (snd == 13) || (snd == 11) )
        {
          flagf = 1;
        }
        else
        {
          flagf = 2;
        }
        $http.post("/api/setSoundsflag" , {obj : flagf})
                .success(function(cbdata)
                {
                  if((3<=snd)&&(snd<=10))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 3:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 4:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 5:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 6:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 7:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 8:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                            case 9:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 10:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page2.html';
                                });
                          
                        }
                        else if((snd == 11) || (snd == 12))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 11:
                              goParam = { cstatus : '3' , rouflag : 999};
                              break;
                            case 12:
                              goParam = { cstatus : '2' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page10.html';
                                });
                          
                        }
                        else if(snd == 1)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page7.html';
                                });
                          
                        }
                        else if(snd == 2)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page8.html';
                                });
                          
                        }

                });

      }

      function setStatusboxWidth()
      {
        console.log("wow excellent.. : "+( parseInt($(".page-container").width()) - 390 ));
        var gijuns = parseInt($(".page-container").width());
        var d_amount;
        if(gijuns<768)
          d_amount = 200;
        else
          d_amount = 400;

        var mywidth = gijuns - d_amount;

        $scope.statusbox={'width':mywidth+'px'};
      }

      $(window).resize(function(){
          
        setStatusboxWidth();

      });

      //end show all status on the top bar

      //start test for transfer

      // handles the callback from the received event
        var handleCallback = function (msg) {
            $scope.$apply(function () {
                //$scope.msg = JSON.parse(msg.data)
                var r_obj = JSON.parse(msg.data);
                console.log("receiver : " + r_obj.flag);
                
                if(r_obj.flag != 0)
                {
                  if(r_obj.flag != 100)
                  {
                    console.log("will chekc for target~");
                    $http.post("/api/CheckForEmitTarget" , { cfeparam : r_obj.shcdata }) // check if this is logined id which is correct target to listen the emit socket signal
                          .success(function(cfedata)
                          {
                            if(cfedata.result == true) // this is correct target
                            {
                                  if(cfedata.flag == 1)
                                  {
                                    console.log("will find....");
                                    showAllStatus();
                                    
                                  }
                                  else if(cfedata.flag == 2){
                                    $http.get("/api/showShopRWmsm")
                                          .success(function(ssrw){
                                            alert(ssrw.msmbody);
                                          });
                                    //console.log("listening for change.....");
                                  }
                            }

                          });
                  }
                  else if(r_obj.flag == 100) // its self signal
                  {
                    console.log("will find....");
                    showAllStatus();
                  }
                  
                }
            });
        }
 
        var source = new EventSource('/stats');
        source.addEventListener('message', handleCallback, false);

      //end test for transfer




    }

})();
