(function (){

  angular
      .module('page7_updateApp', ['ui-notification' , 'ui.bootstrap' , 'ngLoader'])
      .controller('page7_updateController', Page7_updateController)
      .config(function(NotificationProvider) {
          NotificationProvider.setOptions({
              delay: 10000,
              startTop: 20,
              startRight: 10,
              verticalSpacing: 20,
              horizontalSpacing: 20,
              positionX: 'right',
              positionY: 'top'
          });
        });



    function Page7_updateController(Notification ,$scope, $http, $window) {

      $scope.logout_func = logout_func;
      $scope.resetAll = resetAll;
      var selected_mem;
      $scope.updateVisitor = updateVisitor;
      $scope.goTo_savepay = goTo_savepay;
      $scope.goTo_GameHistory = goTo_GameHistory;

      $scope.myinfo = myinfo;

      $scope.pageChanged = pageChanged;
      $scope.onChangeNum_perPage = onChangeNum_perPage;
      $scope.sp_detail_filterWhenClick = sp_detail_filterWhenClick;

      $scope.pageChanged1 = pageChanged1;
      $scope.onChangeNum_perPage1 = onChangeNum_perPage1;
      $scope.g_detail_filterWhenClick = g_detail_filterWhenClick;

      $scope.gameCategory = gameCategory;

      var whole_notice;
      $scope.notify_clicked = notify_clicked;

      $scope.closeNav = closeNav;

      var s_lvl_name = ["본사" , "부본사" , "총판" , "대리"];
       var gameParams = [{gameid : 1 , winflag : 103 , failflag : 3 , gametype : 1}, // Holdem Poker
                        {gameid : 2 , winflag : 104 , failflag : 4 , gametype : 1}, // Majang
                        {gameid : 3 , winflag : 105 , failflag : 5 , gametype : 1}, // Deou Di zhu
                        {gameid : 4 , winflag : 106 , failflag : 6 , gametype : 1}] // Sadari

      var audio = new Audio('./sounds/Maramba.mp3');

      var acdata = [];

      function init() {
        console.log("page7update initer");

        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 1)
                $window.location.href = '/index.html';
              else
              {
                console.log("aya~~")
                $http.get('/api/get_global_acdata')
                      .success(function(ggadata){
                        acdata = ggadata.acdata;
                        $scope.mycid = ggadata.mycid;
                        $scope.mygroup = ggadata.mygroup;
                        $scope.mydeeper = ggadata.mydeeper;

                        if(($scope.mydeeper == 1)&&(acdata[0].ac_visitor <3))
                          logout_func();

                        imp_allow();

                        load_sidebar();
                        $scope.topnav = {url : "TopNav.html"};
                        showAllStatus();
                        loadShopList();

                      });
              }

            });
      }

      init();

      $scope.num_per_page = 10;
      $scope.maxSize = 3;
      $scope.bigTotalItems = 10;
      $scope.bigCurrentPage = 1;

      function pageChanged() {
          sp_detail_filter();
      }

      function onChangeNum_perPage(){
          sp_detail_filterWhenClick();
      }

      $scope.num_per_page1 = 10;
      $scope.maxSize1 = 3;
      $scope.bigTotalItems1 = 10;
      $scope.bigCurrentPage1 = 1;

      function pageChanged1() {
          g_detail_filter();
      }

      function onChangeNum_perPage1(){
          g_detail_filterWhenClick();
      }

      function imp_allow()
      {
          var mystat = {};
          var groupstat = {};
          var finalclc = {}; 
          console.log("$scope.mygroup : " + $scope.mygroup);

          acdata.forEach(function(raudata){
                          if(raudata.uinfo[0] != undefined)
                          {
                            if(raudata.uinfo[0].u_id == $scope.mycid)
                              mystat = raudata;
                          }
                          else if(raudata.ginfo[0] != undefined)
                          {
                            if(raudata.ginfo[0].g_id == $scope.mycid)
                              mystat = raudata;
                            if(raudata.ginfo[0].g_id == $scope.mygroup)
                              groupstat = raudata
                          }
                        });
          if($scope.mygroup < 100) // not manager grouop
              finalclc = mystat;
          else
              {
                
                finalclc = mystat; // init

                var counter = 0;
                for (var key in mystat) {
                    if(counter < 23)
                    {
                      if(mystat[key] == 0)
                        finalclc[key] = groupstat[key];
                      else
                        finalclc[key] = mystat[key];

                      //console.log("final : " + finalclc[key] + " <= group : " + groupstat[key] + " + mine : " + mystat[key]);
                    }  //
                    counter++;
                }
              }
              //console.log("final : " + finalclc.ac_zhong);
            $scope.fang = finalclc;

      }

      function load_sidebar()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                    switch(rsys.result.deep)
                    {
                      case 1:
                        $scope.template = {url : "sidebar.html"};
                        $scope.top = {url : "topbar.html"};
                       break;
                      case 2:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 3:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 4:
                        $scope.template = {url : "sidebar3.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                    }

                    setStatusboxWidth();
                  });
      }

      function myinfo()
      {

        $http.get('/api/set_myinfo')
            .success(function(cbdata){
              
              console.log("_kekeke : " + cbdata[0].savepay);

                var fnum = parseInt(cbdata[0].orig_gid);
                if( fnum == 2 )
                  $window.location.href = '/server-page8(edit&detail).html';
                if( fnum > 2 )
                  $window.location.href = '/server-page15(edit&detail).html';
            });
      }

      //start game table
      function load_games()
      {
        $http.get('/api/findAllGame')
              .success(function(gdata){
                  
                 $scope.mygdata = gdata;
                 $scope.mygamer = gdata[0].ga_id.toString();

                 g_detail_filterWhenClick();

              });
      }

      function gameCategory()
      {
        g_detail_filterWhenClick();
      }

      function g_detail_filterWhenClick()
      {
          var final_param = {};
          final_param['uinfo.u_id'] = $('#u_id').val();

          gameParams.forEach(function(item){
              if( item.gameid == $('#games').val() )
              {
                  final_param['$or'] = [{'ep_type' : item.winflag} , {'ep_type' : item.failflag}];
              }
          });

          console.log("huh~ : " + JSON.stringify(final_param));

          var ggname;
          var gametype;
          var specified_gid = 10000; // this is game id what is the gametype == 2

          var local_gnames = $scope.mygdata;
          local_gnames.forEach(function(gnitem){
              if( gnitem.ga_id == $('#games').val() )
              {
                  ggname = gnitem.ga_title.ga_en;
                  gametype = gnitem.ga_earning_typeid;
                  if(gametype == 2)
                      specified_gid = gnitem.ga_id;
              }
          });

          var failedFilter = 10000; // this is used to show the item what if only( result is failed & gametype ==2)
          gameParams.forEach(function (ffitem) {
              if( ffitem.gameid == specified_gid )
                  failedFilter = ffitem.failflag;
          });
          var additional_filter;
          if(failedFilter != 10000)
              additional_filter = { $or: [  { "ep_type" : {"$gte" : 103 , "$lt" : 124} } , { "ep_type": parseInt(failedFilter) } ] };
          else
              additional_filter = { "ep_type" : {"$gte" : 103 , "$lt" : 124} };


          console.log("final param : " + JSON.stringify(final_param));

          $scope.working1 = true;
        $scope.message1 = 'Loading...'

          $http.post('/api/getWholeMatchedGHData' , { "fp" : final_param , "fpr" : additional_filter })
              .success(function (numdata) {

                  $scope.bigTotalItems1 = (10*Math.ceil(numdata.wghnum/$scope.num_per_page1));
                  console.log("gaja~ : "+numdata.wghnum);
                  g_detail_filter();

                });
      }

      function g_detail_filter()
      {
        
        var final_param = {};
        final_param['uinfo.u_id'] = $('#u_id').val();

        gameParams.forEach(function(item){
          if( item.gameid == $('#games').val() )
            {
              final_param['$or'] = [{'ep_type' : item.winflag} , {'ep_type' : item.failflag}];
            }
        });

        console.log("huh~ : " + JSON.stringify(final_param));

          var ggname;
          var gametype;
          var specified_gid = 10000; // this is game id what is the gametype == 2

          var local_gnames = $scope.mygdata;
          local_gnames.forEach(function(gnitem){
              if( gnitem.ga_id == $('#games').val() )
              {
                  ggname = gnitem.ga_title.ga_en;
                  gametype = gnitem.ga_earning_typeid;
                  if(gametype == 2)
                      specified_gid = gnitem.ga_id;
              }
          });

          var failedFilter = 10000; // this is used to show the item what if only( result is failed & gametype ==2)
          gameParams.forEach(function (ffitem) {
              if( ffitem.gameid == specified_gid )
                  failedFilter = ffitem.failflag;
          });
          var additional_filter;
          if(failedFilter != 10000)
              additional_filter = { $or: [  { "ep_type" : {"$gte" : 103 , "$lt" : 124} } , { "ep_type": parseInt(failedFilter) } ] };
          else
              additional_filter = { "ep_type" : {"$gte" : 103 , "$lt" : 124} };

        console.log("final param : " + JSON.stringify(final_param));

        $scope.working1 = true;
        $scope.message1 = 'Loading...'


        var pageval = { "currentpage" : parseInt($scope.bigCurrentPage1) , "numperpage" : parseInt($scope.num_per_page1) };

                  $http.post('/api/getWholeGameHistory' , { "fp" : final_param , "pv" : pageval, "adfil" : additional_filter })
                      .success(function(wghdata){

                          
                          var wghrdata = wghdata.result;
                          var tempwghr = [];
                          wghrdata.forEach(function(wghitem){
                              if((wghitem.ep_type <= 123) && (wghitem.ep_type >= 103))
                              {
                                  wghitem['str_gresult'] = "Win";
                              }
                              if((wghitem.ep_type <= 23) && (wghitem.ep_type >= 3))
                              {
                                  wghitem['str_gresult'] = "Failed";
                              }

                              wghitem['str_gname'] = ggname;
                              wghitem['str_date'] = (new Date(wghitem.ep_datetime)).toLocaleString();

                              tempwghr.push(wghitem);
                          });

                          console.log("wgh data : " + tempwghr.length);

                          $scope.wghresults = tempwghr;
                          $scope.mywgh = "aa";

                          $scope.working1 = false;

                      });

      }

      //end game table

      // start sp table
      function sp_detail_filterWhenClick()
      {
        obj = {};

          obj['sp_usertype'] = "1";
          obj['sp_myid'] = $('#u_id').val();

        $http.post('/api/find_Save_pay_num' , obj)
            .success(function(cbdata){
              console.log("ca ca ca : " + cbdata.totalamount);
              $scope.bigTotalItems = (10*Math.ceil(cbdata.totalamount/$scope.num_per_page)); 

              sp_detail_filter();
            });
      }

      function sp_detail_filter()
      {
        
        obj = {};

        obj['sp_usertype'] = "1";
        obj['sp_myid'] = $('#u_id').val();

        obj['currentpage'] = parseInt($scope.bigCurrentPage);
        obj['numperpage'] = parseInt($scope.num_per_page);

        $scope.working = true;
        $scope.message = 'Loading...'
        
        $http.post('/api/find_Save_pay_result' , obj)
            .success(function(cbdata){

              //console.log("ca ca ka : " + JSON.stringify(cbdata));

              $scope.spdetailers = cbdata;
              $scope.working = false;

            });

      }
      // end sp table


      function get_su_text(hinter)
      {
        console.log("hari : " + hinter)

        var tglobal_list = $scope.shoplists;

          for(var i=0;i<tglobal_list.length;i++)
          {
            console.log("ya+ :" + tglobal_list[i].u_nickname);
          if(tglobal_list[i].u_nickname == hinter)
            {
              console.log("huk : " + tglobal_list[i].su_text);
              return tglobal_list[i].su_text;
            }
          }  
      }

      function init_datas()
      {
        $http.get('/api/get_selected_mem')
            .success(function(cbdata1){
              selected_mem = cbdata1;
              console.log("getter : " + cbdata1.u_id);
              $('#u_id').val(cbdata1.u_id);
              $('#u_nickname').val(cbdata1.u_nickname);
              $('#u_pw').val(cbdata1.u_pw);
              $('#u_email').val(cbdata1.u_email);
              $('#u_phone').val(cbdata1.u_phone);
              $('#u_uppercode').val( $('#u_uppercode option:contains('+ get_su_text(cbdata1.u_uppercode) +')').val() );
              $('#u_uppertype').val( $('#u_uppertype option:contains('+ cbdata1.u_uppertype +')').val() );
              $('#u_loginip').val(cbdata1.u_loginip);

              console.log("uuflag : " + cbdata1.u_flag);



              if(cbdata1.u_flag == "a")
                $('#acc_status').val("2");
              else
                $('#acc_status').val("1");

              $scope.analised = cbdata1;

              sp_detail_filterWhenClick();
              load_games();

            });
      }

      function logout_func()
      {
        console.log("logouted");
        $http.get('/api/logout_func')
            .success(function(cbdata){
              if(cbdata.logresult == "success")
                $window.location.href = '/index.html';
            });
      }

// - start make awesome tree structured upper shop list part

      function eachtext(nums , txt , sindex)
      {
        nums = 4*nums;
        var prefix = "|";
        for(i=0;i<nums;i++)
          prefix += "-";
        var suffix = " (" + s_lvl_name[sindex] + ") ";
        return prefix + ">" + txt + suffix;
      }

      function make_items_title(maxlvl , global_list)
      {
        console.log("txt_res : " + s_lvl_name.length);
        var inc = s_lvl_name.length - maxlvl -1;
        global_list.forEach(function(item){

          item['su_text'] = eachtext( item['su_lvlnum'] , item.u_nickname , item.u_deep-1 );
          //item['su_text'] = eachtext( item['su_lvlnum'] , item.u_nickname , (item['su_lvlnum'] + inc) );
          console.log('su : ' + item.su_text);
        });

        $scope.shoplists = global_list;

        init_datas();

      }

      function make_Structured_menutitle( lvl , starter , arr , global_list , lvl_gijun )
      {
        $scope.cout++;
        
        if( (arr.length) == $scope.cout )
        {
            console.log("really finished~~" + lvl_gijun);
            make_items_title(lvl_gijun , global_list);
        }

        lvl++;

        arr.forEach(function(item){
          //console.log("item : " + item.u_uppercode + "starter" + starter.u_uppercode);
            if(item.u_uppercode == starter.u_id)
            {
              item['su_lvlnum'] = lvl;
              //console.log("l + :"+lvl + " - " + lvl_gijun);
              global_list.push(item);

              if(lvl_gijun < lvl)
                lvl_gijun = lvl;
              make_Structured_menutitle( lvl , item , arr , global_list , lvl_gijun );

            }
            else{

                
            }
        });
      }

      function get_Structured_shoplist(sflag , arr)
      {
        var start_v;
        var global_list = [];

        if(sflag == 0)
        {
          start_v = arr[0];
          arr[0]['su_lvlnum'] = 0;
          global_list.push(arr[0]);
        }
        else if(sflag == 1)
        {
          arr.forEach(function(item){
            if(item.u_uppercode == "1")
            {
              start_v = item;
              item['su_lvlnum'] = 0;
              global_list.push(item);
            }
          });
        }

        console.log("startv : " + start_v.u_nickname);

        $scope.cout = 0;
        var lvl_gijun = 0;
        
        make_Structured_menutitle( 0 , start_v , arr , global_list , lvl_gijun );

      }

// - end make awesome tree structured upper shop list part

      function loadAllShoplist()
      {
          //console.log("loadshoplist");
          $http.post('/api/getRealAllChildshop')
            .success(function(cbdata){
              console.log('rara : '+cbdata);
              //$scope.shoplists = cbdata;
              get_Structured_shoplist(1 , cbdata);
              //init_datas();

            });
      }

      function loadShopList()
      {
        var forshopcase = [];
        $http.get('/api/getDocofLogined')
            .success(function(cbdata1){
              if(cbdata1[0].u_groupid == '2') {
                console.log("shop");

                $http.post('/api/getRealChildshop')
                    .success(function(cbdata){

                      //forshopcase.push(cbdata1[0]);
                      cbdata.obj.splice(0, 0, cbdata1[0]);
                      //cbdata.obj.push(cbdata1[0]);
                      //console.log("mememememe :     "+ cbdata1[0].u_nickname);
                      forshopcase = cbdata.obj;
                      //console.log("come : "+forshopcase.length);

                      //$scope.shoplists = forshopcase;
                      get_Structured_shoplist(0 , forshopcase);
                      

                    });

              }
              else{
                console.log("manager");
                loadAllShoplist();
              }
            });
      }

      function resetAll()
      {
        init_datas();
      }

      function updateVisitor(v)
      {

        $scope.isdisable = 1;
      
        var tobj = {};

        tobj['u_id'] = $('#u_id').val();
        tobj['u_nickname'] = $('#u_nickname').val();
        tobj['u_pw'] = $('#u_pw').val();
        tobj['u_email'] = $('#u_email').val();
          
          console.log('ar : '+$('#u_uppercode').val().length);
          console.log('ar : '+$('#u_uppertype').val().length);

        
        if( $('#acc_status').val() == "1" )
          tobj['u_flag'] = 'b';
        else 
          tobj['u_flag'] = 'a';

        if ( (tobj.u_id.length >= 2) && (tobj.u_nickname.length >= 2) && (tobj.u_pw.length >= 2) && (tobj.u_email.length != undefined) && ($('#u_uppercode').val().length >= 2) && ($('#u_uppertype').val().length >= 1) )
        {

            tobj['u_phone'] = $('#u_phone').val();
            tobj['u_loginip'] = $('#u_loginip').val();

            tobj['u_uppercode'] = $('#u_uppercode').val();
            tobj['u_uppertype'] = $('#u_uppertype').val();

            console.log("tobj : " + JSON.stringify(tobj) );

            
                          $http.post('/api/update_visitor' , tobj)
                          .success(function(cbdata){

                            if(cbdata.result == true){
                              alert("success");
                              $window.location.href = '/server-page7.html';
                              $scope.isdisable = 0;
                            }

                          }); 
        }
        else
        {
          alert("you must input some specified fields!!!");
          $scope.isdisable = 0;
        }
      }

      function goTo_savepay(obj)
      {
        console.log("objer : "+ obj.u_id);

        $http.post('/api/set_updated_visitor' , obj)
              .success(function(cbdata){

                console.log("how about ?  : " + cbdata.result);
                $window.location.href = "/server-page2.html";

            });         

      }

      function goTo_GameHistory(obj)
      {
        //console.log("objer : " + JSON.stringify(obj));

        $http.post('/api/set_visitor_for_game' , obj)
              .success(function(cbdata){
                $window.location.href = '/server-page3.html';
              });
      }
      
      //start for notice on the top bar

      function checkForSounds(typesound)
      {
        $http.get('/api/getSoundsjson')
            .success(function(sdata)
            {
              if(typesound == 1)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) || (sdata.obj.notify < $scope.len ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
              else if(typesound == 2)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
            });
      }

      function load_notice()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    if(rsys.result.deep == 1){
                      console.log("dfdf");
                     $http.get('/api/get_all_notice')
                        .success(function(cbdata){
                            console.log("rusuky : " + cbdata.obj);
                            whole_notice = cbdata.obj;
                            whole_notice = whole_notice.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

                            var unreads = [];
                            whole_notice.forEach(function(item){
                              if(item.am_status == 0)
                                {
                                  unreads.push(item);
                                  console.log(JSON.stringify(item));
                                }
                            });

                            $scope.ur = unreads;
                            $scope.len = unreads.length;

                            checkForSounds(1);
                             

                        });
                    }
                    else
                      checkForSounds(2);

                  });
      }

      function notify_clicked(iobj)
      {
        whole_notice.forEach(function(item){
          if(item == iobj)
            item.am_status = 1;
        }); 
        $http.post('/api/update_all_notice' , { obj : whole_notice } )
                .success(function(cbdata){
                  if(cbdata.result == true){

                      $http.get('/api/getAllalarm' )
                        .success(function(cbdata1){
                            var atypes = cbdata1.obj;
                            var contents = "";
                            var titles = "";
                            for(var i=0;i<atypes.length;i++)
                              if(atypes[i].a_id == iobj.am_content)
                              {
                                contents = atypes[i].a_content.a_en;
                                titles = atypes[i].a_title.a_en;
                              }

                               var mbody = "<p> 위반대상 : "+ iobj.am_target +" </p>"+
                               "<p> 위반대상 식별자 : "+ iobj.am_id +" </p>"+
                              "<p> 위반날자 : "+ iobj.am_date.toLocaleString().split("T")[0] + " " + iobj.am_date.toLocaleString().split("T")[1] +"</p>"+
                              "<p> 위반내용 : "+ contents +" </p> ";
          Notification.success({title : '<h3 style="text-align: center;"><i style=" -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: orange; color: rgba(187, 48, 23, 0.63); font-size : 42px;" class="fa fa-bell"></i></h3>' ,  message: mbody , delay: 10000});
                              
                              load_notice();
                          });
                  }
                });
      }

      //end for notice on the top bar

      //start show all status on the top bar
      function showAllStatus()
      {
        console.log("yayaya~~~hahaha");
        $http.get("/api/setAFlagZero")
                .success(function(afz){
                  if(afz.result == true)
                  {
                    $http.get("/api/getAllStatus")
                          .success(function(cbdata)
                          {

                            console.log("rat : " + JSON.stringify(cbdata));
                            $scope.as = cbdata;

                            load_notice();

                          });
                  }
                });
        
      }

      function closeNav(snd)
      {
        console.log("heyheyhey");
        var x = document.getElementById("myTopnav");
        x.className = "topnav";

        var flagf;
        if( ((3<=snd)&&(snd<=6)) || (snd == 13) || (snd == 11) )
        {
          flagf = 1;
        }
        else
        {
          flagf = 2;
        }
        $http.post("/api/setSoundsflag" , {obj : flagf})
                .success(function(cbdata)
                {
                  if((3<=snd)&&(snd<=10))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 3:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 4:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 5:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 6:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 7:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 8:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                            case 9:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 10:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page2.html';
                                });
                          
                        }
                        else if((snd == 11) || (snd == 12))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 11:
                              goParam = { cstatus : '3' , rouflag : 999};
                              break;
                            case 12:
                              goParam = { cstatus : '2' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page10.html';
                                });
                          
                        }
                        else if(snd == 1)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page7.html';
                                });
                          
                        }
                        else if(snd == 2)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page8.html';
                                });
                          
                        }

                });

      }
      
      function setStatusboxWidth()
      {
        console.log("wow excellent.. : "+( parseInt($(".page-container").width()) - 390 ));
        var gijuns = parseInt($(".page-container").width());
        var d_amount;
        if(gijuns<768)
          d_amount = 200;
        else
          d_amount = 400;

        var mywidth = gijuns - d_amount;

        $scope.statusbox={'width':mywidth+'px'};
      }

      $(window).resize(function(){
          
        setStatusboxWidth();

      });

      //end show all status on the top bar

      //start test for transfer

      // handles the callback from the received event
        var handleCallback = function (msg) {
            $scope.$apply(function () {
                //$scope.msg = JSON.parse(msg.data)
                var r_obj = JSON.parse(msg.data);
                console.log("receiver : " + r_obj.flag);
                
                if(r_obj.flag != 0)
                {
                  if(r_obj.flag != 100)
                  {
                    console.log("will chekc for target~");
                    $http.post("/api/CheckForEmitTarget" , { cfeparam : r_obj.shcdata }) // check if this is logined id which is correct target to listen the emit socket signal
                          .success(function(cfedata)
                          {
                            if(cfedata.result == true) // this is correct target
                            {
                                  if(cfedata.flag == 1)
                                  {
                                    console.log("will find....");
                                    showAllStatus();
                                    
                                  }
                                  else if(cfedata.flag == 2){
                                    $http.get("/api/showShopRWmsm")
                                          .success(function(ssrw){
                                            alert(ssrw.msmbody);
                                          });
                                    //console.log("listening for change.....");
                                  }
                            }

                          });
                  }
                  else if(r_obj.flag == 100) // its self signal
                  {
                    console.log("will find....");
                    showAllStatus();
                  }
                  
                }
            });
        }
 
        var source = new EventSource('/stats');
        source.addEventListener('message', handleCallback, false);

      //end test for transfer


    }

})();
