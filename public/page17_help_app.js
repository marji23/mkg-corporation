(function (){

  angular
      .module('page17helpApp', ['ngLoader' ,  'ui.bootstrap' , 'ui-notification',"kendo.directives" ])
      .controller('page17helpController', page17helpController)
      .controller('UpdateHelpModal', UpdateHelpModal)
      .config(function(NotificationProvider) {
          NotificationProvider.setOptions({
              delay: 10000,
              startTop: 20,
              startRight: 10,
              verticalSpacing: 20,
              horizontalSpacing: 20,
              positionX: 'right',
              positionY: 'top'
          });
        });

    function UpdateHelpModal($scope, $http, $window , $rootScope , $uibModalInstance) {

      $scope.updateHelp = updateHelp;
      $scope.exitMsg = exitMsg;

      function init() {

        $scope.v = $rootScope.seleditem;
        $scope.v.hn_type = $rootScope.seleditem.hn_type.toString();

      }
      init();

      function exitMsg()
      {
        $uibModalInstance.dismiss('cancel');
      }

      function updateHelp(val)
      {
        console.log("val : " + JSON.stringify(val));

        if(val.hn_title != undefined)
        {
          if(val.hn_comment != undefined)
          {
            console.log("d : " + val.hn_type);
            var will_update = { "id" : val.hn_id , 
                                "obj" : {"hn_title" : val.hn_title , "hn_comment" : val.hn_comment , "hn_type" : val.hn_type} 
                              };
            $http.post("/api/updateHelpDatas" , will_update)
                    .success(function(uhdata){
                      if(uhdata.result == true)
                      {
                        alert("성공적으로 보존되였습니다.");
                        $uibModalInstance.dismiss('cancel');
                        $rootScope.$emit("updateShow");
                      }
                    });
          }
          else
            alert("필수항목을 입력하여야 합니다.");
        }
        else
          alert("필수항목을 입력하여야 합니다.");

      }

    }

    function page17helpController($uibModal , $rootScope , Notification ,$scope, $http, $window) {

      $scope.logout_func = logout_func;
      $scope.myinfo = myinfo;

      $scope.addHelps = addHelps;
      $scope.findHelpDatasWhenClick = findHelpDatasWhenClick;
      $scope.reset_filter = reset_filter;
      $scope.updateHelpDatas = updateHelpDatas;
      $scope.delHelpDatas = delHelpDatas;

      var wholeitems;
      $scope.onChangeNum_perPage = onChangeNum_perPage;
      $scope.pageChanged = pageChanged;


      var whole_notice;
      $scope.notify_clicked = notify_clicked;

      $scope.closeNav = closeNav;
     
      var audio = new Audio('./sounds/Maramba.mp3');

      var acdata = [];

      var hn_typetexts = ['FAQ' , '싸이트리용규정' , '회원정책' , '포커'];


      function init() {
        console.log("page17 help initer");

        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 1)
                $window.location.href = '/index.html';
              else
              {
                console.log("aya~~")
                $http.get('/api/get_global_acdata')
                      .success(function(ggadata){
                        acdata = ggadata.acdata;
                        $scope.mycid = ggadata.mycid;
                        $scope.mygroup = ggadata.mygroup;
                        $scope.mydeeper = ggadata.mydeeper;

                        if(($scope.mydeeper == 1)&&(acdata[0].ac_help <2))
                          logout_func();

                        imp_allow();

                        load_sidebar();
                        $scope.topnav = {url : "TopNav.html"};
                        showAllStatus();

                        findHelpDatasWhenClick();

                      });
              }

            });

      }

      init();

      function imp_allow()
      {
          var mystat = {};
          var groupstat = {};
          var finalclc = {}; 
          console.log("$scope.mygroup : " + $scope.mygroup);

          acdata.forEach(function(raudata){
                          if(raudata.uinfo[0] != undefined)
                          {
                            if(raudata.uinfo[0].u_id == $scope.mycid)
                              mystat = raudata;
                          }
                          else if(raudata.ginfo[0] != undefined)
                          {
                            if(raudata.ginfo[0].g_id == $scope.mycid)
                              mystat = raudata;
                            if(raudata.ginfo[0].g_id == $scope.mygroup)
                              groupstat = raudata
                          }
                        });
          if($scope.mygroup < 100) // not manager grouop
              finalclc = mystat;
          else
              {
                
                finalclc = mystat; // init

                var counter = 0;
                for (var key in mystat) {
                    if(counter < 23)
                    {
                      if(mystat[key] == 0)
                        finalclc[key] = groupstat[key];
                      else
                        finalclc[key] = mystat[key];

                      //console.log("final : " + finalclc[key] + " <= group : " + groupstat[key] + " + mine : " + mystat[key]);
                    }  //
                    counter++;
                }
              }
              //console.log("final : " + finalclc.ac_zhong);
            $scope.fang = finalclc;

      }

      function load_sidebar()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                    switch(rsys.result.deep)
                    {
                      case 1:
                        $scope.template = {url : "sidebar.html"};
                        $scope.top = {url : "topbar.html"};
                       break;
                      case 2:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 3:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 4:
                        $scope.template = {url : "sidebar3.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                    }

                    setStatusboxWidth();
                  });
      }

      function myinfo()
      {

        $http.get('/api/set_myinfo')
            .success(function(cbdata){
              
              console.log("_kekeke : " + cbdata[0].savepay);

                var fnum = parseInt(cbdata[0].orig_gid);
                if( fnum == 2 )
                  $window.location.href = '/server-page8(edit&detail).html';
                if( fnum > 2 )
                  $window.location.href = '/server-page15(edit&detail).html';
            });
      }

      function logout_func()
      {

        console.log("logouted");
        $http.get('/api/logout_func')
            .success(function(cbdata){
              if(cbdata.logresult == "success")
                $window.location.href = '/index.html';
            });
      }

      //start main page func

      $rootScope.$on("updateShow", function(val){
           
          findHelpDatasWhenClick();

         });

      function updateHelpDatas(seleditem)
      {
        console.log("yayaya~ : " + seleditem);
          $rootScope.seleditem = seleditem;

          var modalInstance = $uibModal.open({
              animation: true,
              templateUrl: 'dialogs/updateHelp_dlg.html',
              controller: 'UpdateHelpModal',
              windowClass: 'center-modal',
              resolve: {
               //   items: function () {
                //    return $ctrl.items;
                 // }
              }
          });

          modalInstance.result.then(function (selectedItem) {}, function () {
              //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
          });
      }

      function delHelpDatas(seleditem)
      {
        $http.post("/api/delHelpDatas" , seleditem)
              .success(function(dhddata){
                if(dhddata.result == true)
                  {
                    alert("삭제되였습니다.");
                    findHelpDatasWhenClick();
                  }
              });

      }

      function addHelps()
      {

        var add_data = {};
        if($("#htitle").val() != "")
        {
          add_data['hn_title'] = $("#htitle").val();
          if($("#hcomment").val() != "")
          {
            add_data['hn_comment'] = $("#hcomment").val();
            add_data['hn_type'] = parseInt($("#htype").val());
            add_data['hn_date'] = new Date();
            $http.get("/api/getMaxHNid")
                  .success(function(hnmax)
                  {
                    add_data['hn_id'] = hnmax.result;

                      $http.post("/api/addHelps", { "obj" :  add_data })
                            .success(function(ahdata){
                              if(ahdata.result == true)
                              {
                                alert("성공적으로 추가되였습니다.");
                                findHelpDatasWhenClick();
                              }
                            });
                  });
          }
          else
            alert("필수항목들을 입력하십시오.");
        }
        else
          alert("필수항목들을 입력하십시오.");

      }

      $scope.num_per_page = 10;
      $scope.maxSize = 3;
      $scope.bigTotalItems = 10;
      $scope.bigCurrentPage = 1;

      function pageChanged() {

          findHelpDatas();
      }

      function onChangeNum_perPage(){

        findHelpDatasWhenClick();

      }

      function findHelpDatasWhenClick()
      {
        var fil_param = {};
        var range = {};
        if($("#shtitle").val() != "")
          fil_param['hn_title'] = {'$regex': '.*'+$("#shtitle").val()+'.*'};

        if($("#shcomment").val() != "")
          fil_param['hn_comment'] = {'$regex': '.*'+$("#shcomment").val()+'.*'};

        if($("#shtype").val() != "444")
          fil_param['hn_type'] = parseInt($("#shtype").val());
        else if($("#shtype").val() == "444")
        {
          range['$gte'] = 1;
          fil_param['hn_type'] = range;
        }

        console.log("fil_param : " + JSON.stringify(fil_param));

        $http.post("/api/findHelpDatasNum" , {"obj" : fil_param})
              .success(function(fhddata){
                
                console.log("fhdata : " + fhddata.totalamount);
                $scope.bigTotalItems = (10*Math.ceil(fhddata.totalamount/$scope.num_per_page)); 
                findHelpDatas();

              });
      }

      function findHelpDatas()
      {


        var fil_param = {};
        var range = {};
        if($("#shtitle").val() != "")
          fil_param['hn_title'] = {'$regex': '.*'+$("#shtitle").val()+'.*'};

        if($("#shcomment").val() != "")
          fil_param['hn_comment'] = {'$regex': '.*'+$("#shcomment").val()+'.*'};

        if($("#shtype").val() != "444")
          fil_param['hn_type'] = parseInt($("#shtype").val());
        else if($("#shtype").val() == "444")
        {
          range['$gte'] = 1;
          fil_param['hn_type'] = range;
        }

        var pvobj = {};

        pvobj['currentpage'] = parseInt($scope.bigCurrentPage);
        pvobj['numperpage'] = parseInt($scope.num_per_page);

        console.log("fil_param : " + JSON.stringify(fil_param));

        $http.post("/api/findHelpDatas" , {"obj" : fil_param , "pvobj" : pvobj})
              .success(function(fhddata){
                var final_hns = fhddata.result;

                final_hns.forEach(function(hnitem){
                  hnitem.hn_strdate = (new Date(hnitem.hn_date)).toLocaleString();
                  hnitem.hn_strtype = hn_typetexts[hnitem.hn_type-1];
                });

                $scope.hns = final_hns;
              });

      }

      function reset_filter()
      {
        $("#shtitle").val('');
        $("#shcomment").val('');
        $("#shtype").val('444');

      }

      //end main page func

      //start for notice on the top bar

      function checkForSounds(typesound)
      {
        $http.get('/api/getSoundsjson')
            .success(function(sdata)
            {
              if(typesound == 1)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) || (sdata.obj.notify < $scope.len ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
              else if(typesound == 2)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
            });
      }

      function load_notice()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    if(rsys.result.deep == 1){
                      console.log("dfdf");
                     $http.get('/api/get_all_notice')
                        .success(function(cbdata){
                            console.log("rusuky : " + cbdata.obj);
                            whole_notice = cbdata.obj;
                            whole_notice = whole_notice.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

                            var unreads = [];
                            whole_notice.forEach(function(item){
                              if(item.am_status == 0)
                                {
                                  unreads.push(item);
                                  console.log(JSON.stringify(item));
                                }
                            });

                            $scope.ur = unreads;
                            $scope.len = unreads.length;

                            checkForSounds(1);

                        });
                    }
                    else
                      checkForSounds(2);

                  });
      }

      function notify_clicked(iobj)
      {
        whole_notice.forEach(function(item){
          if(item == iobj)
            item.am_status = 1;
        }); 
        $http.post('/api/update_all_notice' , { obj : whole_notice } )
                .success(function(cbdata){
                  if(cbdata.result == true){

                      $http.get('/api/getAllalarm' )
                        .success(function(cbdata1){
                            var atypes = cbdata1.obj;
                            var contents = "";
                            var titles = "";
                            for(var i=0;i<atypes.length;i++)
                              if(atypes[i].a_id == iobj.am_content)
                              {
                                contents = atypes[i].a_content.a_en;
                                titles = atypes[i].a_title.a_en;
                              }

                               var mbody = "<p> 위반대상 : "+ iobj.am_target +" </p>"+
                               "<p> 위반대상 식별자 : "+ iobj.am_id +" </p>"+
                              "<p> 위반날자 : "+ iobj.am_date.toLocaleString().split("T")[0] + " " + iobj.am_date.toLocaleString().split("T")[1] +"</p>"+
                              "<p> 위반내용 : "+ contents +" </p> ";
          Notification.success({title : '<h3 style="text-align: center;"><i style=" -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: orange; color: rgba(187, 48, 23, 0.63); font-size : 42px;" class="fa fa-bell"></i></h3>' ,  message: mbody , delay: 10000});
                              
                              load_notice();
                          });
                  }
                });
      }

      //end for notice on the top bar

      //start show all status on the top bar
      function showAllStatus()
      {
        console.log("yayaya~~~hahaha");
        $http.get("/api/setAFlagZero")
                .success(function(afz){
                  if(afz.result == true)
                  {
                    $http.get("/api/getAllStatus")
                          .success(function(cbdata)
                          {

                            console.log("rat : " + JSON.stringify(cbdata));
                            $scope.as = cbdata;

                            load_notice();

                          });
                  }
                });
        
      }

      function closeNav(snd)
      {
        console.log("heyheyhey");
        var x = document.getElementById("myTopnav");
        x.className = "topnav";

        var flagf;
        if( ((3<=snd)&&(snd<=6)) || (snd == 13) || (snd == 11) )
        {
          flagf = 1;
        }
        else
        {
          flagf = 2;
        }
        $http.post("/api/setSoundsflag" , {obj : flagf})
                .success(function(cbdata)
                {
                  if((3<=snd)&&(snd<=10))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 3:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 4:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 5:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 6:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 7:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 8:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                            case 9:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 10:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page2.html';
                                });
                          
                        }
                        else if((snd == 11) || (snd == 12))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 11:
                              goParam = { cstatus : '3' , rouflag : 999};
                              break;
                            case 12:
                              goParam = { cstatus : '2' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page10.html';
                                });
                          
                        }
                        else if(snd == 1)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page7.html';
                                });
                          
                        }
                        else if(snd == 2)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page8.html';
                                });
                          
                        }

                });

      }

      function setStatusboxWidth()
      {
        console.log("wow excellent.. : "+( parseInt($(".page-container").width()) - 390 ));
        var gijuns = parseInt($(".page-container").width());
        var d_amount;
        if(gijuns<768)
          d_amount = 200;
        else
          d_amount = 400;

        var mywidth = gijuns - d_amount;

        $scope.statusbox={'width':mywidth+'px'};
      }

      $(window).resize(function(){
          
        setStatusboxWidth();

      });

      //end show all status on the top bar

      //start test for transfer

      // handles the callback from the received event
        var handleCallback = function (msg) {
            $scope.$apply(function () {
                //$scope.msg = JSON.parse(msg.data)
                var r_obj = JSON.parse(msg.data);
                console.log("receiver : " + r_obj.flag);
                
                if(r_obj.flag != 0)
                {
                  if(r_obj.flag != 100)
                  {
                    console.log("will chekc for target~");
                    $http.post("/api/CheckForEmitTarget" , { cfeparam : r_obj.shcdata }) // check if this is logined id which is correct target to listen the emit socket signal
                          .success(function(cfedata)
                          {
                            if(cfedata.result == true) // this is correct target
                            {
                                  if(cfedata.flag == 1)
                                  {
                                    console.log("will find....");
                                    showAllStatus();
                                    
                                  }
                                  else if(cfedata.flag == 2){
                                    $http.get("/api/showShopRWmsm")
                                          .success(function(ssrw){
                                            alert(ssrw.msmbody);
                                          });
                                    //console.log("listening for change.....");
                                  }
                            }

                          });
                  }
                  else if(r_obj.flag == 100) // its self signal
                  {
                    console.log("will find....");
                    showAllStatus();
                  }
                  
                }
            });
        }
 
        var source = new EventSource('/stats');
        source.addEventListener('message', handleCallback, false);

      //end test for transfer

    }

})();
