(function (){

  angular
      .module('page2App', [ 'ngLoader' , 'ui-notification','ngRoute','ngAnimate', 'ngSanitize', 'ui.bootstrap', 'currencyFormat' , 'ngSanitize'])
      .controller('page2Controller', Page2Controller)
      .controller('SetWithdrawlPasswordCtrl', SetWithdrawlPasswordCtrl)
      .config(function(NotificationProvider) {
          NotificationProvider.setOptions({
              delay: 10000,
              startTop: 20,
              startRight: 10,
              verticalSpacing: 20,
              horizontalSpacing: 20,
              positionX: 'right',
              positionY: 'top'
          });
        })
      .filter('numberEx', ['numberFilter', '$locale',
          function(number, $locale) {

            var formats = $locale.NUMBER_FORMATS;
            return function(input, fractionSize) {
              //Get formatted value
              var formattedValue = number(input, fractionSize);

              //get the decimalSepPosition
              var decimalIdx = formattedValue.indexOf(formats.DECIMAL_SEP);
              
              //If no decimal just return
              if (decimalIdx == -1) return formattedValue;

         
              var whole = formattedValue.substring(0, decimalIdx);
              var decimal = (Number(formattedValue.substring(decimalIdx)) || "").toString();
              
              return whole +  decimal.substring(1);
            };
          }
        ]);



    function SetWithdrawlPasswordCtrl($scope, $http, $window , $rootScope , $uibModalInstance) {
      $scope.accept = accept;
      $scope.decline = decline;
      $scope.exitMsg = exitMsg;



      function init() {

        $scope.myheader = $rootScope.header;

      }
      init();

      function exitMsg()
      {
        $uibModalInstance.dismiss('cancel');
      }

      function numberWithCommas(x) {
          var parts = x.toString().split(".");
          parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          return parts.join(".");
      }

      function addToMsgTable(obj , flag)
      {
        $http.get('/api/getMMaxid')
            .success(function(cbdata)
              {
                var flag_txt;
                var flag_title_txt;
                var temp_ftxt;

                if(obj.sp_requested_type == 1) //save
                {
                  flag_title_txt = "Recharge";
                  temp_ftxt = "충전";
                }
                else if(obj.sp_requested_type == 2) //pay
                {
                  flag_title_txt = "Withdrawl";
                  temp_ftxt = "환전";
                }

                if(flag == 1)
                  flag_txt = "에 대한 "+temp_ftxt+"요청이 승인되였습니다.";
                else if(flag == 2)
                  flag_txt = "에 대한 "+temp_ftxt+"요청이 취소되였습니다.";

                console.log("obj : " + JSON.stringify(obj));

                var contents_txt = numberWithCommas(obj.sp_real_money) + " ("+obj.sp_money_code+" ) "+"에 해당한 "+"게임돈 "+numberWithCommas(obj.sp_money)+" "+flag_txt;



                var imp_date = new Date();

                var mdata = { m_contents : contents_txt , m_datetime : imp_date , m_title :  flag_title_txt , 
                              m_flag : 1 , m_id : cbdata.result+1 , m_receiver_nickname : obj.sp_requested_id , 
                              m_receiverid : obj.oiriginal_id };
                console.log("mdata : " + JSON.stringify(mdata) );

                $http.post('/api/addMSGbySP' , mdata)
                    .success(function(cbdata1){

                      console.log(cbdata1.result);
                      
                      //
                      $uibModalInstance.dismiss('cancel');
                      $rootScope.$emit("CallMethod", {});
                      $scope.isdisable = 0;

                    });


              });
      }

      function accept(obj)
      {

        $scope.isdisable = 1;

        console.log("accept : "+obj.sp_requested_id);
        if(( obj.sp_implemented_result == '승인됨' ) || ( obj.sp_implemented_result == '온라인 지불됨' ))
        {
          alert('이미 처리되였습니다.');
          $scope.isdisable = 0;
          $uibModalInstance.dismiss('cancel');
        }
        else
        {

          $http.get('/api/loadShopBalance')
              .success(function(lsbdata){
                console.log("lsbdata : " + lsbdata.lsbalance);
                var lstemp = lsbdata.lsbalance;

                if(((parseInt(lstemp) < parseInt(obj.sp_money)) && (lsbdata.deeper > 1))&&(lsbdata.lsbflag == "no"))
                {
                  alert("잔고가 충분하지 못하므로 충전요청을 처리할수 없습니다.");
                  $scope.isdisable = 0;
                }
                else
                {
                    var impdate = new Date();
                    updata = {sp_implemented_result : '1' , sp_implemented_date : impdate , sp_id : obj.sp_id , sp_requested_id : obj.oiriginal_id};

                    $http.put('/api/update_implemented/'+obj._id ,updata)
                      .success(function(cbdata){
                        
                        $http.get('/api/getEpMaxid')
                          .success(function(cbdata1){

                            var maxid = cbdata1.result+1;
                            var num_max = parseFloat(maxid);

                            var ep_t;
                            if(obj.sp_requested_type == 1)
                              ep_t = 102;
                            if(obj.sp_requested_type == 2)
                              ep_t = 2;

                            ep_t = parseFloat(ep_t);
                            
                            var realmoney = {money : obj.sp_real_money};

                            var updated_ep_data = {ep_id : num_max , ep_datetime : impdate , 
                                    ep_loginid : obj.oiriginal_id , ep_metaid : obj.sp_id ,
                                    ep_money : obj.sp_money ,
                                    ep_type : ep_t
                                    };

                            console.log("val: " + JSON.stringify(updated_ep_data) );

                                $http.post('/api/addEpData' , updated_ep_data)
                                  .success(function(cbdata2){

                                      if(cbdata2.result == true)
                                        {
                                          addToMsgTable(obj , 1);
                                        }

                                  });

                          });



                      });
               }

              });
        }

        
      }

      function decline(obj)
      {

        $scope.isdisable = 1;
        
        console.log("decline : "+obj.sp_requested_id);
        if(( obj.sp_implemented_result == '승인됨' ) || ( obj.sp_implemented_result == '온라인 지불됨' ) || ( obj.sp_implemented_result == '취소됨' ))
        {
          alert('이미 처리되였습니다.');
          $scope.isdisable = 0;
          $uibModalInstance.dismiss('cancel');
        }
        else
        {

          var impdate = new Date();
          updata = {sp_implemented_result : '2' , sp_implemented_date : impdate , sp_id : obj.sp_id , sp_requested_id : obj.oiriginal_id};

          $http.put('/api/update_implemented/'+obj._id ,updata)
            .success(function(cbdata){
              
              if(cbdata.result == 'success')
              {
                addToMsgTable(obj , 2);
              }

            });
        }
      }

    }

    function Page2Controller(Notification , $uibModal, $log, $document, $rootScope, $location , $scope, $http, $window) {

      $scope.save_pay_filter = save_pay_filter;
      $scope.day_filter = day_filter;
      $scope.logout_func = logout_func;
      $scope.detail_filterWhenClick = detail_filterWhenClick;
      $scope.reset_filter = reset_filter;
      $scope.mymodal = mymodal;
      $scope.myinfo = myinfo;
      $scope.onChangeNum_perPage = onChangeNum_perPage;
      $scope.pageChanged = pageChanged;

      var whole_notice;
      $scope.notify_clicked = notify_clicked;

      $scope.closeNav = closeNav;

      var audio = new Audio('./sounds/Maramba.mp3');

      var acdata = [];

      function init() {
        console.log("page2 %%%%%%initer");
        
        $scope.sp_usertype = '1';
        $scope.methodtype = '1';
        $scope.status = '3';
        console.log("ram");
        var wholeitems;
        //$('#methodtype').val(1);

        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 1)
                $window.location.href = '/index.html';
              else
              {
                console.log("aya~~")
                $http.get('/api/get_global_acdata')
                      .success(function(ggadata){
                        acdata = ggadata.acdata;
                        $scope.mycid = ggadata.mycid;
                        $scope.mygroup = ggadata.mygroup;
                        $scope.mydeeper = ggadata.mydeeper;

                        if(($scope.mydeeper == 1)&&(acdata[0].ac_sp <2))
                          logout_func();

                        imp_allow();

                        load_sidebar();
                        $scope.topnav = {url : "TopNav.html"};
                        showAllStatus();

                            $http.get('/api/get_updated_mem')
                             .success(function(cbdata1){
                              if(cbdata1 != null)
                              {
                                  if((cbdata1.u_nickname != "") || (cbdata1.u_nickname != undefined))
                                  {

                                    $("#sp_name").val(cbdata1.u_nickname);
                                    console.log("u_groupid : " + cbdata1.u_groupid);
                                    if(cbdata1.u_groupid == '2')
                                      $scope.sp_usertype = '2';
                                    
                                    if(cbdata1.u_groupid == '1')
                                      $scope.sp_usertype = '1';

                                    $scope.methodtype = '';
                                    CheckFromTopNav();
                                    //detail_filterWhenClick();
                                  }
                                  else
                                    CheckFromTopNav();
                                    //detail_filterWhenClick();
                              }
                              else
                                CheckFromTopNav();
                            });

                        loadBalance();

                      });
              }
              
            });

      }

      init();

      function CheckFromTopNav()
      {
        console.log("check from top nav..........");
        $http.get("/api/GetTopNavVal")
              .success(function(mgtnvdata){
                console.log("gettopnaval-1 : " + JSON.stringify(mgtnvdata));
                if(mgtnvdata.rouflag != 1000)
                {
                  $scope.sp_usertype = mgtnvdata.utype;
                  $scope.methodtype = mgtnvdata.reqtype;
                  $scope.status = mgtnvdata.resulttype;
                  

                  $http.post("/api/SetTopNavVal" , {rouflag : 1000})
                    .success(function(gtnndata){

                      if(gtnndata.result == "success")
                        detail_filterWhenClick();

                    });
                }
                else
                  detail_filterWhenClick();

              });
      }

      function imp_allow()
      {
          var mystat = {};
          var groupstat = {};
          var finalclc = {}; 
          console.log("$scope.mygroup : " + $scope.mygroup);
          console.log("$scope.mycid : " + $scope.mycid);

          acdata.forEach(function(raudata){
                          if(raudata.uinfo[0] != undefined)
                          {
                            if(raudata.uinfo[0].u_id == $scope.mycid)
                              mystat = raudata;
                          }
                          else if(raudata.ginfo[0] != undefined)
                          {
                            if(raudata.ginfo[0].g_id == $scope.mycid)
                              mystat = raudata;
                            if(raudata.ginfo[0].g_id == $scope.mygroup)
                              groupstat = raudata
                          }
                        });
          if($scope.mygroup < 100) // not manager grouop
              finalclc = mystat;
          else
              {
                
                finalclc = mystat; // init

                var counter = 0;
                for (var key in mystat) {
                    if(counter < 23)
                    {
                      if(mystat[key] == 0)
                        finalclc[key] = groupstat[key];
                      else
                        finalclc[key] = mystat[key];

                      //console.log("final : " + finalclc[key] + " <= group : " + groupstat[key] + " + mine : " + mystat[key]);
                    }  //
                    counter++;
                }
              }
              console.log("final : " + JSON.stringify(finalclc));
            $scope.fang = finalclc;

      }

      function load_sidebar()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                    switch(rsys.result.deep)
                    {
                      case 1:
                        $scope.template = {url : "sidebar.html"};
                        $scope.top = {url : "topbar.html"};
                       break;
                      case 2:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 3:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 4:
                        $scope.template = {url : "sidebar3.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                    }

                    setStatusboxWidth();
                  });
      }

      $scope.num_per_page = 10;
      $scope.maxSize = 3;
      $scope.bigTotalItems = 10;
      $scope.bigCurrentPage = 1;

      function pageChanged() {
          detail_filter();
      }

      function onChangeNum_perPage(){
          detail_filterWhenClick();
      }

      function myinfo()
      {

        $http.get('/api/set_myinfo')
            .success(function(cbdata){
              
              console.log("_kekeke : " + cbdata[0].savepay);

                var fnum = parseInt(cbdata[0].orig_gid);
                if( fnum == 2 )
                  $window.location.href = '/server-page8(edit&detail).html';
                if( fnum > 2 )
                  $window.location.href = '/server-page15(edit&detail).html';
            });
      }

      $rootScope.$on("CallMethod", function(){
        console.log("hi~");
        alert("성공적으로 진행되였습니다.");
           detail_filter();
           loadBalance();
        });

      function mymodal(obj)
      {
        console.log("yayaya~ : " + obj.sp_money);
        $rootScope.header = obj;

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'dialogs/implement_sp.html',
            controller: 'SetWithdrawlPasswordCtrl',
            windowClass: 'center-modal',
            resolve: {
             //   items: function () {
              //    return $ctrl.items;
               // }
            }
        });

        modalInstance.result.then(function (selectedItem) {}, function () {
            //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
        });
      }

      function logout_func()
      {
        console.log("logouted");
        $http.get('/api/logout_func')
            .success(function(cbdata){
              if(cbdata.logresult == "success")
                $window.location.href = '/index.html';
            });
      }

      function loadBalance()
      {
        $http.get('/api/loadShopBalance')
              .success(function(lsbdata){
                console.log("lsbdata : " + lsbdata.lsbalance);
                var lstemp = lsbdata.lsbalance;
                shopbalance = lstemp;
                var strls;
                if((lstemp/1000)<1000)
                  { 
                   strls =  (lstemp/1000); 
                   $scope.lsb_unit = "K";
                   }   
                else if((lstemp/1000)>=1000)
                { 
                   strls =  ((lstemp/1000)/1000);
                   $scope.lsb_unit = "M";
                 }

                if(lsbdata.deeper > 1)
                  $scope.lsbmoney = strls;
                else
                {
                  $scope.lsbmoney = "Unlimited";
                  $scope.lsb_unit = "Unlimited";
                }
              });
      }

      function save_pay_filter(types)
      {
        console.log(types);
        switch(types)
        {
          case 11:
            $scope.sp_usertype = "1";
            $scope.methodtype = "1";
            
            detail_filterWhenClick();
            break;
          case 12:
            $scope.sp_usertype = "1";
            $scope.methodtype = "2";
            
            detail_filterWhenClick();
            break;
          case 21:
            $scope.sp_usertype = "2";
            $scope.methodtype = "1";
            
            detail_filterWhenClick();
            break;
          case 22:
            $scope.sp_usertype = "2";
            $scope.methodtype = "2";
            
            detail_filterWhenClick();
            break;
        }
      }

      /* Define new prototype methods on Date object. */
      // Returns Date as a String in YYYY-MM-DD format.
      Date.prototype.toISODateString = function () {
        return this.toISOString().substr(0,10);
      };

      // Returns new Date object offset `n` days from current Date object.
      Date.prototype.toDateFromDays = function (n) {
        n = parseInt(n) || 0;
        var newDate = new Date(this.getTime());
        newDate.setDate(this.getDate() + n);
        return newDate;
      };


      function day_filter(types)
      {
        console.log(types);
        
        switch(types)
        {
          case 31:
            
            var dto = new Date();
            var dfrom  = new Date();
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            detail_filterWhenClick();
            break;
          case 32:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-7);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            detail_filterWhenClick();
            break;
          case 33:
            var dto = new Date();
            var dfrom  = new Date();
            dfrom.setHours(0,0,0,0);
            dfrom.setDate(1);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            detail_filterWhenClick();
            break;
          case 34:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-90);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            detail_filterWhenClick();
            break;
          case 35:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-180);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            detail_filterWhenClick();
            break;
          case 36:
            var dto = new Date();
            var dfrom = "";
            console.log( dto.toISODateString() + " : " +  dfrom );
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            detail_filterWhenClick();
            break;
        }
      }

      function detail_filterWhenClick()
      {
        obj = {};

        if($('#fromdate').val()!="")
          obj['fromdate'] = $('#fromdate').val();

        if($('#todate').val()!="")
          obj['todate'] = $('#todate').val();

        if($scope.sp_usertype!="")
          obj['sp_usertype'] = $scope.sp_usertype;

        if($('#sp_name').val()!="")
          obj['sp_name'] = {'$regex': '.*'+$('#sp_name').val()+'.*'};

        if($('#bk_name').val()!="")
          obj['bk_name'] = {'$regex': '.*'+$('#bk_name').val()+'.*'};

        if($scope.methodtype!="")
          obj['methodtype'] = $scope.methodtype;

        if($scope.status!="")
            obj['status'] = $scope.status;

        if($('#imp_manager').val()!="")
          obj['imp_manager'] = {'$regex': '.*'+$('#imp_manager').val()+'.*'};

        console.log("gettopnaval-2 : " + $scope.sp_usertype + "/" + $scope.methodtype + "/" + $scope.status);
                  

        $http.post('/api/find_Save_pay_num' , obj)
            .success(function(cbdata){

              console.log("up parameter : " + JSON.stringify(obj));
              console.log("WHOLE ITEMS : " + cbdata.totalamount);

              $scope.bigTotalItems = (10*Math.ceil(cbdata.totalamount/$scope.num_per_page)); 

              detail_filter();
            });
      }

      function detail_filter()
      {
        
        obj = {};

        if($('#fromdate').val()!="")
          obj['fromdate'] = $('#fromdate').val();

        if($('#todate').val()!="")
          obj['todate'] = $('#todate').val();

        console.log("user**type : " +  $scope.sp_usertype);

        if($scope.sp_usertype!="")
          obj['sp_usertype'] = $scope.sp_usertype;

        if($('#sp_name').val()!="")
          obj['sp_name'] = {'$regex': '.*'+$('#sp_name').val()+'.*'};

        if($('#bk_name').val()!="")
          obj['bk_name'] = {'$regex': '.*'+$('#bk_name').val()+'.*'};

        if($scope.methodtype!="")
          obj['methodtype'] = $scope.methodtype;
        
        if($scope.status!="")
            obj['status'] = $scope.status;

        if($('#imp_manager').val()!="")
          obj['imp_manager'] = {'$regex': '.*'+$('#imp_manager').val()+'.*'};

        obj['currentpage'] = parseInt($scope.bigCurrentPage);
        obj['numperpage'] = parseInt($scope.num_per_page);

        $scope.working = true;
        $scope.message = 'Loading...'
        
        $http.post('/api/find_Save_pay_result' , obj)
            .success(function(cbdata){

              $scope.detailers = cbdata;
              $scope.working = false;

            });

      }

      function reset_filter()
      {

        $('#fromdate').val("");
        $('#todate').val("");
        
        $('#sp_name').val("");
        
        
        $('#imp_manager').val("");
        $('#bk_name').val("");

        $scope.status = "";
        $scope.methodtype = '1';
        $scope.sp_usertype = '1';


        console.log("reset_filter");
      }

      //start for notice on the top bar

      function checkForSounds(typesound)
      {
        $http.get('/api/getSoundsjson')
            .success(function(sdata)
            {
              console.log("typesound : " + typesound);
              console.log("global soundjson : " + JSON.stringify(sdata));

              if(typesound == 1)
              {
                /*if (audio.paused && audio.currentTime > 0 && !audio.ended) {
                  console.log("playing now....");
                }*/

                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) || (sdata.obj.notify < $scope.len ) )
                  {
                      
                      audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
              else if(typesound == 2)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) )
                  {
                      
                      audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
            });
      }

      function prepareForSounds(pfs)
      {
        $http.get('/api/getSoundsflag')
                                .success(function(ssflag)
                                  {
                                    console.log("ssflag : " + ssflag.obj);
                                    if(ssflag.obj == 1 )
                                    {
                                      $http.get('/api/getSoundsjson')
                                              .success(function(sdata)
                                              {
                                                console.log("before update sounds sp: " + JSON.stringify(sdata.obj));
                                                var temps = sdata.obj;
                                                temps.vsq = $scope.as.v_save_req;
                                                temps.ssq = $scope.as.s_save_req;
                                                temps.vpq = $scope.as.v_pay_req;
                                                temps.spq = $scope.as.s_pay_req;

                                                console.log("temps : " + JSON.stringify(temps) );

                                                $http.post('/api/setSoundsjson' , temps)
                                                .success(function(jdata)
                                                {
                                                  
                                                  console.log("success to update sound json");
                                                  checkForSounds(pfs);
                                                });


                                              });
                                    }
                                    else
                                    {
                                      checkForSounds(pfs);
                                    }

                                  });
      }

      function load_notice()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    if(rsys.result.deep == 1){
                      console.log("dfdf");
                     $http.get('/api/get_all_notice')
                        .success(function(cbdata){
                            console.log("rusuky : " + cbdata.obj);
                            whole_notice = cbdata.obj;
                            whole_notice = whole_notice.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

                            var unreads = [];
                            whole_notice.forEach(function(item){
                              if(item.am_status == 0)
                                {
                                  unreads.push(item);
                                  console.log(JSON.stringify(item));
                                }
                            });

                            $scope.ur = unreads;
                            $scope.len = unreads.length;

                            prepareForSounds(1);
                            
                        });
                    }
                    else
                      prepareForSounds(2);

                  });
      }

      function notify_clicked(iobj)
      {
        whole_notice.forEach(function(item){
          if(item == iobj)
            item.am_status = 1;
        }); 
        $http.post('/api/update_all_notice' , { obj : whole_notice } )
                .success(function(cbdata){
                  if(cbdata.result == true){

                      $http.get('/api/getAllalarm' )
                        .success(function(cbdata1){
                            var atypes = cbdata1.obj;
                            var contents = "";
                            var titles = "";
                            for(var i=0;i<atypes.length;i++)
                              if(atypes[i].a_id == iobj.am_content)
                              {
                                contents = atypes[i].a_content.a_en;
                                titles = atypes[i].a_title.a_en;
                              }

                               var mbody = "<p> 위반대상 : "+ iobj.am_target +" </p>"+
                               "<p> 위반대상 식별자 : "+ iobj.am_id +" </p>"+
                              "<p> 위반날자 : "+ iobj.am_date.toLocaleString().split("T")[0] + " " + iobj.am_date.toLocaleString().split("T")[1] +"</p>"+
                              "<p> 위반내용 : "+ contents +" </p> ";
          Notification.success({title : '<h3 style="text-align: center;"><i style=" -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: orange; color: rgba(187, 48, 23, 0.63); font-size : 42px;" class="fa fa-bell"></i></h3>' ,  message: mbody , delay: 10000});
                              
                              load_notice();
                          });
                  }
                });
      }

      //end for notice on the top bar

      //start show all status on the top bar
      function showAllStatus()
      {
        console.log("yayaya~~~hahaha");
        $http.get("/api/setAFlagZero")
                .success(function(afz){
                  if(afz.result == true)
                  {
                    $http.get("/api/getAllStatus")
                          .success(function(cbdata)
                          {

                            console.log("rat : " + JSON.stringify(cbdata));
                            $scope.as = cbdata;

                            load_notice();

                          });
                  }
                });
        
      }

      function closeNav(snd)
      {
        console.log("heyheyhey");
        var x = document.getElementById("myTopnav");
        x.className = "topnav";

        var flagf;
        if( ((3<=snd)&&(snd<=6)) || (snd == 13) || (snd == 11) )
        {
          flagf = 1;
        }
        else
        {
          flagf = 2;
        }
        $http.post("/api/setSoundsflag" , {obj : flagf})
                .success(function(cbdata)
                {
                  
                  /*var vs_page = {};

                  switch(snd)
                  {
                    case 1:
                      vs_page = {spval : "today"};
                      break;
                    case 2:
                      vs_page = {spval : "today"};
                      break;
                    case 3:
                      vs_page = {spval : "vsq"}; //vsq
                      break;
                    case 4:
                      vs_page = {spval : "ssq"}; //ssq
                      break;
                    case 5:
                      vs_page = {spval : "vpq"}; //vpq
                      break;
                    case 6:
                      vs_page = {spval : "spq"}; //spq
                      break;
                    case 7:
                      vs_page = {spval : "vsd"}; //ssd
                      break;
                    case 8:
                      vs_page = {spval : "vpd"}; //spd
                      break;
                    case 9:
                      vs_page = {spval : "ssd"};
                      break;
                    case 10:
                      vs_page = {spval : "spd"};
                      break;
                    case 11:
                      vs_page = {spval : "rq"};
                      break;
                    case 12:
                      vs_page = {spval : "rd"};
                      break;
                    case 13:
                      vs_page = {spval : "nh"};
                      break;

                  }
                  $http.post("/api/setASparam" , {obj : vs_page})
                    .success(function(mydd)
                    {*/

                        if((3<=snd)&&(snd<=10))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 3:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 4:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 5:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 6:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 7:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 8:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                            case 9:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 10:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page2.html';
                                });
                          
                        }
                        else if((snd == 11) || (snd == 12))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 11:
                              goParam = { cstatus : '3' , rouflag : 999};
                              break;
                            case 12:
                              goParam = { cstatus : '2' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page10.html';
                                });
                          
                        }
                        else if(snd == 1)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page7.html';
                                });
                          
                        }
                        else if(snd == 2)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page8.html';
                                });
                          
                        }
                    //}

                });

      }

      function setStatusboxWidth()
      {
        console.log("wow excellent.. : "+( parseInt($(".page-container").width()) - 390 ));
        var gijuns = parseInt($(".page-container").width());
        var d_amount;
        if(gijuns<768)
          d_amount = 200;
        else
          d_amount = 400;

        var mywidth = gijuns - d_amount;

        $scope.statusbox={'width':mywidth+'px'};
      }

      $(window).resize(function(){
          
        setStatusboxWidth();

      });

      //end show all status on the top bar

      //start test for transfer

      // handles the callback from the received event
        var handleCallback = function (msg) {
            $scope.$apply(function () {
                //$scope.msg = JSON.parse(msg.data)
                var r_obj = JSON.parse(msg.data);
                console.log("receiver : " + r_obj.flag);
                
                if(r_obj.flag != 0)
                {
                  if(r_obj.flag != 100)
                  {
                    console.log("will chekc for target~");
                    $http.post("/api/CheckForEmitTarget" , { cfeparam : r_obj.shcdata }) // check if this is logined id which is correct target to listen the emit socket signal
                          .success(function(cfedata)
                          {
                            if(cfedata.result == true) // this is correct target
                            {
                                  if(cfedata.flag == 1)
                                  {
                                    console.log("will find....");
                                    showAllStatus();
                                    
                                  }
                                  else if(cfedata.flag == 2){
                                    $http.get("/api/showShopRWmsm")
                                          .success(function(ssrw){
                                            alert(ssrw.msmbody);
                                          });
                                    //console.log("listening for change.....");
                                  }
                            }

                          });
                  }
                  else if(r_obj.flag == 100) // its self signal
                  {
                    console.log("will find....");
                    showAllStatus();
                  }
                  
                }
            });
        }
 
        var source = new EventSource('/stats');
        source.addEventListener('message', handleCallback, false);

      //end test for transfer

      
    }

})();
