(function (){

  angular
      .module('ServerApp', [])
      .controller('LRFController', LRFController);

    function LRFController($scope, $http, $window) {
      $scope.userCheck = userCheck;
      $scope.doRemember = doRemember;
      $scope.userRegister = userRegister;

      function init() {

        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                  });

        console.log("init");
        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 0)
                    $window.location.href = '/server-page1.html';
            });

        $http.get('/api/load_managertype')
                .success(function(cbdata){
                  console.log("forcombo : "+cbdata.length);
                  $scope.gtypes = cbdata;
                });

      }
      init();

      function userRegister(reg){
        console.log(reg.u_groupid);
        $http.put('/api/usercheck/'+reg.u_id , {u_id : reg.u_id})
            .success(function(cbdata){
              if(cbdata.result == true)
                alert("There is some user id , please try again with other id")
              else {
                  if(reg.u_pw != reg.u_cpw)
                    alert("please input same password!!!");
                  else {
                    console.log("groupid :  "+ reg.u_groupid);
                    if(reg.checkval==true)
                    {
                      $http
                        .post("/api/userregister", reg)
                        .success(function(cbdata1){
                          if(cbdata1.result == true)
                            alert("success to request!!! wait a moment");
                          else{
                            alert("fail to request!!!");
                          }
                        });
                    }
                    else
                    {
                      alert('please check the box!!!');
                    }
                  }

              }
            });

      }

      function userCheck(users)
      {
        console.log(users);
        $http.put('/api/usercheck/'+"loginstatus" , users)
            .success(function(cbdata){
              console.log("getter : " + cbdata.result);
              if(cbdata.result == true)
              {
                $http.get('/api/get_allow_data')
                        .success(function(gaddata){
                            
                            $window.location.href = '/server-page1.html';
                          });
              }
              else {
                alert("Login information is not correct!!!");
              }
            });
      }

      function doRemember(forobj)
      {
        console.log(forobj);
        $http.put('/api/doremember/'+forobj.u_email , forobj)
            .success(function(cbdata){
              if(cbdata.result == true)
                alert("Your information will goes to your email soon!!!")
              else {
                alert("There is no data");
              }
            });
      }

    }

})();
