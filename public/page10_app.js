(function (){

  angular
      .module('page10App', ['ngLoader' , 'ui-notification','ngRoute','ngAnimate', 'ngSanitize', 'ui.bootstrap'])
      .controller('page10Controller', Page10Controller)
      .controller('Implement_Report', Implement_Report)
      .config(function(NotificationProvider) {
          NotificationProvider.setOptions({
              delay: 10000,
              startTop: 20,
              startRight: 10,
              verticalSpacing: 20,
              horizontalSpacing: 20,
              positionX: 'right',
              positionY: 'top'
          });
        });

    function Implement_Report($scope, $http, $window , $rootScope , $uibModalInstance) {

      $scope.exitMsg = exitMsg;
      $scope.implementing = implementing;
      

      function init() {

        //$scope.v = $rootScope.sel_dmobj;
        $scope.rep = $rootScope.report_body;

      }
      init();

      function exitMsg()
      {
        $uibModalInstance.dismiss('cancel');
      }

      function implementing(imp_rep_body)
      {
        var ir_obj = imp_rep_body;
        if(ir_obj.m_imp_servicer_nickname != undefined){
          alert("이미 처리되였습니다.");
          $rootScope.$emit("CallMethod", {});
          $uibModalInstance.dismiss('cancel');
        }
        else{
          //console.log("ra : " + ir_obj.m_imp_contents);
        
          $http.post('/api/implement_report' , ir_obj)
            .success(function(cbdata){
              if(cbdata.result == true)
              {
                alert("성공적으로 처리되였습니다.");
                $rootScope.$emit("CallMethod", {});
                $uibModalInstance.dismiss('cancel');
              }
            });
        }
      }

    }

    function Page10Controller(Notification ,$uibModal, $log, $document, $rootScope, $location , $scope, $http, $window) {

      $scope.logout_func = logout_func;
      $scope.solvebtn = solvebtn;
      $scope.readyTofindWhenClick = readyTofindWhenClick;
      $scope.reset = reset;
      $scope.day_filter = day_filter;
      $scope.status_filter = status_filter;
     $scope.myinfo = myinfo;
     $scope.onChangeNum_perPage = onChangeNum_perPage;
      $scope.pageChanged = pageChanged;

      var wholeitems;

      var whole_notice;
      $scope.notify_clicked = notify_clicked;

      $scope.closeNav = closeNav;

      var audio = new Audio('./sounds/Maramba.mp3');

      var acdata = [];
      
      function init() {
        console.log("page10-hey initer");
        

        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 1)
                $window.location.href = '/index.html';
              else
              {
                console.log("aya~~")
                $http.get('/api/get_global_acdata')
                      .success(function(ggadata){
                        acdata = ggadata.acdata;
                        $scope.mycid = ggadata.mycid;
                        $scope.mygroup = ggadata.mygroup;
                        $scope.mydeeper = ggadata.mydeeper;

                        if(($scope.mydeeper == 1)&&(acdata[0].ac_report <2))
                          logout_func();
                        
                        imp_allow();

                        load_sidebar();
                        $scope.topnav = {url : "TopNav.html"};
                        showAllStatus();

                        CheckFromTopNav();

                      });
              }

            });
            
      }

      init();

      function CheckFromTopNav()
      {
        console.log("check from top nav..........");
        $http.get("/api/GetTopNavVal")
              .success(function(mgtnvdata){
                console.log("gettopnaval-1 : " + JSON.stringify(mgtnvdata));
                if(mgtnvdata.rouflag != 1000)
                {
                  console.log("guantanamo");
                  $("#report_status").val(mgtnvdata.cstatus);

                  $http.post("/api/SetTopNavVal" , {rouflag : 1000})
                    .success(function(gtnndata){

                      
                      if(gtnndata.result == "success")
                        readyTofindWhenClick();
                      });
                }
                else
                  readyTofindWhenClick();
              });
      }

      function imp_allow()
      {
          var mystat = {};
          var groupstat = {};
          var finalclc = {}; 
          console.log("$scope.mygroup : " + $scope.mygroup);

          acdata.forEach(function(raudata){
                          if(raudata.uinfo[0] != undefined)
                          {
                            if(raudata.uinfo[0].u_id == $scope.mycid)
                              mystat = raudata;
                          }
                          else if(raudata.ginfo[0] != undefined)
                          {
                            if(raudata.ginfo[0].g_id == $scope.mycid)
                              mystat = raudata;
                            if(raudata.ginfo[0].g_id == $scope.mygroup)
                              groupstat = raudata
                          }
                        });
          if($scope.mygroup < 100) // not manager grouop
              finalclc = mystat;
          else
              {
                
                finalclc = mystat; // init

                var counter = 0;
                for (var key in mystat) {
                    if(counter < 23)
                    {
                      if(mystat[key] == 0)
                        finalclc[key] = groupstat[key];
                      else
                        finalclc[key] = mystat[key];

                      //console.log("final : " + finalclc[key] + " <= group : " + groupstat[key] + " + mine : " + mystat[key]);
                    }  //
                    counter++;
                }
              }
              //console.log("final : " + finalclc.ac_zhong);
            $scope.fang = finalclc;

      }

      function load_sidebar()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                    switch(rsys.result.deep)
                    {
                      case 1:
                        $scope.template = {url : "sidebar.html"};
                        $scope.top = {url : "topbar.html"};
                       break;
                      case 2:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 3:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 4:
                        $scope.template = {url : "sidebar3.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                    }

                    setStatusboxWidth();

                  });
      }

      $scope.num_per_page = 10;
      $scope.maxSize = 3;
      $scope.bigTotalItems = 10;
      $scope.bigCurrentPage = 1;

      function pageChanged() {

          readyTofind();
      }

      function onChangeNum_perPage(){
        
          readyTofindWhenClick();

      }

      function myinfo()
      {

        $http.get('/api/set_myinfo')
            .success(function(cbdata){
              
              console.log("_kekeke : " + cbdata[0].savepay);

                var fnum = parseInt(cbdata[0].orig_gid);
                if( fnum == 2 )
                  $window.location.href = '/server-page8(edit&detail).html';
                if( fnum > 2 )
                  $window.location.href = '/server-page15(edit&detail).html';
            });
      }

      $rootScope.$on("CallMethod", function(){

        readyTofindWhenClick();
      });

      function logout_func()
      {
        console.log("logouted");
        $http.get('/api/logout_func')
            .success(function(cbdata){
              if(cbdata.logresult == "success")
                $window.location.href = '/index.html';
            });
      }

      Date.prototype.toISODateString = function () {
        return this.toISOString().substr(0,10);
      };

      // Returns new Date object offset `n` days from current Date object.
      Date.prototype.toDateFromDays = function (n) {
        n = parseInt(n) || 0;
        var newDate = new Date(this.getTime());
        newDate.setDate(this.getDate() + n);
        return newDate;
      };

      function day_filter(types)
      {
        //alert(types);
        console.log(types);
        
        switch(types)
        {
          case 31:
            
            var dto = new Date();
            var dfrom  = new Date();
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            readyTofindWhenClick();
            break;
          case 32:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-7);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            readyTofindWhenClick();
            break;
          case 33:
            var dto = new Date();
            var dfrom  = new Date();
            dfrom.setHours(0,0,0,0);
            dfrom.setDate(1);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            readyTofindWhenClick();
            break;
          case 34:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-90);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            readyTofindWhenClick();
            break;
          case 35:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-180);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            readyTofindWhenClick();
            break;
          case 36:
            var dto = new Date();
            var dfrom = "";
            console.log( dto.toISODateString() + " : " +  dfrom );
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            readyTofindWhenClick();
            break;
        }
      }

      function status_filter(status_t)
      {
        console.log("status :  "+status_t);
        switch(status_t)
        {
          case 41:
            $('#report_status').val( '1' );
            readyTofindWhenClick();
            break;
          case 42:
            $('#report_status').val( '3' );
            readyTofindWhenClick();
            break;
          case 43:
            $('#report_status').val( '2' );
            readyTofindWhenClick();
            break;
        }
      }

      function readyTofindWhenClick()
      {
        console.log("dddd");

        var date_obj = {};
        var filter_param = {};
        var status = $('#report_status').val();

        if($('#fromdate').val()!="")
          date_obj['$gte'] = $("#fromdate").val();

        if($('#todate').val()!="")
          date_obj['$lt'] = $('#todate').val();

        if( ($('#fromdate').val()!="") || ($('#todate').val()!="") )
          filter_param['m_datetime'] = date_obj;

        if($('#nickname').val()!="")
        {
          filter_param['m_sender_nickname'] = {'$regex': '.*'+$("#nickname").val()+'.*'};
        }

        $http.post('/api/find_report_history_Num/'+status , filter_param)
            .success(function(cbdata){
              $scope.bigTotalItems = (10*Math.ceil(cbdata.totalamount/$scope.num_per_page)); 

              readyTofind();
            });
      }

      function readyTofind()
      {
        var date_obj = {};
        var filter_param = {};
        var status = $('#report_status').val();

        if($('#fromdate').val()!="")
          date_obj['$gte'] = $("#fromdate").val();

        if($('#todate').val()!="")
          date_obj['$lt'] = $('#todate').val();

        if( ($('#fromdate').val()!="") || ($('#todate').val()!="") )
          filter_param['m_datetime'] = date_obj;

        if($('#nickname').val()!="")
        {
          filter_param['m_sender_nickname'] = {'$regex': '.*'+$("#nickname").val()+'.*'};
        }

        var pvobj = {};
        pvobj['currentpage'] = parseInt($scope.bigCurrentPage);
        pvobj['numperpage'] = parseInt($scope.num_per_page);

        $scope.working = true;
        $scope.message = 'Loading...'

        $http.put('/api/find_report_history/'+status , {"filter_param" : filter_param , "pvobj" : pvobj})
            .success(function(cbdata){
              console.log("report0 : "+cbdata.length);

              cbdata.forEach(function(item){
                if(item.m_imp_servicer_nickname != undefined)
                  item['imp_status'] = "처리됨";
                else
                  item['imp_status'] = "미결건";

              });
              
              $scope.rep_lists = cbdata;
              
              $scope.working = false;
            });

      }

      function reset()
      {
        $('#nickname').val('');
        $('#fromdate').val('');
        $('#todate').val('');
        $('#report_status').val($("#report_status option:first").val());
      }

      function solvebtn(rep_body)
      {
        $scope.title = "ikimaseyo!!!";
            console.log("hi");

        $rootScope.report_body = rep_body;

            var modalInstance = $uibModal.open({
              animation: true,
              templateUrl: 'dialogs/implement_report.html',
              controller: 'Implement_Report',
              windowClass: 'center-modal',
              resolve: {
               //   items: function () {
                //    return $ctrl.items;
                 // }
              }
          });

          modalInstance.result.then(function (selectedItem) {}, function () {
              //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
          });
        
      }

      //start for notice on the top bar

      function checkForSounds(typesound)
      {
        $http.get('/api/getSoundsjson')
            .success(function(sdata)
            {
              if(typesound == 1)
              {
                console.log("typesound : " + typesound);
                console.log("global soundjson : " + JSON.stringify(sdata));

                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) || (sdata.obj.notify < $scope.len ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
              else if(typesound == 2)
              {
                console.log("typesound : " + typesound);
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
            });
      }

      function prepareForSounds(pfs)
      {
        $http.get('/api/getSoundsflag')
                                .success(function(ssflag)
                                  {
                                    if(ssflag.obj == 1 )
                                    {
                                      $http.get('/api/getSoundsjson')
                                              .success(function(sdata)
                                              {
                                                console.log("before update sounds msm : " + JSON.stringify(sdata.obj));
                                                var temps = sdata.obj;
                                                temps.rpq = $scope.as.report_req;

                                                $http.post('/api/setSoundsjson' , temps)
                                                .success(function(jdata)
                                                {
                                                  console.log("success to update sound json");
                                                  checkForSounds(pfs);
                                                });


                                              });
                                    }
                                    else
                                    {
                                      checkForSounds(pfs);
                                    }

                                  });
      }

      function load_notice()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    if(rsys.result.deep == 1){
                      console.log("dfdf");
                     $http.get('/api/get_all_notice')
                        .success(function(cbdata){
                            console.log("rusuky : " + cbdata.obj);
                            whole_notice = cbdata.obj;
                            whole_notice = whole_notice.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

                            var unreads = [];
                            whole_notice.forEach(function(item){
                              if(item.am_status == 0)
                                {
                                  unreads.push(item);
                                  console.log(JSON.stringify(item));
                                }
                            });

                            $scope.ur = unreads;
                            $scope.len = unreads.length;

                            prepareForSounds(1);

                        });
                    }
                    else
                    {
                      prepareForSounds(2);
                    }

                  });
      }

      function notify_clicked(iobj)
      {
        whole_notice.forEach(function(item){
          if(item == iobj)
            item.am_status = 1;
        }); 
        $http.post('/api/update_all_notice' , { obj : whole_notice } )
                .success(function(cbdata){
                  if(cbdata.result == true){

                      $http.get('/api/getAllalarm' )
                        .success(function(cbdata1){
                            var atypes = cbdata1.obj;
                            var contents = "";
                            var titles = "";
                            for(var i=0;i<atypes.length;i++)
                              if(atypes[i].a_id == iobj.am_content)
                              {
                                contents = atypes[i].a_content.a_en;
                                titles = atypes[i].a_title.a_en;
                              }

                               var mbody = "<p> 위반대상 : "+ iobj.am_target +" </p>"+
                               "<p> 위반대상 식별자 : "+ iobj.am_id +" </p>"+
                              "<p> 위반날자 : "+ iobj.am_date.toLocaleString().split("T")[0] + " " + iobj.am_date.toLocaleString().split("T")[1] +"</p>"+
                              "<p> 위반내용 : "+ contents +" </p> ";
          Notification.success({title : '<h3 style="text-align: center;"><i style=" -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: orange; color: rgba(187, 48, 23, 0.63); font-size : 42px;" class="fa fa-bell"></i></h3>' ,  message: mbody , delay: 10000});
                              
                              load_notice();
                          });
                  }
                });
      }

      //end for notice on the top bar

      //start show all status on the top bar
      function showAllStatus()
      {
        console.log("yayaya~~~hahaha");
        $http.get("/api/setAFlagZero")
                .success(function(afz){
                  if(afz.result == true)
                  {
                    $http.get("/api/getAllStatus")
                          .success(function(cbdata)
                          {

                            console.log("rat : " + JSON.stringify(cbdata));
                            $scope.as = cbdata;

                            load_notice();

                          });
                  }
                });
        
      }

      function closeNav(snd)
      {
        console.log("heyheyhey");
        var x = document.getElementById("myTopnav");
        x.className = "topnav";

        var flagf;
        if( ((3<=snd)&&(snd<=6)) || (snd == 13) || (snd == 11) )
        {
          flagf = 1;
        }
        else
        {
          flagf = 2;
        }
        $http.post("/api/setSoundsflag" , {obj : flagf})
                .success(function(cbdata)
                {
                  if((3<=snd)&&(snd<=10))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 3:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 4:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 5:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 6:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 7:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 8:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                            case 9:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 10:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page2.html';
                                });
                          
                        }
                        else if((snd == 11) || (snd == 12))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 11:
                              goParam = { cstatus : '3' , rouflag : 999};
                              break;
                            case 12:
                              goParam = { cstatus : '2' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page10.html';
                                });
                          
                        }
                        else if(snd == 1)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page7.html';
                                });
                          
                        }
                        else if(snd == 2)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page8.html';
                                });
                          
                        }

                });

      }

      function setStatusboxWidth()
      {
        console.log("wow excellent.. : "+( parseInt($(".page-container").width()) - 390 ));
        var gijuns = parseInt($(".page-container").width());
        var d_amount;
        if(gijuns<768)
          d_amount = 200;
        else
          d_amount = 400;

        var mywidth = gijuns - d_amount;

        $scope.statusbox={'width':mywidth+'px'};
      }

      $(window).resize(function(){
          
        setStatusboxWidth();

      });

      //end show all status on the top bar

      //start test for transfer

      // handles the callback from the received event
        var handleCallback = function (msg) {
            $scope.$apply(function () {
                //$scope.msg = JSON.parse(msg.data)
                var r_obj = JSON.parse(msg.data);
                console.log("receiver : " + r_obj.flag);
                
                if(r_obj.flag != 0)
                {
                  if(r_obj.flag != 100)
                  {
                    console.log("will chekc for target~");
                    $http.post("/api/CheckForEmitTarget" , { cfeparam : r_obj.shcdata }) // check if this is logined id which is correct target to listen the emit socket signal
                          .success(function(cfedata)
                          {
                            if(cfedata.result == true) // this is correct target
                            {
                                  if(cfedata.flag == 1)
                                  {
                                    console.log("will find....");
                                    showAllStatus();
                                    
                                  }
                                  else if(cfedata.flag == 2){
                                    $http.get("/api/showShopRWmsm")
                                          .success(function(ssrw){
                                            alert(ssrw.msmbody);
                                          });
                                    //console.log("listening for change.....");
                                  }
                            }

                          });
                  }
                  else if(r_obj.flag == 100) // its self signal
                  {
                    console.log("will find....");
                    showAllStatus();
                  }
                  
                }
            });
        }
 
        var source = new EventSource('/stats');
        source.addEventListener('message', handleCallback, false);

      //end test for transfer


    }

})();
