(function (){

  angular
      .module('page12App', ['ui-notification','ngRoute','ngAnimate', 'ngSanitize', 'ui.bootstrap'])
      .controller('page12Controller', Page12Controller)
      .controller('addToBlackList', addToBlackList)
      .controller('addBlockIP', addBlockIP)
      .controller('addAlarm', addAlarm)
      .controller('updateAlarm', updateAlarm)
      .config(function(NotificationProvider) {
          NotificationProvider.setOptions({
              delay: 10000,
              startTop: 20,
              startRight: 10,
              verticalSpacing: 20,
              horizontalSpacing: 20,
              positionX: 'right',
              positionY: 'top'
          });
        });

    function addToBlackList($scope, $http, $window , $rootScope , $uibModalInstance) {

      $scope.exitMsg = exitMsg;
      $scope.addMem = addMem;

      function init() {


      }
      init();

      function exitMsg()
      {
        $uibModalInstance.dismiss('cancel');
      }

      function addMem(v)
      {
        //console.log("vvv : " + v.u_nickname);
        if(v != undefined)
        {
          var jsval = { val : v.u_nickname };
          $http.post('/api/addBlackName' , jsval)
            .success(function(cbdata){
              if(cbdata.result == true){
                console.log("success to add~~~");
                $rootScope.$emit("CallMethod", {});
                $uibModalInstance.dismiss('cancel');
              }
            });
        }
        else
        {
          alert("금지회원명을 입력하지않고서는 추가할수 없습니다.");
        }
      }

    }

    function addBlockIP($scope, $http, $window , $rootScope , $uibModalInstance) {

      $scope.exitMsg = exitMsg;
      $scope.addIp = addIp;

      function init() {


      }
      init();

      function exitMsg()
      {
        $uibModalInstance.dismiss('cancel');
      }

      function addIp(v)
      {
        //console.log("vvv : " + v.u_nickname);
        if(v != undefined)
        {
          var jsval = { val : v.u_ip };
          $http.post('/api/addBlockIP' , jsval)
            .success(function(cbdata){
              if(cbdata.result == true){
                console.log("success to add~~~");
                $rootScope.$emit("showBIPs", {});
                $uibModalInstance.dismiss('cancel');
              }
            });
        }
        else
        {
          alert("금지IP주소를 입력하지않고서는 추가할수 없습니다.");
        }
      }

    }

    function addAlarm($scope, $http, $window , $rootScope , $uibModalInstance) {

      $scope.exitMsg = exitMsg;
      $scope.addalarm = addalarm;

      function init() {


      }
      init();

      function exitMsg()
      {
        $uibModalInstance.dismiss('cancel');
      }

      function addalarm(v)
      {
        //console.log("vvv : " + v.u_nickname);
        if(v != undefined)
        {
          if(v.a_id != undefined)
          {
                if( $('#title1').val().length >=2 )
                {
                  if( $('#title2').val().length >=2 )
                  {
                    if( $('#title3').val().length >=2 )
                    {
                        var arr = $rootScope.witems;
                        var vflag = 0; 
                        if(arr != undefined)
                        {
                          arr.forEach(function(item){
                            if( item.a_id == v.a_id )
                              vflag = 1;
                          });
                        }

                        if(vflag == 0)
                        {
                            v['a_title'] = { a_en : $('#title1').val() , a_ch : $('#title2').val() , a_kr : $('#title3').val() };
                            v['a_content'] = { a_en : $('#description1').val() , a_ch : $('#description2').val() , a_kr : $('#description3').val() };

                            v['a_func_status'] = false;
                            v['a_setting_status'] = false;
                              $http.post('/api/addnew_alarm' , v)
                                .success(function(cbdata){
                                  if(cbdata.result == true){
                                    console.log("success to add~~~");
                                    $rootScope.$emit("showAlarms", {});
                                    $uibModalInstance.dismiss('cancel');
                                  }
                                });
                        }
                        else
                        alert("중복되는 식별자입니다. 다시 선택하십시오.");
                    }
                    else
                       alert("Korean 언어에 대응한 제목과 내용을 입력하여야 합니다.");
                  }
                  else
                    alert("Chinese 언어에 대응한 제목과 내용을 입력하여야 합니다.");
                }
                else
                  alert("English 언어에 대응한 제목과 내용을 입력하여야 합니다.");
          }
          else
            alert("항목들을 전부 입력하여야 합니다.");
          
        }
        else
        {
          alert("항목들을 전부 입력하여야 합니다.");
        }
      }
    }

    function updateAlarm($scope, $http, $window , $rootScope , $uibModalInstance) {

          $scope.exitMsg = exitMsg;
          $scope.updatealarm = updatealarm;

          function init() {

            $scope.v = $rootScope.sel_data;
            //if($rootScope.sel_data.a_func_status == true)
            if($rootScope.sel_data.a_func_status == true)
              $scope.fstatus = "1";
            else if($rootScope.sel_data.a_func_status == false)
              $scope.fstatus = "2";
            if($rootScope.sel_data.a_setting_status == true)
              $scope.sstatus = "1";
            else if($rootScope.sel_data.a_setting_status == false)
              $scope.sstatus = "2";
            
          }
          init();

          function exitMsg()
          {
            $uibModalInstance.dismiss('cancel');
          }

          function updatealarm(v, fstatus, sstatus)
          {
            //console.log("vvv : " + v.u_nickname);
            //    $rootScope.sel_data = item;
            // $rootScope.index = index;
            // $rootScope.witems = wholeitems2;
            var wwitem = $rootScope.witems;
            var index = $rootScope.index;
            if(v != undefined)
            {
              if( $('#title1').val().length >=2 )
                {
                  if( $('#title2').val().length >=2 )
                  {
                    if( $('#title3').val().length >=2 )
                    {
                          if(fstatus == '1')
                            v['a_func_status'] = true;
                          else if(fstatus == '2')
                            v['a_func_status'] = false;
                          if(sstatus == '1')
                            v['a_setting_status'] = true;
                          else if(sstatus == '2')
                            v['a_setting_status'] = false;
                          
                          console.log(v);

                            if (index !== -1) {
                                wwitem[index] = v;
                            }

                            $http.post('/api/update_alarm' , wwitem)
                              .success(function(cbdata){
                                if(cbdata.result == true){
                                  console.log("success to add~~~");
                                  $rootScope.$emit("showAlarms", {});
                                  $uibModalInstance.dismiss('cancel');
                                }
                              });
                    }
                    else
                       alert("Korean 언어에 대응한 제목과 내용을 입력하여야 합니다.");
                  }
                  else
                    alert("Chinese 언어에 대응한 제목과 내용을 입력하여야 합니다.");
                }
                else
                  alert("English 언어에 대응한 제목과 내용을 입력하여야 합니다.");
              
            }
            else
            {
              alert("항목들을 전부 입력하여야 합니다.");
            }
          }


    }

    function Page12Controller(Notification ,$uibModal, $log, $document, $rootScope, $location , $scope, $http, $window) 
    {

      $scope.logout_func = logout_func;
      $scope.myinfo = myinfo;

      $scope.addBlackMem = addBlackMem;
      $scope.del = del;
      $scope.newAlarm = newAlarm;
      $scope.updateAlert = updateAlert;
      

      $scope.onChangeNum_perPage = onChangeNum_perPage;
      $scope.pageChanged = pageChanged;
      var wholeitems;


      $scope.onChangeNum_perPage1 = onChangeNum_perPage1;
      $scope.pageChanged1 = pageChanged1;
      var wholeitems1;

      $scope.onChangeNum_perPage2 = onChangeNum_perPage2;
      $scope.pageChanged2 = pageChanged2;
      var wholeitems2;

      var whole_notice;
      $scope.notify_clicked = notify_clicked;

      $scope.closeNav = closeNav;
     
      var audio = new Audio('./sounds/Maramba.mp3');

      var acdata = [];

      function init() {
        console.log("page12-hey initer");

        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 1)
                $window.location.href = '/index.html';
              else
              {
                console.log("aya~~")
                $http.get('/api/get_global_acdata')
                      .success(function(ggadata){
                        acdata = ggadata.acdata;
                        $scope.mycid = ggadata.mycid;
                        $scope.mygroup = ggadata.mygroup;
                        $scope.mydeeper = ggadata.mydeeper;

                        if(($scope.mydeeper == 1)&&(acdata[0].ac_security <2))
                          logout_func();

                        imp_allow();

                        load_sidebar();
                        $scope.topnav = {url : "TopNav.html"};
                        showAllStatus();

                        load_blackname(1);
                        load_blackname(2);
                        load_alarmlist();

                      });
              }

            });

      }

      function imp_allow()
      {
          var mystat = {};
          var groupstat = {};
          var finalclc = {}; 
          console.log("$scope.mygroup : " + $scope.mygroup);

          acdata.forEach(function(raudata){
                          if(raudata.uinfo[0] != undefined)
                          {
                            if(raudata.uinfo[0].u_id == $scope.mycid)
                              mystat = raudata;
                          }
                          else if(raudata.ginfo[0] != undefined)
                          {
                            if(raudata.ginfo[0].g_id == $scope.mycid)
                              mystat = raudata;
                            if(raudata.ginfo[0].g_id == $scope.mygroup)
                              groupstat = raudata
                          }
                        });
          if($scope.mygroup < 100) // not manager grouop
              finalclc = mystat;
          else
              {
                
                finalclc = mystat; // init

                var counter = 0;
                for (var key in mystat) {
                    if(counter < 23)
                    {
                      if(mystat[key] == 0)
                        finalclc[key] = groupstat[key];
                      else
                        finalclc[key] = mystat[key];

                      //console.log("final : " + finalclc[key] + " <= group : " + groupstat[key] + " + mine : " + mystat[key]);
                    }  //
                    counter++;
                }
              }
              //console.log("final : " + finalclc.ac_zhong);
            $scope.fang = finalclc;

      }
// black name
      $scope.num_per_page = 10;
      $scope.maxSize = 3;
      $scope.bigTotalItems = 10;
      $scope.bigCurrentPage = 1;

      function pageChanged() {

          console.log('Page changed to: ' + $scope.bigCurrentPage);
          console.log($scope.bigCurrentPage);
          onChangeNum_perPage();
      }

      function onChangeNum_perPage(){
        console.log("ll : " + $scope.num_per_page);

        if($scope.num_per_page == 1)
          $scope.num_per_page = wholeitems.length;

        $scope.bigTotalItems = (10*Math.ceil(wholeitems.length/$scope.num_per_page));
        var frr = [];
        for(var j = ($scope.bigCurrentPage - 1) * $scope.num_per_page; j < $scope.bigCurrentPage * $scope.num_per_page; j ++)
            {
                if(j < wholeitems.length)
                    frr.push(wholeitems[j]);
            }
            $scope.blist = frr;
            console.log($scope.blist);

      }

//black name

//block ip

      $scope.num_per_page1 = 10;
      $scope.maxSize1 = 3;
      $scope.bigTotalItems1 = 10;
      $scope.bigCurrentPage1 = 1;

      function pageChanged1() {

          console.log('Page changed to: ' + $scope.bigCurrentPage1);
          console.log($scope.bigCurrentPage1);
          onChangeNum_perPage1();
      }

      function onChangeNum_perPage1(){
        console.log("ll : " + $scope.num_per_page1);

        if($scope.num_per_page1 == 1)
          $scope.num_per_page1 = wholeitems1.length;

        $scope.bigTotalItems1 = (10*Math.ceil(wholeitems1.length/$scope.num_per_page1));
        var frr = [];
        for(var j = ($scope.bigCurrentPage1 - 1) * $scope.num_per_page1; j < $scope.bigCurrentPage1 * $scope.num_per_page1; j ++)
            {
                if(j < wholeitems1.length)
                    frr.push(wholeitems1[j]);
            }
            $scope.bIPlist = frr;
            console.log($scope.bIPlist);

      }

//block ip

//new alarm

      $scope.num_per_page2 = 10;
      $scope.maxSize2 = 3;
      $scope.bigTotalItems2 = 10;
      $scope.bigCurrentPage2 = 1;

      function pageChanged2() {

          console.log('Page changed to: ' + $scope.bigCurrentPage2);
          console.log($scope.bigCurrentPage2);
          onChangeNum_perPage2();
      }

      function onChangeNum_perPage2(){
        console.log("ll : " + $scope.num_per_page2);

        if($scope.num_per_page2 == 1)
          $scope.num_per_page2 = wholeitems2.length;

        $scope.bigTotalItems2 = (10*Math.ceil(wholeitems2.length/$scope.num_per_page2));
        var frr = [];
        for(var j = ($scope.bigCurrentPage2 - 1) * $scope.num_per_page2; j < $scope.bigCurrentPage2 * $scope.num_per_page2; j ++)
            {
                if(j < wholeitems2.length)
                    frr.push(wholeitems2[j]);
            }
            $scope.alarms = frr;
            console.log($scope.alarms);

      }
//new alarm


      function myinfo()
      {

        $http.get('/api/set_myinfo')
            .success(function(cbdata){
              
              console.log("_kekeke : " + cbdata[0].savepay);

                var fnum = parseInt(cbdata[0].orig_gid);
                if( fnum == 2 )
                  $window.location.href = '/server-page8(edit&detail).html';
                if( fnum > 2 )
                  $window.location.href = '/server-page15(edit&detail).html';
            });
      }

      init();

      function load_sidebar()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                    switch(rsys.result.deep)
                    {
                      case 1:
                        $scope.template = {url : "sidebar.html"};
                        $scope.top = {url : "topbar.html"};
                       break;
                      case 2:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 3:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 4:
                        $scope.template = {url : "sidebar3.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                    }

                    setStatusboxWidth();
                  });
      }

      function logout_func()
      {
        console.log("logouted");
        $http.get('/api/logout_func')
            .success(function(cbdata){
              if(cbdata.logresult == "success")
                $window.location.href = '/index.html';
            });
      }

      $rootScope.$on("CallMethod", function(){
        load_blackname(1);
        
      });

      $rootScope.$on("showBIPs", function(){
        load_blackname(2);
        
      });

      $rootScope.$on("showAlarms", function(){
        load_alarmlist();
        
      });

      


      function addBlackMem(f)
      {

          if(f == 1){
              var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'dialogs/addBlacklistDlg.html',
                controller: 'addToBlackList',
                windowClass: 'center-modal',
                resolve: {
                 //   items: function () {
                  //    return $ctrl.items;
                   // }
                }
            });

            modalInstance.result.then(function (selectedItem) {}, function () {
                //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
            });
          }
          else if(f == 2){
              var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'dialogs/addBlockIPDlg.html',
                controller: 'addBlockIP',
                windowClass: 'center-modal',
                resolve: {
                 //   items: function () {
                  //    return $ctrl.items;
                   // }
                }
            });

            modalInstance.result.then(function (selectedItem) {}, function () {
                //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
            });
          }
      }

      function load_blackname(f)
      {
        if(f == 1){
            $http.get('/api/get_blacklist')
                .success(function(cbdata){
                 console.log(cbdata[0].sa_arr);
                 wholeitems = cbdata[0].sa_arr;
                 pageChanged();
                 //$scope.blist = cbdata[0].sa_arr;
                });
        }
        else if(f == 2){
            $http.get('/api/get_blockname')
                .success(function(cbdata){
                 console.log(cbdata[0].sa_arr);
                 wholeitems1 = cbdata[0].sa_arr;
                 pageChanged1();
                 //$scope.blist = cbdata[0].sa_arr;
                });
        }
      }

      function del(item , f)
      {
        if(f == 1)
        {
            var index = wholeitems.indexOf(item);
            if (index > -1) {
                  wholeitems.splice(index, 1);
            }

            $http.post('/api/removeBlackitem' , { obj : wholeitems } )
                .success(function(cbdata){
                  if(cbdata.result == true)
                    load_blackname(1);
                });
        }

        else if(f == 2)
        {
            var index = wholeitems1.indexOf(item);
            if (index > -1) {
                  wholeitems1.splice(index, 1);
            }

            $http.post('/api/removeBlockip' , { obj : wholeitems1 } )
                .success(function(cbdata){
                  if(cbdata.result == true)
                    load_blackname(2);
                });
        }

      }

      function newAlarm()
      {
        $rootScope.witems = wholeitems2;

        var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'dialogs/addNewAlarm.html',
                controller: 'addAlarm',
                windowClass: 'center-modal',
                resolve: {
                 //   items: function () {
                  //    return $ctrl.items;
                   // }
                }
            });

            modalInstance.result.then(function (selectedItem) {}, function () {
                //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
            });
      }

      function load_alarmlist()
      {
        $http.get('/api/get_alarmlist')
                .success(function(cbdata){
                 console.log(cbdata.result);

                 wholeitems2 = cbdata.result;

                 wholeitems2.forEach(function(item){
                  item['lan_title'] = item.a_title.a_en;
                  item['lan_content'] = item.a_content.a_en;
                 });
                 pageChanged2();
                 //$scope.blist = cbdata[0].sa_arr;
                });
      }

      function updateAlert(item , index)
      {
        console.log("index : " + index);
        $rootScope.sel_data = item;
        $rootScope.index = index;
        $rootScope.witems = wholeitems2;

        var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'dialogs/updateAlarm.html',
                controller: 'updateAlarm',
                windowClass: 'center-modal',
                resolve: {
                 //   items: function () {
                  //    return $ctrl.items;
                   // }
                }
            });

            modalInstance.result.then(function (selectedItem) {}, function () {
                //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
            });
      }

      //start for notice on the top bar

      function checkForSounds(typesound)
      {
        $http.get('/api/getSoundsjson')
            .success(function(sdata)
            {
              if(typesound == 1)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) || (sdata.obj.notify < $scope.len ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
              else if(typesound == 2)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
            });
      }

      function load_notice()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    if(rsys.result.deep == 1){
                      console.log("dfdf");
                     $http.get('/api/get_all_notice')
                        .success(function(cbdata){
                            console.log("rusuky : " + cbdata.obj);
                            whole_notice = cbdata.obj;
                            whole_notice = whole_notice.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

                            var unreads = [];
                            whole_notice.forEach(function(item){
                              if(item.am_status == 0)
                                {
                                  unreads.push(item);
                                  console.log(JSON.stringify(item));
                                }
                            });

                            $scope.ur = unreads;
                            $scope.len = unreads.length;

                            checkForSounds(1);
                             

                        });
                    }
                    else
                      checkForSounds(2);

                  });
      }

      function notify_clicked(iobj)
      {
        whole_notice.forEach(function(item){
          if(item == iobj)
            item.am_status = 1;
        }); 
        $http.post('/api/update_all_notice' , { obj : whole_notice } )
                .success(function(cbdata){
                  if(cbdata.result == true){

                      $http.get('/api/getAllalarm' )
                        .success(function(cbdata1){
                            var atypes = cbdata1.obj;
                            var contents = "";
                            var titles = "";
                            for(var i=0;i<atypes.length;i++)
                              if(atypes[i].a_id == iobj.am_content)
                              {
                                contents = atypes[i].a_content.a_en;
                                titles = atypes[i].a_title.a_en;
                              }

                               var mbody = "<p> 위반대상 : "+ iobj.am_target +" </p>"+
                               "<p> 위반대상 식별자 : "+ iobj.am_id +" </p>"+
                              "<p> 위반날자 : "+ iobj.am_date.toLocaleString().split("T")[0] + " " + iobj.am_date.toLocaleString().split("T")[1] +"</p>"+
                              "<p> 위반내용 : "+ contents +" </p> ";
          Notification.success({title : '<h3 style="text-align: center;"><i style=" -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: orange; color: rgba(187, 48, 23, 0.63); font-size : 42px;" class="fa fa-bell"></i></h3>' ,  message: mbody , delay: 10000});
                              
                              load_notice();
                          });
                  }
                });
      }

      //end for notice on the top bar

      //start show all status on the top bar
      function showAllStatus()
      {
        console.log("yayaya~~~hahaha");
        $http.get("/api/setAFlagZero")
                .success(function(afz){
                  if(afz.result == true)
                  {
                    $http.get("/api/getAllStatus")
                          .success(function(cbdata)
                          {

                            console.log("rat : " + JSON.stringify(cbdata));
                            $scope.as = cbdata;

                            load_notice();

                          });
                  }
                });
        
      }

      function closeNav(snd)
      {
        console.log("heyheyhey");
        var x = document.getElementById("myTopnav");
        x.className = "topnav";

        var flagf;
        if( ((3<=snd)&&(snd<=6)) || (snd == 13) || (snd == 11) )
        {
          flagf = 1;
        }
        else
        {
          flagf = 2;
        }
        $http.post("/api/setSoundsflag" , {obj : flagf})
                .success(function(cbdata)
                {
                  if((3<=snd)&&(snd<=10))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 3:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 4:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 5:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 6:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 7:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 8:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                            case 9:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 10:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page2.html';
                                });
                          
                        }
                        else if((snd == 11) || (snd == 12))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 11:
                              goParam = { cstatus : '3' , rouflag : 999};
                              break;
                            case 12:
                              goParam = { cstatus : '2' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page10.html';
                                });
                          
                        }
                        else if(snd == 1)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page7.html';
                                });
                          
                        }
                        else if(snd == 2)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page8.html';
                                });
                          
                        }

                });

      }

      function setStatusboxWidth()
      {
        console.log("wow excellent.. : "+( parseInt($(".page-container").width()) - 390 ));
        var gijuns = parseInt($(".page-container").width());
        var d_amount;
        if(gijuns<768)
          d_amount = 200;
        else
          d_amount = 400;

        var mywidth = gijuns - d_amount;

        $scope.statusbox={'width':mywidth+'px'};
      }

      $(window).resize(function(){
          
        setStatusboxWidth();

      });

      //end show all status on the top bar

      //start test for transfer

      // handles the callback from the received event
        var handleCallback = function (msg) {
            $scope.$apply(function () {
                //$scope.msg = JSON.parse(msg.data)
                var r_obj = JSON.parse(msg.data);
                console.log("receiver : " + r_obj.flag);
                
                if(r_obj.flag != 0)
                {
                  if(r_obj.flag != 100)
                  {
                    console.log("will chekc for target~");
                    $http.post("/api/CheckForEmitTarget" , { cfeparam : r_obj.shcdata }) // check if this is logined id which is correct target to listen the emit socket signal
                          .success(function(cfedata)
                          {
                            if(cfedata.result == true) // this is correct target
                            {
                                  if(cfedata.flag == 1)
                                  {
                                    console.log("will find....");
                                    showAllStatus();
                                    
                                  }
                                  else if(cfedata.flag == 2){
                                    $http.get("/api/showShopRWmsm")
                                          .success(function(ssrw){
                                            alert(ssrw.msmbody);
                                          });
                                    //console.log("listening for change.....");
                                  }
                            }

                          });
                  }
                  else if(r_obj.flag == 100) // its self signal
                  {
                    console.log("will find....");
                    showAllStatus();
                  }
                  
                }
            });
        }
 
        var source = new EventSource('/stats');
        source.addEventListener('message', handleCallback, false);

      //end test for transfer

    }

})();
