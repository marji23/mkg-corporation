(function (){

  angular
      .module('nhApp', [ 'ngLoader' , 'ui-notification','ngRoute','ngAnimate', 'ngSanitize', 'ui.bootstrap'])
      .controller('nhController', nhController)
      .config(function(NotificationProvider) {
          NotificationProvider.setOptions({
              delay: 10000,
              startTop: 20,
              startRight: 10,
              verticalSpacing: 20,
              horizontalSpacing: 20,
              positionX: 'right',
              positionY: 'top'
          });
        });

    function nhController(Notification ,$uibModal, $log, $document, $rootScope, $location , $scope, $http, $window) {

      $scope.logout_func = logout_func;
     $scope.myinfo = myinfo;
     $scope.find_filter = find_filter;
     $scope.reset_filter = reset_filter;
     $scope.day_filter = day_filter;


     $scope.onChangeNum_perPage = onChangeNum_perPage;
      $scope.pageChanged = pageChanged;

      var whole_notice;
      $scope.notify_clicked = notify_clicked;

      $scope.closeNav = closeNav;

      var wholeitems;

      var audio = new Audio('./sounds/Maramba.mp3');

      var acdata = [];

      function init() {
        console.log("notice history initer");

        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 1)
                $window.location.href = '/index.html';
              else
              {
                console.log("aya~~")
                $http.get('/api/get_global_acdata')
                      .success(function(ggadata){
                        acdata = ggadata.acdata;
                        $scope.mycid = ggadata.mycid;
                        $scope.mygroup = ggadata.mygroup;
                        $scope.mydeeper = ggadata.mydeeper;

                        if(($scope.mydeeper == 1)&&(acdata[0].ac_allnotify <2))
                          logout_func();

                        imp_allow();

                        $http.get('/api/getAllalarm' )
                              .success(function(cbdata1){
                                $scope.all_alarm_types = cbdata1.obj;
                              });

                        loadAllnotice();

                        load_sidebar();
                        $scope.topnav = {url : "TopNav.html"};
                        showAllStatus();

                      });
              }
              
            });

      }

      init();

      function imp_allow()
      {
          var mystat = {};
          var groupstat = {};
          var finalclc = {}; 
          console.log("$scope.mygroup : " + $scope.mygroup);

          acdata.forEach(function(raudata){
                          if(raudata.uinfo[0] != undefined)
                          {
                            if(raudata.uinfo[0].u_id == $scope.mycid)
                              mystat = raudata;
                          }
                          else if(raudata.ginfo[0] != undefined)
                          {
                            if(raudata.ginfo[0].g_id == $scope.mycid)
                              mystat = raudata;
                            if(raudata.ginfo[0].g_id == $scope.mygroup)
                              groupstat = raudata
                          }
                        });
          if($scope.mygroup < 100) // not manager grouop
              finalclc = mystat;
          else
              {
                
                finalclc = mystat; // init

                var counter = 0;
                for (var key in mystat) {
                    if(counter < 23)
                    {
                      if(mystat[key] == 0)
                        finalclc[key] = groupstat[key];
                      else
                        finalclc[key] = mystat[key];

                      //console.log("final : " + finalclc[key] + " <= group : " + groupstat[key] + " + mine : " + mystat[key]);
                    }  //
                    counter++;
                }
              }
              //console.log("final : " + finalclc.ac_zhong);
            $scope.fang = finalclc;

      }

      function load_sidebar()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                    switch(rsys.result.deep)
                    {
                      case 1:
                        $scope.template = {url : "sidebar.html"};
                        $scope.top = {url : "topbar.html"};
                       break;
                      case 2:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 3:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 4:
                        $scope.template = {url : "sidebar3.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                    }

                    setStatusboxWidth();
                  });
      }

      $scope.num_per_page = 10;
      $scope.maxSize = 3;
      $scope.bigTotalItems = 10;
      $scope.bigCurrentPage = 1;

      function pageChanged() {

          console.log('Page changed to: ' + $scope.bigCurrentPage);
          console.log($scope.bigCurrentPage);
          onChangeNum_perPage();
      }

      function onChangeNum_perPage(){
        console.log("ll : " + $scope.num_per_page);

        if($scope.num_per_page == 1)
          $scope.num_per_page = wholeitems.length;

        $scope.bigTotalItems = (10*Math.ceil(wholeitems.length/$scope.num_per_page));
        var frr = [];
        for(var j = ($scope.bigCurrentPage - 1) * $scope.num_per_page; j < $scope.bigCurrentPage * $scope.num_per_page; j ++)
            {
                if(j < wholeitems.length)
                    frr.push(wholeitems[j]);
            }
            $scope.fresult = frr;
            console.log($scope.fresult);

      }

      function myinfo()
      {

        $http.get('/api/set_myinfo')
            .success(function(cbdata){
              
              console.log("_kekeke : " + cbdata[0].savepay);

                var fnum = parseInt(cbdata[0].orig_gid);
                if( fnum == 2 )
                  $window.location.href = '/server-page8(edit&detail).html';
                if( fnum > 2 )
                  $window.location.href = '/server-page15(edit&detail).html';
            });
      }

      function logout_func()
      {
        console.log("logouted");
        $http.get('/api/logout_func')
            .success(function(cbdata){
              if(cbdata.logresult == "success")
                $window.location.href = '/index.html';
            });
      }

      function loadAllnotice()
      {
        $scope.working = true;
        $scope.message = 'Loading...'

        $http.get('/api/get_rw_all_notice') // get all notice 
                        .success(function(cbdata){
                          if(cbdata.result == true)
                          {
                              $scope.working = false;

                              $scope.rw_notices = cbdata.obj;

                              console.log("rw : " + JSON.stringify($scope.rw_notices) );
                              $http.get('/api/update_all_status') //set all status with read status
                                  .success(function(cbdata2){
                                    if(cbdata2.result == true)
                                        load_notice();
                                  });
                          }
            });
      }

      function find_filter()
      {
        var status_val = parseInt($("#status").val());

        var rw_notices = $scope.rw_notices;
        var all_alarm_types = $scope.all_alarm_types;

        rw_notices = rw_notices.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

          var rw_result = [];
            if($("#nickname").val() != '')
            {
              rw_notices.forEach(function(item){
                  if( item.am_target == $("#nickname").val() )
                    rw_result.push(item);
                });
            }
            else
              rw_result = rw_notices;


            var rrw_result = [];
            if(status_val != 2)
            {
              rw_result.forEach(function(item , index){
                  if( item.am_status == status_val )
                          rrw_result.push(item);

                });
            }
            else
              rrw_result = rw_result;



            var rrrw_result = [];
            console.log("$('#fromdate').val() : " + new Date($('#fromdate').val()) );
            if($('#fromdate').val()!="")
            {  

              var from = new Date($('#fromdate').val());
              rrw_result.forEach(function(item , index){
                
                var idate = new Date( item.am_date );
                if ( from <= idate )
                  rrrw_result.push(item);
              });
            }
            else
              rrrw_result = rrw_result;


            var rrrrw_result = [];
            console.log("$('#todate').val() : " + new Date($('#todate').val()) );
            if($('#todate').val()!="")
            {  
              var to = new Date($('#todate').val());
              rrrw_result.forEach(function(item , index){

                var idate = new Date( item.am_date );
                if ( to > idate )
                  rrrrw_result.push(item);
              });
            }
            else
              rrrrw_result = rrrw_result;



            rrrrw_result.forEach(function(item , index){
                  
                  if(item.am_status == 0)
                    item['str_status'] = "Unread";
                  if(item.am_status == 1)
                    item['str_status'] = "read";

                    item['str_date'] = item.am_date.toLocaleString().split("T")[0] + " " + item.am_date.toLocaleString().split("T")[1];

                  var contents = "";
                  var titles = "";
                            for(var i=0;i<all_alarm_types.length;i++)
                              if(all_alarm_types[i].a_id == item.am_content)
                              {
                                contents = all_alarm_types[i].a_content.a_en;
                                titles = all_alarm_types[i].a_title.a_en;
                              }
                  item['str_title'] = titles;
                    item['str_contents'] = contents;

                });

            //$scope.fresult = rrw_result;

            wholeitems = rrrrw_result;
            pageChanged();


      }

      function reset_filter()
      {
        $("#nickname").val('');
        $("#status").val('0');
        $('#fromdate').val("");
        $('#todate').val("");
      }

      /* Define new prototype methods on Date object. */
      // Returns Date as a String in YYYY-MM-DD format.
      Date.prototype.toISODateString = function () {
        return this.toISOString().substr(0,10);
      };

      // Returns new Date object offset `n` days from current Date object.
      Date.prototype.toDateFromDays = function (n) {
        n = parseInt(n) || 0;
        var newDate = new Date(this.getTime());
        newDate.setDate(this.getDate() + n);
        return newDate;
      };

      function day_filter(types)
      {
        console.log(types);
        
        switch(types)
        {
          case 31:
            
            var dto = new Date();
            var dfrom  = new Date();
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            find_filter();
            break;
          case 32:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-7);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            find_filter();
            break;
          case 33:
            var dto = new Date();
            var dfrom  = new Date();
            dfrom.setHours(0,0,0,0);
            dfrom.setDate(1);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            find_filter();
            break;
          case 34:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-90);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            find_filter();
            break;
          case 35:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-180);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            find_filter();
            break;
          case 36:
            var dto = new Date();
            var dfrom = "";
            console.log( dto.toISODateString() + " : " +  dfrom );
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            find_filter();
            break;
        }
      }

      
      //start for notice on the top bar

      function checkForSounds()
      {
        $http.get('/api/getSoundsjson')
            .success(function(sdata)
            {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) || (sdata.obj.notify < $scope.len ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
            });
      }

      function load_notice()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    if(rsys.result.deep == 1){
                      console.log("dfdf");
                     $http.get('/api/get_all_notice')
                        .success(function(cbdata){
                            console.log("rusuky : " + cbdata.obj);
                            whole_notice = cbdata.obj;
                            whole_notice = whole_notice.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

                            var unreads = [];
                            whole_notice.forEach(function(item){
                              if(item.am_status == 0)
                                {
                                  unreads.push(item);
                                  console.log(JSON.stringify(item));
                                }
                            });

                            $scope.ur = unreads;
                            $scope.len = unreads.length;

                            $http.get('/api/getSoundsflag')
                                .success(function(ssflag)
                                  {
                                    if(ssflag.obj == 1 )
                                    {
                                      $http.get('/api/getSoundsjson')
                                              .success(function(sdata)
                                              {
                                                var temps = sdata.obj;
                                                temps.notify = $scope.len;

                                                $http.post('/api/setSoundsjson' , temps)
                                                .success(function(jdata)
                                                {
                                                  console.log("success to update sound json");
                                                  checkForSounds();
                                                });


                                              });
                                    }
                                    else
                                    {
                                      checkForSounds();
                                    }

                                  });
                             

                        });
                    }

                  });
      }

      function notify_clicked(iobj)
      {
        whole_notice.forEach(function(item){
          if(item == iobj)
            item.am_status = 1;
        }); 
        $http.post('/api/update_all_notice' , { obj : whole_notice } )
                .success(function(cbdata){
                  if(cbdata.result == true){

                      $http.get('/api/getAllalarm' )
                        .success(function(cbdata1){
                            var atypes = cbdata1.obj;
                            var contents = "";
                            var titles = "";
                            for(var i=0;i<atypes.length;i++)
                              if(atypes[i].a_id == iobj.am_content)
                              {
                                contents = atypes[i].a_content.a_en;
                                titles = atypes[i].a_title.a_en;
                              }

                               var mbody = "<p> 위반대상 : "+ iobj.am_target +" </p>"+
                               "<p> 위반대상 식별자 : "+ iobj.am_id +" </p>"+
                              "<p> 위반날자 : "+ iobj.am_date.toLocaleString().split("T")[0] + " " + iobj.am_date.toLocaleString().split("T")[1] +"</p>"+
                              "<p> 위반내용 : "+ contents +" </p> ";
          Notification.success({title : '<h3 style="text-align: center;"><i style=" -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: orange; color: rgba(187, 48, 23, 0.63); font-size : 42px;" class="fa fa-bell"></i></h3>' ,  message: mbody , delay: 10000});
                              
                              load_notice();
                          });
                  }
                });
      }

      //end for notice on the top bar

      //start show all status on the top bar
      function showAllStatus()
      {
        console.log("yayaya~~~hahaha");
        $http.get("/api/setAFlagZero")
                .success(function(afz){
                  if(afz.result == true)
                  {
                    $http.get("/api/getAllStatus")
                          .success(function(cbdata)
                          {

                            console.log("rat : " + JSON.stringify(cbdata));
                            $scope.as = cbdata;

                            load_notice();

                          });
                  }
                });
        
      }

      function closeNav(snd)
      {
        console.log("heyheyhey");
        var x = document.getElementById("myTopnav");
        x.className = "topnav";

        var flagf;
        if( ((3<=snd)&&(snd<=6)) || (snd == 13) || (snd == 11) )
        {
          flagf = 1;
        }
        else
        {
          flagf = 2;
        }
        $http.post("/api/setSoundsflag" , {obj : flagf})
                .success(function(cbdata)
                {
                  if((3<=snd)&&(snd<=10))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 3:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 4:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 5:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 6:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 7:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 8:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                            case 9:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 10:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page2.html';
                                });
                          
                        }
                        else if((snd == 11) || (snd == 12))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 11:
                              goParam = { cstatus : '3' , rouflag : 999};
                              break;
                            case 12:
                              goParam = { cstatus : '2' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page10.html';
                                });
                          
                        }
                        else if(snd == 1)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page7.html';
                                });
                          
                        }
                        else if(snd == 2)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page8.html';
                                });
                          
                        }

                });

      }

      function setStatusboxWidth()
      {
        console.log("wow excellent.. : "+( parseInt($(".page-container").width()) - 390 ));
        var gijuns = parseInt($(".page-container").width());
        var d_amount;
        if(gijuns<768)
          d_amount = 200;
        else
          d_amount = 400;

        var mywidth = gijuns - d_amount;

        $scope.statusbox={'width':mywidth+'px'};
      }

      $(window).resize(function(){
          
        setStatusboxWidth();

      });

      //end show all status on the top bar

      //start test for transfer

      // handles the callback from the received event
        var handleCallback = function (msg) {
            $scope.$apply(function () {
                //$scope.msg = JSON.parse(msg.data)
                var r_obj = JSON.parse(msg.data);
                console.log("receiver : " + r_obj.flag);
                
                if(r_obj.flag != 0)
                {
                  if(r_obj.flag != 100)
                  {
                    console.log("will chekc for target~");
                    $http.post("/api/CheckForEmitTarget" , { cfeparam : r_obj.shcdata }) // check if this is logined id which is correct target to listen the emit socket signal
                          .success(function(cfedata)
                          {
                            if(cfedata.result == true) // this is correct target
                            {
                                  if(cfedata.flag == 1)
                                  {
                                    console.log("will find....");
                                    showAllStatus();
                                    
                                  }
                                  else if(cfedata.flag == 2){
                                    $http.get("/api/showShopRWmsm")
                                          .success(function(ssrw){
                                            alert(ssrw.msmbody);
                                          });
                                    //console.log("listening for change.....");
                                  }
                            }

                          });
                  }
                  else if(r_obj.flag == 100) // its self signal
                  {
                    console.log("will find....");
                    showAllStatus();
                  }
                  
                }
            });
        }
 
        var source = new EventSource('/stats');
        source.addEventListener('message', handleCallback, false);

      //end test for transfer

    }

})();
