(function (){

  angular
      .module('page1App', [ 'ngLoader' , 'ui-notification','ngRoute','ngAnimate', 'ngSanitize', 'ui.bootstrap'])
      .config(function(NotificationProvider) {
          NotificationProvider.setOptions({
              delay: 10000,
              startTop: 20,
              startRight: 10,
              verticalSpacing: 20,
              horizontalSpacing: 20,
              positionX: 'right',
              positionY: 'top'
          });
        })
      .controller('table1_dlg', table1_dlg)
      .controller('table2_dlg', table2_dlg)
      .controller('page1Controller', LRFController);

    //var forshow_arr; // {shop , number of member on each shop , length of each childshop member obj}
    //var formembers_arr; // {membername , memberlevel}
    //var for_d_members_arr; // {membername , memberlevel}

    var whole_notice;

    function table1_dlg($scope, $http, $window , $rootScope , $uibModalInstance) {

      $scope.exitMsg = exitMsg;
      $scope.onChangeNum_perPage = onChangeNum_perPage;
      $scope.pageChanged = pageChanged;
      $scope.mainProcessWhenClicked = mainProcessWhenClicked;

      function init() {
        $scope.searchKeyword = "";

        $scope.myheader = $rootScope.myheader;
        $scope.th_header = $rootScope.th_header;
        $scope.searchtitle = $rootScope.searchtitle;

        $scope.num_per_page = 10;
        $scope.maxSize = 3;
        $scope.bigTotalItems = 10;
        $scope.bigCurrentPage = 1;

        mainProcessWhenClicked();

      }
      init();

      function exitMsg()
      {
        $uibModalInstance.dismiss('cancel');
      }

      function pageChanged() {

          mainProcess();
      }

      function onChangeNum_perPage(){
        
        mainProcessWhenClicked();

      }

      function mainProcessWhenClicked()
      {
        $http.post("/api/getTable1DetailCalc" , {type : $rootScope.nRvalue , keyword : $scope.searchKeyword})
            .success(function(numper){

                $scope.bigTotalItems = (10*Math.ceil(numper.wpagenum/$scope.num_per_page));

                mainProcess();
            });
        
      }

      function mainProcess()
      {
        $http.post("/api/getTable1Detail" , {type : $rootScope.nRvalue , keyword : $scope.searchKeyword 
                                  , pagenum : $scope.bigCurrentPage , pageamount : $scope.num_per_page })
              .success(function(gtd1){

                var tempgtd = gtd1.result;
                $scope.valarrs = tempgtd;

              });
      }

    }

    function table2_dlg($scope, $http, $window , $rootScope , $uibModalInstance) {

      $scope.exitMsg = exitMsg;
      $scope.onChangeNum_perPage = onChangeNum_perPage;
      $scope.pageChanged = pageChanged;
      $scope.mainProcessWhenClicked = mainProcessWhenClicked;

      function init() {

        $scope.searchKeyword = "";

        $scope.num_per_page = 10;
        $scope.maxSize = 3;
        $scope.bigTotalItems = 10;
        $scope.bigCurrentPage = 1;

        mainProcessWhenClicked();

        console.log("sdfasdfasdfasfasdf");

      }
      init();

      function exitMsg()
      {
        $uibModalInstance.dismiss('cancel');
      }

      function pageChanged() {

          mainProcess();
      }

      function onChangeNum_perPage(){
        
        mainProcessWhenClicked();

      }

      function mainProcessWhenClicked()
      {
        $http.post("/api/getTable2DetailCalc" , {maintype : $rootScope.category , subtype : $rootScope.tflag  
                                      , keyword : $scope.searchKeyword})
            .success(function(numper){

                $scope.bigTotalItems = (10*Math.ceil(numper.keitanum/$scope.num_per_page));

                mainProcess();
            });
        
      }

      function mainProcess()
      { 
        $scope.working2 = true;
        $scope.message = 'Loading...';

        $http.post("/api/getTable2Detail" , {maintype : $rootScope.category , subtype : $rootScope.tflag , 
                                  keyword : $scope.searchKeyword , pagenum : $scope.bigCurrentPage 
                                  , pageamount : $scope.num_per_page })
              .success(function(gtd1){

                $scope.working2 = false;

                var tempgkal = gtd1.result;
                $scope.rval = tempgkal;

              });
      }


    }


    function LRFController(Notification , $uibModal, $log, $document, $rootScope, $location , $scope, $http, $window) {

      $scope.logout_func = logout_func;
      $scope.showTable1 = showTable1;
      $scope.myfunc = myfunc;
      $scope.myinfo = myinfo;

      $scope.notify_clicked = notify_clicked;

      $scope.closeNav = closeNav;

      $scope.table2_sub1 = table2_sub1;
      $scope.table2_sub2 = table2_sub2;

      var acdata = [];

      var gameTexts = ["Holdem poker", // Holdem Poker
                        "Majang" , // Majang
                        "Deou Di zhu" , // Deou Di zhu
                        "Sadari" ] // Sadari

      function myfunc()
      {
        alert("me!!!");
      }
      var audio = new Audio('./sounds/Maramba.mp3');

      function init() {
        console.log("initer");

        
        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 1)
                $window.location.href = '/index.html';
              else
              {
                console.log("aya~~")
                $http.get('/api/get_global_acdata')
                      .success(function(ggadata){
                        acdata = ggadata.acdata;
                        $scope.mycid = ggadata.mycid;
                        $scope.mygroup = ggadata.mygroup;
                        $scope.mydeeper = ggadata.mydeeper;

                        imp_allow();

                        load_sidebar();
                        $scope.topnav = {url : "TopNav.html"};
                        showAllStatus();
                        //calc_childshop_num();

                        load_zhonghe();

                      });
              }

            });

      }

      init();

      function load_zhonghe()
      {
        $scope.working1 = true;
        $scope.message = 'Loading...';

        $http.post('/api/getZhonghe')
                  .success(function(gzdata){

                    $scope.childshop_num = gzdata.table1.childshop_num;
                    $scope.member_num = gzdata.table1.member_num;
                    $scope.d_member_num = gzdata.table1.d_member_num;

                    $scope.shop_profit = gzdata.table2.shop_profit;
                    $scope.directmem_profit = gzdata.table2.directmem_profit;
                    $scope.whole_profit = gzdata.table2.whole_profit;

                    var table3val = gzdata.table3;
                    console.log("before~~~: " + JSON.stringify(table3val));
                    console.log("e : " + table3val[1]);

                    for(var i=0; i<table3val.length; i++)
                    {
                      if(table3val[i] == null)
                      {
                        console.log("herear!");
                        table3val[i] = {"_id" : gameTexts[i] , "todayfeed" : 0 , 
                                        "yesterdayfeed" : 0 , "oneweekfeed" : 0 ,
                                        "twoweekfeed" : 0 ,"onemonthfeed" : 0 ,
                                        "threemonthfeed" : 0 ,"allfeed" : 0 };
                      }
                      else
                      {
                        table3val[i]._id = gameTexts[i];
                      }
                    }
                    console.log("after~~~: " + JSON.stringify(table3val));

                    $scope.table3vals = table3val;

                    $scope.working1 = false;

                  });
      }



      function imp_allow()
      {
          var mystat = {};
          var groupstat = {};
          var finalclc = {}; 
          console.log("$scope.mygroup : " + $scope.mygroup);

          acdata.forEach(function(raudata){
                          if(raudata.uinfo[0] != undefined)
                          {
                            if(raudata.uinfo[0].u_id == $scope.mycid)
                              mystat = raudata;
                          }
                          else if(raudata.ginfo[0] != undefined)
                          {
                            if(raudata.ginfo[0].g_id == $scope.mycid)
                              mystat = raudata;
                            if(raudata.ginfo[0].g_id == $scope.mygroup)
                              groupstat = raudata
                          }
                        });
          if($scope.mygroup < 100) // not manager grouop
              finalclc = mystat;
          else
              {
                
                finalclc = mystat; // init

                var counter = 0;
                for (var key in mystat) {
                    if(counter < 23)
                    {
                      if(mystat[key] == 0)
                        finalclc[key] = groupstat[key];
                      else
                        finalclc[key] = mystat[key];

                      //console.log("final : " + finalclc[key] + " <= group : " + groupstat[key] + " + mine : " + mystat[key]);
                    }  //
                    counter++;
                }
              }
              //console.log("final : " + finalclc.ac_zhong);
            $scope.fang = finalclc;

      }

      function load_sidebar()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                    switch(rsys.result.deep)
                    {
                      case 1:
                        $scope.template = {url : "sidebar.html"};
                        $scope.top = {url : "topbar.html"};
                       break;
                      case 2:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 3:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 4:
                        $scope.template = {url : "sidebar3.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                    }

                    setStatusboxWidth();
                  });
      }

      function myinfo()
      {
        console.log("hihi~");

        $http.get('/api/set_myinfo')
            .success(function(cbdata){
              
              console.log("_kekeke : " + cbdata[0].savepay);

                var fnum = parseInt(cbdata[0].orig_gid);
                if( fnum == 2 )
                  $window.location.href = '/server-page8(edit&detail).html';
                if( fnum > 2 )
                  $window.location.href = '/server-page15(edit&detail).html';
            });
      }

      function logout_func()
      {
        console.log("logouted");
        $http.get('/api/logout_func')
            .success(function(cbdata){
              if(cbdata.logresult == "success")
                $window.location.href = '/index.html';
            });
      }

// start table 2

      function table2_sub1(tflag)  // for all shop
      {
        $rootScope.category = 1;
        $rootScope.tflag = tflag;
        

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'dialogs/table2_dlg.html',
            controller: 'table2_dlg',
            windowClass: 'center-modal',
            resolve: {
             //   items: function () {
              //    return $ctrl.items;
               // }
            }
        });

        modalInstance.result.then(function (selectedItem) {}, function () {
            //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
        });
      }

      function table2_sub2(tflag) // for direct mem
      {
        $rootScope.category = 2;
        $rootScope.tflag = tflag;
        

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'dialogs/table2_dlg.html',
            controller: 'table2_dlg',
            windowClass: 'center-modal',
            resolve: {
             //   items: function () {
              //    return $ctrl.items;
               // }
            }
        });

        modalInstance.result.then(function (selectedItem) {}, function () {
            //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
        });
      }

//end table 2

      function showTable1(nRvalue)
      {
        console.log("nrvalue : " + nRvalue);

        switch (nRvalue){
          case 1:
            $rootScope.myheader = "소속대리점 개수";
            $rootScope.th_header = {"header1" : "대리점명" , "header2" : "산하 전체회원수" , "header3" : "직속회원수"};
            $rootScope.searchtitle = "대리점명";
            $rootScope.nRvalue = nRvalue;
            break;
          case 2:
            $rootScope.myheader = "산하대리점들에 소속된 회원수";
            $rootScope.th_header = {"header1" : "회원명" , "header2" : "수준"};
            $rootScope.searchtitle = "회원명";
            $rootScope.nRvalue = nRvalue;
            break;
          case 3:
            $rootScope.myheader = "직속회원수";
            $rootScope.th_header = {"header1" : "회원명" , "header2" : "수준"};
            $rootScope.searchtitle = "회원명";
            $rootScope.nRvalue = nRvalue;
            break;
          default:
            break;

        }

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'dialogs/table1_dlg.html',
            controller: 'table1_dlg',
            windowClass: 'center-modal',
            resolve: {
             //   items: function () {
              //    return $ctrl.items;
               // }
            }
        });

        modalInstance.result.then(function (selectedItem) {}, function () {
            //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
        });
      }
      
      //start for notice on the top bar

      function checkForSounds(typesound)
      {
        $http.get('/api/getSoundsjson')
            .success(function(sdata)
            {
              if(typesound == 1)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) || (sdata.obj.notify < $scope.len ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
              else if(typesound == 2)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
            });
      }

      function load_notice()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    if(rsys.result.deep == 1){
                      console.log("dfdf");
                     $http.get('/api/get_all_notice')
                        .success(function(cbdata){
                            console.log("rusuky : " + cbdata.obj);
                            whole_notice = cbdata.obj;
                            whole_notice = whole_notice.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

                            var unreads = [];
                            whole_notice.forEach(function(item){
                              if(item.am_status == 0)
                                {
                                  unreads.push(item);
                                  console.log(JSON.stringify(item));
                                }
                            });

                            $scope.ur = unreads;
                            $scope.len = unreads.length;

                            checkForSounds(1);
                             

                        });
                    }
                    else
                      checkForSounds(2);

                  });
      }

      function notify_clicked(iobj)
      {
        whole_notice.forEach(function(item){
          if(item == iobj)
            item.am_status = 1;
        }); 
        $http.post('/api/update_all_notice' , { obj : whole_notice } )
                .success(function(cbdata){
                  if(cbdata.result == true){

                      $http.get('/api/getAllalarm' )
                        .success(function(cbdata1){
                            var atypes = cbdata1.obj;
                            var contents = "";
                            var titles = "";
                            for(var i=0;i<atypes.length;i++)
                              if(atypes[i].a_id == iobj.am_content)
                              {
                                contents = atypes[i].a_content.a_en;
                                titles = atypes[i].a_title.a_en;
                              }

                               var mbody = "<p> 위반대상 : "+ iobj.am_target +" </p>"+
                               "<p> 위반대상 식별자 : "+ iobj.am_id +" </p>"+
                              "<p> 위반날자 : "+ iobj.am_date.toLocaleString().split("T")[0] + " " + iobj.am_date.toLocaleString().split("T")[1] +"</p>"+
                              "<p> 위반내용 : "+ contents +" </p> ";
          Notification.success({title : '<h3 style="text-align: center;"><i style=" -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: orange; color: rgba(187, 48, 23, 0.63); font-size : 42px;" class="fa fa-bell"></i></h3>' ,  message: mbody , delay: 10000});
                              
                              load_notice();
                          });
                  }
                });
      }

      //end for notice on the top bar

      //start show all status on the top bar
      function showAllStatus()
      {
        console.log("yayaya~~~hahaha");
        $http.get("/api/setAFlagZero")
                .success(function(afz){
                  if(afz.result == true)
                  {
                    $http.get("/api/getAllStatus")
                          .success(function(cbdata)
                          {

                            console.log("rat : " + JSON.stringify(cbdata));
                            $scope.as = cbdata;

                            load_notice();

                          });
                  }
                });
        
      }

      function closeNav(snd)
      {
        console.log("heyheyhey");
        var x = document.getElementById("myTopnav");
        x.className = "topnav";

        var flagf;
        if( ((3<=snd)&&(snd<=6)) || (snd == 13) || (snd == 11) )
        {
          flagf = 1;
        }
        else
        {
          flagf = 2;
        }
        $http.post("/api/setSoundsflag" , {obj : flagf})
                .success(function(cbdata)
                {
                  if((3<=snd)&&(snd<=10))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 3:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 4:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 5:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 6:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 7:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 8:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                            case 9:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 10:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page2.html';
                                });
                          
                        }
                        else if((snd == 11) || (snd == 12))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 11:
                              goParam = { cstatus : '3' , rouflag : 999};
                              break;
                            case 12:
                              goParam = { cstatus : '2' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page10.html';
                                });
                          
                        }
                        else if(snd == 1)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page7.html';
                                });
                          
                        }
                        else if(snd == 2)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page8.html';
                                });
                          
                        }

                });

      }

      function setStatusboxWidth()
      {
        console.log("wow excellent.. : "+( parseInt($(".page-container").width()) - 390 ));
        var gijuns = parseInt($(".page-container").width());
        var d_amount;
        if(gijuns<768)
          d_amount = 200;
        else
          d_amount = 400;

        var mywidth = gijuns - d_amount;

        $scope.statusbox={'width':mywidth+'px'};
      }

      $(window).resize(function(){
          
        setStatusboxWidth();

      });

      //end show all status on the top bar

      //start test for transfer

      // handles the callback from the received event
        var handleCallback = function (msg) {
            $scope.$apply(function () {
                //$scope.msg = JSON.parse(msg.data)
                var r_obj = JSON.parse(msg.data);
                console.log("receiver : " + r_obj.flag);
                
                if(r_obj.flag != 0)
                {
                  if(r_obj.flag != 100)
                  {
                    console.log("will chekc for target~");
                    $http.post("/api/CheckForEmitTarget" , { cfeparam : r_obj.shcdata }) // check if this is logined id which is correct target to listen the emit socket signal
                          .success(function(cfedata)
                          {
                            if(cfedata.result == true) // this is correct target
                            {
                                  if(cfedata.flag == 1)
                                  {
                                    console.log("will find....");
                                    showAllStatus();
                                    
                                  }
                                  else if(cfedata.flag == 2){
                                    $http.get("/api/showShopRWmsm")
                                          .success(function(ssrw){
                                            alert(ssrw.msmbody);
                                          });
                                    //console.log("listening for change.....");
                                  }
                            }

                          });
                  }
                  else if(r_obj.flag == 100) // its self signal
                  {
                    console.log("will find....");
                    showAllStatus();
                  }
                  
                }
            });
        }
 
        var source = new EventSource('/stats');
        source.addEventListener('message', handleCallback, false);

      //end test for transfer

    }

})();
