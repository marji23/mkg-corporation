var someVar = (function () {
  var globalval = 2; // Local scope, protected from global namespace

  return {
    setVal: function(obj){
      globalval = obj;
    }
  , getVal: function(){
      return globalval;
    }
  };
}());