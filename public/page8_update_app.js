(function (){

  angular
      .module('page8_updateApp', ['ui-notification','ngRoute','ngAnimate', 'ngSanitize', 'ui.bootstrap' , 'ngLoader'])
      .controller('page8_updateController', Page8_updateController)
      .controller('UpdateModal', UpdateModal)
      .controller('NewModal', NewModal)
      .config(function(NotificationProvider) {
          NotificationProvider.setOptions({
              delay: 10000,
              startTop: 20,
              startRight: 10,
              verticalSpacing: 20,
              horizontalSpacing: 20,
              positionX: 'right',
              positionY: 'top'
          });
        });


    function UpdateModal($scope, $http, $window , $rootScope , $uibModalInstance) {

      $scope.updateDM = updateDM;
      $scope.exitMsg = exitMsg;



      function init() {

        $scope.v = $rootScope.sel_dmobj;

      }
      init();

      function exitMsg()
      {
        $uibModalInstance.dismiss('cancel');
      }

      function CheckIsValidDomain(domain) { 
          var re = new RegExp(/^((?:(?:(?:\w[\.\-\+]?)*)\w)+)((?:(?:(?:\w[\.\-\+]?){0,62})\w)+)\.(\w{2,6})$/); 
          if(domain.match(re))
            return true;
          else
          {
            alert("도메인형식에 맞게 입력하십시오!");
            return false;
          }
          return domain.match(re);
      } 

      function ValidateIPaddress(inputText)
      {
         var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
         if(inputText.match(ipformat))
         {
           return true;
         }
         else
         {
           alert("IP형식에 맞게 입력하십시오.!");
          return false;
         }
      }

      function updateDM(val)
      {
        if( val != undefined )
        {
          console.log("val : " + $('#svertype').val());
          if( (val.dm_domain_name != undefined) && (val.dm_ipaddress != undefined) && ($('#svertype').val() != '? string: ?') )
          {
            if((ValidateIPaddress(val.dm_ipaddress) == true)&&(CheckIsValidDomain(val.dm_domain_name) == true))
            {
              console.log("val : " + val.dm_domain_name);
              alert("최종 보존을 하시려면 보존단추를 눌러주십시오.");
              $uibModalInstance.dismiss('cancel');
            }
          }
          else
            alert("도메인 정보를 구체적으로 입력하십시오.");
        }
        else
          alert("도메인 정보를 구체적으로 입력하십시오.");

      }

    }

    function NewModal($scope, $http, $window , $rootScope , $uibModalInstance) {

      $scope.newDM = newDM;
      $scope.exitMsg = exitMsg;



      function init() {


      }
      init();

      function exitMsg()
      {
        $uibModalInstance.dismiss('cancel');
      }

      function CheckIsValidDomain(domain) { 
          var re = new RegExp(/^((?:(?:(?:\w[\.\-\+]?)*)\w)+)((?:(?:(?:\w[\.\-\+]?){0,62})\w)+)\.(\w{2,6})$/); 
          if(domain.match(re))
            return true;
          else
          {
            alert("도메인형식에 맞게 입력하십시오!");
            return false;
          }
          return domain.match(re);
      } 

      function ValidateIPaddress(inputText)
      {
         var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
         if(inputText.match(ipformat))
         {
           return true;
         }
         else
         {
           alert("IP형식에 맞게 입력하십시오.!");
          return false;
         }
      }

      function newDM(val)
      {
        console.log("val : " + JSON.stringify(val));
        if( val != undefined )
        {
          if( (val.dm_domain_name != undefined) && (val.dm_ipaddress != undefined) && (val.dm_service_type != undefined) )
          {
            if((ValidateIPaddress(val.dm_ipaddress) == true)&&(CheckIsValidDomain(val.dm_domain_name) == true))
            {
              alert("최종 보존을 하시려면 보존단추를 눌러주십시오.");
              $rootScope.transferer = val;
              $rootScope.$emit("updateShow", "val");
              $uibModalInstance.dismiss('cancel');
            }
          }
          else
            alert("도메인 정보를 구체적으로 입력하십시오.");
        }
        else
          alert("도메인 정보를 구체적으로 입력하십시오.");
      }

    }



    function Page8_updateController(Notification ,$uibModal, $log, $document, $rootScope, $location , $scope, $http, $window) {

      $scope.logout_func = logout_func;


      $scope.uppershops = uppershops;
      //$scope.resetAll = resetAll;
      var selected_shop;
      var selected_shop_dmlist;
      $scope.updateShop = updateShop;
      $scope.editModal = editModal;
      $scope.deleteModal = deleteModal;
      $scope.closeUpdate = closeUpdate;
      $scope.setOurVal_To_MemVal = setOurVal_To_MemVal;
      $scope.myinfo = myinfo;
      $scope.newDomain = newDomain;

      $scope.pageChanged = pageChanged;
      $scope.onChangeNum_perPage = onChangeNum_perPage;
      $scope.sp_detail_filterWhenClick = sp_detail_filterWhenClick;

      var whole_notice;
      $scope.notify_clicked = notify_clicked;

      $scope.closeNav = closeNav;

//-for uppercode makes awesome
      var s_lvl_name = ["본사" , "부본사" , "총판" , "대리"];
//end - for uppercode makes awesome
  
      var audio = new Audio('./sounds/Maramba.mp3');

      var acdata = [];
      
      function init() {
        console.log("page8update initer");

        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 1)
                $window.location.href = '/index.html';
              else
              {
                console.log("aya~~")
                $http.get('/api/get_global_acdata')
                      .success(function(ggadata){
                        acdata = ggadata.acdata;
                        $scope.mycid = ggadata.mycid;
                        $scope.mygroup = ggadata.mygroup;
                        $scope.mydeeper = ggadata.mydeeper;

                        if(($scope.mydeeper == 1)&&(acdata[0].ac_shop <3))
                          logout_func();

                        imp_allow();

                        load_sidebar();
                        $scope.topnav = {url : "TopNav.html"};
                        showAllStatus();

                        loadShopList();
                        

                      });
              }

            });

      }

      init();

      $scope.num_per_page = 10;
      $scope.maxSize = 3;
      $scope.bigTotalItems = 10;
      $scope.bigCurrentPage = 1;

      function pageChanged() {
          sp_detail_filter();
      }

      function onChangeNum_perPage(){
          sp_detail_filterWhenClick();
      }

      $scope.num_per_page1 = 10;
      $scope.maxSize1 = 3;
      $scope.bigTotalItems1 = 10;
      $scope.bigCurrentPage1 = 1;

      function pageChanged1() {
          s_detail_filter();
      }

      function onChangeNum_perPage1(){
          s_detail_filterWhenClick();
      }

      function imp_allow()
      {
          var mystat = {};
          var groupstat = {};
          var finalclc = {}; 
          console.log("$scope.mygroup : " + $scope.mygroup);

          acdata.forEach(function(raudata){
                          if(raudata.uinfo[0] != undefined)
                          {
                            if(raudata.uinfo[0].u_id == $scope.mycid)
                              mystat = raudata;
                          }
                          else if(raudata.ginfo[0] != undefined)
                          {
                            if(raudata.ginfo[0].g_id == $scope.mycid)
                              mystat = raudata;
                            if(raudata.ginfo[0].g_id == $scope.mygroup)
                              groupstat = raudata
                          }
                        });
          if($scope.mygroup < 100) // not manager grouop
              finalclc = mystat;
          else
              {
                
                finalclc = mystat; // init

                var counter = 0;
                for (var key in mystat) {
                    if(counter < 23)
                    {
                      if(mystat[key] == 0)
                        finalclc[key] = groupstat[key];
                      else
                        finalclc[key] = mystat[key];

                      //console.log("final : " + finalclc[key] + " <= group : " + groupstat[key] + " + mine : " + mystat[key]);
                    }  //
                    counter++;
                }
              }
              //console.log("final : " + finalclc.ac_zhong);
            $scope.fang = finalclc;

      }

      function load_sidebar()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                    switch(rsys.result.deep)
                    {
                      case 1:
                        $scope.template = {url : "sidebar.html"};
                        $scope.top = {url : "topbar.html"};
                       break;
                      case 2:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 3:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 4:
                        $scope.template = {url : "sidebar3.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                    }

                    setStatusboxWidth();
                  });
      }

      function myinfo()
      {

        $http.get('/api/set_myinfo')
            .success(function(cbdata){
              
              console.log("_kekeke : " + cbdata[0].savepay);

                var fnum = parseInt(cbdata[0].orig_gid);
                if( fnum == 2 )
                  $window.location.href = '/server-page8(edit&detail).html';
                if( fnum > 2 )
                  $window.location.href = '/server-page15(edit&detail).html';
            });
      }

      $rootScope.$on("updateShow", function(val){
           
          console.log("aa : " + JSON.stringify($rootScope.transferer) );
          $scope.dlist.push($rootScope.transferer);

         });



      

      function logout_func()
      {
        console.log("logouted");
        $http.get('/api/logout_func')
            .success(function(cbdata){
              if(cbdata.logresult == "success")
                $window.location.href = '/index.html';
            });
      }
      //start shop table

      function get_other_detail(ff_obj) // for money , shop earned , visitor earned
      {
        console.log("getother : " + ff_obj.length);
        $http.post('/api/get_other_detail' , ff_obj)
            .success(function(cbdata){

              console.log(' yeah , great!!!  :  ' + cbdata.length);
              
              $scope.working1 = false;
              $scope.detailers = cbdata;
            });
      }

      function getProfits(f_obj)
      {
        $http.post('/api/startForVisitor_profit', f_obj)
              .success(function(cbdata){
                get_other_detail( cbdata.obj );
              });
      }

      function s_detail_filterWhenClick()
      {
        var obj = {};

        $scope.working1 = true;
        $scope.message1 = 'Loading...'

        //check part
        var checkflag = {};
          checkflag['opt'] = 0; // all members
        checkflag['targetid'] = $scope.v.u_id;
        
        console.log("for matched : " + JSON.stringify(obj));

        $http.post('/api/getMatchedMemData' , {"main" : obj , "other" : checkflag})
              .success(function(gmmdata){
                $scope.bigTotalItems1 = (10*Math.ceil(gmmdata.matched_len/$scope.num_per_page1));   
                
                
                 //pagination
                var pageval = { "currentpage" : parseInt($scope.bigCurrentPage1) , "numperpage" : parseInt($scope.num_per_page1) };

                $http.post('/api/find_member_result_by_detail' , {"main" : obj , "other" : checkflag , "pv" : pageval})
                      .success(function(cbdata){

                        console.log("arrived!!! : " + cbdata.length);
                        getProfits(cbdata);

                      });

              });
      }

      function s_detail_filter()
      {
         var obj = {};


        $scope.working1 = true;
        $scope.message1 = 'Loading...'

        //check part
        var checkflag = {};
          checkflag['opt'] = 0; // all members
        checkflag['targetid'] = $scope.v.u_id;

        //pagination
        var pageval = { "currentpage" : parseInt($scope.bigCurrentPage1) , "numperpage" : parseInt($scope.num_per_page1) };

        console.log("for matched : " + JSON.stringify(obj));

        $http.post('/api/find_member_result_by_detail' , {"main" : obj , "other" : checkflag , "pv" : pageval})
            .success(function(cbdata){

              console.log("arrived!!! : " + cbdata.length);
              getProfits(cbdata);

            });
      }

      //end shop table

      // start sp table
      function sp_detail_filterWhenClick()
      {
        obj = {};

          obj['sp_usertype'] = "2";
          obj['sp_myid'] = $scope.v.u_id;
          obj['rmyprofile'] = "ha";

          console.log("spdetailers ~: " + $scope.v.u_id);

        $http.post('/api/find_Save_pay_num' , obj)
            .success(function(cbdata){
              console.log("ca ca ca : " + cbdata.totalamount);
              $scope.bigTotalItems = (10*Math.ceil(cbdata.totalamount/$scope.num_per_page)); 

              sp_detail_filter();
            });
      }

      function sp_detail_filter()
      {
        
        obj = {};

        obj['sp_usertype'] = "2";
        obj['sp_myid'] = $scope.v.u_id;
        obj['rmyprofile'] = "ha";

        obj['currentpage'] = parseInt($scope.bigCurrentPage);
        obj['numperpage'] = parseInt($scope.num_per_page);

        console.log("spdetailers ~: " + JSON.stringify(obj));

        $scope.working = true;
        $scope.message = 'Loading...'
        
        $http.post('/api/find_Save_pay_result' , obj)
            .success(function(cbdata){

              //console.log("ca ca ka : " + JSON.stringify(cbdata));

              $scope.spdetailers = cbdata;
              $scope.working = false;

            });

      }
      // end sp table



      function uppershops(upp)
      {
        console.log("scope's globallist : "+ $scope.temp_gglist.length );
        var tglobal_list = $scope.myglist;
        
        tglobal_list.forEach(function(item){
          if(item.u_id == upp)
          {
            console.log("found : " + item.u_deep);
            var c_deep = item.u_deep;
            var shoptype = []; 
            console.log(c_deep + '/' + selected_shop.u_deep); 
            for(var i=c_deep; i< selected_shop.u_deep; i++ )
                shoptype.push( { val :(i+1) , txt : s_lvl_name[i] } );

            $scope.shoptypes = shoptype;

           // $('#u_uppercode').val( $('#u_uppercode option:contains('+ get_su_text(selected_shop.u_uppercode) +')').val() );
              

          }
        });

        console.log("after : " + $scope.shoptypes.length);
        $('#u_deep').val(selected_shop.u_deep);

        //$('#u_deep').val( selected_shop.u_deep );

      }

      function init_uppershops(nval , upp)
      {
        var tglobal_list = $scope.myglist;
        
        tglobal_list.forEach(function(item){
          if(item.u_id == upp)
          {
            console.log("hhh : "+item.u_nickname );
            console.log("found : " + item.u_deep);
            var c_deep = item.u_deep;
            var shoptype = []; 
            console.log(c_deep + '/' + nval); 
            for(var i=c_deep; i< nval; i++ )
                shoptype.push( { val :(i+1) , txt : s_lvl_name[i] } );

            $scope.shoptypes = shoptype;

          }
        });

      }

      function init_upperlists(nval)
      {
        //console.log("globallist : "+JSON.stringify(global_list));
        //console.log("nval : " + nval);
        var tglobal_list = $scope.myglist;

        var templist = [];
                  tglobal_list.forEach(function(titem){
                      console.log(selected_shop.u_deep);
                      if(titem.u_deep < nval)
                      {
                        templist.push(titem);
                      } 
                  });
            $scope.shoplists = templist;
      }


// - start make awesome tree structured upper shop list part

      function eachtext(nums , txt , sindex)
      {
        nums = 4*nums;
        var prefix = "|";
        for(i=0;i<nums;i++)
          prefix += "-";
        var suffix = " (" + s_lvl_name[sindex] + ") ";
        return prefix + ">" + txt + suffix;
      }

      function make_items_title(typer , maxlvl , global_list)
      {
        console.log("txt_res : " + s_lvl_name.length);
        var inc = s_lvl_name.length - maxlvl -1;
        global_list.forEach(function(item){

          item['su_text'] = eachtext( item['su_lvlnum'] , item.u_nickname , item.u_deep-1 );
          //item['su_text'] = eachtext( item['su_lvlnum'] , item.u_nickname , (item['su_lvlnum'] + inc) );
          console.log('su : ' + item.su_text);
        });

        $http.get('/api/get_sess_info')
            .success(function(cbdata){
                  
                  var c_deep = cbdata.result;
                  var shoptype = [];  
                  for(var i=c_deep; i< s_lvl_name.length; i++ )
                    shoptype.push( { val :(i+1) , txt : s_lvl_name[i] } );

                  $scope.shoptypes = shoptype;

                  $scope.shoplists = global_list;

                  if(typer == 1)
                  {
                    init_datas1();
                  }
                  else if(typer == 2)
                  {
                    init_datas();
                    console.log("yogi nun 2");
                  }
            });

        $scope.myglist = global_list;

      }

      function make_Structured_menutitle(typer , lvl , starter , arr , global_list , lvl_gijun )
      {
        $scope.cout++;
        
        if( (arr.length) == $scope.cout )
        {
            console.log("really finished~~" + lvl_gijun);
            $scope.temp_gglist = global_list;

            make_items_title(typer , lvl_gijun , global_list);
        }

        lvl++;

        arr.forEach(function(item){
          //console.log("item : " + item.u_uppercode + "starter" + starter.u_uppercode);
            if(item.u_uppercode == starter.u_id)
            {
              item['su_lvlnum'] = lvl;
              //console.log("l + :"+lvl + " - " + lvl_gijun);
              global_list.push(item);

              if(lvl_gijun < lvl)
                lvl_gijun = lvl;
              make_Structured_menutitle(typer , lvl , item , arr , global_list , lvl_gijun);

            }
            else{

                
            }
        });
      }

      function get_Structured_shoplist(typer , sflag , arr)
      {
        var start_v;
        var global_list = [];

        if(sflag == 0)
        {
          start_v = arr[0];
          arr[0]['su_lvlnum'] = 0;
          global_list.push(arr[0]);
        }
        else if(sflag == 1)
        {
          arr.forEach(function(item){
            if(item.u_uppercode == "1")
            {
              start_v = item;
              item['su_lvlnum'] = 0;
              global_list.push(item);
            }
          });
        }

        console.log("startv : " + start_v.u_nickname);

        $scope.cout = 0;
        var lvl_gijun = 0;
        
        make_Structured_menutitle(typer , 0 , start_v , arr , global_list , lvl_gijun);

      }

// - end make awesome tree structured upper shop list part

      function get_su_text(hinter)
      { 
        var tglobal_list = $scope.myglist;
        console.log("hari : " + hinter)

          for(var i=0;i<tglobal_list.length;i++)
          {
            console.log("ya+ :" + tglobal_list[i].u_nickname);
          if(tglobal_list[i].u_nickname == hinter)
            {
              console.log("huk : " + tglobal_list[i].su_text);
              return tglobal_list[i].su_text;
            }
          }  
      }

      function init_datas()
      {
        $http.get('/api/get_selected_shop')
            .success(function(cbdata){
              selected_shop = cbdata.shopobj;
              selected_shop_dmlist = cbdata.dmobj;

              selected_shop_dmlist.forEach(function(item) {
                  if(item.dm_service_type == "2")
                    item.dm_service_type = "self";
                  if(item.dm_service_type == "3")
                    item.dm_service_type = "market";
              });


              //alert("selected_shop : " + selected_shop.u_uppercode);
              //console.log("selected_shop :  " + JSON.stringify(selected_shop));
              if(selected_shop.fflag == 1)
                loadAllShoplist1();
              console.log("selected_shop.fflag : " + selected_shop.fflag);

              $scope.v = selected_shop;
              $scope.cpw = selected_shop.u_pw;

              sp_detail_filterWhenClick();
              s_detail_filterWhenClick();
              

              init_uppershops(selected_shop.u_deep , selected_shop.tempu_uppercode);

              init_upperlists(selected_shop.u_deep);

              console.log("yogiya~ : " + selected_shop.tempu_uppercode);
              $('#u_uppercode').val( $('#u_uppercode option:contains('+ get_su_text(selected_shop.u_uppercode) +')').val() );
              $('#u_deep').val( selected_shop.u_deep );
              $scope.dlist = selected_shop_dmlist;

              $scope.analised = selected_shop ;


            });
      }

      function init_datas1()
      {
        $http.get('/api/get_selected_shop')
            .success(function(cbdata){
              selected_shop = cbdata.shopobj;
              selected_shop_dmlist = cbdata.dmobj;

              selected_shop_dmlist.forEach(function(item) {
                  if(item.dm_service_type == "2")
                    item.dm_service_type = "self";
                  if(item.dm_service_type == "3")
                    item.dm_service_type = "market";
              });

              console.log("rampard~ : " + selected_shop.u_uppercode);
              //alert("selected_shop : " + selected_shop.u_uppercode);
              //console.log("selected_shop :  " + JSON.stringify(selected_shop));

              $scope.v = selected_shop;
              $scope.cpw = selected_shop.u_pw;

              sp_detail_filterWhenClick();
              s_detail_filterWhenClick();

              //$('#u_uppercode').val('oo')

              init_uppershops(selected_shop.u_deep , selected_shop.u_uppercode );

              init_upperlists(selected_shop.u_deep);

              console.log("uppercoder : " + selected_shop.u_uppercode);
              $('#u_uppercode').val( selected_shop.u_uppercode );
              $('#u_deep').val( selected_shop.u_deep );

              $scope.dlist = selected_shop_dmlist;

              $scope.analised = selected_shop ;

              
            });
      }

      function loadAllShoplist1()
      {
          //console.log("loadshoplist");
          $http.post('/api/getRealAllChildshop')
            .success(function(cbdata){
              console.log('rara : '+cbdata);
              //$scope.shoplists = cbdata;
              get_Structured_shoplist(1, 1 , cbdata);
              
            });
      }

      function loadAllShoplist()
      {
          //console.log("loadshoplist");
          $http.post('/api/getRealAllChildshop')
            .success(function(cbdata){
              console.log('rara : '+cbdata);
              //$scope.shoplists = cbdata;
              get_Structured_shoplist(2 , 1 , cbdata);
              

            });
      }

      function loadShopList()
      {
        var forshopcase = [];
        $http.get('/api/getDocofLogined')
            .success(function(cbdata1){
              if(cbdata1[0].u_groupid == '2') {
                console.log("shop");

                $http.post('/api/getRealChildshop')
                    .success(function(cbdata){

                      //forshopcase.push(cbdata1[0]);
                      cbdata.obj.splice(0, 0, cbdata1[0]);
                      //cbdata.obj.push(cbdata1[0]);
                      //console.log("mememememe :     "+ cbdata1[0].u_nickname);
                      forshopcase = cbdata.obj;
                      //console.log("come : "+forshopcase.length);

                      //$scope.shoplists = forshopcase;
                      console.log("yogini!");
                      get_Structured_shoplist(2 , 0 , forshopcase);

                    });

              }
              else{
                console.log("manager");
                loadAllShoplist();
              }
            });
      }

     function ValidateIPaddress(inputText)
      {
         var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
         if(inputText.match(ipformat))
         {
           return true;
         }
         else
         {
           alert("IP형식에 맞게 입력하십시오.!");
          return false;
         }
      }

      function updateShop(v , cpw , dlist)
      {

        $scope.isdisable = 1;
        
                            console.log("result dlist: " + dlist );
                            console.log("juyu : " + $('#u_deep').val());

        if( ( (v.u_id.length >= 2) && (v.u_nickname.length >= 2) && (v.u_pw.length >= 2) && (cpw.length >= 2) && (v.u_email.length != undefined) && (v.u_email.length >=2) ) && ( (v.u_uppercode == '1') || ($('#u_uppercode').val().length >= 2) ) )
        {
            if(v.u_uppercode != '1')
            {
              v['u_uppercode'] = $('#u_uppercode').val();
              v['u_deep'] = parseInt($('#u_deep').val());
            }
            else if(v.u_uppercode == '1')
            {
              v['u_uppercode'] = '1';
              v['u_deep'] = 1;
            }

            if(v.u_pw == cpw){
              v['u_groupid'] = 2;
              v['u_flag'] = 'a';
              v['u_logcount'] = 0;
              v['u_registerdate'] = new Date();
              v['u_ticheng1'] = parseFloat(v['u_ticheng1']);
              v['u_ticheng2'] = parseFloat(v['u_ticheng2']);
              v['fflag'] = 0;

                 

                var domain_arr = $scope.dlist;
                var svtype = "";

                domain_arr.forEach(function(item) {

                    item.dm_domain_name = item.dm_domain_name.replace(/\s/g, '');
                    item.dm_ipaddress = item.dm_ipaddress.replace(/\s/g, '');

                        
                    item.dm_service_houseid = v.u_id;         
                   
                });
                  $http.post('/api/checkInDlist' , { dobj : domain_arr })
                                .success(function(cbdata1){
                                    if(cbdata1.result == true)
                                    {
                                        var passflg = 0;
                                        if(v.u_loginip != undefined)
                                        {
                                          if(v.u_loginip.length > 0)
                                          {
                                            if(ValidateIPaddress(v.u_loginip) == false)
                                              passflg = 1;
                                          }
                                        }
                                        else
                                        {
                                          v['u_loginip'] = "";
                                        }

                                        if(passflg == 0)
                                        {
                                              domain_arr.forEach(function(item) {
                                                      if(item.dm_service_type == "self" )
                                                              svtype = "2";
                                                      if(item.dm_service_type == "market" )
                                                              svtype = "3";

                                                  item.dm_service_type = svtype;
                                              });

                                            console.log("domain_arr : " + JSON.stringify(domain_arr) );
                                              
                                          var up_obj = { shopinfo : v , domain_handle : domain_arr };

                                              $http.post('/api/update_shop' , up_obj)
                                              .success(function(cbdata){

                                                if(cbdata.result == true){
                                                  
                                                }

                                              });

                                              console.log(v);
                                              alert("Success to Update!!!");
                                              $window.location.href = '/server-page8.html';
                                              
                                              $scope.isdisable = 0;
                                        }
                                    }
                                    else
                                    {
                                      alert("중복되는 도메인 이 존재하므로 다른 도메인을 리용합십시오.");
                                      $scope.isdisable = 0;
                                    }
                                });

            }
            else
            {
              alert("input same password!!!");
              $scope.isdisable = 0;
            }
          }
          else
          {
            alert("you must input some fields!!!");
            $scope.isdisable = 0;
          }
        }

        function editModal(sel_dmobj)
        {
          console.log("yayaya~ : " + sel_dmobj);
          $rootScope.sel_dmobj = sel_dmobj;

          var modalInstance = $uibModal.open({
              animation: true,
              templateUrl: 'dialogs/dm_update.html',
              controller: 'UpdateModal',
              windowClass: 'center-modal',
              resolve: {
               //   items: function () {
                //    return $ctrl.items;
                 // }
              }
          });

          modalInstance.result.then(function (selectedItem) {}, function () {
              //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
          });
        }

        function deleteModal(sel_dmobj)
        {
          selected_shop_dmlist.forEach(function(item, index, object) {
                if (item.dm_domain_name === sel_dmobj.dm_domain_name) {
                  object.splice(index, 1);
                }
              });
          $scope.dlist = selected_shop_dmlist;

        }

        function closeUpdate()
        {
          location.reload();
        }

        function setOurVal_To_MemVal(typeflag)
        {
          $http.post('/api/setOurVal_To_MemVal')
                                .success(function(cbdata){

                                  if(typeflag == 1)
                                    $window.location.href = "/server-page2.html";

                                  if(typeflag == 2)
                                    $window.location.href = "/server-page7.html";

                                });
        }

        function newDomain()
        {

          var modalInstance = $uibModal.open({
              animation: true,
              templateUrl: 'dialogs/dm_new.html',
              controller: 'NewModal',
              windowClass: 'center-modal',
              resolve: {
               //   items: function () {
                //    return $ctrl.items;
                 // }
              }
          });

          modalInstance.result.then(function (selectedItem) {}, function () {
              //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
          });
        }


        //start for notice on the top bar

      function checkForSounds(typesound)
      {
        $http.get('/api/getSoundsjson')
            .success(function(sdata)
            {
              if(typesound == 1)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) || (sdata.obj.notify < $scope.len ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
              else if(typesound == 2)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
            });
      }

      function load_notice()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    if(rsys.result.deep == 1){
                      console.log("dfdf");
                     $http.get('/api/get_all_notice')
                        .success(function(cbdata){
                            console.log("rusuky : " + cbdata.obj);
                            whole_notice = cbdata.obj;
                            whole_notice = whole_notice.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

                            var unreads = [];
                            whole_notice.forEach(function(item){
                              if(item.am_status == 0)
                                {
                                  unreads.push(item);
                                  console.log(JSON.stringify(item));
                                }
                            });

                            $scope.ur = unreads;
                            $scope.len = unreads.length;

                            checkForSounds(1);
                             

                        });
                    }
                    else
                      checkForSounds(2);

                  });
      }

      function notify_clicked(iobj)
      {
        whole_notice.forEach(function(item){
          if(item == iobj)
            item.am_status = 1;
        }); 
        $http.post('/api/update_all_notice' , { obj : whole_notice } )
                .success(function(cbdata){
                  if(cbdata.result == true){

                      $http.get('/api/getAllalarm' )
                        .success(function(cbdata1){
                            var atypes = cbdata1.obj;
                            var contents = "";
                            var titles = "";
                            for(var i=0;i<atypes.length;i++)
                              if(atypes[i].a_id == iobj.am_content)
                              {
                                contents = atypes[i].a_content.a_en;
                                titles = atypes[i].a_title.a_en;
                              }

                               var mbody = "<p> 위반대상 : "+ iobj.am_target +" </p>"+
                               "<p> 위반대상 식별자 : "+ iobj.am_id +" </p>"+
                              "<p> 위반날자 : "+ iobj.am_date.toLocaleString().split("T")[0] + " " + iobj.am_date.toLocaleString().split("T")[1] +"</p>"+
                              "<p> 위반내용 : "+ contents +" </p> ";
          Notification.success({title : '<h3 style="text-align: center;"><i style=" -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: orange; color: rgba(187, 48, 23, 0.63); font-size : 42px;" class="fa fa-bell"></i></h3>' ,  message: mbody , delay: 10000});
                              
                              load_notice();
                          });
                  }
                });
      }

      //end for notice on the top bar

      //start show all status on the top bar
      function showAllStatus()
      {
        console.log("yayaya~~~hahaha");
        $http.get("/api/setAFlagZero")
                .success(function(afz){
                  if(afz.result == true)
                  {
                    $http.get("/api/getAllStatus")
                          .success(function(cbdata)
                          {

                            console.log("rat : " + JSON.stringify(cbdata));
                            $scope.as = cbdata;

                            load_notice();

                          });
                  }
                });
        
      }

      function closeNav(snd)
      {
        console.log("heyheyhey");
        var x = document.getElementById("myTopnav");
        x.className = "topnav";

        var flagf;
        if( ((3<=snd)&&(snd<=6)) || (snd == 13) || (snd == 11) )
        {
          flagf = 1;
        }
        else
        {
          flagf = 2;
        }
        $http.post("/api/setSoundsflag" , {obj : flagf})
                .success(function(cbdata)
                {
                  if((3<=snd)&&(snd<=10))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 3:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 4:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 5:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 6:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 7:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 8:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                            case 9:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 10:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page2.html';
                                });
                          
                        }
                        else if((snd == 11) || (snd == 12))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 11:
                              goParam = { cstatus : '3' , rouflag : 999};
                              break;
                            case 12:
                              goParam = { cstatus : '2' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page10.html';
                                });
                          
                        }
                        else if(snd == 1)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page7.html';
                                });
                          
                        }
                        else if(snd == 2)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page8.html';
                                });
                          
                        }

                });

      }

      function setStatusboxWidth()
      {
        console.log("wow excellent.. : "+( parseInt($(".page-container").width()) - 390 ));
        var gijuns = parseInt($(".page-container").width());
        var d_amount;
        if(gijuns<768)
          d_amount = 200;
        else
          d_amount = 400;

        var mywidth = gijuns - d_amount;

        $scope.statusbox={'width':mywidth+'px'};
      }

      $(window).resize(function(){
          
        setStatusboxWidth();

      });

      //end show all status on the top bar

      //start test for transfer

      // handles the callback from the received event
        var handleCallback = function (msg) {
            $scope.$apply(function () {
                //$scope.msg = JSON.parse(msg.data)
                var r_obj = JSON.parse(msg.data);
                console.log("receiver : " + r_obj.flag);
                
                if(r_obj.flag != 0)
                {
                  if(r_obj.flag != 100)
                  {
                    console.log("will chekc for target~");
                    $http.post("/api/CheckForEmitTarget" , { cfeparam : r_obj.shcdata }) // check if this is logined id which is correct target to listen the emit socket signal
                          .success(function(cfedata)
                          {
                            if(cfedata.result == true) // this is correct target
                            {
                                  if(cfedata.flag == 1)
                                  {
                                    console.log("will find....");
                                    showAllStatus();
                                    
                                  }
                                  else if(cfedata.flag == 2){
                                    $http.get("/api/showShopRWmsm")
                                          .success(function(ssrw){
                                            alert(ssrw.msmbody);
                                          });
                                    //console.log("listening for change.....");
                                  }
                            }

                          });
                  }
                  else if(r_obj.flag == 100) // its self signal
                  {
                    console.log("will find....");
                    showAllStatus();
                  }
                  
                }
            });
        }
 
        var source = new EventSource('/stats');
        source.addEventListener('message', handleCallback, false);

      //end test for transfer

      }

})();
