(function (){

  angular
      .module('page15App', ['ui-notification','ngRoute','ngAnimate', 'ngSanitize', 'ui.bootstrap'])
      .controller('page15Controller', Page15Controller)
      .controller('addGroupList', addGroupList)
      .controller('updateGroupList', updateGroupList)
      .config(function(NotificationProvider) {
          NotificationProvider.setOptions({
              delay: 10000,
              startTop: 20,
              startRight: 10,
              verticalSpacing: 20,
              horizontalSpacing: 20,
              positionX: 'right',
              positionY: 'top'
          });
        });

    function addGroupList($scope, $http, $window , $rootScope , $uibModalInstance) {

      $scope.exitMsg = exitMsg;
      $scope.addGr = addGr;

      function init() {


      }
      init();

      function exitMsg()
      {
        $uibModalInstance.dismiss('cancel');
      }

      function addGr(v)
      {
        if( v == undefined )
          alert("please type group name");
        else
        {
          if(v.g_name != undefined)
          {
            if(v.g_id != undefined)
            {
              var checkflag = 0;

                if($("#grouptype").val() == '1')
                {
                  if((v.g_id>=100)&&(v.g_id<=200))
                    checkflag = 1;
                  else
                    alert("관리자그룹은 그룹식별자가 100~200 사이에서 선택할수 있습니다.");
                }
                else if($("#grouptype").val() == '2')
                {
                  if((v.g_id>=3)&&(v.g_id<100))
                    checkflag = 1;
                  else
                    alert("관리자그룹은 그룹식별자가 3~99 사이에서 선택할수 있습니다.");
                }
                if(checkflag == 1)
                
                $http.post("/api/checkGrId" , v)
                      .success(function(cgndata){
                        if(cgndata.result == false) // un okay to add
                        {
                          checkflag = 0;
                          alert("이미 리용중인 식별자가 있습니다. 다른것을 입력하십시오.");
                        }
                        else if(cgndata.result == true) // okay to add
                        {
                          checkflag = 1;
                          console.log("will add~~~");
                          if(checkflag == 1)
                          {
                            $http.post('/api/checkGrName' , v)
                            .success(function(cbdata2){
                              if(cbdata2.result == true){
                                  $http.post('/api/addGroupitem' , v)
                                    .success(function(cbdata1){
                                      if (cbdata1.result == true){

                                        alert("success to add");
                                        $uibModalInstance.dismiss('cancel');
                                        $rootScope.$emit("addedSignal");

                                      }
                                    });
                              }
                              else
                                alert("There is same group name in database!!!");
                            });
                          }
                        }
                      });

                    /**/
                
              }
              else
                alert("please type group id");
          }
          else
            alert("please type group name");

        }
      }

    }

    function updateGroupList($scope, $http, $window , $rootScope , $uibModalInstance) {

      $scope.exitMsg = exitMsg;
      $scope.updateGr = updateGr;

      function init() {

        var sel = $rootScope.sel_item;
        sel.g_id = parseInt(sel.g_id);
        $scope.v = sel;

      }
      init();

      function exitMsg()
      {
        $uibModalInstance.dismiss('cancel');
      }

      function updateGr(v)
      {
        if( v == undefined )
          alert("please type group info");
        else{
          if( (v.g_name != undefined) && (v.g_id != undefined) ){

                    v.g_id = v.g_id.toString();
                      $http.post('/api/checkGroupItem' , v)
                        .success(function(cbdata1){
                          console.log( " finalcome :  " + cbdata1.result );
                          if(cbdata1.result == "ok"){
                            //alert("success");
                                $http.post('/api/updateGroupItem' , v)
                                    .success(function(cbdata2){
                                        if(cbdata2.result == true)
                                        {
                                          alert("update successfully!!!");
                                          $uibModalInstance.dismiss('cancel');
                                          $rootScope.$emit("addedSignal");
                                        }
                                    });
                          }
                          else
                            alert(cbdata1.result);
                        });
          }
          else
            alert("please type group info");

        }
      }

    }

    function Page15Controller(Notification ,$uibModal, $log, $document, $rootScope, $location , $scope, $http, $window) {

      $scope.logout_func = logout_func;
      $scope.addGroup = addGroup;
      $scope.updateGroup = updateGroup;
      $scope.deleteItem = deleteItem;
     $scope.myinfo = myinfo;
     $scope.onChangeNum_perPage = onChangeNum_perPage;
      $scope.pageChanged = pageChanged;

      var whole_notice;
      $scope.notify_clicked = notify_clicked;

      $scope.closeNav = closeNav;

      var wholeitems;

      var audio = new Audio('./sounds/Maramba.mp3');

      var acdata = [];

      function init() {
        console.log("page12-hey initer");

        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 1)
                $window.location.href = '/index.html';
              else
              {
                console.log("aya~~")
                $http.get('/api/get_global_acdata')
                      .success(function(ggadata){
                        acdata = ggadata.acdata;
                        $scope.mycid = ggadata.mycid;
                        $scope.mygroup = ggadata.mygroup;
                        $scope.mydeeper = ggadata.mydeeper;

                        if(($scope.mydeeper == 1)&&(acdata[0].ac_groupmanage <2))
                          logout_func();

                        imp_allow();

                        load_sidebar();
                        $scope.topnav = {url : "TopNav.html"};
                        showAllStatus();

                        loadGrouplist();

                      });
              }
              
            });

      }

      init();

      function imp_allow()
      {
          var mystat = {};
          var groupstat = {};
          var finalclc = {}; 
          console.log("$scope.mygroup : " + $scope.mygroup);

          acdata.forEach(function(raudata){
                          if(raudata.uinfo[0] != undefined)
                          {
                            if(raudata.uinfo[0].u_id == $scope.mycid)
                              mystat = raudata;
                          }
                          else if(raudata.ginfo[0] != undefined)
                          {
                            if(raudata.ginfo[0].g_id == $scope.mycid)
                              mystat = raudata;
                            if(raudata.ginfo[0].g_id == $scope.mygroup)
                              groupstat = raudata
                          }
                        });
          if($scope.mygroup < 100) // not manager grouop
              finalclc = mystat;
          else
              {
                
                finalclc = mystat; // init

                var counter = 0;
                for (var key in mystat) {
                    if(counter < 23)
                    {
                      if(mystat[key] == 0)
                        finalclc[key] = groupstat[key];
                      else
                        finalclc[key] = mystat[key];

                      //console.log("final : " + finalclc[key] + " <= group : " + groupstat[key] + " + mine : " + mystat[key]);
                    }  //
                    counter++;
                }
              }
              //console.log("final : " + finalclc.ac_zhong);
            $scope.fang = finalclc;

      }

      function load_sidebar()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                    switch(rsys.result.deep)
                    {
                      case 1:
                        $scope.template = {url : "sidebar.html"};
                        $scope.top = {url : "topbar.html"};
                       break;
                      case 2:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 3:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 4:
                        $scope.template = {url : "sidebar3.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                    }

                    setStatusboxWidth();
                  });
      }

      $scope.num_per_page = 10;
      $scope.maxSize = 3;
      $scope.bigTotalItems = 10;
      $scope.bigCurrentPage = 1;

      function pageChanged() {

          console.log('Page changed to: ' + $scope.bigCurrentPage);
          console.log($scope.bigCurrentPage);
          onChangeNum_perPage();
      }

      function onChangeNum_perPage(){
        console.log("ll : " + $scope.num_per_page);

        if($scope.num_per_page == 1)
          $scope.num_per_page = wholeitems.length;

        $scope.bigTotalItems = (10*Math.ceil(wholeitems.length/$scope.num_per_page));
        var frr = [];
        for(var j = ($scope.bigCurrentPage - 1) * $scope.num_per_page; j < $scope.bigCurrentPage * $scope.num_per_page; j ++)
            {
                if(j < wholeitems.length)
                    frr.push(wholeitems[j]);
            }
            $scope.groups = frr;
            console.log($scope.groups);

      }

      function myinfo()
      {

        $http.get('/api/set_myinfo')
            .success(function(cbdata){
              
              console.log("_kekeke : " + cbdata[0].savepay);

                var fnum = parseInt(cbdata[0].orig_gid);
                if( fnum == 2 )
                  $window.location.href = '/server-page8(edit&detail).html';
                if( fnum > 2 )
                  $window.location.href = '/server-page15(edit&detail).html';
            });
      }

      $rootScope.$on("addedSignal", function(){

        loadGrouplist();
        
      });

      function logout_func()
      {
        console.log("logouted");
        $http.get('/api/logout_func')
            .success(function(cbdata){
              if(cbdata.logresult == "success")
                $window.location.href = '/index.html';
            });
      }

      function loadGrouplist()
      {
        $http.get('/api/getGroupinfo')
            .success(function(cbdata){
              wholeitems = cbdata;
              pageChanged();
              //$scope.groups = cbdata;
            });
      }

      function addGroup()
      {
        $scope.title = "ikimaseyo!!!";
            console.log("hi");

        //$rootScope.report_body = rep_body;

            var modalInstance = $uibModal.open({
              animation: true,
              templateUrl: 'dialogs/addGroupDlg.html',
              controller: 'addGroupList',
              windowClass: 'center-modal',
              resolve: {
               //   items: function () {
                //    return $ctrl.items;
                 // }
              }
          });

          modalInstance.result.then(function (selectedItem) {}, function () {
              //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
          });
      }

      function updateGroup(sel_item)
      {
        $scope.title = "ikimaseyo!!!";
            console.log("hi");

        $rootScope.sel_item = sel_item;

            var modalInstance = $uibModal.open({
              animation: true,
              templateUrl: 'dialogs/updateGroupDlg.html',
              controller: 'updateGroupList',
              windowClass: 'center-modal',
              resolve: {
               //   items: function () {
                //    return $ctrl.items;
                 // }
              }
          });

          modalInstance.result.then(function (selectedItem) {}, function () {
              //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
          });
      }

      function deleteItem(sel_item)
      {
        /*console.log(sel_item);
        $http.post('/api/deleteGrItem' , sel_item)
            .success(function(cbdata){
              if(cbdata.result == true)
                loadGrouplist();
                //alert("success to remove");
            });*/
        $http.post('/api/checkGroupid' , sel_item)
            .success(function(cbdata1){
              if(cbdata1.result == true)
              {
                $http.post('/api/deleteGrItem' , sel_item)
                  .success(function(cbdata){
                    if(cbdata.result == true)
                      loadGrouplist();
                      alert("삭제되였습니다.");
                  });
              }
              else
                alert("이미 이 그룹을 리용하고 있기때문에 삭제할수 없습니다.");
            });
      }

      //start for notice on the top bar

      function checkForSounds(typesound)
      {
        $http.get('/api/getSoundsjson')
            .success(function(sdata)
            {
              if(typesound == 1)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) || (sdata.obj.notify < $scope.len ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
              else if(typesound == 2)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
            });
      }

      function load_notice()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    if(rsys.result.deep == 1){
                      console.log("dfdf");
                     $http.get('/api/get_all_notice')
                        .success(function(cbdata){
                            console.log("rusuky : " + cbdata.obj);
                            whole_notice = cbdata.obj;
                            whole_notice = whole_notice.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

                            var unreads = [];
                            whole_notice.forEach(function(item){
                              if(item.am_status == 0)
                                {
                                  unreads.push(item);
                                  console.log(JSON.stringify(item));
                                }
                            });

                            $scope.ur = unreads;
                            $scope.len = unreads.length;

                            checkForSounds(1);
                             

                        });
                    }
                    else
                      checkForSounds(2);

                  });
      }

      function notify_clicked(iobj)
      {
        whole_notice.forEach(function(item){
          if(item == iobj)
            item.am_status = 1;
        }); 
        $http.post('/api/update_all_notice' , { obj : whole_notice } )
                .success(function(cbdata){
                  if(cbdata.result == true){

                      $http.get('/api/getAllalarm' )
                        .success(function(cbdata1){
                            var atypes = cbdata1.obj;
                            var contents = "";
                            var titles = "";
                            for(var i=0;i<atypes.length;i++)
                              if(atypes[i].a_id == iobj.am_content)
                              {
                                contents = atypes[i].a_content.a_en;
                                titles = atypes[i].a_title.a_en;
                              }

                               var mbody = "<p> 위반대상 : "+ iobj.am_target +" </p>"+
                               "<p> 위반대상 식별자 : "+ iobj.am_id +" </p>"+
                              "<p> 위반날자 : "+ iobj.am_date.toLocaleString().split("T")[0] + " " + iobj.am_date.toLocaleString().split("T")[1] +"</p>"+
                              "<p> 위반내용 : "+ contents +" </p> ";
          Notification.success({title : '<h3 style="text-align: center;"><i style=" -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: orange; color: rgba(187, 48, 23, 0.63); font-size : 42px;" class="fa fa-bell"></i></h3>' ,  message: mbody , delay: 10000});
                              
                              load_notice();
                          });
                  }
                });
      }

      //end for notice on the top bar

      //start show all status on the top bar
      function showAllStatus()
      {
        console.log("yayaya~~~hahaha");
        $http.get("/api/setAFlagZero")
                .success(function(afz){
                  if(afz.result == true)
                  {
                    $http.get("/api/getAllStatus")
                          .success(function(cbdata)
                          {

                            console.log("rat : " + JSON.stringify(cbdata));
                            $scope.as = cbdata;

                            load_notice();

                          });
                  }
                });
        
      }

      function closeNav(snd)
      {
        console.log("heyheyhey");
        var x = document.getElementById("myTopnav");
        x.className = "topnav";

        var flagf;
        if( ((3<=snd)&&(snd<=6)) || (snd == 13) || (snd == 11) )
        {
          flagf = 1;
        }
        else
        {
          flagf = 2;
        }
        $http.post("/api/setSoundsflag" , {obj : flagf})
                .success(function(cbdata)
                {
                  if((3<=snd)&&(snd<=10))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 3:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 4:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 5:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 6:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 7:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 8:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                            case 9:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 10:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page2.html';
                                });
                          
                        }
                        else if((snd == 11) || (snd == 12))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 11:
                              goParam = { cstatus : '3' , rouflag : 999};
                              break;
                            case 12:
                              goParam = { cstatus : '2' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page10.html';
                                });
                          
                        }
                        else if(snd == 1)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page7.html';
                                });
                          
                        }
                        else if(snd == 2)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page8.html';
                                });
                          
                        }

                });

      }

      function setStatusboxWidth()
      {
        console.log("wow excellent.. : "+( parseInt($(".page-container").width()) - 390 ));
        var gijuns = parseInt($(".page-container").width());
        var d_amount;
        if(gijuns<768)
          d_amount = 200;
        else
          d_amount = 400;

        var mywidth = gijuns - d_amount;

        $scope.statusbox={'width':mywidth+'px'};
      }

      $(window).resize(function(){
          
        setStatusboxWidth();

      });

      //end show all status on the top bar

      //start test for transfer

      // handles the callback from the received event
        var handleCallback = function (msg) {
            $scope.$apply(function () {
                //$scope.msg = JSON.parse(msg.data)
                var r_obj = JSON.parse(msg.data);
                console.log("receiver : " + r_obj.flag);
                
                if(r_obj.flag != 0)
                {
                  if(r_obj.flag != 100)
                  {
                    console.log("will chekc for target~");
                    $http.post("/api/CheckForEmitTarget" , { cfeparam : r_obj.shcdata }) // check if this is logined id which is correct target to listen the emit socket signal
                          .success(function(cfedata)
                          {
                            if(cfedata.result == true) // this is correct target
                            {
                                  if(cfedata.flag == 1)
                                  {
                                    console.log("will find....");
                                    showAllStatus();
                                    
                                  }
                                  else if(cfedata.flag == 2){
                                    $http.get("/api/showShopRWmsm")
                                          .success(function(ssrw){
                                            alert(ssrw.msmbody);
                                          });
                                    //console.log("listening for change.....");
                                  }
                            }

                          });
                  }
                  else if(r_obj.flag == 100) // its self signal
                  {
                    console.log("will find....");
                    showAllStatus();
                  }
                  
                }
            });
        }
 
        var source = new EventSource('/stats');
        source.addEventListener('message', handleCallback, false);

      //end test for transfer

    }

})();
