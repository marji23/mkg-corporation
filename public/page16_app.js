(function (){

  angular
      .module('page16_App', ['ui-notification', 'ui.bootstrap'])
      .controller('page16_Controller', page16_Controller)
      .config(function(NotificationProvider) {
          NotificationProvider.setOptions({
              delay: 10000,
              startTop: 20,
              startRight: 10,
              verticalSpacing: 20,
              horizontalSpacing: 20,
              positionX: 'right',
              positionY: 'top'
          });
        });



    function page16_Controller(Notification ,$scope, $http, $window) {

      $scope.logout_func = logout_func;
      $scope.myinfo = myinfo;

      var whole_notice;
      $scope.notify_clicked = notify_clicked;
      $scope.closeNav = closeNav;

      $scope.groupChanged = groupChanged;
      $scope.managerChanged = managerChanged;
      $scope.shortcut = shortcut;
      $scope.saveAllower = saveAllower;

      var acdata = [];

     var audio = new Audio('./sounds/Maramba.mp3');

      function init() {
        console.log("page16 initer");

        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 1)
                $window.location.href = '/index.html';
              else
              {
                console.log("aya~~")
                $http.get('/api/get_global_acdata')
                      .success(function(ggadata){
                        acdata = ggadata.acdata;
                        $scope.mycid = ggadata.mycid;
                        $scope.mygroup = ggadata.mygroup;
                        $scope.mydeeper = ggadata.mydeeper;

                        if(($scope.mydeeper == 1)&&(acdata[0].ac_allows <2))
                          logout_func();

                        imp_allow();

                        console.log("length : " + acdata.length);

                        load_sidebar();
                        $scope.topnav = {url : "TopNav.html"};
                        showAllStatus();

                        loadGroupList();


                      });
              }
              
            });

      }

      init();

      function imp_allow()
      {
          var mystat = {};
          var groupstat = {};
          var finalclc = {}; 
          console.log("$scope.mygroup : " + $scope.mygroup);



          acdata.forEach(function(raudata){
                          if(raudata.uinfo[0] != undefined)
                          {
                            if(raudata.uinfo[0].u_id == $scope.mycid)
                              mystat = raudata;
                          }
                          else if(raudata.ginfo[0] != undefined)
                          {
                            if(raudata.ginfo[0].g_id == $scope.mycid)
                              mystat = raudata;
                            if(raudata.ginfo[0].g_id == $scope.mygroup)
                              groupstat = raudata
                          }
                        });
          if($scope.mygroup < 100) // not manager grouop
              finalclc = mystat;
          else
              {
                
                finalclc = mystat; // init

                var counter = 0;
                for (var key in mystat) {
                    if(counter < 23)
                    {
                      if(mystat[key] == 0)
                        finalclc[key] = groupstat[key];
                      else
                        finalclc[key] = mystat[key];

                      //console.log("final : " + finalclc[key] + " <= group : " + groupstat[key] + " + mine : " + mystat[key]);
                    }  //
                    counter++;
                }
              }
              //console.log("final : " + finalclc.ac_zhong);
            $scope.fang = finalclc;

      }

      function load_sidebar()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                    switch(rsys.result.deep)
                    {
                      case 1:
                        $scope.template = {url : "sidebar.html"};
                        $scope.top = {url : "topbar.html"};
                       break;
                      case 2:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 3:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 4:
                        $scope.template = {url : "sidebar3.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                    }

                    setStatusboxWidth();
                  });
      }


      function myinfo()
      {

        $http.get('/api/set_myinfo')
            .success(function(cbdata){
              
              console.log("_kekeke : " + cbdata[0].savepay);

                var fnum = parseInt(cbdata[0].orig_gid);
                if( fnum == 2 )
                  $window.location.href = '/server-page8(edit&detail).html';
                if( fnum > 2 )
                  $window.location.href = '/server-page15(edit&detail).html';
            });
      }

      function logout_func()
      {
        console.log("logouted");
        $http.get('/api/logout_func')
            .success(function(cbdata){
              if(cbdata.logresult == "success")
                $window.location.href = '/index.html';
            });
      }

      //start main part

      function saveAllower()
      {
        var tempaw = $scope.searched;
        var willgo = {};
        var finaled = {};

        acdata.forEach(function(accitem){
          if(accitem.ac_id == tempaw[0].id)
          {
            console.log("qqqqqqqqqq : " + tempaw[0].id);
            willgo["ac_zhong"] = accitem.ac_zhong = parseFloat(tempaw[0].val);
            willgo["ac_report"] = accitem.ac_report = parseFloat(tempaw[1].val);
            willgo["ac_sendmsm"] = accitem.ac_sendmsm = parseFloat(tempaw[2].val);
            willgo["ac_showmsm"] = accitem.ac_showmsm = parseFloat(tempaw[3].val);
            willgo["ac_gongji"] = accitem.ac_gongji = parseFloat(tempaw[4].val);
            willgo["ac_help"] = accitem.ac_help = parseFloat(tempaw[5].val);
            willgo["ac_sp"] = accitem.ac_sp = parseFloat(tempaw[6].val);
            willgo["ac_visitor"] = accitem.ac_visitor = parseFloat(tempaw[7].val);
            willgo["ac_shop"] = accitem.ac_shop = parseFloat(tempaw[8].val);
            willgo["ac_allgame"] = accitem.ac_allgame = parseFloat(tempaw[9].val);
            willgo["ac_pokerhistory"] = accitem.ac_pokerhistory = parseFloat(tempaw[10].val);
            willgo["ac_gamemanage"] = accitem.ac_gamemanage = parseFloat(tempaw[11].val);
            willgo["ac_security"] = accitem.ac_security = parseFloat(tempaw[12].val);
            willgo["ac_eb_reg"] = accitem.ac_eb_reg = parseFloat(tempaw[13].val);
            willgo["ac_eb_imp"] = accitem.ac_eb_imp = parseFloat(tempaw[14].val);
            willgo["ac_groupmanage"] = accitem.ac_groupmanage = parseFloat(tempaw[15].val);
            willgo["ac_manager"] = accitem.ac_manager = parseFloat(tempaw[16].val);
            willgo["ac_allows"] = accitem.ac_allows = parseFloat(tempaw[17].val);
            willgo["ac_myinfo"] = accitem.ac_myinfo = parseFloat(tempaw[18].val);
            willgo["ac_pmethod"] = accitem.ac_pmethod = parseFloat(tempaw[19].val);
            willgo["ac_allnotify"] = accitem.ac_allnotify = parseFloat(tempaw[20].val);
            willgo["ac_allstatus"] = accitem.ac_allstatus = parseFloat(tempaw[21].val);
            willgo["ac_payments"] = accitem.ac_payments = parseFloat(tempaw[22].val);


            finaled["founderid"] = accitem.ac_id;
            finaled["up_obj"] =  willgo;

          }
        });

        
        var accheckflag = 0;
        var acchecktarget = 0;

        if(finaled.up_obj.ac_payments == 3)
          {
            console.log("~~~~~~~~~~~");

            acdata.forEach(function(raadata){
                  if(raadata.ac_payments == 3)
                  {
                    accheckflag = 1;
                    acchecktarget = raadata.ac_id;
                  }
                });
          }
        else
        {
          accheckflag = 0;
        }

        if(accheckflag == 1)
        {
            alert("이미 결산처리에 관한 권한을 부여하고 있으므로 당신은 최대한 읽기 권한만 설정할수 있습니다.  당신이 원한다면 본사에 문의하십시오.");
        }
        else
        {
          $http.post("/api/updateAllowers" , { obj : finaled })
                .success(function(cbdata){
                  alert("success to update");
                  imp_allow();
                });
        }

      }

      function managerChanged(obj)
      {
        console.log("here is manager : " + obj);

        $scope.tochanger = obj;

        var src = ["설정 삭제" , "설정 해제" , "읽기 설정" , "쓰기 설정"];
        var tempdata = [];

        acdata.forEach(function(acitem){
            console.log("acid : " + acitem.ac_id);
            if(acitem.ac_id == parseFloat(obj))
            {
              console.log("hai za : " + acitem.ac_zhong);
              tempdata.push({id : acitem.ac_id , title : "종합화면 기능" , val : acitem.ac_zhong.toString() , kval : src[acitem.ac_zhong]});
              tempdata.push({id : acitem.ac_id , title : "의견목록관리 기능" , val : acitem.ac_report.toString() , kval : src[acitem.ac_report]});
              tempdata.push({id : acitem.ac_id , title : "통보문송신 기능" , val : acitem.ac_sendmsm.toString() , kval : src[acitem.ac_sendmsm]});
              tempdata.push({id : acitem.ac_id , title : "통보문송신리력 기능" , val : acitem.ac_showmsm.toString() , kval : src[acitem.ac_showmsm]});
              tempdata.push({id : acitem.ac_id , title : "공지사항 기능" , val : acitem.ac_gongji.toString() , kval : src[acitem.ac_gongji]});
              tempdata.push({id : acitem.ac_id , title : "도움말 기능" , val : acitem.ac_help.toString() , kval : src[acitem.ac_help]});
              tempdata.push({id : acitem.ac_id , title : "충환전 기능" , val : acitem.ac_sp.toString() , kval : src[acitem.ac_sp]});
              tempdata.push({id : acitem.ac_id , title : "회원관리 기능" , val : acitem.ac_visitor.toString() , kval : src[acitem.ac_visitor]});
              tempdata.push({id : acitem.ac_id , title : "대리점 기능" , val : acitem.ac_shop.toString() , kval : src[acitem.ac_shop]});
              tempdata.push({id : acitem.ac_id , title : "게임전반상황 기능" , val : acitem.ac_allgame.toString() , kval : src[acitem.ac_allgame]});
              tempdata.push({id : acitem.ac_id , title : "Poker리력 기능" , val : acitem.ac_pokerhistory.toString() , kval : src[acitem.ac_pokerhistory]});
              tempdata.push({id : acitem.ac_id , title : "게임관리 기능" , val : acitem.ac_gamemanage.toString() , kval : src[acitem.ac_gamemanage]});
              tempdata.push({id : acitem.ac_id , title : "보안설정 기능" , val : acitem.ac_security.toString() , kval : src[acitem.ac_security]});
              tempdata.push({id : acitem.ac_id , title : "이벤트/보너스등록 기능" , val : acitem.ac_eb_reg.toString() , kval : src[acitem.ac_eb_reg]});
              tempdata.push({id : acitem.ac_id , title : "이벤트/보너스구현 기능" , val : acitem.ac_eb_imp.toString() , kval : src[acitem.ac_eb_imp]});
              tempdata.push({id : acitem.ac_id , title : "그룹관리 기능" , val : acitem.ac_groupmanage.toString() , kval : src[acitem.ac_groupmanage]});
              tempdata.push({id : acitem.ac_id , title : "관리자목록 기능" , val : acitem.ac_manager.toString() , kval : src[acitem.ac_manager]});
              tempdata.push({id : acitem.ac_id , title : "권한설정 기능" , val : acitem.ac_allows.toString() , kval : src[acitem.ac_allows]});
              tempdata.push({id : acitem.ac_id , title : "나의정보 기능" , val : acitem.ac_myinfo.toString() , kval : src[acitem.ac_myinfo]});
              tempdata.push({id : acitem.ac_id , title : "지불방식설정 기능" , val : acitem.ac_pmethod.toString() , kval : src[acitem.ac_pmethod]});
              tempdata.push({id : acitem.ac_id , title : "notification 전체보기 기능" , val : acitem.ac_allnotify.toString() , kval : src[acitem.ac_allnotify]});
              tempdata.push({id : acitem.ac_id , title : "전체상태현시 기능" , val : acitem.ac_allstatus.toString() , kval : src[acitem.ac_allstatus]});
              tempdata.push({id : acitem.ac_id , title : "결산 기능" , val : acitem.ac_payments.toString() , kval : src[acitem.ac_payments]});
            }
        });

        acdata.forEach(function(acitem){
            if(acitem.ginfo[0] != undefined)
            {
              
              if(acitem.ginfo[0].g_id == parseFloat($scope.target_group))
              {
                console.log("acid1111 : " + acitem.ac_allnotify);

                tempdata[0]["gval"] = src[acitem.ac_zhong];
                tempdata[1]["gval"] = src[acitem.ac_report];
                tempdata[2]["gval"] = src[acitem.ac_sendmsm];
                tempdata[3]["gval"] = src[acitem.ac_showmsm];
                tempdata[4]["gval"] = src[acitem.ac_gongji];
                tempdata[5]["gval"] = src[acitem.ac_help];
                tempdata[6]["gval"] = src[acitem.ac_sp];
                tempdata[7]["gval"] = src[acitem.ac_visitor];
                tempdata[8]["gval"] = src[acitem.ac_shop];
                tempdata[9]["gval"] = src[acitem.ac_allgame];
                tempdata[10]["gval"] = src[acitem.ac_pokerhistory];
                tempdata[11]["gval"] = src[acitem.ac_gamemanage];
                tempdata[12]["gval"] = src[acitem.ac_security];
                tempdata[13]["gval"] = src[acitem.ac_eb_reg];
                tempdata[14]["gval"] = src[acitem.ac_eb_imp];
                tempdata[15]["gval"] = src[acitem.ac_groupmanage];
                tempdata[16]["gval"] = src[acitem.ac_manager];
                tempdata[17]["gval"] = src[acitem.ac_allows];
                tempdata[18]["gval"] = src[acitem.ac_myinfo];
                tempdata[19]["gval"] = src[acitem.ac_pmethod];
                tempdata[20]["gval"] = src[acitem.ac_allnotify];
                tempdata[21]["gval"] = src[acitem.ac_allstatus];
                tempdata[22]["gval"] = src[acitem.ac_payments];
              }
            }
        });

        $scope.searched = tempdata;

      }

      function shortcut(vals)
      {
        var tempvals = 0;
        switch(vals)
        {
          
          case 0:
            tempvals = 0;
            break;
          case 1:
            tempvals = 1;
            break;
          case 2:
            tempvals = 2;
            break;
          case 3:
            tempvals = 3;
            break;
        }

        var tempsearched = $scope.searched;
        tempsearched.forEach(function(tsitem){
          tsitem.val = tempvals.toString();
        });

        $scope.searched = tempsearched;

      }

      function groupChanged(obj , listmanager , listgroup)
      {

        console.log("eventerer. " + listmanager.length + " / " + obj);

        var arr = [];

        listmanager.forEach(function(litem){
            if(litem.u_groupid == obj)
              arr.push(litem);
        }); 

        console.log("seled : " + arr.length);

        $scope.seled_managers = arr;

        console.log("group len : " + listgroup.length);
        listgroup.forEach(function(litem){
            if(litem.g_id == obj)
              {
                console.log("groupid : " + litem.u_ac_metaid);
                $scope.commonvalue = litem.u_ac_metaid;
                $scope.target_manager = litem.u_ac_metaid.toString();
              }
        });

        managerChanged($scope.target_manager);

      }

      function loadGroupList()
      {
        $http.get('/api/getGroupinfo')
              .success(function(cbdata){
                  $scope.groups = cbdata;
                  $scope.target_group = cbdata[2].g_id.toString();

                  var src = ["설정 삭제" , "설정 해제" , "읽기 설정" , "쓰기 설정"];
                  $scope.retx = src;
                  
                  $http.get('/api/get_firstsite_user')
                        .success(function(cmdata){
                          $scope.managers = cmdata.result;
                          console.log("cmdata : " + cmdata.result.length);

                          groupChanged(cbdata[2].g_id , cmdata.result , cbdata);

                          $scope.our = { "111" : "aa" , "112" : "cc" , "3" : "rr" , "2" : "oo"};

                        });
              });
      }

      //end main part

      //start for notice on the top bar

      function checkForSounds(typesound)
      {
        $http.get('/api/getSoundsjson')
            .success(function(sdata)
            {
              if(typesound == 1)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) || (sdata.obj.notify < $scope.len ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
              else if(typesound == 2)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
            });
      }

      function load_notice()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    if(rsys.result.deep == 1){
                      console.log("dfdf");
                     $http.get('/api/get_all_notice')
                        .success(function(cbdata){
                            console.log("rusuky : " + cbdata.obj);
                            whole_notice = cbdata.obj;
                            whole_notice = whole_notice.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

                            var unreads = [];
                            whole_notice.forEach(function(item){
                              if(item.am_status == 0)
                                {
                                  unreads.push(item);
                                  console.log(JSON.stringify(item));
                                }
                            });

                            $scope.ur = unreads;
                            $scope.len = unreads.length;

                            checkForSounds(1);
                             

                        });
                    }
                    else
                      checkForSounds(2);

                  });
      }

      function notify_clicked(iobj)
      {
        whole_notice.forEach(function(item){
          if(item == iobj)
            item.am_status = 1;
        }); 
        $http.post('/api/update_all_notice' , { obj : whole_notice } )
                .success(function(cbdata){
                  if(cbdata.result == true){

                      $http.get('/api/getAllalarm' )
                        .success(function(cbdata1){
                            var atypes = cbdata1.obj;
                            var contents = "";
                            var titles = "";
                            for(var i=0;i<atypes.length;i++)
                              if(atypes[i].a_id == iobj.am_content)
                              {
                                contents = atypes[i].a_content.a_en;
                                titles = atypes[i].a_title.a_en;
                              }

                               var mbody = "<p> 위반대상 : "+ iobj.am_target +" </p>"+
                               "<p> 위반대상 식별자 : "+ iobj.am_id +" </p>"+
                              "<p> 위반날자 : "+ iobj.am_date.toLocaleString().split("T")[0] + " " + iobj.am_date.toLocaleString().split("T")[1] +"</p>"+
                              "<p> 위반내용 : "+ contents +" </p> ";
          Notification.success({title : '<h3 style="text-align: center;"><i style=" -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: orange; color: rgba(187, 48, 23, 0.63); font-size : 42px;" class="fa fa-bell"></i></h3>' ,  message: mbody , delay: 10000});
                              
                              load_notice();
                          });
                  }
                });
      }

      //end for notice on the top bar

      //start show all status on the top bar
      function showAllStatus()
      {
        console.log("yayaya~~~hahaha");
        $http.get("/api/setAFlagZero")
                .success(function(afz){
                  if(afz.result == true)
                  {
                    $http.get("/api/getAllStatus")
                          .success(function(cbdata)
                          {

                            console.log("rat : " + JSON.stringify(cbdata));
                            $scope.as = cbdata;

                            load_notice();

                          });
                  }
                });
        
      }

      function closeNav(snd)
      {
        console.log("heyheyhey");
        var x = document.getElementById("myTopnav");
        x.className = "topnav";

        var flagf;
        if( ((3<=snd)&&(snd<=6)) || (snd == 13) || (snd == 11) )
        {
          flagf = 1;
        }
        else
        {
          flagf = 2;
        }
        $http.post("/api/setSoundsflag" , {obj : flagf})
                .success(function(cbdata)
                {
                  if((3<=snd)&&(snd<=10))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 3:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 4:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 5:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 6:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 7:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 8:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                            case 9:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 10:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page2.html';
                                });
                          
                        }
                        else if((snd == 11) || (snd == 12))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 11:
                              goParam = { cstatus : '3' , rouflag : 999};
                              break;
                            case 12:
                              goParam = { cstatus : '2' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page10.html';
                                });
                          
                        }
                        else if(snd == 1)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page7.html';
                                });
                          
                        }
                        else if(snd == 2)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page8.html';
                                });
                          
                        }

                });

      }

      function setStatusboxWidth()
      {
        console.log("wow excellent.. : "+( parseInt($(".page-container").width()) - 390 ));
        var gijuns = parseInt($(".page-container").width());
        var d_amount;
        if(gijuns<768)
          d_amount = 200;
        else
          d_amount = 400;

        var mywidth = gijuns - d_amount;

        $scope.statusbox={'width':mywidth+'px'};
      }

      $(window).resize(function(){
          
        setStatusboxWidth();

      });

      //end show all status on the top bar

      //start test for transfer

      // handles the callback from the received event
        var handleCallback = function (msg) {
            $scope.$apply(function () {
                //$scope.msg = JSON.parse(msg.data)
                var r_obj = JSON.parse(msg.data);
                console.log("receiver : " + r_obj.flag);
                
                if(r_obj.flag != 0)
                {
                  if(r_obj.flag != 100)
                  {
                    console.log("will chekc for target~");
                    $http.post("/api/CheckForEmitTarget" , { cfeparam : r_obj.shcdata }) // check if this is logined id which is correct target to listen the emit socket signal
                          .success(function(cfedata)
                          {
                            if(cfedata.result == true) // this is correct target
                            {
                                  if(cfedata.flag == 1)
                                  {
                                    console.log("will find....");
                                    showAllStatus();
                                    
                                  }
                                  else if(cfedata.flag == 2){
                                    $http.get("/api/showShopRWmsm")
                                          .success(function(ssrw){
                                            alert(ssrw.msmbody);
                                          });
                                    //console.log("listening for change.....");
                                  }
                            }

                          });
                  }
                  else if(r_obj.flag == 100) // its self signal
                  {
                    console.log("will find....");
                    showAllStatus();
                  }
                  
                }
            });
        }
 
        var source = new EventSource('/stats');
        source.addEventListener('message', handleCallback, false);

      //end test for transfer


    }

})();
