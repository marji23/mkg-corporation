(function (){

  angular
      .module('page3_pokerApp', ['ngLoader' , 'ui.bootstrap', 'ui-notification' , 'chart.js'])
      .controller('page3_pokerController', page3_pokerController)
      .config(function(NotificationProvider) {
          NotificationProvider.setOptions({
              delay: 10000,
              startTop: 20,
              startRight: 10,
              verticalSpacing: 20,
              horizontalSpacing: 20,
              positionX: 'right',
              positionY: 'top'
          });
        })
      .config(['ChartJsProvider', function (ChartJsProvider) {
        // Configure all charts
        ChartJsProvider.setOptions({
          chartColors: ['#536b5f', '#838469'],
          responsive: true
        });
        // Configure all line charts
        ChartJsProvider.setOptions('line', {
          showLines: true
        });
      }]);



    function page3_pokerController(Notification ,$scope, $http, $window , $timeout) {

      $scope.logout_func = logout_func;
      $scope.myinfo = myinfo;
      $scope.doAnalyse = doAnalyse;
      $scope.reset_filter = reset_filter;
      $scope.day_filter = day_filter;

      $scope.reset_filter1 = reset_filter1;
      $scope.AnalyseByVisitorWhenClick = AnalyseByVisitorWhenClick;

      var whole_notice;
      $scope.notify_clicked = notify_clicked;

      $scope.closeNav = closeNav;

      $scope.onChangeNum_perPage = onChangeNum_perPage;
      $scope.pageChanged = pageChanged;
      var wholeitems;

      var acdata = [];

     var audio = new Audio('./sounds/Maramba.mp3');

      function init() {
        console.log("page3 poker initer");

        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 1)
                $window.location.href = '/index.html';
              else
              {
                console.log("aya~~")
                $http.get('/api/get_global_acdata')
                      .success(function(ggadata){
                        acdata = ggadata.acdata;
                        $scope.mycid = ggadata.mycid;
                        $scope.mygroup = ggadata.mygroup;
                        $scope.mydeeper = ggadata.mydeeper;

                        if(($scope.mydeeper == 1)&&(acdata[0].ac_pokerhistory <2))
                          logout_func();

                        imp_allow();

                        load_sidebar();
                        $scope.topnav = {url : "TopNav.html"};
                        showAllStatus();

                      });
              }
              

            });

      }

      init();

      function imp_allow()
      {
          var mystat = {};
          var groupstat = {};
          var finalclc = {}; 
          console.log("$scope.mygroup : " + $scope.mygroup);

          acdata.forEach(function(raudata){
                          if(raudata.uinfo[0] != undefined)
                          {
                            if(raudata.uinfo[0].u_id == $scope.mycid)
                              mystat = raudata;
                          }
                          else if(raudata.ginfo[0] != undefined)
                          {
                            if(raudata.ginfo[0].g_id == $scope.mycid)
                              mystat = raudata;
                            if(raudata.ginfo[0].g_id == $scope.mygroup)
                              groupstat = raudata
                          }
                        });
          if($scope.mygroup < 100) // not manager grouop
              finalclc = mystat;
          else
              {
                
                finalclc = mystat; // init

                var counter = 0;
                for (var key in mystat) {
                    if(counter < 23)
                    {
                      if(mystat[key] == 0)
                        finalclc[key] = groupstat[key];
                      else
                        finalclc[key] = mystat[key];

                      //console.log("final : " + finalclc[key] + " <= group : " + groupstat[key] + " + mine : " + mystat[key]);
                    }  //
                    counter++;
                }
              }
              //console.log("final : " + finalclc.ac_zhong);
            $scope.fang = finalclc;

      }

      $scope.num_per_page = 10;
      $scope.maxSize = 3;
      $scope.bigTotalItems = 10;
      $scope.bigCurrentPage = 1;

      function pageChanged() {

          AnalyseByVisitor();
      }

      function onChangeNum_perPage(){
        
        AnalyseByVisitorWhenClick();

      }

      function load_sidebar()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                    switch(rsys.result.deep)
                    {
                      case 1:
                        $scope.template = {url : "sidebar.html"};
                        $scope.top = {url : "topbar.html"};
                       break;
                      case 2:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 3:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 4:
                        $scope.template = {url : "sidebar3.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                    }

                    setStatusboxWidth();
                  });
      }

      function myinfo()
      {

        $http.get('/api/set_myinfo')
            .success(function(cbdata){
              
              console.log("_kekeke : " + cbdata[0].savepay);

                var fnum = parseInt(cbdata[0].orig_gid);
                if( fnum == 2 )
                  $window.location.href = '/server-page8(edit&detail).html';
                if( fnum > 2 )
                  $window.location.href = '/server-page15(edit&detail).html';
            });
      }

      function logout_func()
      {
        console.log("logouted");
        $http.get('/api/logout_func')
            .success(function(cbdata){
              if(cbdata.logresult == "success")
                $window.location.href = '/index.html';
            });
      }

      //start for notice on the top bar

      function checkForSounds(typesound)
      {
        $http.get('/api/getSoundsjson')
            .success(function(sdata)
            {
              if(typesound == 1)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) || (sdata.obj.notify < $scope.len ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
              else if(typesound == 2)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
            });
      }

      function load_notice()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    if(rsys.result.deep == 1){
                      console.log("dfdf");
                     $http.get('/api/get_all_notice')
                        .success(function(cbdata){
                            console.log("rusuky : " + cbdata.obj);
                            whole_notice = cbdata.obj;
                            whole_notice = whole_notice.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

                            var unreads = [];
                            whole_notice.forEach(function(item){
                              if(item.am_status == 0)
                                {
                                  unreads.push(item);
                                  console.log(JSON.stringify(item));
                                }
                            });

                            $scope.ur = unreads;
                            $scope.len = unreads.length;

                            checkForSounds(1);
                             

                        });
                    }
                    else
                      checkForSounds(2);

                  });
      }

      function notify_clicked(iobj)
      {
        whole_notice.forEach(function(item){
          if(item == iobj)
            item.am_status = 1;
        }); 
        $http.post('/api/update_all_notice' , { obj : whole_notice } )
                .success(function(cbdata){
                  if(cbdata.result == true){

                      $http.get('/api/getAllalarm' )
                        .success(function(cbdata1){
                            var atypes = cbdata1.obj;
                            var contents = "";
                            var titles = "";
                            for(var i=0;i<atypes.length;i++)
                              if(atypes[i].a_id == iobj.am_content)
                              {
                                contents = atypes[i].a_content.a_en;
                                titles = atypes[i].a_title.a_en;
                              }

                               var mbody = "<p> 위반대상 : "+ iobj.am_target +" </p>"+
                               "<p> 위반대상 식별자 : "+ iobj.am_id +" </p>"+
                              "<p> 위반날자 : "+ iobj.am_date.toLocaleString().split("T")[0] + " " + iobj.am_date.toLocaleString().split("T")[1] +"</p>"+
                              "<p> 위반내용 : "+ contents +" </p> ";
          Notification.success({title : '<h3 style="text-align: center;"><i style=" -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: orange; color: rgba(187, 48, 23, 0.63); font-size : 42px;" class="fa fa-bell"></i></h3>' ,  message: mbody , delay: 10000});
                              
                              load_notice();
                          });
                  }
                });
      }

      //end for notice on the top bar

      //start show all status on the top bar
      function showAllStatus()
      {
        console.log("yayaya~~~hahaha");
        $http.get("/api/setAFlagZero")
                .success(function(afz){
                  if(afz.result == true)
                  {
                    $http.get("/api/getAllStatus")
                          .success(function(cbdata)
                          {

                            console.log("rat : " + JSON.stringify(cbdata));
                            $scope.as = cbdata;

                            load_notice();

                          });
                  }
                });
        
      }

      function closeNav(snd)
      {
        console.log("heyheyhey");
        var x = document.getElementById("myTopnav");
        x.className = "topnav";

        var flagf;
        if( ((3<=snd)&&(snd<=6)) || (snd == 13) || (snd == 11) )
        {
          flagf = 1;
        }
        else
        {
          flagf = 2;
        }
        $http.post("/api/setSoundsflag" , {obj : flagf})
                .success(function(cbdata)
                {
                  if((3<=snd)&&(snd<=10))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 3:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 4:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 5:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 6:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 7:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 8:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                            case 9:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 10:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page2.html';
                                });
                          
                        }
                        else if((snd == 11) || (snd == 12))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 11:
                              goParam = { cstatus : '3' , rouflag : 999};
                              break;
                            case 12:
                              goParam = { cstatus : '2' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page10.html';
                                });
                          
                        }
                        else if(snd == 1)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page7.html';
                                });
                          
                        }
                        else if(snd == 2)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page8.html';
                                });
                          
                        }

                });

      }

      function setStatusboxWidth()
      {
        console.log("wow excellent.. : "+( parseInt($(".page-container").width()) - 390 ));
        var gijuns = parseInt($(".page-container").width());
        var d_amount;
        if(gijuns<768)
          d_amount = 200;
        else
          d_amount = 400;

        var mywidth = gijuns - d_amount;

        $scope.statusbox={'width':mywidth+'px'};
      }

      $(window).resize(function(){
          
        setStatusboxWidth();

      });

      //end show all status on the top bar

      //start test for transfer

      // handles the callback from the received event
        var handleCallback = function (msg) {
            $scope.$apply(function () {
                //$scope.msg = JSON.parse(msg.data)
                var r_obj = JSON.parse(msg.data);
                console.log("receiver : " + r_obj.flag);
                
                if(r_obj.flag != 0)
                {
                  if(r_obj.flag != 100)
                  {
                    console.log("will chekc for target~");
                    $http.post("/api/CheckForEmitTarget" , { cfeparam : r_obj.shcdata }) // check if this is logined id which is correct target to listen the emit socket signal
                          .success(function(cfedata)
                          {
                            if(cfedata.result == true) // this is correct target
                            {
                                  if(cfedata.flag == 1)
                                  {
                                    console.log("will find....");
                                    showAllStatus();
                                    
                                  }
                                  else if(cfedata.flag == 2){
                                    $http.get("/api/showShopRWmsm")
                                          .success(function(ssrw){
                                            alert(ssrw.msmbody);
                                          });
                                    //console.log("listening for change.....");
                                  }
                            }

                          });
                  }
                  else if(r_obj.flag == 100) // its self signal
                  {
                    console.log("will find....");
                    showAllStatus();
                  }
                  
                }
            });
        }
 
        var source = new EventSource('/stats');
        source.addEventListener('message', handleCallback, false);

      //end test for transfer

      //start chart
      
      $scope.onClick = function (points, evt) {
                          console.log(points, evt);
                        };

      /* Define new prototype methods on Date object. */
      // Returns Date as a String in YYYY-MM-DD format.
      Date.prototype.toISODateString = function () {
        return this.toISOString().substr(0,10);
      };

      // Returns new Date object offset `n` days from current Date object.
      Date.prototype.toDateFromDays = function (n) {
        n = parseInt(n) || 0;
        var newDate = new Date(this.getTime());
        newDate.setDate(this.getDate() + n);
        return newDate;
      };
      
      function doAnalyse()
      {
        var oneDay = 60*60*1000; // hours*minutes*seconds*milliseconds
        var firstDate = new Date($('#fromdate').val());
        var secondDate = new Date($('#todate').val());

        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

        if(diffDays < 24)
        {
          alert("최소 1일 정도의 리력을 평가할수 있으므로 범위를 1일 이상으로 설정하십시오.");
        }
        else{

          if($('#todate').val() != "")
          {
              if( (firstDate < secondDate) || ($('#fromdate').val() == "") )
              {
                var param = {};
                if($('#fromdate').val())
                  param['from'] = firstDate;
                if($('#todate').val() != "")
                  param['to'] = secondDate;

                $scope.working = true;
                $scope.message = 'Loading...'

                 $http.post('/api/CalcPokerHistory' , param )
                      .success(function(cbdata){

                            $scope.colors = [
                                  {
                                    backgroundColor: "rgba(159,204,0, 0.2)",
                                    pointBackgroundColor: "rgba(159,204,0, 1)",
                                    pointHoverBackgroundColor: "rgba(159,204,0, 0.8)",
                                    borderColor: "rgba(159,204,0, 1)",
                                    pointBorderColor: '#fff',
                                    pointHoverBorderColor: "rgba(159,204,0, 1)"
                                  },"rgba(250,109,33,0.5)","#9a9a9a","rgb(233,177,69)"
                                ];
                            $scope.labels = cbdata.bottombar;
                            $scope.series = ['사용자수', '리윤'];
                            $scope.data = [
                              cbdata.num_player,
                              cbdata.riyoun
                            ];

                            $scope.working = false;
                        

                      });
              }
              else
                alert("시작날자가 마지막날자보다 작아야 합니다.");
          }
          else
            alert("날자범위를 지정하여야 합니다.");
        }
      }

      function AnalyseByVisitorWhenClick()
      {
        var dayparam = {};
        var w_param = {};
        if($('#u_nickname').val() != "")
          w_param['ppeinfo.u_nickname'] = {'$regex': '.*'+$('#u_nickname').val()+'.*'};

        if($('#fromd').val() != "")
          dayparam['$gte'] = new Date($('#fromd').val());
        if($('#tod').val() != "")
          dayparam['$lt'] = new Date($('#tod').val());

        if( ($('#fromd').val() != "") || ($('#tod').val() != "") )
            w_param['event_time'] = dayparam;

        $scope.working1 = true;
        $scope.message = 'Loading...'

        $http.post('/api/getGResultByVisitorNum' , w_param)
                .success(function(ccdata){

                    $scope.working1 = false;
                    $scope.bigTotalItems = (10*Math.ceil(ccdata.ppenum/$scope.num_per_page)); 
                    AnalyseByVisitor();

                });
      }

      function AnalyseByVisitor()
      {

        var dayparam = {};
        var w_param = {};
        if($('#u_nickname').val() != "")
          w_param['ppeinfo.u_nickname'] = {'$regex': '.*'+$('#u_nickname').val()+'.*'};

        if($('#fromd').val() != "")
          dayparam['$gte'] = new Date($('#fromd').val());
        if($('#tod').val() != "")
          dayparam['$lt'] = new Date($('#tod').val());

        if( ($('#fromd').val() != "") || ($('#tod').val() != "") )
            w_param['event_time'] = dayparam;

        var p_param = {"currentpage" : parseInt($scope.bigCurrentPage) , "numperpage" : parseInt($scope.num_per_page)};

          $scope.working1 = true;
          $scope.message = 'Loading...'

        $http.post('/api/getGResultByVisitor' , {"w_param" : w_param , "p_param" : p_param})
                .success(function(ccdata){

                    console.log("ccdata : " + ccdata);

                    $scope.pdatas = ccdata.obj;

                    $scope.working1 = false;
                });


      }

      function day_filter(types)
      {
        console.log(types);
        
        switch(types)
        {
          case 31:
            
            var dto = new Date();
            var dfrom  = new Date();
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            doAnalyse();
            break;
          case 32:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-7);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            doAnalyse();
            break;
          case 33:
            var dto = new Date();
            var dfrom  = new Date();
            dfrom.setHours(0,0,0,0);
            dfrom.setDate(1);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            doAnalyse();
            break;
          case 34:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-90);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            doAnalyse();
            break;
          case 35:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-180);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            doAnalyse();
            break;
          case 36:
            var dto = new Date();
            var dfrom = "";
            console.log( dto.toISODateString() + " : " +  dfrom );
            $('#todate').val(dto);
            $('#fromdate').val(dfrom);
            doAnalyse();
            break;
           case 41:
            
            var dto = new Date();
            var dfrom  = new Date();
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#tod').val(dto);
            $('#fromd').val(dfrom);
            AnalyseByVisitorWhenClick();
            break;
          case 42:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-7);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#tod').val(dto);
            $('#fromd').val(dfrom);
            AnalyseByVisitorWhenClick();
            break;
          case 43:
            var dto = new Date();
            var dfrom  = new Date();
            dfrom.setHours(0,0,0,0);
            dfrom.setDate(1);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#tod').val(dto);
            $('#fromd').val(dfrom);
            AnalyseByVisitorWhenClick();
            break;
          case 44:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-90);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#tod').val(dto);
            $('#fromd').val(dfrom);
            AnalyseByVisitorWhenClick();
            break;
          case 45:
            var dto = new Date();
            var dfrom  = dto.toDateFromDays(-180);
            dfrom.setHours(0,0,0,0);
            console.log( dto.toISODateString() + " : " +  dfrom.toISODateString());
            $('#tod').val(dto);
            $('#fromd').val(dfrom);
            AnalyseByVisitorWhenClick();
            break;
          case 46:
            var dto = new Date();
            var dfrom = "";
            console.log( dto.toISODateString() + " : " +  dfrom );
            $('#tod').val(dto);
            $('#fromd').val(dfrom);
            AnalyseByVisitorWhenClick();
            break;
        }
      }

      function reset_filter()
      {
        $('#fromdate').val("");
        $('#todate').val("");

        //console.log("reset_filter");
      }

      function reset_filter1()
      {
        $('#fromd').val("");
        $('#tod').val("");
        $('#u_nickname').val("");

        //console.log("reset_filter");
      }
      // Simulate async data update
      /*$timeout(function () {
        $scope.data = [
          [28, 48, 40, 19, 86, 27, 90],
          [65, 59, 80, 81, 56, 55, 40]
        ];
      }, 3000);*/

      //end chart


    }

})();
