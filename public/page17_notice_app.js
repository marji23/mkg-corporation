(function (){

  angular
      .module('page17noticeApp', ['ngLoader' ,  'ui.bootstrap' , 'ngSanitize' , 'ui-notification',"kendo.directives" ])
      .directive('ckeditor', ['$parse', ckeditorDirective])
      .controller('page17noticeController', page17noticeController)
      .controller('UpdateHelpModal', UpdateHelpModal)
      .config(function(NotificationProvider) {
          NotificationProvider.setOptions({
              delay: 10000,
              startTop: 20,
              startRight: 10,
              verticalSpacing: 20,
              horizontalSpacing: 20,
              positionX: 'right',
              positionY: 'top'
          });
        });

        var setImmediate = window && window.setImmediate ? window.setImmediate : function (fn) {
    setTimeout(fn, 0);
  };

    function ckeditorDirective($parse) {
      return {
        restrict: 'A',
        require: ['ckeditor', 'ngModel'],
        controller: [
          '$scope',
          '$element',
          '$attrs',
          '$parse',
          '$q',
          ckeditorController
        ],
        link: function (scope, element, attrs, ctrls) {
          // get needed controllers
          var controller = ctrls[0]; // our own, see below
          var ngModelController = ctrls[1];

          // Initialize the editor content when it is ready.
          controller.ready().then(function initialize() {
            // Sync view on specific events.
            ['dataReady', 'change', 'blur', 'saveSnapshot'].forEach(function (event) {
              controller.onCKEvent(event, function syncView() {
                ngModelController.$setViewValue(controller.instance.getData() || '');
              });
            });

            controller.instance.setReadOnly(!! attrs.readonly);
            attrs.$observe('readonly', function (readonly) {
              controller.instance.setReadOnly(!! readonly);
            });

            // Defer the ready handler calling to ensure that the editor is
            // completely ready and populated with data.
            setImmediate(function () {
              $parse(attrs.ready)(scope);
            });
          });

          // Set editor data when view data change.
          ngModelController.$render = function syncEditor() {
            controller.ready().then(function () {
              // "noSnapshot" prevent recording an undo snapshot
              controller.instance.setData(ngModelController.$viewValue || '', {
                noSnapshot: true,
                callback: function () {
                  // Amends the top of the undo stack with the current DOM changes
                  // ie: merge snapshot with the first empty one
                  // https://docs.ckeditor.com/#!/api/CKEDITOR.editor-event-updateSnapshot
                  controller.instance.fire('updateSnapshot');
                }
              });
            });
          };
        }
      };
    }

    function ckeditorController($scope, $element, $attrs, $parse, $q) {
      var config = $parse($attrs.ckeditor)($scope) || {};
      var editorElement = $element[0];
      var instance;
      var readyDeferred = $q.defer(); // a deferred to be resolved when the editor is ready

      // Create editor instance.
      if (editorElement.hasAttribute('contenteditable') &&
          editorElement.getAttribute('contenteditable').toLowerCase() == 'true') {
        instance = this.instance = CKEDITOR.inline(editorElement, config);
      }
      else {
        instance = this.instance = CKEDITOR.replace(editorElement, config);
      }

      /**
       * Listen on events of a given type.
       * This make all event asynchronous and wrapped in $scope.$apply.
       *
       * @param {String} event
       * @param {Function} listener
       * @returns {Function} Deregistration function for this listener.
       */

      this.onCKEvent = function (event, listener) {
        instance.on(event, asyncListener);

        function asyncListener() {
          var args = arguments;
          setImmediate(function () {
            applyListener.apply(null, args);
          });
        }

        function applyListener() {
          var args = arguments;
          $scope.$apply(function () {
            listener.apply(null, args);
          });
        }

        // Return the deregistration function
        return function $off() {
          instance.removeListener(event, applyListener);
        };
      };

      this.onCKEvent('instanceReady', function() {
        readyDeferred.resolve(true);
      });

      /**
       * Check if the editor if ready.
       *
       * @returns {Promise}
       */
      this.ready = function ready() {
        return readyDeferred.promise;
      };

      // Destroy editor when the scope is destroyed.
      $scope.$on('$destroy', function onDestroy() {
        // do not delete too fast or pending events will throw errors
        readyDeferred.promise.then(function() {
          instance.destroy(false);
        });
      });
    }

    function UpdateHelpModal($scope, $http, $window , $rootScope , $uibModalInstance , $sce) {

      $scope.updateHelp = updateHelp;
      $scope.exitMsg = exitMsg;

      function init() {

        $scope.v = $rootScope.seleditem;
        $scope.mcke = { content : $rootScope.seleditem.hn_title };

      }
      init();

      function exitMsg()
      {
        $uibModalInstance.dismiss('cancel');
      }

      function updateHelp(val)
      {
        console.log("val : " + JSON.stringify(val));

        if($scope.mcke.content.length > 0)
        {
          if(val.hn_comment != undefined)
          {
            console.log("d : " + val.hn_type);
            var will_update = { "id" : val.hn_id , 
                                "obj" : {"hn_title" : $scope.mcke.content , "hn_comment" : val.hn_comment} 
                              };
            $http.post("/api/updateHelpDatas" , will_update)
                    .success(function(uhdata){
                      if(uhdata.result == true)
                      {
                        alert("성공적으로 보존되였습니다.");
                        $uibModalInstance.dismiss('cancel');
                        $rootScope.$emit("updateShow");
                      }
                    });
          }
          else
            alert("필수항목을 입력하여야 합니다.");
        }
        else
          alert("필수항목을 입력하여야 합니다.");

      }

    }

    function page17noticeController($uibModal , $rootScope , Notification ,$scope, $http, $window , $sce) {

      $scope.logout_func = logout_func;
      $scope.myinfo = myinfo;

      $scope.addHelps = addHelps;
      $scope.findHelpDatasWhenClick = findHelpDatasWhenClick;
      $scope.reset_filter = reset_filter;
      $scope.updateHelpDatas = updateHelpDatas;
      $scope.delHelpDatas = delHelpDatas;

      var wholeitems;
      $scope.onChangeNum_perPage = onChangeNum_perPage;
      $scope.pageChanged = pageChanged;


      var whole_notice;
      $scope.notify_clicked = notify_clicked;

      $scope.closeNav = closeNav;
     
      var audio = new Audio('./sounds/Maramba.mp3');

      var acdata = [];

      function init() {
        console.log("page17 notice initer");

        

        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 1)
                $window.location.href = '/index.html';
              else
              {
                console.log("aya~~")
                $http.get('/api/get_global_acdata')
                      .success(function(ggadata){
                        acdata = ggadata.acdata;
                        $scope.mycid = ggadata.mycid;
                        $scope.mygroup = ggadata.mygroup;
                        $scope.mydeeper = ggadata.mydeeper;

                        if(($scope.mydeeper == 1)&&(acdata[0].ac_gongji <2))
                          logout_func();

                        imp_allow();

                        load_sidebar();
                        $scope.topnav = {url : "TopNav.html"};
                        showAllStatus();

                        findHelpDatasWhenClick();

                      });
              }

            });

      }

      init();

      function imp_allow()
      {
          var mystat = {};
          var groupstat = {};
          var finalclc = {}; 
          console.log("$scope.mygroup : " + $scope.mygroup);

          acdata.forEach(function(raudata){
                          if(raudata.uinfo[0] != undefined)
                          {
                            if(raudata.uinfo[0].u_id == $scope.mycid)
                              mystat = raudata;
                          }
                          else if(raudata.ginfo[0] != undefined)
                          {
                            if(raudata.ginfo[0].g_id == $scope.mycid)
                              mystat = raudata;
                            if(raudata.ginfo[0].g_id == $scope.mygroup)
                              groupstat = raudata
                          }
                        });
          if($scope.mygroup < 100) // not manager grouop
              finalclc = mystat;
          else
              {
                
                finalclc = mystat; // init

                var counter = 0;
                for (var key in mystat) {
                    if(counter < 23)
                    {
                      if(mystat[key] == 0)
                        finalclc[key] = groupstat[key];
                      else
                        finalclc[key] = mystat[key];

                      //console.log("final : " + finalclc[key] + " <= group : " + groupstat[key] + " + mine : " + mystat[key]);
                    }  //
                    counter++;
                }
              }
              //console.log("final : " + finalclc.ac_zhong);
            $scope.fang = finalclc;

      }

      function load_sidebar()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                    switch(rsys.result.deep)
                    {
                      case 1:
                        $scope.template = {url : "sidebar.html"};
                        $scope.top = {url : "topbar.html"};
                       break;
                      case 2:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 3:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 4:
                        $scope.template = {url : "sidebar3.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                    }

                    setStatusboxWidth();
                  });
      }

      function myinfo()
      {

        $http.get('/api/set_myinfo')
            .success(function(cbdata){
              
              console.log("_kekeke : " + cbdata[0].savepay);

                var fnum = parseInt(cbdata[0].orig_gid);
                if( fnum == 2 )
                  $window.location.href = '/server-page8(edit&detail).html';
                if( fnum > 2 )
                  $window.location.href = '/server-page15(edit&detail).html';
            });
      }

      function logout_func()
      {

        console.log("logouted");
        $http.get('/api/logout_func')
            .success(function(cbdata){
              if(cbdata.logresult == "success")
                $window.location.href = '/index.html';
            });
      }

      //start main page func

      $rootScope.$on("updateShow", function(val){
           
          findHelpDatasWhenClick();

         });

      function updateHelpDatas(seleditem)
      {
        console.log("yayaya~ : " + seleditem);
          $rootScope.seleditem = seleditem;

          var modalInstance = $uibModal.open({
              animation: true,
              templateUrl: 'dialogs/updateNotice_dlg.html',
              controller: 'UpdateHelpModal',
              windowClass: 'center-modal',
              resolve: {
               //   items: function () {
                //    return $ctrl.items;
                 // }
              }
          });

          modalInstance.result.then(function (selectedItem) {}, function () {
              //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
          });
      }

      function delHelpDatas(seleditem)
      {
        $http.post("/api/delHelpDatas" , seleditem)
              .success(function(dhddata){
                if(dhddata.result == true)
                  {
                    alert("삭제되였습니다.");
                    findHelpDatasWhenClick();
                  }
              });

      }

      function addHelps()
      {
        console.log("ckeditor : " + $("#hcomment").val().length);
        
        var add_data = {};
        if($scope.cke.content.length > 0)
        {
          add_data['hn_title'] = $scope.cke.content;
          if($("#hcomment").val().length > 2)
          {

            add_data['hn_comment'] = $("#hcomment").val();
            add_data['hn_type'] = 0;
            add_data['hn_date'] = new Date();
            $http.get("/api/getMaxHNid")
                  .success(function(hnmax)
                  {
                    add_data['hn_id'] = hnmax.result;

                      $http.post("/api/addHelps", { "obj" :  add_data })
                            .success(function(ahdata){
                              if(ahdata.result == true)
                              {
                                alert("성공적으로 추가되였습니다.");
                                findHelpDatasWhenClick();
                              }
                            });
                  });
          }
          else
            alert("필수항목들을 입력하십시오.");
        }
        else
          alert("필수항목들을 입력하십시오.");
        

      }

      $scope.num_per_page = 10;
      $scope.maxSize = 3;
      $scope.bigTotalItems = 10;
      $scope.bigCurrentPage = 1;

      function pageChanged() {

          findHelpDatas();
      }

      function onChangeNum_perPage(){

        findHelpDatasWhenClick();

      }

      function findHelpDatasWhenClick()
      {
        var fil_param = {};
        if($("#shtitle").val() != "")
          fil_param['hn_title'] = {'$regex': '.*'+$("#shtitle").val()+'.*'};

        if($("#shcomment").val() != "")
          fil_param['hn_comment'] = {'$regex': '.*'+$("#shcomment").val()+'.*'};

          fil_param['hn_type'] = 0;

        console.log("fil_param : " + JSON.stringify(fil_param));

        $http.post("/api/findHelpDatasNum" , {"obj" : fil_param})
              .success(function(fhddata){

                console.log("fhttttdata : " + fhddata.totalamount);
                $scope.bigTotalItems = (10*Math.ceil(fhddata.totalamount/$scope.num_per_page)); 
                findHelpDatas();

              })
      }

      function findHelpDatas()
      {


        var fil_param = {};
        if($("#shtitle").val() != "")
          fil_param['hn_title'] = {'$regex': '.*'+$("#shtitle").val()+'.*'};

        if($("#shcomment").val() != "")
          fil_param['hn_comment'] = {'$regex': '.*'+$("#shcomment").val()+'.*'};

          fil_param['hn_type'] = 0;

        var pvobj = {};

        pvobj['currentpage'] = parseInt($scope.bigCurrentPage);
        pvobj['numperpage'] = parseInt($scope.num_per_page);

        console.log("fil_param : " + JSON.stringify(fil_param));

        $http.post("/api/findHelpDatas" , {"obj" : fil_param , "pvobj" : pvobj})
              .success(function(fhddata){
                var final_hns = fhddata.result;

                final_hns.forEach(function(hnitem){
                  hnitem.hn_strdate = (new Date(hnitem.hn_date)).toLocaleString();
                  hnitem['newmyhtml'] = $sce.trustAsHtml(hnitem.hn_title);
                });

                $scope.hns = final_hns;
              })

      }

      function reset_filter()
      {
        $("#shtitle").val('');
        $("#shcomment").val('');

      }

      //end main page func

      //start for notice on the top bar

      function checkForSounds(typesound)
      {
        $http.get('/api/getSoundsjson')
            .success(function(sdata)
            {
              if(typesound == 1)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) || (sdata.obj.notify < $scope.len ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
              else if(typesound == 2)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
            });
      }

      function load_notice()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    if(rsys.result.deep == 1){
                      console.log("dfdf");
                     $http.get('/api/get_all_notice')
                        .success(function(cbdata){
                            console.log("rusuky : " + cbdata.obj);
                            whole_notice = cbdata.obj;
                            whole_notice = whole_notice.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

                            var unreads = [];
                            whole_notice.forEach(function(item){
                              if(item.am_status == 0)
                                {
                                  unreads.push(item);
                                  console.log(JSON.stringify(item));
                                }
                            });

                            $scope.ur = unreads;
                            $scope.len = unreads.length;

                            checkForSounds(1);

                        });
                    }
                    else
                      checkForSounds(2);

                  });
      }

      function notify_clicked(iobj)
      {
        whole_notice.forEach(function(item){
          if(item == iobj)
            item.am_status = 1;
        }); 
        $http.post('/api/update_all_notice' , { obj : whole_notice } )
                .success(function(cbdata){
                  if(cbdata.result == true){

                      $http.get('/api/getAllalarm' )
                        .success(function(cbdata1){
                            var atypes = cbdata1.obj;
                            var contents = "";
                            var titles = "";
                            for(var i=0;i<atypes.length;i++)
                              if(atypes[i].a_id == iobj.am_content)
                              {
                                contents = atypes[i].a_content.a_en;
                                titles = atypes[i].a_title.a_en;
                              }

                               var mbody = "<p> 위반대상 : "+ iobj.am_target +" </p>"+
                               "<p> 위반대상 식별자 : "+ iobj.am_id +" </p>"+
                              "<p> 위반날자 : "+ iobj.am_date.toLocaleString().split("T")[0] + " " + iobj.am_date.toLocaleString().split("T")[1] +"</p>"+
                              "<p> 위반내용 : "+ contents +" </p> ";
          Notification.success({title : '<h3 style="text-align: center;"><i style=" -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: orange; color: rgba(187, 48, 23, 0.63); font-size : 42px;" class="fa fa-bell"></i></h3>' ,  message: mbody , delay: 10000});
                              
                              load_notice();
                          });
                  }
                });
      }

      //end for notice on the top bar

      //start show all status on the top bar
      function showAllStatus()
      {
        console.log("yayaya~~~hahaha");
        $http.get("/api/setAFlagZero")
                .success(function(afz){
                  if(afz.result == true)
                  {
                    $http.get("/api/getAllStatus")
                          .success(function(cbdata)
                          {

                            console.log("rat : " + JSON.stringify(cbdata));
                            $scope.as = cbdata;

                            load_notice();

                          });
                  }
                });
        
      }

      function closeNav(snd)
      {
        console.log("heyheyhey");
        var x = document.getElementById("myTopnav");
        x.className = "topnav";

        var flagf;
        if( ((3<=snd)&&(snd<=6)) || (snd == 13) || (snd == 11) )
        {
          flagf = 1;
        }
        else
        {
          flagf = 2;
        }
        $http.post("/api/setSoundsflag" , {obj : flagf})
                .success(function(cbdata)
                {
                  if((3<=snd)&&(snd<=10))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 3:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 4:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 5:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 6:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 7:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 8:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                            case 9:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 10:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page2.html';
                                });
                          
                        }
                        else if((snd == 11) || (snd == 12))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 11:
                              goParam = { cstatus : '3' , rouflag : 999};
                              break;
                            case 12:
                              goParam = { cstatus : '2' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page10.html';
                                });
                          
                        }
                        else if(snd == 1)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page7.html';
                                });
                          
                        }
                        else if(snd == 2)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page8.html';
                                });
                          
                        }

                });

      }

      function setStatusboxWidth()
      {
        console.log("wow excellent.. : "+( parseInt($(".page-container").width()) - 390 ));
        var gijuns = parseInt($(".page-container").width());
        var d_amount;
        if(gijuns<768)
          d_amount = 200;
        else
          d_amount = 400;

        var mywidth = gijuns - d_amount;

        $scope.statusbox={'width':mywidth+'px'};
      }

      $(window).resize(function(){
          
        setStatusboxWidth();

      });

      //end show all status on the top bar

      //start test for transfer

      // handles the callback from the received event
        var handleCallback = function (msg) {
            $scope.$apply(function () {
                //$scope.msg = JSON.parse(msg.data)
                var r_obj = JSON.parse(msg.data);
                console.log("receiver : " + r_obj.flag);
                
                if(r_obj.flag != 0)
                {
                  if(r_obj.flag != 100)
                  {
                    console.log("will chekc for target~");
                    $http.post("/api/CheckForEmitTarget" , { cfeparam : r_obj.shcdata }) // check if this is logined id which is correct target to listen the emit socket signal
                          .success(function(cfedata)
                          {
                            if(cfedata.result == true) // this is correct target
                            {
                                  if(cfedata.flag == 1)
                                  {
                                    console.log("will find....");
                                    showAllStatus();
                                    
                                  }
                                  else if(cfedata.flag == 2){
                                    $http.get("/api/showShopRWmsm")
                                          .success(function(ssrw){
                                            alert(ssrw.msmbody);
                                          });
                                    //console.log("listening for change.....");
                                  }
                            }

                          });
                  }
                  else if(r_obj.flag == 100) // its self signal
                  {
                    console.log("will find....");
                    showAllStatus();
                  }
                  
                }
            });
        }
 
        var source = new EventSource('/stats');
        source.addEventListener('message', handleCallback, false);

      //end test for transfer

    }

})();
