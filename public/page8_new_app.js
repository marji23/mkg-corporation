(function (){

  angular
      .module('page8_new_App', ['ui-notification','ngRoute','ngAnimate', 'ngSanitize', 'ui.bootstrap' ])
      .controller('page8_new_Controller', Page8_new_Controller)
      .controller('UpdateModal', UpdateModal)
      .controller('NewModal', NewModal)
      .config(function(NotificationProvider) {
          NotificationProvider.setOptions({
              delay: 10000,
              startTop: 20,
              startRight: 10,
              verticalSpacing: 20,
              horizontalSpacing: 20,
              positionX: 'right',
              positionY: 'top'
          });
        });


    function UpdateModal($scope, $http, $window , $rootScope , $uibModalInstance) {

      $scope.updateDM = updateDM;
      $scope.exitMsg = exitMsg;



      function init() {

        $scope.v = $rootScope.sel_dmobj;

      }
      init();

      function exitMsg()
      {
        $uibModalInstance.dismiss('cancel');
      }

      function CheckIsValidDomain(domain) { 
          var re = new RegExp(/^((?:(?:(?:\w[\.\-\+]?)*)\w)+)((?:(?:(?:\w[\.\-\+]?){0,62})\w)+)\.(\w{2,6})$/); 
          if(domain.match(re))
            return true;
          else
          {
            alert("도메인형식에 맞게 입력하십시오!");
            return false;
          }
          return domain.match(re);
      } 

      function ValidateIPaddress(inputText)
      {
         var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
         if(inputText.match(ipformat))
         {
           return true;
         }
         else
         {
           alert("IP형식에 맞게 입력하십시오.!");
          return false;
         }
      }

      function updateDM(val)
      {
        if( val != undefined )
        {
          console.log("val : " + $('#svertype').val());
          if( (val.dm_domain_name != undefined) && (val.dm_ipaddress != undefined) && ($('#svertype').val() != '? string: ?') )
          {
            if((ValidateIPaddress(val.dm_ipaddress) == true)&&(CheckIsValidDomain(val.dm_domain_name) == true))
            {
              console.log("val : " + val.dm_domain_name);
              alert("최종 보존을 하시려면 보존단추를 눌러주십시오.");
              $uibModalInstance.dismiss('cancel');
            }
          }
          else
            alert("도메인 정보를 구체적으로 입력하십시오.");
        }
        else
          alert("도메인 정보를 구체적으로 입력하십시오.");

      }

    }

    function NewModal($scope, $http, $window , $rootScope , $uibModalInstance) {

      $scope.newDM = newDM;
      $scope.exitMsg = exitMsg;



      function init() {


      }
      init();

      function exitMsg()
      {
        $uibModalInstance.dismiss('cancel');
      }

      function CheckIsValidDomain(domain) { 
          var re = new RegExp(/^((?:(?:(?:\w[\.\-\+]?)*)\w)+)((?:(?:(?:\w[\.\-\+]?){0,62})\w)+)\.(\w{2,6})$/); 
          if(domain.match(re))
            return true;
          else
          {
            alert("도메인형식에 맞게 입력하십시오!");
            return false;
          }
          return domain.match(re);
      } 

      function ValidateIPaddress(inputText)
      {
         var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
         if(inputText.match(ipformat))
         {
           return true;
         }
         else
         {
           alert("IP형식에 맞게 입력하십시오.!");
          return false;
         }
      }

      function newDM(val)
      {
        console.log("val : " + JSON.stringify(val));
        if( val != undefined )
        {
          if( (val.dm_domain_name != undefined) && (val.dm_ipaddress != undefined) && (val.dm_service_type != undefined) )
          {
            if((ValidateIPaddress(val.dm_ipaddress) == true)&&(CheckIsValidDomain(val.dm_domain_name) == true))
            {
              alert("최종 보존을 하시려면 보존단추를 눌러주십시오.");
              $rootScope.transferer = val;
              $rootScope.$emit("updateShow", "val");
              $uibModalInstance.dismiss('cancel');
            }
          }
          else
            alert("도메인 정보를 구체적으로 입력하십시오.");
        }
        else
          alert("도메인 정보를 구체적으로 입력하십시오.");
      }

    }

    function Page8_new_Controller(Notification ,$uibModal, $log, $document, $rootScope, $location , $scope, $http, $window) {

      $scope.logout_func = logout_func;
      $scope.addNewShop = addNewShop;
      $scope.myinfo = myinfo;
      $scope.uppershops = uppershops;

      $scope.newDomain = newDomain;
      $scope.editModal = editModal;
      $scope.deleteModal = deleteModal;

      $scope.closeUpdate = closeUpdate;

      var s_lvl_name = ["본사" , "부본사" , "총판" , "대리"];

      //$scope.updateVisitor = updateVisitor;

      var whole_notice;
      $scope.notify_clicked = notify_clicked;

      $scope.closeNav = closeNav;
     
      var audio = new Audio('./sounds/Maramba.mp3');

      var acdata = [];

      function init() {
        console.log("page8-new-hey initer");

        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 1)
                $window.location.href = '/index.html';
              else
              {
                console.log("aya~~")
                $http.get('/api/get_global_acdata')
                      .success(function(ggadata){
                        acdata = ggadata.acdata;
                        $scope.mycid = ggadata.mycid;
                        $scope.mygroup = ggadata.mygroup;
                        $scope.mydeeper = ggadata.mydeeper;

                        if(($scope.mydeeper == 1)&&(acdata[0].ac_shop <3))
                          logout_func();

                        imp_allow();

                        load_sidebar();
                        $scope.topnav = {url : "TopNav.html"};
                        showAllStatus();

                        $scope.dlist = [];
                        loadShopList();

                      });
              }

            });

      }

      init();

      function imp_allow()
      {
          var mystat = {};
          var groupstat = {};
          var finalclc = {}; 
          console.log("$scope.mygroup : " + $scope.mygroup);

          acdata.forEach(function(raudata){
                          if(raudata.uinfo[0] != undefined)
                          {
                            if(raudata.uinfo[0].u_id == $scope.mycid)
                              mystat = raudata;
                          }
                          else if(raudata.ginfo[0] != undefined)
                          {
                            if(raudata.ginfo[0].g_id == $scope.mycid)
                              mystat = raudata;
                            if(raudata.ginfo[0].g_id == $scope.mygroup)
                              groupstat = raudata
                          }
                        });
          if($scope.mygroup < 100) // not manager grouop
              finalclc = mystat;
          else
              {
                
                finalclc = mystat; // init

                var counter = 0;
                for (var key in mystat) {
                    if(counter < 23)
                    {
                      if(mystat[key] == 0)
                        finalclc[key] = groupstat[key];
                      else
                        finalclc[key] = mystat[key];

                      //console.log("final : " + finalclc[key] + " <= group : " + groupstat[key] + " + mine : " + mystat[key]);
                    }  //
                    counter++;
                }
              }
              //console.log("final : " + finalclc.ac_zhong);
            $scope.fang = finalclc;

      }

      function load_sidebar()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                    switch(rsys.result.deep)
                    {
                      case 1:
                        $scope.template = {url : "sidebar.html"};
                        $scope.top = {url : "topbar.html"};
                       break;
                      case 2:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 3:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 4:
                        $scope.template = {url : "sidebar3.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                    }

                    setStatusboxWidth();
                  });
      }

      function closeUpdate()
        {
          location.reload();
        }

      function myinfo()
      {

        $http.get('/api/set_myinfo')
            .success(function(cbdata){
              
              console.log("_kekeke : " + cbdata[0].savepay);

                var fnum = parseInt(cbdata[0].orig_gid);
                if( fnum == 2 )
                  $window.location.href = '/server-page8(edit&detail).html';
                if( fnum > 2 )
                  $window.location.href = '/server-page15(edit&detail).html';
            });
      }

      function logout_func()
      {
        console.log("logouted");
        $http.get('/api/logout_func')
            .success(function(cbdata){
              if(cbdata.logresult == "success")
                $window.location.href = '/index.html';
            });
      }

      $rootScope.$on("updateShow", function(val){
           
          console.log("aa : " + JSON.stringify($rootScope.transferer) );
          $scope.dlist.push($rootScope.transferer);

         });

      function uppershops(nval)
      {
        console.log("hhh : "+nval );
        var tglobal_list = $scope.myglist;

        tglobal_list.forEach(function(item){
          if(item.u_id == nval)
          {
            console.log("found : " + item.u_deep);
            var c_deep = item.u_deep;
            var shoptype = [];  
            for(var i=c_deep; i< s_lvl_name.length; i++ )
                shoptype.push( { val :(i+1) , txt : s_lvl_name[i] } );

            $scope.shoptypes = shoptype;

          }
        });

      }

      

// - start make awesome tree structured upper shop list part

      function eachtext(nums , txt , sindex)
      {
        nums = 4*nums;
        var prefix = "|";
        for(i=0;i<nums;i++)
          prefix += "-";
        var suffix = " (" + s_lvl_name[sindex] + ") ";
        return prefix + ">" + txt + suffix;
      }

      function make_items_title(maxlvl , global_list)
      {
        console.log("txt_res : " + s_lvl_name.length);
        var inc = s_lvl_name.length - maxlvl -1;
        global_list.forEach(function(item){

          //item['su_text'] = eachtext( item['su_lvlnum'] , item.u_nickname , (item['su_lvlnum'] + inc) );
          item['su_text'] = eachtext( item['su_lvlnum'] , item.u_nickname , item.u_deep-1 );
          console.log('su : ' + item.su_text);
        });

        

        $http.get('/api/get_sess_info')
            .success(function(cbdata){
                  
                  var c_deep = cbdata.result;
                  var shoptype = [];  
                  for(var i=c_deep; i< s_lvl_name.length; i++ )
                    shoptype.push( { val :(i+1) , txt : s_lvl_name[i] } );

                  
                  var templist = [];

                  global_list.forEach(function(ttitem){
                    if(ttitem.u_deep < 4)
                      templist.push(ttitem);
                  });

                  $scope.shoplists = templist;
                  $scope.shoptypes = shoptype;
            });

        $scope.myglist = global_list;

      }

      function make_Structured_menutitle( lvl , starter , arr , global_list , lvl_gijun )
      {
        $scope.cout++;
        
        if( (arr.length) == $scope.cout )
        {
            console.log("really finished~~" + lvl_gijun);
            make_items_title(lvl_gijun , global_list);
        }

        lvl++;

        arr.forEach(function(item){
          //console.log("item : " + item.u_uppercode + "starter" + starter.u_uppercode);
            if(item.u_uppercode == starter.u_id)
            {
              item['su_lvlnum'] = lvl;
              //console.log("l + :"+lvl + " - " + lvl_gijun);
              global_list.push(item);

              if(lvl_gijun < lvl)
                lvl_gijun = lvl;
              make_Structured_menutitle( lvl , item , arr , global_list , lvl_gijun );

            }
            else{

                
            }
        });
      }

      function get_Structured_shoplist(sflag , arr)
      {
        var start_v;
        var global_list = [];

        if(sflag == 0)
        {
          start_v = arr[0];
          arr[0]['su_lvlnum'] = 0;
          global_list.push(arr[0]);
        }
        else if(sflag == 1)
        {
          arr.forEach(function(item){
            if(item.u_uppercode == "1")
            {
              start_v = item;
              item['su_lvlnum'] = 0;
              global_list.push(item);
            }
          });
        }

        console.log("startv : " + start_v.u_nickname);

        $scope.cout = 0;
        var lvl_gijun = 0;
        
        make_Structured_menutitle( 0 , start_v , arr , global_list , lvl_gijun);

      }

// - end make awesome tree structured upper shop list part

      function loadAllShoplist()
      {
          //console.log("loadshoplist");
          $http.post('/api/getRealAllChildshop')
            .success(function(cbdata){
              console.log('rara : '+cbdata);
              get_Structured_shoplist(1 , cbdata);
              //$scope.shoplists = cbdata;

              
            });
      }

      function loadShopList()
      {
        var forshopcase = [];
        $http.get('/api/getDocofLogined')
            .success(function(cbdata1){
              if(cbdata1[0].u_groupid == '2') {
                console.log("shop");

                $http.post('/api/getRealChildshop')
                    .success(function(cbdata){

                      //forshopcase.push(cbdata1[0]);
                      cbdata.obj.splice(0, 0, cbdata1[0]);
                      //cbdata.obj.push(cbdata1[0]);
                      //console.log("mememememe :     "+ cbdata1[0].u_nickname);
                      forshopcase = cbdata.obj;
                      //console.log("come : "+forshopcase.length);

                      //$scope.shoplists = forshopcase;
                      get_Structured_shoplist(0 , forshopcase);
                      

                    });

              }
              else{
                console.log("manager");
                loadAllShoplist();
              }
            });
      }

      function addNewShop(v , cpw , dlist)
      {

        $scope.isdisable = 1;
        
        console.log("deep : " + $('#u_deep').val() );
        if($('#u_deep').val() == null)
        {
          alert("대리는 산하에 그 어떤 종류의 대리도 가질수 없습니다.");
          $scope.isdisable = 0;
        }
        if(v != undefined)
        {
          if((v.u_id != undefined) && (v.u_nickname != undefined) && (v.u_pw != undefined) && (v.u_email != undefined))
          {
            if ( (v.u_id.length >= 2) && (v.u_nickname.length >= 2) && (v.u_pw.length >= 2) && (cpw.length >= 2) && (v.u_email.length != undefined) && ($('#u_uppercode').val().length >= 2) && ($('#u_deep').val() != null) )
            {
                if(v.u_pw == cpw){
                  v['u_groupid'] = 2;
                  v['u_flag'] = 'a';
                  v['u_logcount'] = 0;
                  v['u_uppercode'] = $('#u_uppercode').val();
                  v['u_registerdate'] = new Date();
                  v['u_ticheng1'] = parseFloat(v['u_ticheng1']);
                  v['u_ticheng2'] = parseFloat(v['u_ticheng2']);
                  v['u_loginip'] = "";
                  v['u_deep'] = parseInt($('#u_deep').val());
                  

                  $http.post('/api/usidcheck/' , {u_id : v.u_id})
                    .success(function(cbdata){

                          if(cbdata.result == true){
                            //$('#commenter').val("There is some user id , please try again with other id");
                            alert("There is some user id , please try again with other id");
                            $scope.isdisable = 0;
                          }
                          else {
                                  var domain_arr = $scope.dlist;
                                  var svtype = "";

                                  domain_arr.forEach(function(item) {

                                      item.dm_domain_name = item.dm_domain_name.replace(/\s/g, '');
                                      item.dm_ipaddress = item.dm_ipaddress.replace(/\s/g, '');

                                          
                                      item.dm_service_houseid = v.u_id;         
                                     
                                  });
                                    $http.post('/api/checkInDlist' , { dobj : domain_arr })
                                                  .success(function(cbdata1){
                                                    console.log("re : " + cbdata1.result);
                                                      if(cbdata1.result == true)
                                                      {
                                                            domain_arr.forEach(function(item) {
                                                                    if(item.dm_service_type == "self" )
                                                                            svtype = "2";
                                                                    if(item.dm_service_type == "market" )
                                                                            svtype = "3";

                                                                item.dm_service_type = svtype;
                                                            });

                                                          console.log("domain_arr : " + JSON.stringify(domain_arr) );
                                                            
                                                        var up_obj = { shopinfo : v , domain_handle : domain_arr };

                                                            $http.post('/api/add_shop' , up_obj)
                                                              .success(function(cbdata2){

                                                                if(cbdata2.result == true){
                                                                  console.log(v);
                                                                  alert("Success to register!!!");
                                                                  location.reload();

                                                                  $scope.isdisable = 0;
                                                                }

                                                              });
                                                      }
                                                      else
                                                      {
                                                        alert("중복되는 도메인 이 존재하므로 다른 도메인을 리용합십시오.");
                                                        $scope.isdisable = 0;
                                                      }
                                                  });

                                 
                                }
                  
                      });
                }
                else
                {
                  alert("input same password!!!");
                  $scope.isdisable = 0;
                }
              }
              else
              {
                alert("you must input some fields!!!");
                $scope.isdisable = 0;
              }
            }
            else
              {
                alert("you must input some fields!!!");
                $scope.isdisable = 0;
              }
          }
          else
              {
                alert("you must input some fields!!!");
                $scope.isdisable = 0;
              }
        }

        function newDomain()
        {

          var modalInstance = $uibModal.open({
              animation: true,
              templateUrl: 'dialogs/dm_new.html',
              controller: 'NewModal',
              windowClass: 'center-modal',
              resolve: {
               //   items: function () {
                //    return $ctrl.items;
                 // }
              }
          });

          modalInstance.result.then(function (selectedItem) {}, function () {
              //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
          });
        }

        function editModal(sel_dmobj)
        {
          console.log("yayaya~ : " + sel_dmobj);
          $rootScope.sel_dmobj = sel_dmobj;

          var modalInstance = $uibModal.open({
              animation: true,
              templateUrl: 'dialogs/dm_update.html',
              controller: 'UpdateModal',
              windowClass: 'center-modal',
              resolve: {
               //   items: function () {
                //    return $ctrl.items;
                 // }
              }
          });

          modalInstance.result.then(function (selectedItem) {}, function () {
              //$log.info('Set WithrawlPaswordDialog Modal dismissed at: ' + new Date());
          });
        }

        function deleteModal(sel_dmobj)
        {
          var alldm = $scope.dlist;

          alldm.forEach(function(item, index, object) {
                if (item.dm_domain_name === sel_dmobj.dm_domain_name) {
                  object.splice(index, 1);
                }
              });

          $scope.dlist = alldm;

        }

        //start for notice on the top bar

      function checkForSounds(typesound)
      {
        $http.get('/api/getSoundsjson')
            .success(function(sdata)
            {
              if(typesound == 1)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) || (sdata.obj.notify < $scope.len ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
              else if(typesound == 2)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
            });
      }

      function load_notice()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    if(rsys.result.deep == 1){
                      console.log("dfdf");
                     $http.get('/api/get_all_notice')
                        .success(function(cbdata){
                            console.log("rusuky : " + cbdata.obj);
                            whole_notice = cbdata.obj;
                            whole_notice = whole_notice.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

                            var unreads = [];
                            whole_notice.forEach(function(item){
                              if(item.am_status == 0)
                                {
                                  unreads.push(item);
                                  console.log(JSON.stringify(item));
                                }
                            });

                            $scope.ur = unreads;
                            $scope.len = unreads.length;

                            checkForSounds(1);
                             

                        });
                    }
                    else
                      checkForSounds(2);

                  });
      }

      function notify_clicked(iobj)
      {
        whole_notice.forEach(function(item){
          if(item == iobj)
            item.am_status = 1;
        }); 
        $http.post('/api/update_all_notice' , { obj : whole_notice } )
                .success(function(cbdata){
                  if(cbdata.result == true){

                      $http.get('/api/getAllalarm' )
                        .success(function(cbdata1){
                            var atypes = cbdata1.obj;
                            var contents = "";
                            var titles = "";
                            for(var i=0;i<atypes.length;i++)
                              if(atypes[i].a_id == iobj.am_content)
                              {
                                contents = atypes[i].a_content.a_en;
                                titles = atypes[i].a_title.a_en;
                              }

                               var mbody = "<p> 위반대상 : "+ iobj.am_target +" </p>"+
                               "<p> 위반대상 식별자 : "+ iobj.am_id +" </p>"+
                              "<p> 위반날자 : "+ iobj.am_date.toLocaleString().split("T")[0] + " " + iobj.am_date.toLocaleString().split("T")[1] +"</p>"+
                              "<p> 위반내용 : "+ contents +" </p> ";
          Notification.success({title : '<h3 style="text-align: center;"><i style=" -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: orange; color: rgba(187, 48, 23, 0.63); font-size : 42px;" class="fa fa-bell"></i></h3>' ,  message: mbody , delay: 10000});
                              
                              load_notice();
                          });
                  }
                });
      }

      //end for notice on the top bar

      //start show all status on the top bar
      function showAllStatus()
      {
        console.log("yayaya~~~hahaha");
        $http.get("/api/setAFlagZero")
                .success(function(afz){
                  if(afz.result == true)
                  {
                    $http.get("/api/getAllStatus")
                          .success(function(cbdata)
                          {

                            console.log("rat : " + JSON.stringify(cbdata));
                            $scope.as = cbdata;

                            load_notice();

                          });
                  }
                });
        
      }

      function closeNav(snd)
      {
        console.log("heyheyhey");
        var x = document.getElementById("myTopnav");
        x.className = "topnav";

        var flagf;
        if( ((3<=snd)&&(snd<=6)) || (snd == 13) || (snd == 11) )
        {
          flagf = 1;
        }
        else
        {
          flagf = 2;
        }
        $http.post("/api/setSoundsflag" , {obj : flagf})
                .success(function(cbdata)
                {
                  if((3<=snd)&&(snd<=10))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 3:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 4:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 5:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 6:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 7:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 8:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                            case 9:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 10:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page2.html';
                                });
                          
                        }
                        else if((snd == 11) || (snd == 12))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 11:
                              goParam = { cstatus : '3' , rouflag : 999};
                              break;
                            case 12:
                              goParam = { cstatus : '2' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page10.html';
                                });
                          
                        }
                        else if(snd == 1)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page7.html';
                                });
                          
                        }
                        else if(snd == 2)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page8.html';
                                });
                          
                        }

                });

      }

      function setStatusboxWidth()
      {
        console.log("wow excellent.. : "+( parseInt($(".page-container").width()) - 390 ));
        var gijuns = parseInt($(".page-container").width());
        var d_amount;
        if(gijuns<768)
          d_amount = 200;
        else
          d_amount = 400;

        var mywidth = gijuns - d_amount;

        $scope.statusbox={'width':mywidth+'px'};
      }

      $(window).resize(function(){
          
        setStatusboxWidth();

      });

      //end show all status on the top bar

      //start test for transfer

      // handles the callback from the received event
        var handleCallback = function (msg) {
            $scope.$apply(function () {
                //$scope.msg = JSON.parse(msg.data)
                var r_obj = JSON.parse(msg.data);
                console.log("receiver : " + r_obj.flag);
                
                if(r_obj.flag != 0)
                {
                  if(r_obj.flag != 100)
                  {
                    console.log("will chekc for target~");
                    $http.post("/api/CheckForEmitTarget" , { cfeparam : r_obj.shcdata }) // check if this is logined id which is correct target to listen the emit socket signal
                          .success(function(cfedata)
                          {
                            if(cfedata.result == true) // this is correct target
                            {
                                  if(cfedata.flag == 1)
                                  {
                                    console.log("will find....");
                                    showAllStatus();
                                    
                                  }
                                  else if(cfedata.flag == 2){
                                    $http.get("/api/showShopRWmsm")
                                          .success(function(ssrw){
                                            alert(ssrw.msmbody);
                                          });
                                    //console.log("listening for change.....");
                                  }
                            }

                          });
                  }
                  else if(r_obj.flag == 100) // its self signal
                  {
                    console.log("will find....");
                    showAllStatus();
                  }
                  
                }
            });
        }
 
        var source = new EventSource('/stats');
        source.addEventListener('message', handleCallback, false);

      //end test for transfer
        
        
      }

})();
