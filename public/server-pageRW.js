(function (){

  angular
      .module('pageRW_App', ['ngLoader' ,  'ui.bootstrap' , 'ui-notification' , 'currencyFormat' , 'ngSanitize'])
      .controller('pageRW_Controller', pageRW_Controller)
      .config(function(NotificationProvider) {
          NotificationProvider.setOptions({
              delay: 10000,
              startTop: 20,
              startRight: 10,
              verticalSpacing: 20,
              horizontalSpacing: 20,
              positionX: 'right',
              positionY: 'top'
          });
        })
      .filter('numberEx', ['numberFilter', '$locale',
          function(number, $locale) {

            var formats = $locale.NUMBER_FORMATS;
            return function(input, fractionSize) {
              //Get formatted value
              var formattedValue = number(input, fractionSize);

              //get the decimalSepPosition
              var decimalIdx = formattedValue.indexOf(formats.DECIMAL_SEP);
              
              //If no decimal just return
              if (decimalIdx == -1) return formattedValue;

         
              var whole = formattedValue.substring(0, decimalIdx);
              var decimal = (Number(formattedValue.substring(decimalIdx)) || "").toString();
              
              return whole +  decimal.substring(1);
            };
          }
        ]);



    function pageRW_Controller(Notification ,$scope, $http, $window , currencyFormatService , $sce) {

      $scope.logout_func = logout_func;
      $scope.myinfo = myinfo;
      $scope.opclic = opclic;
      $scope.opclic1 = opclic1;
      $scope.changed_method = changed_method;



      function opclic(optype)
      {
        $scope.optionval = optype;

        $http.get('/api/getMyUpperCardInfo')
              .success(function(gmuci){
                console.log("card data : " + JSON.stringify(gmuci.cardsinfo));
                var tempinfo = gmuci.cardsinfo;
                var cards_txt = "";
                if ($('#sreq_method').is(":checked")) // only for static method
                {
                  switch($scope.optionval)
                  {
                    case 1: // union
                      if(tempinfo.union != undefined)
                        cards_txt = "Bank Name : " + tempinfo.union.bankname + "<br/>" +
                                    "Bank ID : " + tempinfo.union.id + "<br/>" +
                                    "Bank Username : " + tempinfo.union.username;
                      break;
                    case 2: //zhifubao
                      if(tempinfo.zh != undefined)
                        cards_txt = "Account ID : " + tempinfo.zh.id + "<br/>" +
                                    "Account Username : " + tempinfo.zh.username;
                      break;
                    case 3: //wechat or weixin
                      if(tempinfo.wx != undefined)
                        cards_txt = "Account ID : " + tempinfo.wx.id + "<br/>" +
                                    "Account Username : " + tempinfo.wx.username;
                      break;
                    case 4: // caifutong
                      if(tempinfo.ten != undefined)
                        cards_txt = "Account ID : " + tempinfo.ten.id + "<br/>" +
                                    "Account Username : " + tempinfo.ten.username;
                      break;
                    case 5: //paypal
                      if(tempinfo.pp != undefined)
                        cards_txt = "Account ID : " + tempinfo.pp.id + "<br/>" +
                                    "Account Username : " + tempinfo.pp.username;
                      break;
                  }
                  $scope.bankinfo_body = cards_txt;
                  if(cards_txt != "")
                    $scope.customHtml = $sce.trustAsHtml("<h4 class='block'>송금대상 정보</h4><p>"+cards_txt+"</p>");
                  else
                    $scope.customHtml = $sce.trustAsHtml("<h4>상위대리점에 이 카드에 해당한 계정이 없으므로 송금할수 없습니다.</h4>");
                }
              });

      }

      function opclic1(optype)
      {
        $scope.optionval1 = optype;
      }

      $scope.reset = reset;
      $scope.doSaveRequest = doSaveRequest;
      $scope.doPayRequest = doPayRequest;
      $scope.cancelReq = cancelReq;
      var wei_str = "";

      var whole_notice;
      $scope.notify_clicked = notify_clicked;

      $scope.closeNav = closeNav;

      var audio = new Audio('./sounds/Maramba.mp3');

      var wholeitems;
      $scope.onChangeNum_perPage = onChangeNum_perPage;
      $scope.pageChanged = pageChanged;

      var wholeitems1;
      $scope.onChangeNum_perPage1 = onChangeNum_perPage1;
      $scope.pageChanged1 = pageChanged1;

      var status_txt = ["승인" , "취소" , "대기" , "온라인지불됨"];
      var shopbalance = 0;

      function init() {
        console.log("pageRW initer");

        $http.get('/api/session_check')
            .success(function(cbdata){
              console.log("cbdata : " + cbdata.flag);
              if(cbdata.flag == 1)
                $window.location.href = '/index.html';

              load_sidebar();

              $scope.topnav = {url : "TopNav.html"};
            });

            showAllStatus();

            convertCurrency();

            $("#sreq_method").prop('checked', true);

            showRWReqResultWhenClick(1);

            showRWReqResultWhenClick(2);

            loadBalance();

            changed_method();

            opclic(1);
            opclic1(1);
            
            //$scope.opsets1 = '2';
            //$scope.opsets = '1';

      }

      init();

      function load_sidebar()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    console.log("rsys : " + rsys.result);
                    switch(rsys.result.deep)
                    {
                      case 1:
                        $scope.template = {url : "sidebar.html"};
                        $scope.top = {url : "topbar.html"};
                       break;
                      case 2:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 3:
                        $scope.template = {url : "sidebar2.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                      case 4:
                        $scope.template = {url : "sidebar3.html"};
                        $scope.top = {url : "topbar2.html"};
                       break;
                    }

                    setStatusboxWidth();
                  });
      }


      function myinfo()
      {

        $http.get('/api/set_myinfo')
            .success(function(cbdata){
              
              console.log("_kekeke : " + cbdata[0].savepay);

                var fnum = parseInt(cbdata[0].orig_gid);
                if( fnum == 2 )
                  $window.location.href = '/server-page8(edit&detail).html';
                if( fnum > 2 )
                  $window.location.href = '/server-page15(edit&detail).html';
            });
      }

      function logout_func()
      {
        console.log("logouted");
        $http.get('/api/logout_func')
            .success(function(cbdata){
              if(cbdata.logresult == "success")
                $window.location.href = '/index.html';
            });
      }

      function bojong_data(beddata)
      {
        beddata.forEach(function(beditem){
            beditem['str_status'] = status_txt[parseInt(beditem.sp_implemented_result)-1];    
            if((beditem.sp_money/1000)<1000) 
               beditem['str_gmoney'] =  (beditem.sp_money/1000) + "K";    
            else if((beditem.sp_money/1000)>=1000) 
               beditem['str_gmoney'] =  ((beditem.sp_money/1000)/1000) + "M";   
            beditem['str_date'] = (new Date(beditem.sp_datetime)).toLocaleString(); 
          });
        return beddata;
      }


// start for shop recharge request

      function changed_method()
      {
        if ($('#sreq_method').is(":checked"))
        {
          $scope.bplane = 1;
        }
        else
          $scope.bplane = 0;

        opclic($scope.optionval);
      }

      $scope.num_per_page = 10;
      $scope.maxSize = 3;
      $scope.bigTotalItems = 10;
      $scope.bigCurrentPage = 1;

      function pageChanged() {

          showRWReqResult(1);
      }

      function onChangeNum_perPage(){
        
        showRWReqResultWhenClick(1);

      }

      function showRWReqResultWhenClick(reqtype) // show the result
      {
        if(reqtype == 1)
          $scope.working = true;
        else if(reqtype == 2)
          $scope.working1 = true;

        $scope.message = 'Loading...';

        $http.post("/api/loadAllMySPRequestNum" , {"sp_requested_type" : reqtype})
                    .success(function(rdata){

                      console.log("sp whole amount `~~:" + rdata.obj);
                      
                      if(reqtype == 1) //save
                      {
                        $scope.bigTotalItems = (10*Math.ceil(rdata.obj/$scope.num_per_page)); 
                        showRWReqResult(1);
                      }
                      else if(reqtype == 2)
                      {
                        $scope.bigTotalItems1 = (10*Math.ceil(rdata.obj/$scope.num_per_page1)); 
                        showRWReqResult(2);
                      }

                    });
      }

      function showRWReqResult(reqtype) // show the result
      {
        if(reqtype == 1)
          $scope.working = true;
        else if(reqtype == 2)
          $scope.working1 = true;

        $scope.message = 'Loading...';

        var pvparam = {};
        if(reqtype == 1)
        {
        pvparam['currentpage'] = parseInt($scope.bigCurrentPage);
        pvparam['numperpage'] = parseInt($scope.num_per_page);
        }
        else if(reqtype == 2)
        {
          pvparam['currentpage'] = parseInt($scope.bigCurrentPage1);
          pvparam['numperpage'] = parseInt($scope.num_per_page1);
        }


        $http.post("/api/loadAllMySPRequest" , {"sp_requested_type" : reqtype , "pvparam" : pvparam})
                    .success(function(rdata){
                      console.log("rdata : " + rdata.obj.length);

                      var temparr = rdata.obj;

                      if(reqtype == 1)
                      {
                        $scope.save_reqs = bojong_data(temparr);
                        $scope.working = false;
                      }
                      else if(reqtype == 2)
                      {
                        $scope.pay_reqs = bojong_data(temparr);
                        $scope.working1 = false;
                      }
                        
                });
      }

      function doMainForSaveImp(my_up , sothers)
      {
        if($scope.bankinfo_body != "")
        {
          if($("#reqid").val() != "")
          {
            sothers['sru_bank_id'] = $("#reqid").val();
            if($("#requsername").val() != "")
            {
               sothers['sru_bank_username'] = $("#requsername").val();

               my_up['sp_others'] = sothers;
               my_up['sp_money'] = parseInt($("#moneyval").val()); //game money
               //console.log(Math.round(1.34567 * 100) / 100);
               my_up['sp_real_money'] = Math.round(parseFloat($scope.rate*($scope.moneyval/10000)) * 100) / 100; //real money
                switch($scope.countryval)
                {
                  case '0':
                    my_up['sp_money_code'] = "CNY";
                    break;
                  case '1':
                    my_up['sp_money_code'] = "KRW";
                    break;
                  case '2':
                    my_up['sp_money_code'] = "USD";
                    break;
                }
              my_up['sp_requested_type'] = 1;
              my_up['sp_datetime'] = new Date();
                $http.get("/api/getMaxSPid")
                      .success(function(spmax){
                        console.log("spmax : " + spmax.maxid);
                        my_up['sp_id'] = spmax.maxid;

                        console.log("spdata : " + JSON.stringify(my_up));

                        $http.post("/api/doRequestRW" , { obj : my_up })
                          .success(function(crdata){
                            if(crdata.result == true)
                            {
                              showRWReqResultWhenClick(1);
                              alert("success to request");
                            }
                          });

                      });
                
            }
            else
              alert("필수항목을 전부 입력하여야 합니다.");
          }
          else
            alert("필수항목을 전부 입력하여야 합니다.");
        }
        else
          alert("이 대리점에 해당한 지불경로가 없으므로 요청할수 없습니다.");
      }

      function doSaveImpWithDB(my_up) // insert db & send signal
      {
        var sothers = {};
          if($scope.optionval == '1') // choose union
          {
            console.log("ov1 : " + $scope.optionval);
            my_up['sp_bank_info'] = 10;
            if($("#bankname").val() != "")
            {
              sothers['sru_bank_name'] = $("#bankname").val();
              doMainForSaveImp(my_up , sothers);
            }
            else
              alert("필수항목을 전부 입력하여야 합니다.");
          }
          else
          {
            console.log("ov2 : " + $scope.optionval);
            switch($scope.optionval)
            {
              case 2: //zhifubao
                my_up['sp_bank_info'] = 3;
                break;
              case 3: //wechat or weixin
                my_up['sp_bank_info'] = 2;
                break;
              case 4: // caifutong
                my_up['sp_bank_info'] = 4;
                break;
              case 5: //paypal
                my_up['sp_bank_info'] = 1;
                break;
            }

            console.log("myup : " + JSON.stringify(my_up));
            
            doMainForSaveImp(my_up , sothers);
          }

      }

      function cancelReq(svobj , svtype)
      {
        $http.post("/api/cancelReq" , {obj : svobj})
              .success(function(svitem){
                if(svitem.result == true)
                {
                  alert("취소되였습니다.");
                  showRWReqResultWhenClick(svtype);

                  if(svtype == 2)
                    loadBalance();
                }
              });
      }

      function doManualSaveRequest()
      {
        var up_param = {sp_implemented_result : '3'};
        doSaveImpWithDB(up_param);

      }

      function doAutoSaveRequest()
      {
        var up_param = {sp_implemented_result : '4'};
        alert("waiting for implement payment api....");
      }

      function doSaveRequest()
      {

        if ($('#sreq_method').is(":checked"))
          doManualSaveRequest();
        else
          doAutoSaveRequest();
        
      }

      function reset(resettype)
      {
        if(resettype == 1) // save
        {
          $("#sreq_method").prop('checked', true);
          $scope.countryval = '0';
          $scope.opsets = '1';
          $("#reqid").val("");
          $("#bankname").val("");
          $("#requsername").val("");
          $scope.moneyval = '100000';
        }
        else if(resettype == 2) // pay
        {
          $scope.countryval1 = '0';
          $scope.opsets1 = '1';
          $("#reqid1").val("");
          $scope.moneyval1 = '100000';
        }
      }
      function convertCurrency()
      {
        console.log("cv : " + $scope.countryval);
        

        var ctype;
        
        $http.get('http://api.fixer.io/latest?base=USD').then(function(res){
            
            switch($scope.countryval)
            {
              case '0':
                ctype = res.data.rates.CNY;
                $scope.unit = currencyFormatService.getByCode('CNY').uniqSymbol.grapheme;
                break; 
              case '1':
                ctype = res.data.rates.KRW;
                $scope.unit = currencyFormatService.getByCode('KRW').uniqSymbol.grapheme;
                break;
              case '2':
                ctype = 1;
                $scope.unit = currencyFormatService.getByCode('USD').uniqSymbol.grapheme;
                break;
            }
            $scope.rate = ctype;

        });
      }

      

      $scope.$watch('countryval',function(){ convertCurrency(); });


// end for shop recharge request

//start for shop withdrawal request
      
      function loadBalance()
      {
        $http.get('/api/loadShopBalance')
              .success(function(lsbdata){
                console.log("lsbdata : " + lsbdata.lsbalance);
                var lstemp = lsbdata.lsbalance;
                shopbalance = lstemp;
                var strls;
                if((lstemp/1000)<1000)
                  { 
                   strls =  (lstemp/1000); 
                   $scope.lsb_unit = "K";
                   }   
                else if((lstemp/1000)>=1000)
                { 
                   strls =  ((lstemp/1000)/1000);
                   $scope.lsb_unit = "M";
                 }
                $scope.lsbmoney = strls;
              });
      }
      
      $scope.num_per_page1 = 10;
      $scope.maxSize1 = 3;
      $scope.bigTotalItems1 = 10;
      $scope.bigCurrentPage1 = 1;

      function pageChanged1() {

          showRWReqResult(2);
      }

      function onChangeNum_perPage1(){
        
        showRWReqResultWhenClick(2);

      }

      function convertCurrency1()
      {
        console.log("cv : " + $scope.countryval1);
        

        var ctype;
        
        $http.get('http://api.fixer.io/latest?base=USD').then(function(res){
            
            switch($scope.countryval1)
            {
              case '0':
                ctype = res.data.rates.CNY;
                $scope.unit1 = currencyFormatService.getByCode('CNY').uniqSymbol.grapheme;
                break; 
              case '1':
                ctype = res.data.rates.KRW;
                $scope.unit1 = currencyFormatService.getByCode('KRW').uniqSymbol.grapheme;
                break;
              case '2':
                ctype = 1;
                $scope.unit1 = currencyFormatService.getByCode('USD').uniqSymbol.grapheme;
                break;
            }
            $scope.rate1 = ctype;

        });
      }

      $scope.$watch('countryval1',function(){ convertCurrency1(); });

      function doMainForPayImp()
      {
        var my_up = {};
        var sothers = {};
        if($("#reqid1").val() != "")
        {
          sothers['sru_bank_id'] = $("#reqid1").val();

          my_up['sp_others'] = sothers;

          switch($scope.optionval1)
              {
                case 1: // union
                  my_up['sp_bank_info'] = 10;
                  break;
                case 2: //zhifubao
                  my_up['sp_bank_info'] = 3;
                  break;
                case 3: //wechat or weixin
                  my_up['sp_bank_info'] = 2;
                  break;
                case 4: // caifutong
                  my_up['sp_bank_info'] = 4;
                  break;
                case 5: //paypal
                  my_up['sp_bank_info'] = 1;
                  break;
              }
          my_up['sp_money'] = parseInt($("#moneyval1").val()); //game money
             //console.log(Math.round(1.34567 * 100) / 100);
             my_up['sp_real_money'] = Math.round(parseFloat($scope.rate1*($scope.moneyval1/10000)) * 100) / 100; //real money
              switch($scope.countryval1)
              {
                case '0':
                  my_up['sp_money_code'] = "CNY";
                  break;
                case '1':
                  my_up['sp_money_code'] = "KRW";
                  break;
                case '2':
                  my_up['sp_money_code'] = "USD";
                  break;
              }
            my_up['sp_requested_type'] = 2;
            my_up['sp_implemented_result'] = '3';
            my_up['sp_datetime'] = new Date();
              $http.get("/api/getMaxSPid")
                    .success(function(spmax){
                      console.log("spmax : " + spmax.maxid);
                      my_up['sp_id'] = spmax.maxid;

                      console.log("spdata : " + JSON.stringify(my_up));

                      $http.post("/api/doRequestRW" , { obj : my_up })
                        .success(function(crdata){
                          if(crdata.result == true)
                          {
                            showRWReqResultWhenClick(2);
                            loadBalance();
                            alert("success to request");
                          }
                        });

                    });
        }
        else
          alert("필수항목을 입력하십시오.");
      }

      function doPayRequest()
      {
        console.log("moneyval1 : " + $scope.moneyval1);
        $http.get('/api/loadShopBalance')
              .success(function(lsbdata){
                console.log("lsbdata : " + lsbdata.lsbalance);
                var lstemp = lsbdata.lsbalance;
                shopbalance = lstemp;
                var strls;
                if((lstemp/1000)<1000)
                  { 
                   strls =  (lstemp/1000); 
                   $scope.lsb_unit = "K";
                   }   
                else if((lstemp/1000)>=1000)
                { 
                   strls =  ((lstemp/1000)/1000);
                   $scope.lsb_unit = "M";
                 }
                $scope.lsbmoney = strls;

                if($scope.moneyval1 > lstemp)
                  alert(" 잔고가 충분하지 않기때문에 선택한 환전요청을 할수 없습니다. 잔고를 확인하십시오.");
                else
                {
                  console.log("okay");
                  doMainForPayImp();
                }

              });
      }

  
//end for shop withdrawal request


      //start for notice on the top bar

      function checkForSounds(typesound)
      {
        $http.get('/api/getSoundsjson')
            .success(function(sdata)
            {
              if(typesound == 1)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) || (sdata.obj.notify < $scope.len ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
              else if(typesound == 2)
              {
                if( (sdata.obj.vsq < $scope.as.v_save_req ) || (sdata.obj.ssq < $scope.as.s_save_req ) || 
                  (sdata.obj.vpq < $scope.as.v_pay_req ) || (sdata.obj.spq < $scope.as.s_pay_req ) ||
                  (sdata.obj.rpq < $scope.as.report_req ) )
                  {
                                            audio.loop = true;
                      audio.play();
                  }
                else
                {
                  console.log("will null");
                  audio.pause();
                }
              }
            });
      }

      function load_notice()
      {
        $http.get('/api/readSysinfo')
                  .success(function(rsys){
                    if(rsys.result.deep == 1){
                      console.log("dfdf");
                     $http.get('/api/get_all_notice')
                        .success(function(cbdata){
                            console.log("rusuky : " + cbdata.obj);
                            whole_notice = cbdata.obj;
                            whole_notice = whole_notice.sort(function(a,b) { 
                                  return new Date(b.am_date).getTime() - new Date(a.am_date).getTime() 
                              });

                            var unreads = [];
                            whole_notice.forEach(function(item){
                              if(item.am_status == 0)
                                {
                                  unreads.push(item);
                                  console.log(JSON.stringify(item));
                                }
                            });

                            $scope.ur = unreads;
                            $scope.len = unreads.length;

                            checkForSounds(1);
                             

                        });
                    }
                    else
                      checkForSounds(2);

                  });
      }

      function notify_clicked(iobj)
      {
        whole_notice.forEach(function(item){
          if(item == iobj)
            item.am_status = 1;
        }); 
        $http.post('/api/update_all_notice' , { obj : whole_notice } )
                .success(function(cbdata){
                  if(cbdata.result == true){

                      $http.get('/api/getAllalarm' )
                        .success(function(cbdata1){
                            var atypes = cbdata1.obj;
                            var contents = "";
                            var titles = "";
                            for(var i=0;i<atypes.length;i++)
                              if(atypes[i].a_id == iobj.am_content)
                              {
                                contents = atypes[i].a_content.a_en;
                                titles = atypes[i].a_title.a_en;
                              }

                               var mbody = "<p> 위반대상 : "+ iobj.am_target +" </p>"+
                               "<p> 위반대상 식별자 : "+ iobj.am_id +" </p>"+
                              "<p> 위반날자 : "+ iobj.am_date.toLocaleString().split("T")[0] + " " + iobj.am_date.toLocaleString().split("T")[1] +"</p>"+
                              "<p> 위반내용 : "+ contents +" </p> ";
          Notification.success({title : '<h3 style="text-align: center;"><i style=" -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: orange; color: rgba(187, 48, 23, 0.63); font-size : 42px;" class="fa fa-bell"></i></h3>' ,  message: mbody , delay: 10000});
                              
                              load_notice();
                          });
                  }
                });
      }

      //end for notice on the top bar

      //start show all status on the top bar
      function showAllStatus()
      {
        console.log("yayaya~~~hahaha");
        $http.get("/api/setAFlagZero")
                .success(function(afz){
                  if(afz.result == true)
                  {
                    $http.get("/api/getAllStatus")
                          .success(function(cbdata)
                          {

                            console.log("rat : " + JSON.stringify(cbdata));
                            $scope.as = cbdata;

                            load_notice();

                          });
                  }
                });
        
      }

      function closeNav(snd)
      {
        console.log("heyheyhey");
        var x = document.getElementById("myTopnav");
        x.className = "topnav";

        var flagf;
        if( ((3<=snd)&&(snd<=6)) || (snd == 13) || (snd == 11) )
        {
          flagf = 1;
        }
        else
        {
          flagf = 2;
        }
        $http.post("/api/setSoundsflag" , {obj : flagf})
                .success(function(cbdata)
                {
                  if((3<=snd)&&(snd<=10))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 3:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 4:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '3' , rouflag : 999};
                              break;
                            case 5:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 6:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '3' , rouflag : 999};
                              break;
                            case 7:
                              goParam = {utype : '1' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 8:
                              goParam = {utype : '1' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                            case 9:
                              goParam = {utype : '2' , reqtype : '1' , resulttype : '5' , rouflag : 999};
                              break;
                            case 10:
                              goParam = {utype : '2' , reqtype : '2' , resulttype : '5' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page2.html';
                                });
                          
                        }
                        else if((snd == 11) || (snd == 12))
                        {
                          var goParam = {};
                          switch(snd)
                          {
                            case 11:
                              goParam = { cstatus : '3' , rouflag : 999};
                              break;
                            case 12:
                              goParam = { cstatus : '2' , rouflag : 999};
                              break;
                          }
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page10.html';
                                });
                          
                        }
                        else if(snd == 1)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page7.html';
                                });
                          
                        }
                        else if(snd == 2)
                        {
                          var goParam = { dfilter : 31 , rouflag : 999};
                          $http.post("/api/SetTopNavVal" , goParam)
                                .success(function(stnvdata){
                                  if(stnvdata.result == "success")
                                    $window.location.href = '/server-page8.html';
                                });
                          
                        }

                });

      }

      function setStatusboxWidth()
      {
        console.log("wow excellent.. : "+( parseInt($(".page-container").width()) - 390 ));
        var gijuns = parseInt($(".page-container").width());
        var d_amount;
        if(gijuns<768)
          d_amount = 200;
        else
          d_amount = 400;

        var mywidth = gijuns - d_amount;

        $scope.statusbox={'width':mywidth+'px'};
      }

      $(window).resize(function(){
          
        setStatusboxWidth();

      });

      //end show all status on the top bar

      //start test for transfer

      // handles the callback from the received event
        var handleCallback = function (msg) {
            $scope.$apply(function () {
                //$scope.msg = JSON.parse(msg.data)
                var r_obj = JSON.parse(msg.data);
                console.log("receiver : " + r_obj.flag);
                
                if(r_obj.flag != 0)
                {
                  if(r_obj.flag != 100)
                  {
                    console.log("will chekc for target~");
                    $http.post("/api/CheckForEmitTarget" , { cfeparam : r_obj.shcdata }) // check if this is logined id which is correct target to listen the emit socket signal
                          .success(function(cfedata)
                          {
                            if(cfedata.result == true) // this is correct target
                            {
                                  if(cfedata.flag == 1)
                                  {
                                    console.log("will find....");
                                    showAllStatus();
                                    
                                  }
                                  else if(cfedata.flag == 2){
                                    $http.get("/api/showShopRWmsm")
                                          .success(function(ssrw){
                                            alert(ssrw.msmbody);
                                            showRWReqResultWhenClick(1);
                                            showRWReqResultWhenClick(2);
                                            
                                          });
                                    //console.log("listening for change.....");
                                  }
                            }

                          });
                  }
                  else if(r_obj.flag == 100) // its self signal
                  {
                    console.log("will find....");
                    showAllStatus();
                  }
                  
                }
            });
        }
 
        var source = new EventSource('/stats');
        source.addEventListener('message', handleCallback, false);

      //end test for transfer


    }

})();
