var express = require('express');
var bodyParser = require('body-parser');
var nodemailer = require("nodemailer");
var session = require('express-session');
var dateFormat = require('dateformat');
var async = require("async");
var asyncLoop = require('node-async-loop');
var autoNumber = require('mongoose-auto-number');
var multer = require('multer');
var os = require('os');
const requestIp = require('request-ip');
const publicIp = require('public-ip');
var fs = require('fs');
var gutil = require('gulp-util');
var jsonfile = require('jsonfile')
var io=require('socket.io-client');
var clientio=require('socket.io-client');
var exchange = require("exchange-rates");
var oxr = require('open-exchange-rates');

var app = express();


var shopcbdata = {};
var refermsm = {}; // for show the result of the sp .
var gprogressval = 0;


//-server part

//-client part
var clientsocket = clientio.connect('http://localhost:10110');
clientsocket.on('get_signal_from_shops' , function(cbobj) // from shops
  {

    shopcbdata = cbobj;
    shopcbdata['signaltype'] = 1;
    allstatus_flag = cbobj.result;

    console.log("from shop : -- af : " + allstatus_flag + " / " + "shopcbdata : " + JSON.stringify(shopcbdata));
   
  });
clientsocket.on('get_signal_from_visitors' , function(cbobj) // from visitors
  {
    shopcbdata = cbobj;
    shopcbdata['signaltype'] = 2;
    allstatus_flag = cbobj.result;
    console.log("from shop : -- af : " + allstatus_flag + " / " + "shopcbdata : " + JSON.stringify(shopcbdata));
   
  });



//-client part

var smtpTransport = nodemailer.createTransport({
    service: "gmail",
    host: "smtp.gmail.com",
    auth: {
        user: "jhmun216@gmail.com",
        pass: "a-wufshjdsus-a"
    }
});

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/serverdatabase');
autoNumber.init(mongoose);
//mongoose.connect('mongodb://localhost:27017/admin_server');

var lrfSchema = mongoose.Schema({
  u_id: {type: String},
  u_pw: {type: String},
  u_email: {type: String},
  u_nickname: {type: String},
  u_phone: {type: String},
  u_groupid: {type: String},
  u_uppercode: {type: String},
  u_uppertype: {type: String},
  u_loginip: {type: String},
  u_lastloginip: {type: String},
  u_lastlogin: {type: Date},
  u_lastlogout: {type: Date},
  u_flag: {type: String},
  u_registerdate: {type: Date},
  u_logcount: {type: Number},
  str_registerdate : {type: String},
  str_lastlogin : {type: String},
  str_duration : {type: String},
  u_ticheng1: {type: Number},
  u_ticheng2: {type: Number},
  orig_gid: {type: String} ,
  savepay : {type: String} ,
  balance : {type: String} ,

  u_deep : {type: Number} ,

  u_pm : {type: Object},


  today_shop_earned : {type: String} ,
  whole_shop_earned : {type: String} ,
  today_visitor_earned : {type: String} ,
  whole_visitor_earned : {type: String} ,

  tempu_uppercode : {type: String},

  eb_array : {type: Object} ,

  fflag : {type: Number},

  u_ac_metaid : {type : Number},

  paths : {type : Array},

  orig_uppertype : {type : String} ,

  u_verifyflag : {type : Number},

  u_displaybalanceflag : {type : Number},

  u_level : {type : Number},

  u_metaid : {type : String},

  u_photoindex : {type : Number}


}, {collection: 'user_info'});

lrfSchema.index({ paths : 1 , u_metaid : 1 , u_id : 1 , u_nickname : 1 , u_uppercode : 1 , u_groupid : 1 });
var lrfModel = mongoose.model('lrfModel', lrfSchema);

var groupSchema = mongoose.Schema({
  g_id: {type: String},
  g_name: {type: String}
}, {collection: 'group_info'});

var groupModel = mongoose.model('groupModel', groupSchema);

var spSchema = mongoose.Schema({
  sp_id: {type: Number},
  sp_datetime: {type: Date},
  sp_implemented_date: {type: Date},
  sp_requested_type: {type: Number},
  sp_requested_id: {type: String},
  sp_servicer_id: {type: String},
  sp_money: {type: Number},
  sp_real_money: {type: Number},
  sp_money_code: {type: String},
  sp_bank_info: {type:Number},
  sp_implemented_result: {type:String},

  pakgal_spname: {type: String},
  req_type: {type: String},
  datetime_text : {type: String},
  impdate_text : {type: String},

  sp_others : {type: Object},

  oiriginal_id : {type: String},

  upcode : {type: String},

  orig_implemented_result : {type: String},

  sp_implemented_result: {type: String},

  sp_bankinfo_str : {type: String}

}, {collection: 'save_pay_request'});

var spModel = mongoose.model('spModel', spSchema);


var epSchema = mongoose.Schema({
  ep_id: {type: Number},
  ep_datetime: {type: Date},
  ep_loginid: {type: String},
  ep_metaid: {type: Number},
  ep_money: {type: Number},
  ep_others: {type: Object},

  ep_type: {type: Number},

  ep_flag: {type: Number},
  ep_processed : {type : Number}

}, {collection: 'earn_pay_history'});

epSchema.index({ "ep_others.game_id" : 1 , "ep_others.ep_ag_arr" : 1 , ep_loginid : 1 , ep_type : 1 , ep_datetime : 1 });
var epModel = mongoose.model('epModel', epSchema);


var ebSchema = mongoose.Schema({
  eb_id: {type: Number},
  eb_title: {type: String},
  eb_comments: {type: String},
  eb_type: {type: String},
  eb_period: {type: String},
  eb_codestatus: {type: Boolean},

  eb_allow: {type: Boolean},

  period_txts: {type: String},
  type_txts: {type: String},
  status_txts: {type: String},
  allow_txts: {type: String}


}, {collection: 'event_bonus'});

var ebModel = mongoose.model('ebModel', ebSchema);


var ebImpSchema = mongoose.Schema({
  ebi_id: {type: Number},
  ebi_start_adv_date: {type: Date},
  ebi_end_adv_date: {type: Date},
  ebi_start_date: {type: Date},
  ebi_end_date: {type: Date},
  ebi_eb_money: {type: Number},

  ebi_eb_id: {type: Number},

  ebi_adv_link: {type: String},

  ebname: {type: String},
  sd: {type: String},
  ed: {type: String},
  sad: {type: String},
  ead: {type: String},
  ebtype: {type: String},
  strtype: {type: String},
  ebcomment: {type: String}


}, {collection: 'event_bonus_implement'});

ebImpSchema.index({ ebi_id : 1 });
var ebImpModel = mongoose.model('ebImpModel', ebImpSchema);


var giSchema = mongoose.Schema({
  ga_id: {type: Number},
  ga_title: {type: Object},
  ga_comments: {type: Object},
  ga_earning_typeid: {type: Number},
  ga_earning_calcv: {type: Number},
  ga_calc_period: {type: Number},
  ga_other_setting: {type: Object}, 

  str_ga_typeid: {type: String},
  str_ga_period: {type: String},
  str_ga_category: {type: String},

  ga_icon_path: {type: String},

  ga_date: {type: Date},
  ga_popular: {type: Number},
  ga_category: {type: Number}


}, {collection: 'game'});

var giModel = mongoose.model('giModel', giSchema);


var mSchema = mongoose.Schema({
  m_contents: {type: String},
  m_datetime: {type: Date},
  m_id: { type: Number },
  m_imp_contents: {type: String},
  m_imp_date: {type: Date},
  m_imp_servicer: {type: String},
  m_receiverid: {type: String},

  m_senderid: {type: String},
  m_title: {type: String},

  m_type: {type : Object},
  m_lowl: {type : Number},
  m_highl: {type : Number},
  m_flag: {type : Number},
  m_receiver_nickname: {type : String},
  m_sender_nickname: {type : String},

  m_imp_datetext: {type: String},
  m_imp_servicer_nickname: {type : String},
  m_datetext: {type : String}

}, {collection: 'message'});

var mModel = mongoose.model('mModel', mSchema);


var dmSchema = mongoose.Schema({
  dm_domain_name: {type: String},
  dm_ipaddress: {type: String},
  dm_service_houseid: {type: String},
  dm_service_type: {type: String}

}, {collection: 'domain_list'});

dmSchema.index({ dm_service_houseid : 1 , dm_domain_name : 1});
var dmModel = mongoose.model('dmModel', dmSchema);


var saSchema = mongoose.Schema({
  
  sa_id : {type: Number},
  sa_typename : {type: String},
  sa_arr : {type: Array},

  txt_fstatus : {type : String},
  txt_sstatus : {type : String}

}, {collection: 'security'});

var saModel = mongoose.model('saModel', saSchema);


var pprSchema = mongoose.Schema({

  id: {type : Number},
  player_id : {type : String},
  round_fee : {type : Number},
  round_start_time : {type : Date},
  history_version : {type : String}

} , {collection : 'poker_result_history'});

pprSchema.index({ round_start_time : 1 });
var pprModel = mongoose.model('pprModel' , pprSchema);


var ppeSchema = mongoose.Schema({

  id: { type : Number },
  player_id: { type : String },
  room_id: { type : String },
  event_type: { type : String },
  event_time: { type : Date },
  event_money: { type : Number },
  event_reason: { type : String }

} , {collection : 'poker_play_event'});

ppeSchema.index({ event_time : 1 });
var ppeModel = mongoose.model('ppeModel' , ppeSchema);


var hnSchema = mongoose.Schema({

  hn_id: {type : Number},
  hn_type: {type : Number},
  hn_title: {type : String},
  hn_comment: {type : String},
  hn_date: {type : Date}

} , {collection : 'help_notice'});

var hnModel = mongoose.model('hnModel' , hnSchema);


var acSchema = mongoose.Schema({

  ac_id : {type : Number},
  ac_zhong : {type : Number},
  ac_report : {type : Number},
  ac_sendmsm : {type : Number},
  ac_showmsm : {type : Number},
  ac_gongji : {type : Number},
  ac_help : {type : Number},
  ac_sp : {type : Number},
  ac_visitor : {type : Number},
  ac_shop : {type : Number},
  ac_allgame : {type : Number},
  ac_pokerhistory : {type : Number},
  ac_gamemanage : {type : Number},
  ac_security : {type : Number},
  ac_eb_reg : {type : Number},
  ac_eb_imp : {type : Number},
  ac_groupmanage : {type : Number},
  ac_manager : {type : Number},
  ac_allows : {type : Number},
  ac_myinfo : {type : Number},
  ac_pmethod : {type : Number},
  ac_allnotify : {type : Number},
  ac_allstatus : {type : Number},
  ac_payments : {type : Number}

} , {collection : 'allower'});
var acModel = mongoose.model('acModel' , acSchema);

var sfSchema = mongoose.Schema({

  a : {type : Number},
  b : {type : Number},
  c : {type : Number},
  d : {type : Number},
  sf_id : {type : Number}

} , {collection : "suffixes"});
var sfModel = mongoose.model('sfModel' , sfSchema);


var bpSchema = mongoose.Schema({

  bp_imper : {type : String},
  bp_impdate : {type : Date},
  bp_impamount : {type : Number}

} , {collection : "betting_payment"});
var bpModel = mongoose.model('bpModel' , bpSchema);


app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
//app.use(session({secret: 'ssshhhhh'}));
app.use(session({
  secret: 'yangpamanchi',
  name: 'session1'
}));

var siteinfo;

var group_arr = ["Whole Manager", "Game Manager", "User Manager"];
//var sess;
var sp_texts = [ "충전" , "환전" ];
var req_results_texts = [ "승인됨" , "취소됨" , "대기" , "온라인 지불됨" ];
var eb_period_texts = [ "즉시" , "1회" , "매일" , "매주" , "매월" ];
var eb_type_texts = [ "event" , "bonus" ];
var sp_bankinfo_texts = ['Union pay','paypal','wechat','zhifubao','caifutong'];

//--global variables--

var allstatus_flag = 0;
var g_allow_data = [];

var socket = io.connect('http://101.102.224.50');

//app.get("/api/usercheck", userCheck);
app.get("/api/readSysinfo", readSysinfo);

app.get("/api/load_managertype", load_Managertype);
app.put("/api/usercheck/:id", userCheck);
app.put("/api/doremember/:id", doRemember);
app.post("/api/userregister", userRegister);

app.get("/api/session_check", sess_check);
app.get("/api/logout_func", logout_func);

app.get("/api/childshop_num/:id", childShop_num); //get all childshop directly by logined user
app.get("/api/visitor_num/:id", visitor_num); //get all visitor pair for each child shop
app.get("/api/childshop/:id", childShop); // get childshop directly by logined user
app.get("/api/DirectlyVisitor_num/:id", d_visitor_num); //get members directly by logined user.

app.post("/api/find_Save_pay_result", find_spr); //find save pay result by detail filter.

app.post("/api/add_visitor", add_visitor); //add visitor
app.post("/api/update_visitor", update_visitor); //find save pay result by detail filter.

app.get("/api/getAllShop", getAllShop); //get members directly by logined user.

app.get("/api/getHeadquarterShop_id", getHeadquarterShop_id); //get members directly by logined user.

app.get("/api/getDocofLogined", getDocofLogined); //get Document of logined user

app.post("/api/find_member_result_by_detail", find_member_rbd); //find save pay result by detail filter.

app.post("/api/set_selected_visitor", set_selected_visitor); //find save pay result by detail filter.
app.get("/api/get_selected_mem", get_selected_mem); //get selected visitor info.

app.post("/api/set_updated_visitor", set_updated_visitor); //set global updated visitor id for go to savepay page with that info.
app.get("/api/get_updated_mem", get_updated_mem); //get setted updated visitor info.

app.post("/api/set_selected_game", set_selected_game); //set global updated game id for go to savepay page with that info.
app.get("/api/get_selected_game", get_selected_game); //get setted updated game info.

app.post("/api/get_other_detail", get_other_detail); //get shop earned , visitor earned , money fieald detail.

app.post("/api/findAllGameing", findAllGameing); //get all members from game table
app.get("/api/findAllGame", findAllGame); //get all members from game table
app.get("/api/getGaMaxid", getGaMaxid); //get max id from game table
app.post("/api/add_game", add_game); //add visitor
app.post("/api/update_game", update_game); //add visitor

app.post("/api/send_message", send_message); //send message
app.post("/api/find_message_history", find_message_history); //find message history
app.put("/api/find_report_history/:id", find_report_history); //find report history

app.post("/api/find_shop_result_by_detail", find_shop_rbd); //find save pay result by detail filter.
app.put("/api/update_implemented/:id", update_implemented); //update sp info

app.post("/api/add_shop", add_shop); //add shop

app.post("/api/set_selected_shop", set_selected_shop); //set selected shop info.
app.get("/api/get_selected_shop", get_selected_shop); //get selected shop info.
app.post("/api/update_shop", update_shop); //update shop

app.get("/api/getEpMaxid", getEpMaxid); //get max id from earn pay table
app.post("/api/addEpData", addEpData); //add ep data when accept save/pay request.

app.post("/api/removeDomain", removeDomain); //remove domain from the domain list on selected shop

app.post("/api/setOurVal_To_MemVal", setOurVal_To_MemVal); //set selected shop data to selected mem data for showing data on other page

app.post("/api/implement_report", implement_report);

app.post("/api/checkForSendMSM", checkForSendMSM); // check id is correct for send msm

app.get("/api/getMMaxid", getMMaxid); //get max id from message table

app.post("/api/addMSGbySP", addMSGbySP); //when do save/pay , add message to send client.

app.get("/api/getGroupinfo", getGroupinfo); //get all of the group
app.get("/api/getGroupMaxid", getGroupMaxid); // get max id from group table
app.post("/api/addGroupitem", addGroupitem); // add Group item to the table

app.post("/api/checkGroupItem", checkGroupItem); // check group item for update

app.post("/api/updateGroupItem" , updateGroupItem); // update group item

app.post("/api/updateGroupItem" , updateGroupItem); // update group item

app.post("/api/deleteGrItem" , deleteGrItem); // update group item

app.post("/api/checkGrName" , checkGrName); // check group name for add

app.get("/api/getAllManager" , getAllManager); // get all of the manager info

app.get("/api/getManagerGlist" , getManagerGlist); // get groupid for only manager.

app.post("/api/add_manager" , add_manager); // add manager

app.post("/api/set_sel_manager" , set_sel_manager); // set sel manager
app.get("/api/get_sel_manager" , get_sel_manager); // get sel manager
app.post("/api/update_manager" , update_manager); //update manager
app.post("/api/remove_manager", remove_manager); //remove manager

app.get("/api/set_myinfo" , set_myinfo); // set logined info to the global val for update

app.post("/api/maintain_game", maintain_game); //maintain game

app.post("/api/checkGroupid" , checkGroupid); //check group id to decide whether delete it or not delete it.

app.get("/api/getAllEvData" , getAllEvData); // get all data from the event_bonus table
app.post("/api/addEvData" , addEvData); // add event bonus data 
app.get("/api/getMaxEvID" , getMaxEvID); // get max id in eb table
app.post("/api/set_global_ebdata" , set_global_ebdata); //set selected ebdata to global variable
app.get("/api/get_global_ebdata" , get_global_ebdata); //get selected ebdata from global variable

app.post("/api/updateEvData" , updateEvData); // update selected ebdata
app.post("/api/check_forDel" , check_forDel); //check for Delete Ebdata 
app.post("/api/remove_ebdata" , remove_ebdata); //remove selected ebdata

app.get("/api/get_sess_info" , get_sess_info);


app.post("/api/imp_ebdata" , imp_ebdata); //implement ebdata
app.get("/api/getImpMaxid" , getImpMaxid); //get max id of imp table 

app.post("/api/find_ebi_data" , find_ebi_data); //find ebi data

app.post("/api/check_forEBIDel" , check_forEBIDel); //check for Delete Ebidata 
app.post("/api/del_EBIdata", del_EBIdata); // delete ebi data
app.post("/api/update_ebidata/:id", update_ebidata); // update ebi data

app.post("/api/addBlackName", addBlackName); 
app.get("/api/get_blacklist", get_blacklist);
app.post("/api/removeBlackitem", removeBlackitem);

app.post("/api/addBlockIP", addBlockIP);
app.get("/api/get_blockname", get_blockname); 
app.post("/api/removeBlockip", removeBlockip);


app.get("/api/get_alarmlist", get_alarmlist); 
app.post("/api/addnew_alarm", addnew_alarm);
app.post("/api/update_alarm", update_alarm);


app.post("/api/checkInBlist", checkInBlist);  // when visitor register , check that nickname if in the blackname list.

app.get("/api/get_all_notice", get_all_notice); // get all of the notice.
app.post("/api/update_all_notice", update_all_notice); // when clicked the unread msm , update the whole data

app.get("/api/get_rw_all_notice", get_rw_all_notice); // get current all notice
app.get("/api/update_all_status", update_all_status); // set all status with read

app.post("/api/checkInDlist", checkInDlist); // check for if same domain to be register in domain list

app.get("/api/getAllalarm", getAllalarm); // get all alert item from the list

app.post("/api/CalcPokerHistory" , CalcPokerHistory); // calc poker history for graph

app.post("/api/startForShop_profit" , startForShop_profit); // calc shop self & bonsa profit
app.post("/api/startForVisitor_profit" , startForVisitor_profit); // calc visitor bonsa profit

app.get("/api/getAllStatus" , getAllStatus); // get all status for the top bar


app.get("/api/getSoundsflag" , getSoundsflag); // get sounds flag
app.post("/api/setSoundsflag" , setSoundsflag); // set sounds flag

app.get("/api/getSoundsjson" , getSoundsjson); // get sounds flag
app.post("/api/setSoundsjson" , setSoundsjson); // set sounds flag

app.get("/api/getASparam" , getASparam); // get all status param for redirect page
app.post("/api/setASparam" , setASparam); // set all status param for redirect page

app.post("/api/getGResultByVisitor" , getGResultByVisitor); // get game result by player.

app.post("/api/getWholeGameHistory" , getWholeGameHistory);// get whole game history.
app.post("/api/getWholeMatchedGHData" , getWholeMatchedGHData);

app.post("/api/checkGaid" , checkGaid); // check if same game id in table

app.post("/api/setPaymentMethod" , setPaymentMethod); // set payment method for logined shop
app.get("/api/loadPaymentMethod" , loadPaymentMethod); // set payment method for logined shop

app.get("/api/getMaxSPid" , getMaxSPid); //get max id in sp table
app.post("/api/doRequestRW" , doRequestRW); // do request shop sp

app.post("/api/loadAllMySPRequest" , loadAllMySPRequest); // load all my sp request

app.get("/api/showShopRWmsm", showShopRWmsm); // make shop rw msm

app.post("/api/cancelReq" , cancelReq); // cancel rw request

app.get("/api/loadShopBalance" , loadShopBalance); // load shop balance

app.get("/api/getMyUpperCardInfo" , getMyUpperCardInfo); // get upper shop's card info

app.post("/api/getProfitByAllV" , getProfitByAllV); // get start profit by each visitor or shop.
app.post("/api/getProfitByAllV_m" , getProfitByAllV_m);

app.post("/api/getProfitByGame" , getProfitByGame); // get profit by each game and periods.

app.get("/api/getMaxHNid" , getMaxHNid); // get max id in help_notice table
app.post("/api/addHelps" , addHelps); // add help datas

app.post("/api/findHelpDatas" , findHelpDatas);
app.post("/api/updateHelpDatas" , updateHelpDatas);
app.post("/api/delHelpDatas" , delHelpDatas);

app.post("/api/CheckForEmitTarget" , CheckForEmitTarget);
app.get("/api/setAFlagZero" , setAFlagZero);

app.post("/api/checkGrId" , checkGrId);

app.post("/api/set_visitor_for_game" , set_visitor_for_game);
app.get("/api/get_visitor_for_game" , get_visitor_for_game);

app.get("/api/get_firstsite_user" , get_firstsite_user);

app.get("/api/get_allow_data" , get_allow_data);

app.get("/api/get_global_acdata" , get_global_acdata);

app.post("/api/updateAllowers" , updateAllowers);

app.get("/api/getMax_acid" , getMax_acid);

app.post("/api/checkForEbiToImp" , checkForEbiToImp);

app.post("/api/getMatchedMemData" , getMatchedMemData);

app.post("/api/getMatchedShopData" , getMatchedShopData);

app.post("/api/getZhonghe" , getZhonghe);
app.post("/api/getTable1Detail" , getTable1Detail);
app.post("/api/getTable1DetailCalc" , getTable1DetailCalc);


app.post("/api/getTable2Detail" , getTable2Detail);
app.post("/api/getTable2DetailCalc" , getTable2DetailCalc);

app.post("/api/find_Save_pay_num" , find_Save_pay_num);

app.post("/api/getGResultByVisitorNum" , getGResultByVisitorNum);

app.post("/api/find_report_history_Num/:id" , find_report_history_Num);

app.post("/api/find_message_history_Num" , find_message_history_Num);

app.post("/api/findHelpDatasNum" , findHelpDatasNum);

app.post("/api/findAllGameNum" , findAllGameNum);

app.post("/api/loadAllMySPRequestNum" , loadAllMySPRequestNum);

app.post("/api/SetTopNavVal" , SetTopNavVal);
app.post("/api/SetTopNavNull" , SetTopNavNull);
app.get("/api/GetTopNavVal" , GetTopNavVal);

app.post("/api/doCheckOut" , doCheckOut);

app.post("/api/usidcheck" , usidcheck);

app.get("/api/getProgressval" , getProgressval);
app.post("/api/setProgressval" , setProgressval);

app.post("/api/getWholeBPNum" , getWholeBPNum);
app.post("/api/getBPhistory" , getBPhistory);

app.post("/api/expectationImp" , expectationImp);

app.post("/api/checkByacid" , checkByacid);

app.post("/api/getRealChildshop" , getRealChildshop);
app.post("/api/getRealAllChildshop" , getRealAllChildshop);
app.post("/api/getRealChildshopWithHierachied" , getRealChildshopWithHierachied);

//--global functions--

function SetTopNavVal(req, res)
{
  var fromdoe = req.body;
 
  if(fromdoe.rouflag != undefined)
    req.session.goParam.rouflag = fromdoe.rouflag;

  if(fromdoe.utype != undefined)
    req.session.goParam.utype = fromdoe.utype;

  if(fromdoe.reqtype != undefined)
    req.session.goParam.reqtype = fromdoe.reqtype;

  if(fromdoe.resulttype != undefined)
    req.session.goParam.resulttype = fromdoe.resulttype;

  if(fromdoe.cstatus != undefined)
    req.session.goParam.cstatus = fromdoe.cstatus;

  if(fromdoe.dfilter != undefined)
    req.session.goParam.dfilter = fromdoe.dfilter;

  console.log("settopnavnull!!!! : " + JSON.stringify(req.session.goParam));

  res.json({result : "success"});
}

function SetTopNavNull(req, res)
{
  

  req.session.goParam.rouflag = 1000;

  res.json({result : "success" , tam : req.session.goParam});
}

function GetTopNavVal(req, res)
{
    res.json(req.session.goParam);
}

function setAFlagZero(req, res)
{
  allstatus_flag = 0;
  res.json({ result : true });
}

function readSysinfo(req, res)
{
  var file = './public/sysinfo.json';
  jsonfile.readFile(file, function(err, obj) {
    siteinfo = obj;
    res.json({ result : siteinfo });
  });
}


function get_sess_info(req, res)
{
  res.json( {result : req.session.u_deep} );
}

function sess_check(req, res)
{
  var sess = req.session;
  console.log("sess_check : " + sess.u_id);
  console.log("session val : " + JSON.stringify(sess));
    //Session set when user Request our app via URL
    if((sess.u_id) && (sess.u_deep == siteinfo.deep)) {
    /*
    * This line check Session existence.
    * If it existed will do some action.
    */
    console.log("exist session");
        res.json({flag : 0 , sess_val : sess.u_id});
    }
    else {
      console.log("no session");
        res.json({flag : 1 , sess_val : sess.u_id});
    }
}

function logout_func(req, res)
{
  var logouteddate = new Date();
                var target = { u_id : req.session.u_id };
                var updata = { u_lastlogout :  logouteddate};
                update_userstatus( target , updata );

  req.session.u_id = null;
  console.log(req.session.u_id + ":)");
  res.json({logresult : "success"});
}

function getDocofLogined(req, res)
{
  lrfModel
    .find({ u_id : req.session.u_id })
    .then(
      function(cbdata) {
      
        res.json(cbdata);

      },
      function(err) {

      }
    );
}


//--end global functions--

//--lrf functions--

function load_Managertype(req , res)
{
  groupModel
    .find({ g_id: { $gt: 2 } })
    .then(
      function(cbdata) {

          res.json(cbdata);
      },
      function(err) {
        console.log("err");
        res.sendStatus(400);
      }
    );
}

function update_userstatus(target , updatedata)
{
  lrfModel
    .update(target , updatedata)
    .then(
      function(cbdata) {
        console.log(updatedata);
      },
      function(err) {

      }
    );
}

function update_lastloginip(ipstr , mycuid)
{
  lrfModel
    .update({ u_id : mycuid } , { u_lastloginip : ipstr })
    .then(
      function(cbdata) {
        console.log("ip updated !!! ");
      },
      function(err) {

      }
    );
}

function userCheck(req, res){
  var ubodyId = req.params.id;
  var ubody = req.body;
  console.log("ubodyid :  "+ubodyId+"   ubody :   "+JSON.stringify(ubody));

  ubody['u_deep'] = siteinfo.deep;
  lrfModel
    .find(ubody)
    .then(
      function(cbdata) {
        var checkResult = {};
        console.log("test1 : "+ cbdata.length);
        if(cbdata.length == 1)
        {
          console.log("really ? ");
          if(ubodyId == "foraddvisitor")
          {
            console.log("when exist for vsistor");
            checkResult = {result : true};
          }
          else{
            if(cbdata[0].u_flag=="a")
            {
              if(ubodyId == "loginstatus")
                {
                  const clientIp = requestIp.getClientIp(req); 
                    var ipstr = clientIp.toString();
                    var ip_arr = ipstr.split(":");
                    console.log(cbdata[0].u_loginip+" / "+ip_arr[3]);
                  if(cbdata[0].u_loginip == ip_arr[3])  
                  {
                    console.log("passed");
                    var sess = req.session;
                    sess.u_id = cbdata[0].u_id;
                    sess.u_metaid = cbdata[0].u_metaid;
                    sess.u_nickname = cbdata[0].u_nickname;
                    sess.u_groupid = cbdata[0].u_groupid;
                    sess.u_deep = cbdata[0].u_deep;
                    sess.u_uppercode = cbdata[0].u_uppercode;
                    sess.paths = cbdata[0].paths;

                    sess.sounds_json = {vsq : 0 , ssq : 0 , vpq : 0 , spq : 0 , rpq : 0 , notify : 0};
                    sess.status_sound = 0;
                    sess.testdata = 0;

                    sess.game_playername = "";
                    sess.selected_shop = [];
                    sess.selected_shop_dmlist = [];

                    sess.selected_updated_visitor = [];
                    sess.selected_visitor = {a:'a'};
                    sess.set_selected_game = {};

                    sess.selected_manager = {};
                    sess.selected_ebdata = {};

                    req.session.progressbar = 0;

                    sess.goParam = {rouflag : 1000 , utype : "" , reqtype : "" , resulttype : "" , 
                                    cstatus : "" , dfilter : 0};

                    var logineddate = new Date();
                    var target = { u_id : cbdata[0].u_id };

                    if(cbdata[0].u_logcount == undefined)
                      cbdata[0].u_logcount = 0;
                    var tempc = cbdata[0].u_logcount;
                    tempc += 1;
                    //console.log("+log count :   "+ tempc);

                    var updata = { u_lastlogin :  logineddate , u_logcount : tempc};
                    update_userstatus( target , updata );

                    
                    update_lastloginip(ip_arr[3] , req.session.u_id);

                    console.log("ipaddresser :   "+ip_arr[3]);

                    console.log("sess uid : " + sess.u_id);
                  }
                  else if(cbdata[0].u_loginip != ip_arr[3]){
                    if(cbdata[0].u_loginip.length != 0)
                      res.json({result : false});
                    else
                    {
                      console.log("passed");
                        var sess = req.session;
                        sess.u_id = cbdata[0].u_id;
                        sess.u_metaid = cbdata[0].u_metaid;
                        sess.u_nickname = cbdata[0].u_nickname;
                        sess.u_groupid = cbdata[0].u_groupid;
                        sess.u_deep = cbdata[0].u_deep;
                        sess.u_uppercode = cbdata[0].u_uppercode;
                        sess.paths = cbdata[0].paths;

                        sess.sounds_json = {vsq : 0 , ssq : 0 , vpq : 0 , spq : 0 , rpq : 0 , notify : 0};
                        sess.status_sound = 0;
                        sess.testdata = 0;

                        sess.game_playername = "";
                        sess.selected_shop = [];
                        sess.selected_shop_dmlist = [];

                        sess.selected_updated_visitor = [];
                        sess.selected_visitor = {a:'a'};
                        sess.set_selected_game = {};

                        sess.selected_manager = {};
                        sess.selected_ebdata = {};

                        req.session.progressbar = 0;

                        sess.goParam = {rouflag : 1000 , utype : "" , reqtype : "" , resulttype : "" , 
                                    cstatus : "" , dfilter : 0};

                        var logineddate = new Date();
                        var target = { u_id : cbdata[0].u_id };

                        if(cbdata[0].u_logcount == undefined)
                          cbdata[0].u_logcount = 0;
                        var tempc = cbdata[0].u_logcount;
                        tempc += 1;
                        //console.log("+log count :   "+ tempc);

                        var updata = { u_lastlogin :  logineddate , u_logcount : tempc};
                        update_userstatus( target , updata );

                        
                        update_lastloginip(ip_arr[3] , req.session.u_id);

                        console.log("ipaddresser :   "+ip_arr[3]);

                        console.log("sess uid : " + sess.u_id);
                    }
                  }

                }
              checkResult = {result : true};
            }
            else {
              checkResult = {result : false};
            }
          }
        }
        else {
          if(ubodyId == "foraddvisitor")
          {
            console.log("when none for vsistor");
            checkResult = {result : false};
          }
          else
            checkResult = {result : false};
        } 
        //if(cbdata != "")

          console.log("checkresult :   "+checkResult.result);
            res.json(checkResult);
      },
      function(err) {
        console.log("err");
        res.sendStatus(400);
      }
    );
}

function doRemember(req, res){
  var ubodyId = req.params.id;
  var ubody = req.body;
  console.log(ubody.u_email);

  lrfModel
    .find(ubody)
    .then(
      function(cbdata) {
        var checkResult = {};
        if(cbdata[0] != null)
        {
          checkResult = {result : true};

          var mailOptions={
            to : ubody.u_email,
            subject : "Your information",
            text : "id : "+ cbdata[0].u_id + "  password : " + cbdata[0].u_pw
            }
            console.log(mailOptions);
            smtpTransport.sendMail(mailOptions, function(error, response){
               if(error){
                      console.log(error);
                  //res.end("error");
               }
               else{
                      console.log("Message sent: " + response.message);
                  //res.end("sent");
                   }
            });

        }
        else {
          checkResult = {result : false};
        }
        //if(cbdata != "")
          res.json(checkResult);
      },
      function(err) {
        console.log("err");
        res.sendStatus(400);
      }
    );
}

function userRegister(req, res){
  var users = req.body;
  console.log(users);
  var registeringdate = new Date();
  var checkResult = {};
  lrfModel
    .create({u_nickname : users.u_nickname ,
            u_email : users.u_email ,
            u_phone : users.u_phone ,
            u_groupid : users.u_groupid ,
            u_id : users.u_id ,
            u_pw : users.u_pw ,
            u_flag : "b",
            u_logcount : 0 , 
            u_registerdate : registeringdate
            })
    .then(
      function (postObj){
        checkResult = {result : true};
          res.json(checkResult);
      },
      function (error){
        checkResult = {result : false};
          res.json(checkResult);
      }
    );
}

//--end lrf functions--

//-- page1 functions--


function CalcLength(wjarr , wuarr)
{
  var sums = 0;
  wuarr.forEach(function(witem){
    sums += wjarr[witem].length;
  });
  return sums;
}

function findUpperArray(wjarr , wuarr , windex)
{
  var finalres = [];
  wuarr.forEach(function(witem){
    //console.log("---*windex*--- : " + witem);
    
    var temparr = [];
    temparr = wjarr[witem];
    //console.log("data : " + temparr[0]);
    temparr.forEach(function(wwitem){
      //console.log("temparr indexes : " + wwitem);
      if(wwitem.u_id == windex)
      {
        //console.log("templen : " + temparr.length);
        finalres = temparr;
      }

    });

  });
  //console.log("finalres : " + finalres.length);
  return finalres;
}

function currentPosition(arr , indo)
{
  var finalindo;
  arr.forEach(function(aitem , index){
    if(aitem.u_id == indo)
      finalindo = index; 
  });
  return finalindo;
}

function doHiheracied(mainCalcarr , mindex , wholeJSONarr , wholeUPitems , shoparrs , tree_arr , finaltree , zuiroot , res)
{
  lrfModel.find({ u_uppercode : mainCalcarr[mindex].u_id , u_groupid: "2"})
            .then(function(cbdata){
                if(cbdata.length > 0)
                {
                  wholeJSONarr[mainCalcarr[mindex].u_id] = cbdata;
                  wholeUPitems.push(mainCalcarr[mindex].u_id);

                  //console.log("i have childs : " + mainCalcarr[mindex].u_id);
                  doHiheracied(cbdata , 0 , wholeJSONarr , wholeUPitems , shoparrs , tree_arr , finaltree , zuiroot , res);
                }
                else if(cbdata.length == 0)
                {
                  var deadline = CalcLength(wholeJSONarr , wholeUPitems);
                  //console.log("shoplen : " + shoparrs.length + " / " + "deadline : " + deadline);

                  if(shoparrs.length<deadline)
                  {
                    shoparrs.push(mainCalcarr[mindex]);
                    if(mainCalcarr[mindex].u_uppercode == zuiroot)
                          {

                            finaltree.push({text : mainCalcarr[mindex].u_nickname ,  // for make tree structured
                                   id : mainCalcarr[mindex].u_id});
                            tree_arr = [];
                          }
                    else
                      tree_arr.push({text : mainCalcarr[mindex].u_nickname ,  // for make tree structured
                                   id : mainCalcarr[mindex].u_id});

                      if(shoparrs.length == deadline)
                      {
                        //console.log("*****THE END*****");
                        var tempfinal = [{ text : "root" , id : zuiroot , items : finaltree}];
                        //console.log("final tree_arr : " + JSON.stringify(tempfinal));
                        res.json({obj : shoparrs , treeobj : tempfinal});
                        
                      }
                      else
                      {
                        if((mindex+1) != mainCalcarr.length)
                        {
                          //console.log("my next item is available : " + mainCalcarr[mindex].u_id);
                          doHiheracied(mainCalcarr , mindex+1 , wholeJSONarr , wholeUPitems , shoparrs , tree_arr , finaltree , zuiroot , res);
                        }
                        else
                        {
                          //console.log("my next item isn't available : " + mainCalcarr[mindex].u_id);
                          
                          var nextArray = findUpperArray(wholeJSONarr , wholeUPitems , mainCalcarr[mindex].u_uppercode);
                          var currentPs = currentPosition(nextArray , mainCalcarr[mindex].u_uppercode);
                          //console.log("currentUpperPosition : " + currentPs);

                          shoparrs.push(nextArray[currentPs]);
                          // for make tree structured
                          var tempjson = {text : nextArray[currentPs].u_nickname , 
                                          id : nextArray[currentPs].u_id , items : tree_arr};

                          ///console.log("each changed level uppercode : " + nextArray[currentPs].u_uppercode);
                          //console.log("zuiroot : " + zuiroot);
                          //console.log("tree_arr step : " + JSON.stringify(tree_arr));
                          if(nextArray[currentPs].u_uppercode == zuiroot)
                          {

                            finaltree.push(tempjson);
                            tree_arr = [];
                          }
                          else
                          {
                            tree_arr = [];
                            tree_arr.push(tempjson);
                          }
                          
                          // for make tree structured

                          //console.log("nextArray : " + nextArray);
                          doHiheracied(nextArray , currentPs+1 , wholeJSONarr , wholeUPitems , shoparrs , tree_arr , finaltree , zuiroot , res);
                        }
                      }
                  }
                }
            });
}

function childShop_num(req, res){

  var finder_id;
  if(req.params.id == "x")
    finder_id = req.session.u_metaid;
  else
    finder_id = req.params.id;
  //tree_arr.push({ text : "root" , id : finder_id });

  lrfModel.find({ u_uppercode : finder_id , u_groupid: "2"})
            .then(function(cbdata){
                var mainCalcarr = cbdata;
                var mindex = 0;
                var wholeJSONarr = {};
                var wholeUPitems = [];
                var shoparrs = [];
                var tree_arr = [];
                var finaltree = [];
                var zuiroot = finder_id;

                wholeJSONarr[finder_id] = mainCalcarr;
                wholeUPitems.push(finder_id);

                if(cbdata.length != 0)
                  doHiheracied(mainCalcarr , mindex , wholeJSONarr , wholeUPitems , shoparrs , tree_arr , finaltree , zuiroot , res);
                else
                  res.json({obj : [] , treeobj : []});
            });

}


function get_realvisitornum(ss_arr , shop_visitor_arr , res , mainroot)
{
  //console.log("ss_arr : "+ ss_arr.length);
  var mainshop_title="";
  var mainshop_id="";
  var index = 0;
  lrfModel
    .find({ u_id: mainroot })
    .then(
      function (cbdata){
        mainshop_title = cbdata[0].u_nickname;
        mainshop_id = cbdata[0].u_id;

        //console.log("mainshop_title : " + mainshop_title);
        if(ss_arr.length != 0)
        {
          for(var i=0;i<ss_arr.length;i++)
          {
            lrfModel
              .find({u_uppercode: ss_arr[i].u_id , u_groupid: "1"})
              .then(
                function (cbdata1){
                  index++;
                  for(var j=0;j<cbdata1.length;j++)
                  {
                    shop_visitor_arr.push(cbdata1[j]);
                  }
                  //console.log("index : "+i+"  shop_length : " + shop_visitor_arr.length);
                  if( index==(ss_arr.length) )
                  {
                    //console.log("stitle : "+ mainshop_title + " svlength : " + shop_visitor_arr.length);
                    res.json({sid : mainshop_id , stitle : mainshop_title , obj : shop_visitor_arr});
                  }
                },
                function (err1){

                }
              );
          }
        }
        else{
          res.json({sid : mainshop_id , stitle : mainshop_title , obj : shop_visitor_arr});
        }

      },
      function (err){

      }
    );
}

function doHiheraciedForMem(mainCalcarr , mindex , wholeJSONarr , wholeUPitems , shoparrs , zuiroot , res , shop_visitor_arr)
{
  //console.log("arigato~" + mindex);
  lrfModel.find({ u_uppercode : mainCalcarr[mindex].u_id , u_groupid: "2"})
            .then(function(cbdata){
                if(cbdata.length > 0)
                {
                  wholeJSONarr[mainCalcarr[mindex].u_id] = cbdata;
                  wholeUPitems.push(mainCalcarr[mindex].u_id);

                  //console.log("i have childs : " + mainCalcarr[mindex].u_id);
                  doHiheraciedForMem(cbdata , 0 , wholeJSONarr , wholeUPitems , shoparrs , zuiroot , res , shop_visitor_arr);
                }
                else if(cbdata.length == 0)
                {
                  var deadline = CalcLength(wholeJSONarr , wholeUPitems);
                  //console.log("shoplen : " + shoparrs.length + " / " + "deadline : " + deadline);

                  if(shoparrs.length<deadline)
                  {
                    shoparrs.push(mainCalcarr[mindex]);

                      if(shoparrs.length == deadline)
                      {
                        //console.log("*****THE END*****");
                        //var tempfinal = [{ text : "root" , id : zuiroot , items : finaltree}];
                        //console.log("final tree_arr : " + JSON.stringify(tempfinal));
                        //res.json({obj : shoparrs , treeobj : tempfinal});
                        get_realvisitornum(shoparrs , shop_visitor_arr , res , zuiroot);

                        
                      }
                      else
                      {
                        if((mindex+1) != mainCalcarr.length)
                        {
                          //console.log("my next item is available : " + mainCalcarr[mindex].u_id);
                          doHiheraciedForMem(mainCalcarr , mindex+1 , wholeJSONarr , wholeUPitems , shoparrs , zuiroot , res , shop_visitor_arr);
                        }
                        else
                        {
                          //console.log("my next item isn't available : " + mainCalcarr[mindex].u_id);
                          
                          var nextArray = findUpperArray(wholeJSONarr , wholeUPitems , mainCalcarr[mindex].u_uppercode);
                          var currentPs = currentPosition(nextArray , mainCalcarr[mindex].u_uppercode);
                          //console.log("currentUpperPosition : " + currentPs);

                          shoparrs.push(nextArray[currentPs]);

                          //console.log("nextArray : " + nextArray);
                          doHiheraciedForMem(nextArray , currentPs+1 , wholeJSONarr , wholeUPitems , shoparrs, zuiroot , res , shop_visitor_arr);
                        }
                      }
                  }
                }
            });
}

function visitor_num(req, res){


  var shop_visitor_arr = [];
 // console.log("rrr : " + req.params.id);

  lrfModel
    .find({u_uppercode: req.params.id , u_groupid: "1"})
    .then(
      function (cbdata){
            for(var i=0;i<cbdata.length;i++)
              {shop_visitor_arr.push(cbdata[i]);}

            lrfModel.find({ u_uppercode : req.params.id , u_groupid: "2"})
            .then(function(cbdata1){

                var mainCalcarr = cbdata1;
                //console.log("gh : " + cbdata1);
                var mindex = 0;
                var wholeJSONarr = {};
                var wholeUPitems = [];
                var shoparrs = [];
                var zuiroot = req.params.id;
                var finder_id = req.params.id;

                wholeJSONarr[finder_id] = mainCalcarr;
                wholeUPitems.push(finder_id);

                //console.log("len : " + cbdata1.length);

                if(cbdata1.length != 0)
                   doHiheraciedForMem(mainCalcarr , mindex , wholeJSONarr , wholeUPitems , shoparrs , zuiroot , res , shop_visitor_arr);
                else
                   get_realvisitornum([] , shop_visitor_arr , res , zuiroot);
            });
          
          //get_visitorsnum(req.params.id , ss_arr , res , shop_visitor_arr , req.params.id , temp , global1);

      },
      function (err){

      }
    );



}

function childShop(req, res){

//console.log("req.params.id : "+req.params.id);
  var finder_id;
  if(req.params.id == "x")
    finder_id = req.session.u_metaid;
  else
    finder_id = req.params.id;

  lrfModel
    .find({ u_uppercode: finder_id  , u_groupid: 2})
    .then(
      function (cbdata){
        res.json(cbdata);
      },
      function (err){

      }
    );
}

function d_visitor_num(req, res){

  var finder_id;
  if(req.params.id == "x")
    finder_id = req.session.u_metaid;
  else
    finder_id = req.params.id;

  lrfModel
    .find({ u_uppercode: finder_id  , u_groupid: 1})
    .then(
      function (cbdata){
        res.json(cbdata);
      },
      function (err){

      }
    );
}

//-- end page1 functions--

//-- start page2 functions--

function bojong3(resulter , res , mysesor) // self bojong
{
  for( var i=0; i<resulter.length; i++)
  {
      resulter[i]['pakgal_spname'] = sp_texts[resulter[i].sp_requested_type-1] ;

      //console.log("pakgal : " + resulter[i]['pakgal_spname'] );

      

      resulter[i]['datetime_text'] = dateFormat(resulter[i].sp_datetime , 'yyyy-mm-dd hh:mm:ss');
      if(resulter[i].sp_implemented_result == 3)
        resulter[i]['impdate_text'] = "";
      else  
        resulter[i]['impdate_text'] = dateFormat(resulter[i].sp_implemented_date , 'yyyy-mm-dd hh:mm:ss');

      resulter[i]['orig_implemented_result'] = resulter[i].sp_implemented_result;
      resulter[i].sp_implemented_result = req_results_texts[ parseInt(resulter[i].sp_implemented_result)-1 ];

      resulter[i]['upcode'] = mysesor.u_uppercode;
      console.log(resulter[i]['datetime_text']);

  }
  res.json( resulter );
  //console.log("bojong3 :   " + resulter);
}

function bojong2(c2 , resulter , res , indexarr , mysesor) //for servicer name
  {
    //console.log('bojong2 : ser : ' + resulter[c2].sp_servicer_id);
    //console.log("indexarr len : " + indexarr);
    //console.log("c2 : " + c2);
    if(c2 == indexarr.length)
          {

            //console.log('bojong2 :   '+resulter);

            bojong3(resulter , res , mysesor);

          }

      lrfModel
      .find( {u_id : resulter[ indexarr[c2] ].sp_servicer_id } )
      .then(
        function (cbdata){

          if(c2 < indexarr.length)
          {

              resulter[ indexarr[c2] ].sp_servicer_id = cbdata[0].u_nickname;

              //console.log('re : ' + resulter[ indexarr[c2] ].sp_servicer_id);
            c2++;
            bojong2(c2 , resulter , res , indexarr , mysesor);
          }

        },
        function (err){

        }
      );  
    

  }

function bojong1(c1 , resulter , res , mysesor) //for requrester type and name
  {

      if(c1 == resulter.length)
          {

            //console.log('bojong1 :   '+resulter);
            var c2 = 0;
            var indexarr = [];
            for(var j=0; j<resulter.length; j++)
            {
              //console.log("!!! : "+resulter[j].sp_servicer_id);
              if( (resulter[j].sp_servicer_id != '') && (resulter[j].sp_servicer_id != undefined) )
                indexarr.push(j);
            }
            
            bojong2(c2 , resulter , res , indexarr , mysesor);

          }

      lrfModel
      .find( {u_id : resulter[c1].sp_requested_id } )
      .then(
        function (cbdata){
          
          if(c1 < resulter.length)
          {
              var spb = parseInt(resulter[c1].sp_bank_info);
              var rval = 5 - Math.abs(5-spb);
              resulter[c1]['sp_bankinfo_str'] = sp_bankinfo_texts[rval];

              resulter[c1]['oiriginal_id'] = resulter[c1].sp_requested_id;
              resulter[c1].sp_requested_id = cbdata[0].u_nickname;
              if(cbdata[0].u_groupid == 1)
                resulter[c1]['req_type'] = 'user';
              if(cbdata[0].u_groupid == 2)
                resulter[c1]['req_type'] = 'shop';

              //console.log('re : ' + resulter[c1].req_type);
            c1++;
            bojong1(c1 , resulter , res , mysesor);
          }
          

        },
        function (err){

        }
      );  

  }

function find_Save_pay_num(req, res)
{
  var obj = req.body;
    
    var fil_obj = {};
    var date_obj = {};
    if(obj.bk_name != undefined)
      fil_obj['sp_others.sru_bank_username'] = obj.bk_name;

    if(obj.methodtype != undefined)
      fil_obj['sp_requested_type'] = parseInt(obj.methodtype);

    if(obj.status != undefined)
    {
      if(parseInt(obj.status) != 5)
        fil_obj['sp_implemented_result'] = obj.status;
      else if(parseInt(obj.status) == 5)
        fil_obj['$and'] = [ {"sp_implemented_result": { $ne: '3' }} , {"sp_implemented_result": { $ne: '4' }} ];
    }

    if(obj.fromdate != undefined)
       date_obj['$gte'] = new Date(obj.fromdate);

    if(obj.todate != undefined)
      date_obj['$lt'] = new Date(obj.todate);

    if((obj.fromdate != undefined) || (obj.todate != undefined))
      fil_obj['sp_datetime'] = date_obj;

    if(obj.sp_name != undefined)
      fil_obj['uinfo2.u_nickname'] = obj.sp_name;

    if(obj.imp_manager != undefined)
      fil_obj['uinfo1.u_nickname'] = obj.imp_manager;

    if(obj.sp_usertype != undefined)
      fil_obj['uinfo2.u_groupid'] = obj.sp_usertype;


    if(obj.sp_myid != undefined)
      fil_obj['uinfo2.u_id'] = obj.sp_myid;

      var param = [];
      if(obj.imp_manager != undefined)
      {
          param = [
              {
                $lookup:{
                    from: "user_info",
                    localField: "sp_servicer_id",
                    foreignField: "u_id",
                    as: "uinfo1"
                }
              },

              {
                $lookup:{
                    from: "user_info",
                    localField: "sp_requested_id",
                    foreignField: "u_id",
                    as: "uinfo2"
                }
              },

              {
                    $unwind: "$uinfo1"
              },
              {
                    $unwind: "$uinfo2"
              }];
          
       }
       else
       {
          console.log("servicer is null~~~");
              param = [
              {
                $lookup:{
                    from: "user_info",
                    localField: "sp_requested_id",
                    foreignField: "u_id",
                    as: "uinfo2"
                }
              },
              {
                    $unwind: "$uinfo2"
              }];
       }

       if(req.session.u_uppercode == "1")
       {
        fil_obj['$or'] = [ { 'uinfo2.u_uppercode' : req.session.u_metaid }, { 'uinfo2.u_uppertype' : '3' } ];
       } // its bonsa

       else
       {
          fil_obj['uinfo2.u_uppercode'] = req.session.u_metaid; 

          if(fil_obj['uinfo2.u_id'] == fil_obj['uinfo2.u_uppercode'])
            fil_obj['uinfo2.u_uppercode'] = req.session.u_uppercode;
       }

       console.log("my filobj~~ : " + JSON.stringify(fil_obj));

       param.push({
                $match : fil_obj
              });
       param.push({
                    $count : "totalnum"
                  });

       spModel.aggregate(param,function (err,docs) {

                //console.log("datas : " + docs.length );
                console.log("err : " + err );
                if(docs.length > 0)
                {
                  res.json({"totalamount" : docs[0].totalnum});
                }
                else{
                  
                  res.json({"totalamount" : 0});
                }

            });
}

function find_spr(req, res){

     var obj = req.body;
    
    var fil_obj = {};
    var date_obj = {};

    if(obj.bk_name != undefined)
      fil_obj['sp_others.sru_bank_username'] = obj.bk_name;

    if(obj.methodtype != undefined)
      fil_obj['sp_requested_type'] = parseInt(obj.methodtype);

    if(obj.status != undefined)
    {
      if(parseInt(obj.status) != 5)
        fil_obj['sp_implemented_result'] = obj.status;
      else if(parseInt(obj.status) == 5)
        fil_obj['$and'] = [ {"sp_implemented_result": { $ne: '3' }} , {"sp_implemented_result": { $ne: '4' }} ];
    }

    if(obj.fromdate != undefined)
       date_obj['$gte'] = new Date(obj.fromdate);

    if(obj.todate != undefined)
      date_obj['$lt'] = new Date(obj.todate);

    if((obj.fromdate != undefined) || (obj.todate != undefined))
      fil_obj['sp_datetime'] = date_obj;

    if(obj.sp_name != undefined)
      fil_obj['uinfo2.u_nickname'] = obj.sp_name;

    if(obj.imp_manager != undefined)
      fil_obj['uinfo1.u_nickname'] = obj.imp_manager;

    if(obj.sp_usertype != undefined)
      fil_obj['uinfo2.u_groupid'] = obj.sp_usertype;


    if(obj.sp_myid != undefined)
      fil_obj['uinfo2.u_id'] = obj.sp_myid;

      var param = [];
      if(obj.imp_manager != undefined)
      {
          param = [
              { $sort : { "sp_datetime" : -1} },
              {
                $lookup:{
                    from: "user_info",
                    localField: "sp_servicer_id",
                    foreignField: "u_id",
                    as: "uinfo1"
                }
              },

              {
                $lookup:{
                    from: "user_info",
                    localField: "sp_requested_id",
                    foreignField: "u_id",
                    as: "uinfo2"
                }
              },

              {
                    $unwind: "$uinfo1"
              },
              {
                    $unwind: "$uinfo2"
              }];
          
       }
       else
       {
          console.log("servicer is null~~~");
              param = [
              { $sort : { "sp_datetime" : -1} },
              {
                $lookup:{
                    from: "user_info",
                    localField: "sp_requested_id",
                    foreignField: "u_id",
                    as: "uinfo2"
                }
              },
              {
                    $unwind: "$uinfo2"
              }];
       }

       if(req.session.u_uppercode == "1")
       {
        fil_obj['$or'] = [ { 'uinfo2.u_uppercode' : req.session.u_metaid }, { 'uinfo2.u_uppertype' : '3' } ];
       } // its bonsa

       else
       {
          fil_obj['uinfo2.u_uppercode'] = req.session.u_metaid; 

          if(fil_obj['uinfo2.u_id'] == fil_obj['uinfo2.u_uppercode'])
            fil_obj['uinfo2.u_uppercode'] = req.session.u_uppercode;
       }



       param.push({
                $match : fil_obj
              });
       param.push({
                    $skip : parseInt((obj.currentpage-1)*obj.numperpage)
                  });
       param.push({
                    $limit : parseInt(obj.numperpage)
                  });

       console.log("fil param : " + JSON.stringify(param));

       spModel.aggregate(param,function (err,docs) {

                //console.log("datas : " + docs.length );
                console.log("err : " + err );
                if(docs.length > 0)
                {
                  var c1 = 0;
                  var mysesor = req.session;
                  bojong1(c1 , docs , res , mysesor);
                }
                else{
                  var empty = [];
                  res.json(empty);
                }

            });

  }

  function get_updated_mem(req, res)
  {

    var temp = req.session.selected_updated_visitor;
    req.session.selected_updated_visitor = null;
    //console.log("from updated visitor : "+selected_updated_visitor);
    console.log("get updated member data : " + JSON.stringify(temp));
    res.json(temp);
  }

  function update_implemented(req, res)
  {
    var update_imp = req.body;
    var indexer = req.params.id;

    

    update_imp['sp_servicer_id'] = req.session.u_id;
    console.log("update index : " + indexer + " update data :  " + JSON.stringify(update_imp) );

    spModel.aggregate([
                      {
                        $lookup:{
                              from: "user_info",
                              localField: "sp_requested_id",
                              foreignField: "u_id",
                              as: "spsinfo"
                                }
                      }, 
                      { 
                        $match : { "sp_id" :  update_imp.sp_id} 
                      } 
                      ],function (err,docs) {
                        console.log("ss _id : " + indexer);
                                console.log("updated ddd : " + JSON.stringify(docs));
                                console.log(docs[0].sp_requested_type + "/" + docs[0].spsinfo[0].u_groupid);
                                if(docs[0].sp_requested_type == 1)
                                {
                                  if(docs[0].spsinfo[0].u_groupid == '1')
                                    req.session.sounds_json.vsq -= 1;
                                  if(docs[0].spsinfo[0].u_groupid == '2')
                                    req.session.sounds_json.ssq -= 1;
                                }
                                else if(docs[0].sp_requested_type == 2)
                                {
                                  if(docs[0].spsinfo[0].u_groupid == '1')
                                    req.session.sounds_json.vpq -= 1;
                                  if(docs[0].spsinfo[0].u_groupid == '2')
                                    req.session.sounds_json.spq -= 1;
                                }

                                if(req.session.sounds_json.vsq < 0)
                                  req.session.sounds_json.vsq = 0;
                                if(req.session.sounds_json.ssq < 0)
                                  req.session.sounds_json.ssq = 0;
                                if(req.session.sounds_json.vpq < 0)
                                  req.session.sounds_json.vpq = 0;
                                if(req.session.sounds_json.spq < 0)
                                  req.session.sounds_json.spq = 0;

                                console.log("soundssss : " + JSON.stringify(req.session.sounds_json));

                                spModel
                                  .update({_id : indexer} , update_imp)
                                  .then(
                                    function (cbdata){
                  
                                          res.json({result : "success"});

                                          allstatus_flag = 100;

                                          console.log("emit data : " + JSON.stringify({"receiverid":update_imp.sp_requested_id, "receiverdeep" : docs[0].spsinfo[0].u_deep , "result" : 2 ,  "diffid":update_imp.sp_id}));


                                          if(docs[0].spsinfo[0].u_groupid == '1') // VISITOR
                                          clientsocket.emit('TRANSFER_SHOPVISITOR' , {"receiverid":update_imp.sp_requested_id, "receiverdeep" : 100 , "result" : 2 ,  "diffid":update_imp.sp_id});
                                          if(docs[0].spsinfo[0].u_groupid == '2') // SHOP
                                          clientsocket.emit('TRANSFER_SHOPSHOP' , {"receiverid":update_imp.sp_requested_id, "receiverdeep" : docs[0].spsinfo[0].u_deep , "result" : 2 ,  "diffid":update_imp.sp_id});
                                  },
                                  function (err){

                                  }
                                );

                       });


             
  }

  function addEpData(req , res)
  {
    var epbody = req.body;
    epModel
      .create(epbody)
      .then(
        function (postObj){
          checkResult = {result : true};
            res.json(checkResult);
        },
        function (error){
          checkResult = {result : false};
            res.json(checkResult);
        }
      );
  }

  function addMSGbySP(req, res)
  {
    var m_body = req.body;
    //console.log("m_body1 : " + JSON.stringify(m_body));
    m_body['m_senderid'] = req.session.u_id;
    lrfModel
      .find({ u_id : m_body.m_receiverid })
      .then(
        function (cbdata){

          //console.log("cbdata 0 ; " + parseInt(cbdata[0].u_groupid));

          var type_obj = { receiver_type : parseInt(cbdata[0].u_groupid) , send_type : 0 , level_type : 4 };
          m_body['m_type'] = type_obj;

          //console.log("m_body2 : " + JSON.stringify(m_body));

            mModel
              .create(m_body)
              .then(
                function (cbdata1){
                  res.json( {result : true} );
                });
            
        });

  }

  //--- end page 2 ---

  //--- start page 7 new ---

  function usidcheck(req , res) {
    
    lrfModel.find(req.body)
              .then(function (usiddata) {
                  if(usiddata.length != 0)
                    res.json({result : true});
                  else
                    res.json({result : false});
              });

  }

  function add_visitor(req, res)
  {
    var users = req.body;
    users['u_registerdate'] = new Date();
    console.log(users);
    var checkResult = {};
    lrfModel.find({"u_id" : users.u_uppercode})
              .then(function(forpth){
               var uppath = forpth[0].paths;

               users['paths'] = uppath;

                                          lrfModel
                                            .create(users)
                                            .then(
                                              function (postObj){
                                                checkResult = {result : true};
                                                  res.json(checkResult);

                                                  allstatus_flag = 100;

                                              },
                                              function (error){
                                                checkResult = {result : false};
                                                  res.json(checkResult);
                                              }
                                            );

              });
    
  }

  function checkInBlist(req, res)
  {
    var name = req.body.u_nickname;
    saModel.find( { sa_id : 1 } )
          .then(function (cbdata) {

            if(cbdata.length > 0) 
            {
              if(cbdata[0].sa_arr.length > 0)
              {
                cbdata[0].sa_arr.forEach(function(item){
                  if(item == name)
                    res.json({ result : false });
                });
                res.json({ result : true });
              }
              else
                res.json({ result : true });
            }
            else
              res.json({ result : true });

      });
  }

  //--- end page 7 new ---

  //--- start page 7---

  function doVisitorBonasProfit(ssbody , res)
  {
    var end_date = new Date();
    var start_date = new Date();
    start_date.setHours(0,0,0,0);
    //console.log("st : " + start_date.toString() + " - ed : " + end_date.toString());

    

    async.eachSeries(ssbody,function(item,callback) 
                            {
                                //console.log("ssbody item : " + item.u_id);

                                epModel.aggregate([
                                {
                                    $match : { ep_loginid : "aaa" , ep_type : 126 ,  "ep_others.ep_ag_arr" : item.u_id   }
                                },
                                {
                                    $group : {

                                    _id : "$ep_loginid" , 
                                    allfeed : { $sum : "$ep_money" } , 
                                    todayfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", start_date ]} , {$lt: [ "$ep_datetime", end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                } 

                                    }
                                }
                                ],function (err,docs) {

                                    var str_res = JSON.stringify(docs);
                                    //console.log("shop self profit : " + str_res );
                                      
                                      if(allfeed(str_res) != null)
                                      {
                                       item['whole_bonsa_profit'] = allfeed(str_res)[2];
                                        //console.log("n1 : " + item.whole_bonsa_profit);
                                      }
                                      if(todayfeed(str_res) != null)
                                      {
                                       item['today_bonsa_profit'] = todayfeed(str_res)[2];
                                        //console.log("n2 : " + item.today_bonsa_profit);
                                      }

                                    
                                    callback();
                                });

                            },
                            function(err) {
                                if (err) throw err;

                                //console.log("hallo");
                                

                                res.json( { obj : ssbody } );


                    });
  }

  function startForVisitor_profit(req,res)
  {
    console.log("start shope profit");
    var sbody = req.body;
    doVisitorBonasProfit(sbody , res);
    //doShopBonasProfit(sbody , res);
  }

  function getAllShop(req, res)
  {
    console.log("$%$%");
    lrfModel
      .find({u_groupid : '2'})
      .then(
        function (cbdata){
            console.log('getaallshop : ' + cbdata.length);
            res.json(cbdata);
        },
        function (error){
          
            res.json(error);
        }
      );
  }

  function setforshop(shopobj , res , req)
  {
    
    console.log(" here3 : " + JSON.stringify(shopobj));
    req.session.selected_shop = shopobj;

    
    
    console.log("finalsetter : " + JSON.stringify(req.session.selected_shop));

    dmModel
      .find({ dm_service_houseid : req.session.selected_shop.u_id })
      .then(
        function (cbdata){
            console.log('yogi~ : ' + cbdata.length);

            req.session.selected_shop_dmlist = cbdata;
            res.json(shopobj);

        },
        function (error){
          
            res.json(error);
        }
      );
  }

  function get_comment_from_eventbonus_table(pbody , res , fflag ,req)
  {

           // Array to hold async tasks
         
           var records = [ [1,2] , [] , [3] ];
           var cout = 0;
           var gijun = 0;
           var findex = 0;
           var sindex = 0;
           //console.log("pbody len : " + pbody.length);

           pbody.forEach(function(pitem){

            
            if(pitem.eb_array !=undefined){
              if(pitem.eb_array.length != 0){
                  //console.log("in : " + JSON.stringify(pitem.eb_array));
                  gijun += pitem.eb_array.length;
                }
            }

            //console.log("out : " + JSON.stringify(pitem.eb_array));
           });

           //console.log("gijun : " + gijun);
           if(gijun != 0){
               pbody.forEach(function(pitem){
                  //console.log("uid : " +pitem.u_id);
                  if(pitem.eb_array !=undefined){
                      if(pitem.eb_array.length != 0){

                            sindex = 0;
                            //console.log('pitem length : ' + pitem.eb_array.length);
                            async.eachSeries(pitem.eb_array,function(item,callback) 
                            {
                                var prvtime = new Date().getTime();

                                ebImpModel.find( { ebi_id : item.metaid } , function (err, rows) {

                                    var cttime = new Date().getTime();
                                    console.log("periods2~~ : " + (cttime - prvtime));

                                    cout++;
                                    item.comment = rows[0].ebcomment;
                                    callback(err)
                                });

                                sindex++;
                            },
                            function(err) {
                                if (err) throw err;

                                if(cout == gijun){
                                    //console.log("earn/pay detail done : " + pbody.length);
                                    if(fflag == 1)
                                    {
                                      //console.log("here2 : "+ pbody[0].savepay );
                                      var fnum = parseInt(pbody[0].orig_gid);
                                      if(fnum == 2)
                                      {
                                        pbody[0]['fflag'] = 1;
                                        setforshop(pbody[0] ,res , req);
                                      }
                                      if(fnum > 2)
                                      {
                                        req.session.selected_manager = pbody[0];
                                        res.json(pbody);
                                      }
                                    }
                                    res.json(pbody);
                                }
                            });

                        }

                  }
                  findex++;

              });
          }
          else{
            console.log("cc fflga : " + fflag);
            if(fflag == 1)
              {
                //console.log("here2 : "+ pbody[0].savepay );
                var fnum = parseInt(pbody[0].orig_gid);
                if(fnum == 2)
                {
                  pbody[0]['fflag'] = 1;
                  setforshop(pbody[0] ,res , req);
                }
                if(fnum > 2)
                {
                  req.session.selected_manager = pbody[0];
                  res.json(pbody);
                }
                
              }
            //console.log("pbody lenenen : " + pbody.length);
            res.json(pbody);
          }

  }

  function do_other_details(pbody , cout , res , fflag ,req) // get visitor earned, shop earned , save/pay amount , etc by id
  {
    console.log("cout : " + cout + "len : " + pbody.length);
    var st = new Date();
    var et = new Date();
    st.setHours(0,0,0,0);
    et.setHours(23,59,59,0);
    //console.log("current date : "+ et.toLocaleString());
    if(cout == pbody.length)
    {
      //res.json(pbody);
      //console.log("do other detail : " + pbody[0].savepay);
      get_comment_from_eventbonus_table( pbody , res , fflag ,req);
    }
    if(cout<pbody.length)
    {
      epModel
          .find( { ep_loginid : pbody[cout].u_id } )
          .then(
            function (cbdata){

              var save_amount = 0;
              var pay_amount = 0;
              var balance = 0;
              var win_num = 0;
              var failed_num = 0;
              var whole_shop_earned = 0;
              var today_shop_earned = 0;
              var whole_visitor_earned = 0;
              var today_visitor_earned = 0;
              var eb_array = [];
             

              //console.log("cbdata :   "+cbdata.length);
              if(cbdata.length>0)
              { 
                for(var i=0; i<cbdata.length; i++)
                {

                      if((cbdata[i].ep_type == 101) || (cbdata[i].ep_type == 102))
                        save_amount += cbdata[i].ep_money;

                      if((cbdata[i].ep_type == 1) || (cbdata[i].ep_type == 2))
                        pay_amount += cbdata[i].ep_money;


                      if((cbdata[i].ep_type >= 103) && (cbdata[i].ep_type <= 123)){

                        win_num += 1;
                        if(cbdata[i].ep_others != undefined)
                        {
                          if(cbdata[i].ep_others.money != undefined)
                            whole_shop_earned += cbdata[i].ep_others.money;
                        }
                        if(cbdata[i].ep_money != undefined)
                         whole_visitor_earned += cbdata[i].ep_money;

                        if((cbdata[i].ep_datetime >= st) && (cbdata[i].ep_datetime <= et))
                        {
                            if(cbdata[i].ep_others != undefined) {
                                if (cbdata[i].ep_others.money != undefined)
                                    today_shop_earned += cbdata[i].ep_others.money;
                            }
                            if(cbdata[i].ep_money != undefined)
                              today_visitor_earned += cbdata[i].ep_money;
                        }

                      }


                      if((cbdata[i].ep_type >= 3) && (cbdata[i].ep_type <= 23)){

                        failed_num += 1;
                          if(cbdata[i].ep_others != undefined) {
                              if (cbdata[i].ep_others.money != undefined)
                                  whole_shop_earned += cbdata[i].ep_others.money;
                          }
                          if(cbdata[i].ep_money != undefined)
                            whole_visitor_earned -= cbdata[i].ep_money;

                        if((cbdata[i].ep_datetime >= st) && (cbdata[i].ep_datetime <= et))
                        {
                            if(cbdata[i].ep_others != undefined) {
                                if (cbdata[i].ep_others.money != undefined)
                                    today_shop_earned += cbdata[i].ep_others.money;
                            }
                            if(cbdata[i].ep_money != undefined)
                              today_visitor_earned -= cbdata[i].ep_money;
                        }
                      }

                      if((cbdata[i].ep_type == 124) || (cbdata[i].ep_type == 125))
                      {
                          whole_visitor_earned += cbdata[i].ep_money;

                          if((cbdata[i].ep_datetime >= st) && (cbdata[i].ep_datetime <= et))
                            today_visitor_earned += cbdata[i].ep_money;

                          var ebtype;
                          if(cbdata[i].ep_type == 124)
                            ebtype = "event";
                          if(cbdata[i].ep_type == 125)
                            ebtype = "bonus";

                          eb_array.push( {id : cbdata[i].ep_id , datetime : cbdata[i].ep_datetime.toLocaleString() , type : ebtype , amount : cbdata[i].ep_money , metaid : cbdata[i].ep_metaid , comment : "" } );

                          //console.log({datetime : cbdata[i].ep_datetime , type : ebtype , amount : cbdata[i].ep_money , metaid : cbdata[i].ep_metaid , comment : "" });

                      }

                   // console.log("sssce : " + i);

                }
                balance = save_amount - pay_amount;


              pbody[cout]['savepay'] = save_amount + " / " + pay_amount;
              pbody[cout]['balance'] = balance;
              pbody[cout]['winfailed'] = win_num + " / " + failed_num;
              pbody[cout]['today_shop_earned'] = today_shop_earned;
              pbody[cout]['whole_shop_earned'] = whole_shop_earned;
              pbody[cout]['today_visitor_earned'] = today_visitor_earned;
              pbody[cout]['whole_visitor_earned'] = whole_visitor_earned;
              //console.log("cout :  " + cout);
              //console.log("eb_array :  " + pbody[cout]['savepay']);
              pbody[cout]['eb_array'] = eb_array;

              }

              

              cout++;
              do_other_details(pbody , cout , res , fflag ,req);
            },
            function (err){

            }
          );

    }
  }

  function get_other_detail(req, res)
  {
    var ubody = req.body;
    console.log("ubobobody :  " + ubody.length);
    if( ubody.length !=0 )
    {
    var ccout = 0;
    var fflag = 0;
    do_other_details(ubody , ccout , res , fflag , req);
    }
    else
    {
      res.json(ubody);
    }

    //console.log("here's other detail   : " + ubody[0].u_id);
  }

  function format_duration(reqval)
  {
    var calc_sec = Math.round(reqval / 1000);
    var sec = Math.round(calc_sec % 60);
    var min = Math.round(Math.round(calc_sec / 60) % 60);
    var hours = Math.round(Math.round(calc_sec / 3600) % 60);
    var day = Math.round(Math.round(calc_sec / (3600*24) ) % 24);
    var duration_str = day + "d : " + hours + "h : " + min + "m : " + sec + "s";
    return duration_str;
  }

  function getMatchedMemData(req, res)
  {
    var obj = req.body.main;
    
    var fil_obj = {};

    var date_obj = {};

    fil_obj['u_groupid'] = '1';

    if(obj.fromdate != undefined)
       date_obj['$gte'] = obj.fromdate;

    if(obj.todate != undefined)
      date_obj['$lt'] = obj.todate;

    if((obj.fromdate != undefined) || (obj.todate != undefined))
      fil_obj['u_registerdate'] = date_obj;

    if(obj.uu_id != undefined)
      fil_obj['u_id'] = obj.uu_id;

    if(obj.uu_nickname != undefined)
      fil_obj['u_nickname'] = obj.uu_nickname;

    if(obj.uu_mail != undefined)
      fil_obj['u_email'] = obj.uu_mail;

    if(obj.uu_ip != undefined)
      fil_obj['u_lastloginip'] = obj.uu_ip;

    if(obj.u_uppertype != undefined)
      fil_obj['u_uppertype'] = obj.u_uppertype;

    
    //check flag

    var cfval = req.body.other;


    lrfModel.find({ "u_id" : cfval.targetid })
            .then(function(addtata){

              var tg_paths = addtata[0].paths; // will modify
              
              if(cfval.opt == 0) // all members
                 fil_obj['paths'] = { $all: tg_paths } ;
              else
                 fil_obj['paths'] = tg_paths ;

               console.log("for matched : " + JSON.stringify(fil_obj));

              lrfModel
                .find(fil_obj)
                .count()
                .then(
                  function (cbdata){
                      console.log('all matched length!! : ' + cbdata);
                      res.json({ matched_len : cbdata });
                      
                  },
                  function (error){
                    
                      res.json(error);
                  }
                );

            });
    
  }

  function bojong_for_mem(c2 , mem_resulter , res)
  {

    
    if(c2 == mem_resulter.length)
          {

            //console.log('bojong_for_mem :   '+mem_resulter.length);
            res.json(mem_resulter);
          }


    if(c2 < mem_resulter.length)
     {
          //console.log("upperco : " + mem_resulter[c2].u_uppercode);
          lrfModel
          .find( { u_id : mem_resulter[c2].u_uppercode } )
          .then(
            function (cbdata){

                  //console.log("c2 : " + c2 + " : memlen : " + mem_resulter.length + " : visitor- " + mem_resulter[ c2 ].u_id);

                  mem_resulter[ c2 ].u_uppercode = cbdata[0].u_nickname;
                  mem_resulter[ c2 ]['str_registerdate'] = mem_resulter[ c2 ].u_registerdate.toLocaleString();
                  if(mem_resulter[ c2 ].u_lastlogin != undefined)
                  mem_resulter[ c2 ]['str_lastlogin'] = mem_resulter[ c2 ].u_lastlogin.toLocaleString();

                  if((mem_resulter[ c2 ].u_lastlogin != undefined) && (mem_resulter[ c2 ].u_lastlogout != undefined)){
                      var hourDiff = mem_resulter[ c2 ].u_lastlogout - mem_resulter[ c2 ].u_lastlogin;  
                      mem_resulter[ c2 ]['str_duration'] = format_duration(hourDiff);
                    }

                  mem_resulter[ c2 ]['orig_uppertype'] = mem_resulter[ c2 ].u_uppertype;

                  if( mem_resulter[ c2 ].u_uppertype == '2' )
                    mem_resulter[ c2 ].u_uppertype = 'self';
                  if( mem_resulter[ c2 ].u_uppertype == '3' )
                    mem_resulter[ c2 ].u_uppertype = 'market';

                  //console.log("changeddate : " + mem_resulter[ c2 ].str_registerdate);
                  //mem_resulter[ c2 ]['str_lastlogin'] = mem_resulter[ c2 ].u_lastlogin.toLocaleString();

                c2++;
                bojong_for_mem(c2 , mem_resulter , res );

            },
            function (err){

            }
          ); 
      } 
  }

  function find_member_rbd(req, res)
  {

    console.log("kal ko!");

    var obj = req.body.main;
    
    var fil_obj = {};

    var date_obj = {};

    fil_obj['u_groupid'] = '1';

    if(obj.fromdate != undefined)
       date_obj['$gte'] = obj.fromdate;

    if(obj.todate != undefined)
      date_obj['$lt'] = obj.todate;

    if((obj.fromdate != undefined) || (obj.todate != undefined))
      fil_obj['u_registerdate'] = date_obj;

    if(obj.uu_id != undefined)
      fil_obj['u_id'] = obj.uu_id;

    if(obj.uu_nickname != undefined)
      fil_obj['u_nickname'] = obj.uu_nickname;

    if(obj.uu_mail != undefined)
      fil_obj['u_email'] = obj.uu_mail;

    if(obj.uu_ip != undefined)
      fil_obj['u_lastloginip'] = obj.uu_ip;

    if(obj.u_uppertype != undefined)
      fil_obj['u_uppertype'] = obj.u_uppertype;
    

    //check flag

    var cfval = req.body.other;

    lrfModel.find({ "u_id" : cfval.targetid })
            .then(function(addtata){
             

              console.log("tg_paths : " + addtata[0].paths);
              var tg_paths = addtata[0].paths; // will modify
              
              if(cfval.opt == 0) // all members
                 fil_obj['paths'] = { $all: tg_paths } ;
              else
                 fil_obj['paths'] = tg_paths ;

              console.log("filobj : " + JSON.stringify(fil_obj) );

              console.log("skip :" + parseInt((req.body.pv.currentpage-1)*req.body.pv.numperpage) + " -> limit : " + parseInt(req.body.pv.numperpage));

              lrfModel
                .find(fil_obj)
                .sort({ "u_registerdate" : -1 })
                .skip(parseInt((req.body.pv.currentpage-1)*req.body.pv.numperpage))
                .limit(parseInt(req.body.pv.numperpage))
                .then(
                  function (cbdata){

                      console.log('hey guys!! : ' + cbdata.length);
                      var c2 = 0;
                      bojong_for_mem(c2 , cbdata , res);
                      
                  },
                  function (error){
                    
                      res.json(error);
                  }
                );

            });

  }

  function set_selected_visitor(req , res)
  {
    req.session.selected_visitor = req.body;
    console.log("to selected visitor : "+req.session.selected_visitor.u_id);
    res.json({});
  }

  //--- end page 7---

  //--- start page update 7 ---

  function set_visitor_for_game(req, res)
  {
    req.session.game_playername = req.body.u_nickname;
    console.log("set player : " + req.session.game_playername);
    res.json({});
  }

  function get_selected_mem(req, res)
  {
    var temprjson = req.session.selected_visitor;
    //console.log("temprjson*** : " + JSON.stringify(temprjson));
    res.json(req.session.selected_visitor);
  }

  function update_visitor(req , res)
  {
    var ubody = req.body;

    lrfModel.find({"u_id" : ubody.u_uppercode})
              .then(function(forpth){
               var uppath = forpth[0].paths;

               ubody['paths'] = uppath;

                lrfModel
                  .update({u_id : ubody.u_id} , ubody)
                  .then(
                    function(cbdata) {
                      
                      res.json({result : true})

                    },
                    function(err) {

                    }
                  );
    });
  }

  function set_updated_visitor(req , res)
  {
    req.session.selected_updated_visitor = req.body;
    //console.log("to updated visitor : "+selected_updated_visitor.u_id);
    res.json({result : true});
  }

  //--- end page update 7 ---

  //--- start page 9 ---
  function findAllGameNum(req, res)
  {

    giModel
      .find({}).count()
      .then(
        function (cbdata){

          res.json({ "totalamount" : cbdata });

        },
        function (error){
          
            res.json(error);
        }
      );
  }
  function findAllGameing(req, res)
  {
    var pvmparam = req.body.pvpage;

    giModel
      .aggregate([
                      {
                        $skip : parseInt((pvmparam.currentpage-1)*pvmparam.numperpage)
                      },
                      {
                        $limit : parseInt(pvmparam.numperpage)
                      }

                    ],function(err, cbdata){
                        
                      res.json(cbdata);

                    });
  }

  function findAllGame(req, res)
  {

    giModel
      .find({})
      .then(
        function (cbdata){

          res.json(cbdata);

        },
        function (error){
          
            res.json(error);
        }
      );
  }

  function set_selected_game(req , res)
  {
    req.session.set_selected_game = req.body;
    //console.log("to updated game : "+set_selected_game.ga_id);
    res.json({result : true});
  }

  //--- end page 9 ---

  //--- start page 9 new

  function checkGaid(req, res)
  {
    var gidbody = req.body;
    giModel.find({ ga_id : gidbody.objid })
            .then(function(ggdatas){
              //console.log("ggd : " + ggdatas.length);
              if(ggdatas.length == 0) //there is no same id
                res.json({ result : true });
              else
                res.json({ result : false });
            });
  }

  function getGaMaxid(req , res)
  {
    console.log("here is gamaxid");

    giModel.find().sort({ga_id:-1}).limit(1)
                .then(function (cbdata){
                  console.log("come come");
                  if(cbdata.length == 0)
                  {
                    res.json({ result : 0 });
                  }
                  else{
                  console.log("fff : "+cbdata[0].ga_id);
                  res.json({ result : cbdata[0].ga_id });
                      }
                },
                function (err)
                {
                  console.log("none!!!");
                });
  }



  //--- end page 9 new


  function add_game(req, res)
  {
    var gameobj = req.body;

    console.log(gameobj);
    giModel
      .create(gameobj)
      .then(
        function (postObj){
          checkResult = {result : true};
            res.json(checkResult);
        },
        function (error){
          checkResult = {result : false};
            res.json(checkResult);
        }
      );
  }


  //--- start page 9 update

  function get_selected_game(req , res)
  {
    //console.log("get selected game :   "+set_selected_game);
    res.json(req.session.set_selected_game);
  }

  function update_game(req, res)
  {

    var gameobj = req.body;

    console.log(gameobj);


          giModel
          .update( {_id : gameobj._id} , gameobj)
          .then(
            function (postObj){
              checkResult = {result : true};
                res.json(checkResult);
            },
            function (error){
              checkResult = {result : false};
                res.json(checkResult);
            }
          );

  }

  function maintain_game(req, res)
  {
    var ubody = req.body;
    giModel
      .update({_id : ubody._id} , ubody)
      .then(
        function(cbdata) {
          
          res.json({result : true})

        },
        function(err) {

        }
      );
  }

  //--- end page 9 update

  //-- start page 10 send message

  function getHeadquarterShop_id(req , res)
  {
    lrfModel
      .find({u_uppercode : '1'})
      .then(
        function(cbdata) {
          
          res.json(cbdata)

        },
        function(err) {

        }
      );
  }

  function send_message(req, res)
  {
    var mobj = req.body;

    console.log(mobj);
    mModel
      .create(mobj)
      .then(
        function (postObj){
          checkResult = {result : true};
            res.json(checkResult);
        },
        function (error){
          checkResult = {result : false};
            res.json(checkResult);
        }
      );
  }

  function checkForSendMSM(req, res)
  {
    var fil_param = req.body;
    console.log("param : " + fil_param.u_id );
    lrfModel
      .find( { u_id : fil_param.u_id } )
      .then(
        function (cbdata){
          console.log("checking...." + cbdata.length);
          

          if(cbdata.length == 0 )
            checkResult = {result : false};
          if(cbdata.length == 1 )
            checkResult = {result : true , founded : cbdata[0] };
          
            
            

          console.log("result : " + checkResult.result);
            res.json(checkResult);
        }
      );
  }

  function getMMaxid(req , res)
  {
    mModel.find().sort({m_id:-1}).limit(1)
                .then(function (cbdata){
                  if(cbdata.length == 0)
                  {
                    res.json({ result : 0 });
                  }
                  else
                  {
                  console.log("fff : "+cbdata[0].m_id);
                  res.json({ result : cbdata[0].m_id });
                  }
                },
                function (err)
                {

                });
  }

  //-- end page 10 send message

  //- start page 10 show message 

  function find_message_history_Num(req , res)
  {
    var obj = req.body;
    
    obj['uinfo.u_metaid'] = req.session.u_metaid;

    if(obj.m_datetime != undefined)
    {
      if(obj.m_datetime['$lt'] != undefined)
      obj.m_datetime['$lt'] = new Date(obj.m_datetime['$lt']);

      if(obj.m_datetime['$gte'] != undefined)
      obj.m_datetime['$gte'] = new Date(obj.m_datetime['$gte']);
    }

    console.log(obj);

    mModel.aggregate([{
                        $lookup:{
                              from: "user_info",
                              localField: "m_senderid",
                              foreignField: "u_id",
                              as: "uinfo"
                                }
                      },
                      { $match : obj },
                      { $count : "pagenum" }
                      ],function (err,docs) {

                        console.log("num of found : " + JSON.stringify(docs));
                        if(docs.length == 0)
                          res.json({ "totalamount" : 0 });
                        else
                          res.json({ "totalamount" : docs[0].pagenum });

                      });

  }

  function find_message_history(req , res)
  {
    var obj = req.body.filter_param;
    var pvmyobj = req.body.pvparam;

    if(obj.m_datetime != undefined)
    {
      if(obj.m_datetime['$lt'] != undefined)
      obj.m_datetime['$lt'] = new Date(obj.m_datetime['$lt']);
      if(obj.m_datetime['$gte'] != undefined)
      obj.m_datetime['$gte'] = new Date(obj.m_datetime['$gte']);
    }

    obj['uinfo.u_metaid'] = req.session.u_metaid;

    console.log(obj);
    mModel
      .aggregate([
                      {
                        $lookup:{
                              from: "user_info",
                              localField: "m_senderid",
                              foreignField: "u_id",
                              as: "uinfo"
                                }
                      },
                      { $sort : { "m_datetime" : -1} },
                      { 
                        $match : obj 
                      },
                      {
                        $skip : parseInt((pvmyobj.currentpage-1)*pvmyobj.numperpage)
                      },
                      {
                        $limit : parseInt(pvmyobj.numperpage)
                      }

                    ],function(err, cbdata){
                        for(var i=0; i<cbdata.length; i++)
                        {
                          cbdata[i]['m_datetext'] = dateFormat(cbdata[i].m_datetime , 'yyyy-mm-dd hh:mm:ss');
                        }
                        res.json(cbdata);
                    });
  }

  //- end page 10 show message 

  //- start page 10-

  function find_report_history_Num(req , res)
  {
    var obj = req.body;
    var imp_status = req.params.id;
    var final_result = [];

        obj['m_receiverid'] = req.session.u_metaid;
        obj['m_flag'] = 2;

        if(obj.m_datetime != undefined)
        {
          if(obj.m_datetime['$lt'] != undefined)
          obj.m_datetime['$lt'] = new Date(obj.m_datetime['$lt']);
          if(obj.m_datetime['$gte'] != undefined)
          obj.m_datetime['$gte'] = new Date(obj.m_datetime['$gte']);
        }

        switch(imp_status)
        {
                  case '2':
                    obj['m_imp_servicer'] = { $exists : true };
                    break;
                  case '3':
                    obj['m_imp_servicer'] = { $exists : false };
                    break;
        }

        console.log("params : " + JSON.stringify(obj));
        mModel
          .aggregate([{ $match : obj },
                      { $count : "pagenum"}
                      ],function (err,cbdata) {

                        console.log("pagenum : " + JSON.stringify(cbdata));

                        if(cbdata.length != 0)
                          res.json({ "totalamount" : cbdata[0].pagenum });
                        else
                          res.json({ "totalamount" : 0 });

                      });
  }

  function find_report_history(req , res)
  {
    var obj = req.body.filter_param;
    var mypvobj = req.body.pvobj;
    var imp_status = req.params.id;
    var final_result = [];

        obj['m_receiverid'] = req.session.u_metaid;
        obj['m_flag'] = 2;

        if(obj.m_datetime != undefined)
        {
          if(obj.m_datetime['$lt'] != undefined)
          obj.m_datetime['$lt'] = new Date(obj.m_datetime['$lt']);
          if(obj.m_datetime['$gte'] != undefined)
          obj.m_datetime['$gte'] = new Date(obj.m_datetime['$gte']);
        }

        switch(imp_status)
        {
                  case '2':
                    obj['m_imp_servicer'] = { $exists : true };
                    break;
                  case '3':
                    obj['m_imp_servicer'] = { $exists : false };
                    break;
        }

        console.log("params : " + JSON.stringify(obj));
        mModel
          .aggregate([ 
                      { 
                        $sort : { "m_datetime" : -1} 
                      }, 
                      { 
                        $match : obj 
                      },
                      {
                        $skip : parseInt((mypvobj.currentpage-1)*mypvobj.numperpage)
                      },
                      {
                        $limit : parseInt(mypvobj.numperpage)
                      }
                      ],function (err,cbdata) {

                        console.log("msm list : " + cbdata);
                        for(var i=0; i<cbdata.length; i++)
                        {
                          
                          if( cbdata[i]['m_imp_servicer'] != undefined )
                          {

                            cbdata[i]['m_imp_servicer_nickname'] = cbdata[i].m_imp_servicer;
                            cbdata[i]['m_imp_datetext'] = dateFormat(cbdata[i].m_imp_date , 'yyyy-mm-dd hh:mm:ss');
                          }
                        }

                        final_result = cbdata;
                    

                        for(var i=0; i<final_result.length; i++)
                        {

                          final_result[i]['m_datetext'] = dateFormat(final_result[i].m_datetime , 'yyyy-mm-dd hh:mm:ss');
                          
                        }

                        res.json(final_result);

                      });
  }

  function implement_report(req, res)
  {
    var irBody = req.body;
    var currentDate = new Date();

    var imp_body = {m_imp_date : currentDate , m_imp_servicer : req.session.u_nickname , m_imp_contents : irBody.m_imp_contents}; 

    mModel
      .update({ _id : irBody._id } , imp_body)
      .then(
        function (cbdata){  

          req.session.sounds_json.rpq -= 1;
          if(req.session.sounds_json.rpq < 0)
            req.session.sounds_json.rpq = 0;

          console.log("soundsjs : " + JSON.stringify(req.session.sounds_json));

          console.log("_id : " + irBody._id + "imp_body : " + JSON.stringify(imp_body));
          res.json({ result : true });

          allstatus_flag = 100;

          console.log("emit data : " + JSON.stringify({"receiverid":irBody.m_senderid, "receiverdeep" : 100 , "result" : 3 ,  "diffid":irBody.m_id}));

          clientsocket.emit('TRANSFER_SHOPVISITOR' , {"receiverid":irBody.m_senderid, "receiverdeep" : 100 , "result" : 3 ,  "diffid":irBody.m_id});

        });
  }

  //- end page 10-

  //-start page 8

  function allfeed(str)
  {
    return str.match(/([^\?]*)\allfeed":(\d*)/);
  }

  function todayfeed(str)
  {
    return str.match(/([^\?]*)\odayfeed":(\d*)/);
  }



  function doShopBonasProfit(ssbody , res)
  {
    var end_date = new Date();
    var start_date = new Date();
    start_date.setHours(0,0,0,0);
    console.log("st : " + start_date.toString() + " - ed : " + end_date.toString());

    async.eachSeries(ssbody,function(item,callback) 
                            {
                                console.log("ssbody item : " + item.u_id);

                                epModel.aggregate([
                                {
                                    $match : { ep_loginid : "aaa" , ep_type : 126 ,  "ep_others.ep_ag_arr" : item.u_id   }
                                },
                                {
                                    $group : {

                                    _id : "$ep_loginid" , 
                                    allfeed : { $sum : "$ep_money" } , 
                                    todayfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", start_date ]} , {$lt: [ "$ep_datetime", end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                } 

                                    }
                                }
                                ],function (err,docs) {

                                    var str_res = JSON.stringify(docs);
                                    console.log("shop bonsa profit : " + str_res );

                                      
                                      if(allfeed(str_res) != null)
                                      {
                                       item['whole_bonsa_profit'] = allfeed(str_res)[2];
                                        console.log("n1 : " + item.whole_bonsa_profit);
                                      }
                                      if(todayfeed(str_res) != null)
                                      {
                                       item['today_bonsa_profit'] = todayfeed(str_res)[2];
                                        console.log("n2 : " + item.today_bonsa_profit);
                                      }

                                    
                                    callback();
                                });

                            },
                            function(err) {
                                if (err) throw err;

                                console.log("hallo");

                                res.json( { obj : ssbody } );


                    });
  }

  function doShopSelfProfit(ssbody , res)
  {
    var end_date = new Date();
    var start_date = new Date();
    start_date.setHours(0,0,0,0);
    console.log("st : " + start_date.toString() + " - ed : " + end_date.toString());

    async.eachSeries(ssbody,function(item,callback) 
                            {
                                console.log("ssbody item : " + item.u_id);

                                epModel.aggregate([
                                {
                                    $match : { ep_loginid : item.u_id , ep_type : 126 }
                                },
                                {
                                    $group : {

                                    _id : "$ep_loginid" , 
                                    allfeed : { $sum : "$ep_money" } , 
                                    todayfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", start_date ]} , {$lt: [ "$ep_datetime", end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                } 

                                    }
                                }
                                ],function (err,docs) {
                                    var str_res = JSON.stringify(docs);
                                    console.log("shop self profit : " + str_res );

                                      
                                      if(allfeed(str_res) != null)
                                      {
                                       item['whole_self_profit'] = allfeed(str_res)[2];
                                        console.log("n1 : " + item.whole_self_profit);
                                      }
                                      if(todayfeed(str_res) != null)
                                      {
                                       item['today_self_profit'] = todayfeed(str_res)[2];
                                        console.log("n2 : " + item.today_self_profit);
                                      }

                                    callback();
                                });

                            },
                            function(err) {
                                if (err) throw err;

                                console.log("hallo");

                                doShopBonasProfit(ssbody , res);


                    });

    
  }

  function startForShop_profit(req,res)
  {
    console.log("start shope profit");
    var sbody = req.body;
    doShopSelfProfit(sbody , res);
    //doShopBonasProfit(sbody , res);
  }


  function bojong_for_shop(c2 , mem_resulter , res)
  {

    console.log("here is shop bojong : " + c2 + " / memnum : " + mem_resulter.length);
    if(c2 == mem_resulter.length)
          {

            console.log('bojong_for_mem :   '+mem_resulter.length);
            res.json(mem_resulter);
          }


    if(c2 < mem_resulter.length)
     {
          console.log("uppercodeerer : " + mem_resulter[c2].u_uppercode);
          lrfModel
          .find( { u_id : mem_resulter[c2].u_uppercode } )
          .then(
            function (cbdata){

                  
                  mem_resulter[ c2 ]["tempu_uppercode"] = mem_resulter[ c2 ].u_uppercode;  

                  mem_resulter[ c2 ].u_uppercode = cbdata[0].u_nickname;
                  mem_resulter[ c2 ]['str_registerdate'] = mem_resulter[ c2 ].u_registerdate.toLocaleString();
                  if(mem_resulter[ c2 ].u_lastlogin != undefined)
                  mem_resulter[ c2 ]['str_lastlogin'] = mem_resulter[ c2 ].u_lastlogin.toLocaleString();

                  if((mem_resulter[ c2 ].u_lastlogin != undefined) && (mem_resulter[ c2 ].u_lastlogout != undefined)){
                    var hourDiff = mem_resulter[ c2 ].u_lastlogout - mem_resulter[ c2 ].u_lastlogin;  
                    mem_resulter[ c2 ]['str_duration'] = format_duration(hourDiff);
                  }

                  if( mem_resulter[ c2 ].u_uppertype == '2' )
                    mem_resulter[ c2 ].u_uppertype = 'self';
                  if( mem_resulter[ c2 ].u_uppertype == '3' )
                    mem_resulter[ c2 ].u_uppertype = 'market';



                  console.log("c2 : " + c2);
                  //mem_resulter[ c2 ]['str_lastlogin'] = mem_resulter[ c2 ].u_lastlogin.toLocaleString();

                c2++;
                bojong_for_shop(c2 , mem_resulter , res );

            },
            function (err){

            }
          ); 
      } 
  }

  function getMatchedShopData(req, res)
  {

    var obj = req.body.main;
    
    var fil_obj = {};

    var date_obj = {};

    fil_obj['u_groupid'] = '2';

    if(obj.fromdate != undefined)
       date_obj['$gte'] = new Date(obj.fromdate);

    if(obj.todate != undefined)
      date_obj['$lt'] = new Date(obj.todate);

    if((obj.fromdate != undefined) || (obj.todate != undefined))
      fil_obj['u_registerdate'] = date_obj;

    if(obj.uu_id != undefined)
      fil_obj['u_id'] = obj.uu_id;

    if(obj.uu_nickname != undefined)
      fil_obj['u_nickname'] = obj.uu_nickname;

    if(obj.uu_mail != undefined)
      fil_obj['u_email'] = obj.uu_mail;

    if(obj.uu_ip != undefined)
      fil_obj['u_lastloginip'] = obj.uu_ip;

    if(obj.uu_domain != undefined)
      fil_obj['domainobj.dm_domain_name'] = obj.uu_domain;

    //check flag

    var cfval = req.body.other;

    console.log("shop - cfval_targetid : " + cfval.targetid);

    lrfModel.find({ "u_id" : cfval.targetid })
            .then(function(addtata){

              console.log("tg_paths : " + addtata[0].paths);
              var tg_paths = addtata[0].paths; // will modify
              
              if(cfval.opt == 0) // all members
                 fil_obj['$and'] = [ { "u_id": { $ne: cfval.targetid } } , { "paths": { $all: tg_paths } } ] ;
              else{
                 
                 fil_obj['$and'] = [ { "u_id": { $ne: cfval.targetid } } , { "paths": { $all: tg_paths } } , { "paths": { $size: (tg_paths.length+1) } } ] ;
              }
                 

              console.log("filobj : " + JSON.stringify(fil_obj) );

              lrfModel.aggregate([
                      {
                        $lookup:{
                              from: "domain_list",
                              localField: "u_id",
                              foreignField: "dm_service_houseid",
                              as: "domainobj"
                                }
                      }, 
                      { 
                        $match : fil_obj 
                      },
                      { $count : "pagenum"}
                      ],function (err,cbdata) {
                        if(cbdata.length != 0)
                          res.json({ matched_len : cbdata[0].pagenum });
                        else
                          res.json({ matched_len : 0 });

                      });


            });
  }

  function find_shop_rbd(req, res)
  {

    //var tt = ticheng_val_A('tsid' , '2');

    console.log("kal ko shop!");

    var obj = req.body.main;
    
    var fil_obj = {};

    var date_obj = {};

    fil_obj['u_groupid'] = '2';

    if(obj.fromdate != undefined)
       date_obj['$gte'] = new Date(obj.fromdate);

    if(obj.todate != undefined)
      date_obj['$lt'] = new Date(obj.todate);

    if((obj.fromdate != undefined) || (obj.todate != undefined))
      fil_obj['u_registerdate'] = date_obj;

    if(obj.uu_id != undefined)
      fil_obj['u_id'] = obj.uu_id;

    if(obj.uu_nickname != undefined)
      fil_obj['u_nickname'] = obj.uu_nickname;

    if(obj.uu_mail != undefined)
      fil_obj['u_email'] = obj.uu_mail;

    if(obj.uu_ip != undefined)
      fil_obj['u_lastloginip'] = obj.uu_ip;

    if(obj.uu_domain != undefined)
      fil_obj['domainobj.dm_domain_name'] = obj.uu_domain;

    //check flag

    var cfval = req.body.other;

    //console.log("cfval_targetid : " + cfval.targetid);
    

    lrfModel.find({ "u_id" : cfval.targetid })
            .then(function(addtata){

              console.log("tg_paths : " + addtata[0].paths);
              var tg_paths = addtata[0].paths; // will modify
              
              if(cfval.opt == 0) // all members
                 fil_obj['$and'] = [ { "u_id": { $ne: cfval.targetid } } , { "paths": { $all: tg_paths } } ] ;
              else{
                 
                 fil_obj['$and'] = [ { "u_id": { $ne: cfval.targetid } } , { "paths": { $all: tg_paths } } , { "paths": { $size: (tg_paths.length+1) } } ] ;
              }
                 

              console.log("filobj : " + JSON.stringify(fil_obj) );

              console.log("skip :" + parseInt((req.body.pv.currentpage-1)*req.body.pv.numperpage) + " -> limit : " + parseInt(req.body.pv.numperpage));

              lrfModel.aggregate([
                      { $sort: { "u_registerdate": -1 } },
                      {
                        $lookup:{
                              from: "domain_list",
                              localField: "u_id",
                              foreignField: "dm_service_houseid",
                              as: "domainobj"
                                }
                      }, 
                      { 
                        $match : fil_obj 
                      },
                      {
                        $skip : parseInt((req.body.pv.currentpage-1)*req.body.pv.numperpage)
                      },
                      {
                        $limit : parseInt(req.body.pv.numperpage)
                      }
                      ],function (err,cbdata) {
                        var c2 = 0;
                        bojong_for_shop(c2 , cbdata , res);

                      });

            });


  }

  function set_selected_shop(req, res)
  {
    req.session.selected_shop = req.body;
    

    dmModel
      .find({ dm_service_houseid : req.session.selected_shop.u_id })
      .then(
        function (cbdata){
            console.log('yogi~ : ' + cbdata.length);
            req.session.selected_shop_dmlist = cbdata;
            res.json({});

        },
        function (error){
          
            res.json(error);
        }
      );
  }

  //-end page 8

  //-start page 8 new

  function add_shop(req, res)
  {
    var up_obj = req.body;

    var shop_obj = up_obj.shopinfo;
    var domain_obj = up_obj.domain_handle;
    
    console.log(shop_obj);
    var checkResult = {};

    lrfModel.find({"u_id" : shop_obj.u_uppercode})
              .then(function(forpth){
               var uppath = forpth[0].paths;

               var updated_sf = {};

               sfModel.find({"sf_id" : 1})
                        .then(function(sfdata){
                            switch(uppath.length)
                             {
                              case 1:
                                uppath.push("b" + (sfdata[0].b + 1));
                                shop_obj['paths'] = uppath;
                                updated_sf = { "b" : (sfdata[0].b + 1) };
                                break;
                              case 2:
                                uppath.push("c" + (sfdata[0].c + 1));
                                shop_obj['paths'] = uppath;
                                updated_sf = { "c" : (sfdata[0].c + 1) };
                                break;
                              case 3:
                                uppath.push("d" + (sfdata[0].d + 1));
                                shop_obj['paths'] = uppath;
                                updated_sf = { "d" : (sfdata[0].d + 1) };
                                break;
                             }

                             sfModel.update({"sf_id" : 1} , updated_sf)
                                      .then(function(upsf){

                                          lrfModel
                                            .create(shop_obj)
                                            .then(
                                              function (postObj){

                                              //console.log("erroring...");
                                                    allstatus_flag = 100;

                                                    dmModel
                                                      .create(domain_obj)
                                                      .then(
                                                        function (postObj1){

                                                              checkResult = {result : true};
                                                              res.json(checkResult);
                                                        },
                                                        function (err1){

                                                            checkResult = {result : false};
                                                            res.json(checkResult);

                                                        }
                                                      );

                                              },
                                              function (error){
                                                checkResult = {result : false};
                                                  res.json(checkResult);
                                              }
                                            );

                              });
                        });
     });
  }

  //-end page 8 new

  //-start page 8 update

  function get_selected_shop(req, res)
  {
    console.log("sel shop : " + JSON.stringify(req.session.selected_shop));
    res.json({ shopobj : req.session.selected_shop , dmobj : req.session.selected_shop_dmlist });
  }

  function update_a_shop(tid , upid , res , checkResult)
  {
   
    console.log("tid : " + tid);

    lrfModel.find({ "u_id" : upid })
            .then(function(gatas){
              var muppath = gatas[0].paths;

              console.log("upid : " + muppath );
              
              var fparam = {};

              for(var i=0; i<muppath.length; i++){

                var ftemp = "paths."+i;
                fparam[ftemp] = muppath[i];
              }
              /*console.log("fparm : " + JSON.stringify(fparam));
              console.log("first : " + JSON.stringify({ "paths": { $all : tid } }));
              console.log("second : " + JSON.stringify({$set : fparam}));
              console.log("third : " + JSON.stringify({multi: true}));*/

              lrfModel.update(
                      { "paths": { $all : tid } },
                      {$set : fparam},
                      {multi: true}
                      )
                      .then(function(err , icdata){
                          res.json(checkResult);                
                      });

            });
  }

  function update_shop(req , res)
  {
    var up_obj = req.body;

    var shop_obj = up_obj.shopinfo;
    var domain_obj = up_obj.domain_handle;

    lrfModel
      .update({u_id : shop_obj.u_id} , shop_obj)
      .then(
        function(cbdata) {

                dmModel
                .remove( { dm_service_houseid : shop_obj.u_id } )
                .then(
                  function(cb) {

                    console.log("domain_obj : " + domain_obj);

                    dmModel
                      .create(domain_obj)
                      .then(
                        function (postObj1){

                              checkResult = {result : true};
                              //res.json(checkResult);
                              update_a_shop(shop_obj.paths , shop_obj.u_uppercode , res , checkResult);
                              
                        },
                        function (err1){

                            checkResult = {result : false};
                            res.json(checkResult);

                        }
                      );

                  });


        },
        function(err) {
          res.json({result : false})
        }
      );
  }

  function removeDomain(req, res)
  {
    var dmbody = req.body;
    dmModel
      .remove({_id : dmbody._id})
      .then(
        function(cbdata) {
          res.json({ result : true });
        });
  }

  function setOurVal_To_MemVal(req, res)
  {
    req.session.selected_updated_visitor = req.session.selected_shop; // for goto savepay
    res.json({});
  }

  //-end page 8 update

  //-start earn pay history

  function getEpMaxid(req , res)
  {
    console.log("ep max id>>")
    epModel.find().sort({ep_id:-1}).limit(1)
                .then(function (cbdata){
                  if(cbdata.length == 0)
                  {
                    res.json({ result : 0 });
                  }
                  else
                  {
                  console.log("fff : "+cbdata[0].ep_id);
                  res.json({ result : cbdata[0].ep_id });
                  }
                },
                function (err)
                {

                });
  }

  //-end earn pay history

  //- start page 15
function checkGrId(req, res)
{
  var fparams = req.body;
  groupModel.find({g_id : fparams.g_id})
              .then(function(cbdata){
                  if(cbdata.length > 0)
                    res.json({result : false});
                  else
                    res.json({result : true});
              });

}

 function checkGroupid(req, res)
 {
  var gid = req.body.g_id;
  console.log("gid : " + gid );
  lrfModel
      .find( { u_groupid : gid } )
      .then(
        function(cbdata) {
          console.log("len : " + cbdata.length);
          if(cbdata.length > 0)
            res.json( { result : false } );
          else
            res.json( { result : true } );
        });
}

  function getGroupinfo(req , res)
  {
    groupModel
      .find()
      .then(
        function(cbdata) {
          res.json(cbdata);
        });
  }

  function getGroupMaxid(req , res)
  {
    console.log("gm max id>>")
    groupModel.find()
                .then(function (cbdata){
                  console.log("fff : "+cbdata.length);

                  var temp=0;
                  for(var i=0;i<cbdata.length;i++)
                    if( temp < parseInt(cbdata[i].g_id) )
                      temp = parseInt(cbdata[i].g_id);

                  res.json({ result : temp.toString() });
                },
                function (err)
                {

                });
  }

  function addGroupitem(req , res)
  {
    var grbody = req.body;
    groupModel
      .create(grbody)
      .then(
        function(cbdata) {
          res.json({ result : true });
        });
  }

  function checkSecondStep(grbody , res)
  {
    console.log("passed one~");
    groupModel
      .find( {g_name : grbody.g_name} )
      .then(
        function(cbdata) {
          console.log("cbdata1~ : " + cbdata[0]);
          if(cbdata[0] == undefined){
            console.log("hey~");
            res.json({ result : "ok" });
          }
          else{
            
            if(cbdata[0]._id == grbody._id){
              console.log("hihih");
              res.json({ result : "ok" });
            }
            else
              res.json({ result : "그룹명이 반복되고 있습니다." });

          }

        });
  }

  function checkGroupItem(req, res)
  {
    var grbody = req.body;
    groupModel
      .find( {g_id : grbody.g_id} )
      .then(
        function(cbdata) {
          console.log("cbdata~ : " + cbdata[0]);
          if(cbdata[0] == undefined)
            checkSecondStep(grbody , res);
          else if(cbdata[0] != undefined){

            if(cbdata[0]._id == grbody._id)
              checkSecondStep(grbody , res);
            else
              res.json({ result : "식별자값이 반복되고 있습니다." });

          }

        });
  }

  function updateGroupItem(req, res)
  {
    var upbody = req.body;
    console.log("heyey : " + JSON.stringify(upbody));
    groupModel
          .update( {_id : upbody._id} , upbody )
          .then(
            function(cbdata) {
              res.json( {result : true} );
            });
  }

  function deleteGrItem(req, res)
  {
    var upbody = req.body;

    groupModel
          .remove( {_id : upbody._id} )
          .then(
            function(cbdata) {
              res.json( {result : true} );
            });
  }

  function checkGrName(req, res)
  {
    var upbody = req.body;

    groupModel
          .find( {g_name : upbody.g_name} )
          .then(
            function(cbdata) {
              if(cbdata[0] == undefined)
                res.json( {result : true} );
              else
                res.json( {result : false} );
            });
  }

  //- end page 15

  //-start page 15 list

  function getAllManager(req, res)
  {
    lrfModel
          .find( {$where: "this.u_groupid >= 100 && this.u_groupid <= 200"} )
          .then(
            function(cbdata) {
                    var cout = 0;
                    async.eachSeries(cbdata,function(item,callback) 
                            {

                                groupModel.find( { g_id : item.u_groupid }, function (err, rows) {
                                    cout++;
                                    item.u_groupid = rows[0].g_name;
                                    callback(err);
                                });

                            },
                            function(err) {
                                if (err) throw err;

                                if(cout == cbdata.length){
                                    //console.log("manager : " + JSON.stringify(cbdata));

                                    cbdata.forEach(function(pitem){
                                      pitem['str_registerdate'] = pitem.u_registerdate.toLocaleString();
                                      if(pitem.u_lastlogin != undefined)
                                        pitem['str_lastlogin'] = pitem.u_lastlogin.toLocaleString();
                                      if((pitem.u_lastlogin != undefined) && (pitem.u_lastlogout != undefined)){
                                          var hourDiff = pitem.u_lastlogout - pitem.u_lastlogin;  
                                          pitem['str_duration'] = format_duration(hourDiff);
                                        }
                                    });

                                    res.json(cbdata);

                                }
                    });
            });
  }

  function set_sel_manager(req , res)
  {
    req.session.selected_manager = req.body;
    res.json( {result : true} );
  }

  function remove_manager(req, res)
  {
    lrfModel
          .remove( {_id : req.body._id} )
          .then(
            function(cbdata) {
              res.json( {result : true} );
            });
  }

  //-end page 15 list

  //-start page 15 new

  function getMax_acid(req, res)
  {
    acModel.find().sort({ac_id:-1}).limit(1)
                .then(function (cbdata){
                  if(cbdata.length == 0)
                  {
                    res.json({ result : 0 });
                  }
                  else
                  {
                  console.log("fff : "+cbdata[0].ac_id+1);
                  res.json({ result : cbdata[0].ac_id+1 });
                  }
                },
                function (err)
                {

                });
  }

  function getManagerGlist(req , res)
  {
    groupModel
          .find( {$where: "this.g_id >= 100 && this.g_id <= 200"} )
          .then(
            function(cbdata) {

                res.json(cbdata);

            });
  }

  function add_manager(req, res)
  {
    var mana_body = req.body;

    lrfModel.find({u_groupid : '2' , u_uppercode : '1'})
            .then(function(ammosi){
                mana_body['u_metaid'] = ammosi[0].u_id;
                mana_body['paths'] = ammosi[0].paths;

                lrfModel
                  .create( mana_body )
                  .then(
                    function(cbdata) {

                        var defaultval = { ac_id : mana_body.u_ac_metaid , ac_zhong : 0 , ac_report : 0 ,
                                           ac_sendmsm : 0 , ac_showmsm : 0 , ac_gongji : 0 , ac_help : 0 ,
                                           ac_sp : 0 , ac_visitor : 0 , ac_shop : 0 , ac_allgame : 0 , 
                                           ac_pokerhistory : 0 , ac_gamemanage : 0 , ac_security : 0 , 
                                           ac_eb_reg : 0 , ac_eb_imp : 0 , ac_groupmanage : 0 , ac_manager : 0,
                                           ac_allows : 0 , ac_myinfo : 0 , ac_pmethod : 0 , ac_allnotify : 0 ,
                                           ac_allstatus : 0 };

                        acModel.create(defaultval)
                                .then(function(acresdata){

                                  res.json( { result : true } );

                                });

                    });

            });

  }

  //-end page 15 new

  //-start page 15 update

  function get_sel_manager(req, res)
  {
    res.json( {obj : req.session.selected_manager} );
  }

  function update_manager(req, res)
  {
    var mana_body = req.body;
    lrfModel
          .update( { _id : mana_body._id } , mana_body )
          .then(
            function(cbdata) {

                res.json( { result : true } );

            });
  }

  //-end page 15 update

  //-profile changing

  function set_myinfo(req , res)
  {
    var loginedid = req.session.u_id;

    lrfModel
          .find( { u_id : loginedid } )
          .then(
            function(cbdata) {
                    
                    var getter1 = cbdata[0];
                    console.log("getter1 : " + getter1.u_groupid);
                      groupModel.find( { g_id : getter1.u_groupid } )
                                .then(
                                   function (cbdata1) {
                                
                                      getter1['orig_gid'] = getter1.u_groupid;  
                                      getter1.u_groupid = cbdata1[0].g_name;

                                      getter1['str_registerdate'] = getter1.u_registerdate.toLocaleString();

                                      if(getter1.u_lastlogin != undefined)
                                          getter1['str_lastlogin'] = getter1.u_lastlogin.toLocaleString();

                                      if((getter1.u_lastlogin != undefined) && (getter1.u_lastlogout != undefined)){
                                          var hourDiff = getter1.u_lastlogout - getter1.u_lastlogin;  
                                          getter1['str_duration'] = format_duration(hourDiff);
                                      }

                                      getter1['tempu_uppercode'] = getter1.u_uppercode;
                                      /*lrfModel
                                        .find( { u_id : getter1.u_uppercode } )
                                        .then(function(lredata){
                                          getter1.u_uppercode = lredata[0].u_nickname;*/

                                          var tem_arr = [];
                                          tem_arr.push(getter1);

                                          var ccout = 0;
                                          var fflag = 1;
                                          console.log("ya : " + tem_arr );

                                          do_other_details(tem_arr , ccout , res , fflag ,req);

                                        //}); 

                                      
                                    
                                });

            });

  }

  //-profile changing

  //-start page 14 register

  function getAllEvData(req, res)
  {
    ebModel
          .find( {} )
          .then(
            function(cbdata) {

              cbdata.forEach(function(item){
                item['period_txts'] = eb_period_texts[parseInt(item.eb_period)-1];
                item['type_txts'] = eb_type_texts[parseInt(item.eb_type)-1];
                if(item.eb_codestatus == true)
                  item['status_txts'] = "구현됨";
                else
                  item['status_txts'] = "구현안됨";

                if(item.eb_allow == true)
                  item['allow_txts'] = "유효";
                else
                  item['allow_txts'] = "무효";
              });

              res.json(cbdata);

            },
            function(err){

            });

  }

  function set_global_ebdata(req, res)
  {
    req.session.selected_ebdata = req.body;
    res.json( {result : true} );
  }

  function check_forDel(req, res)
  {
    var seldata = req.body;
    var chflag = 0;
    console.log("eb id : " + seldata.eb_id);
    ebImpModel
          .find( { ebi_eb_id : seldata.eb_id } )
          .then(
            function(cbdata) {
                console.log("ebi result : " + cbdata.length);
                if(cbdata.length > 0)
                {
                    
                     res.json( { result : true } );
                }
                else{

                  console.log("checking done...~" + chflag);
                  res.json( {result : false} );
                }

            });
  }

  

  function remove_ebdata(req, res)
  {
    var para = req.body;
    ebModel
          .remove( { _id : para._id } )
          .then(
            function(cbdata) {
              res.json( {result : true} );
            });
  }

  function checkForEbiToImp(req, res)
  {
    var eti = req.body;
    
    ebImpModel.find({ ebi_eb_id : eti.eb_id })
                .then(function(cbdata){
                    if(cbdata.length > 0)
                      res.json({ result : false });
                    else
                      res.json({ result : true });
                });
  }

  //-end page 14 register

  //-start page 14 register new

  function getMaxEvID(req, res)
  {
    ebModel.find().sort({eb_id:-1}).limit(1)
                .then(function (cbdata){
                  if(cbdata.length == 0)
                  {
                    res.json({ result : 1 });
                  }
                  else
                  {
                  console.log("fff : "+cbdata[0].eb_id);
                  res.json({ result : cbdata[0].eb_id + 1 });
                  }
                },
                function (err)
                {

                });
  }

  function addEvData(req, res)
  {
    var ebody = req.body;
    ebModel
          .create( ebody )
          .then(
            function(cbdata) {
              res.json( {result : true} );
            });
  }

  //-end page 14 register new

  //-start page 14 register update

  function get_global_ebdata(req, res)
  {
    res.json( { result : req.session.selected_ebdata } );
  }

  function updateEvData(req, res)
  {
    var ebody = req.body;
    ebModel
          .update( { _id : ebody._id } , ebody )
          .then(
            function(cbdata) {
              res.json( {result : true} );
            });
  }

  //-end page 14 register update

  //-start page 14 imp new

  function getImpMaxid(req, res)
  {
    ebImpModel.find().sort({ebi_id:-1}).limit(1)
                .then(function (cbdata){
                  if(cbdata.length == 0)
                  {
                    res.json({ result : 1 });
                  }
                  else
                  {
                  console.log("fff : "+cbdata[0].ebi_id);
                  res.json({ result : cbdata[0].ebi_id + 1 });
                  }
                },
                function (err)
                {

                });
  }

  function imp_ebdata(req , res)
  {
    var iebody = req.body;
    console.log(iebody);
    ebImpModel
          .create( iebody )
          .then(
            function(cbdata) {

                res.json( { result : true } );

            });
  }

  //-end page 14 imp new 
  
  //-start page 14 imp

  function find_ebi_data(req, res)
  {
    var fil = req.body;
    console.log("fil : " + fil);
    ebImpModel
          .find( fil )
          .then(
            function(cbdata) {

                if(cbdata.length>0)
                {
                  console.log(fil);
                  cbdata.forEach(function(item){
                      item['sd'] = item.ebi_start_date.toLocaleString();
                      item['ed'] = item.ebi_end_date.toLocaleString();
                      item['sad'] = item.ebi_start_adv_date.toLocaleString();
                      item['ead'] = item.ebi_end_adv_date.toLocaleString();
                      if(item.ebtype == '1')
                        item['strtype'] = "이벤트";
                      if(item.ebtype == '2')
                        item['strtype'] = "보너스";
                  });
                  res.json( { obj : cbdata } );
                }
                else
                  res.json( { obj : [] } );

            });
  }

  function check_forEBIDel(req, res)
  {
    var seldata = req.body;
                     var method;
                     if(seldata.ebtype == '1')
                      method = 124;
                     else if(seldata.ebtype == '2')
                      method = 125;

      console.log( { ep_metaid : seldata.ebi_id , ep_type : method } );

      epModel.find( { ep_metaid : seldata.ebi_id , ep_type : method })
            .then(function (cbdata) {
                  if(cbdata.length>0)
                    res.json({ result : true });
                  else
                    res.json({ result : false });                
          });
                            
                
  }

  function del_EBIdata(req, res)
  {
    var seldata = req.body;
    ebImpModel.remove( { _id : seldata._id })
            .then(function (cbdata) {
              res.json({ result : true });
            });
  }

  //-end page 14 imp

  //-start page 14 imp update

  function update_ebidata(req, res)
  {
    var target = req.params.id;
    var seldata = req.body;
    ebImpModel.update( { _id : target } , seldata )
            .then(function (cbdata) {
              res.json({ result : true });
            });
  }

  //-end page 14 imp update

  //-start page12

  function addBlackName(req ,res)
  {
    
    var bbody = req.body;
    saModel.find( { sa_id : 1 } )
            .then(function (cbdata) {
              if(cbdata.length > 0)
              {
                saModel.update( { sa_id : 1 } , { $push: { sa_arr : bbody.val } } )
                  .then(function (cbdata) {
                    res.json({ result : true });
                  });
              }
              else
              {
                saModel.create( { sa_id : 1 , sa_arr : bbody.val , sa_typename : "금지회원명" } )
                  .then(function (cbdata) {
                    res.json({ result : true });
                  });
              }

            });
    
  }

  function get_blacklist(req, res)
  {
    saModel.find( { sa_id : 1 } )
          .then(function (cbdata) {
              res.json(cbdata);
          });
  }

  function removeBlackitem(req, res)
  {
    saModel.update( { sa_id : 1 } , { sa_arr : req.body.obj } )
            .then(function (cbdata) {
              res.json({ result : true });
            });
  }

  function addBlockIP(req ,res)
  {
    var bbody = req.body;
    saModel.find( { sa_id : 2 } )
            .then(function (cbdata) {
              if(cbdata.length > 0)
              {
                saModel.update( { sa_id : 2 } , { $push: { sa_arr : bbody.val } } )
                  .then(function (cbdata) {
                    res.json({ result : true });
                  });
              }
              else
              {
                saModel.create( { sa_id : 2 , sa_arr : bbody.val , sa_typename : "금지IP주소" } )
                  .then(function (cbdata) {
                    res.json({ result : true });
                  });
              }

            });
    
  }

  function get_blockname(req, res)
  {
    saModel.find( { sa_id : 2 } )
          .then(function (cbdata) {
              res.json(cbdata);
          });
  }

  function removeBlockip(req, res)
  {
    saModel.update( { sa_id : 2 } , { sa_arr : req.body.obj } )
            .then(function (cbdata) {
              res.json({ result : true });
            });
  }

  function get_alarmlist(req, res)
  {
    saModel.find( { sa_id : 3 } )
          .then(function (cbdata) {

              cbdata[0].sa_arr.forEach(function(item){
                if(item.a_func_status == true)
                  item['txt_fstatus'] = "구현됨";
                else if(item.a_func_status == false)
                  item['txt_fstatus'] = "구현안됨";
                
                if(item.a_setting_status == true)
                  item['txt_sstatus'] = "설정";
                else if(item.a_setting_status == false)
                  item['txt_sstatus'] = "해제";

              });

              console.log(cbdata[0].sa_arr);
              res.json({ result : cbdata[0].sa_arr});
          });
  }

  function addnew_alarm(req ,res)
  {
    var bbody = req.body;
    saModel.find( { sa_id : 3 } )
            .then(function (cbdata) {
              if(cbdata.length > 0)
              {
                saModel.update( { sa_id : 3 } , { $push: { sa_arr : bbody } } )
                  .then(function (cbdata) {
                    res.json({ result : true });
                  });
              }
              else
              {
                saModel.create( { sa_id : 3 , sa_arr : bbody , sa_typename : "경보기능" } )
                  .then(function (cbdata) {
                    res.json({ result : true });
                  });
              }

            });
    
  }

  function update_alarm(req, res)
  {
    var bbody = req.body;
    saModel.update( { sa_id : 3 } , { sa_arr : bbody } )
            .then(function (cbdata) {
              res.json({ result : true });
            });
  }

  //-end page12

  //-start notice

  function get_all_notice(req, res)
  {
        saModel.find( { sa_id : 4 } )
          .then(function (cbdata) {
              res.json({ obj : cbdata[0].sa_arr});
          });
  }

  function update_all_notice(req, res)
  {
    var bbody = req.body;
    console.log("bddd : " + bbody.obj);
    saModel.update( { sa_id : 4 } , { sa_arr : bbody.obj } )
            .then(function (cbdata) {
              //req.session.sounds_json.notify -= 1;

              console.log("jsonsss : " + JSON.stringify(req.session.sounds_json)); 

              res.json({ result : true });

              allstatus_flag = 100;

            });
  }

  function get_rw_all_notice(req, res)
  {
    var bbody = [];
    var temp = [];
    saModel.find( { sa_id : 4 } )
          .then(function (cbdata1) {
              
              bbody = cbdata1[0].sa_arr;

              res.json({ result : true  , obj : bbody });
              
          });
  }

  function update_all_status(req, res)
  {
    saModel.find( { sa_id : 4 } )
          .then(function (cbdata1) {
              
              bbody = cbdata1[0].sa_arr;
              
              
              bbody.forEach(function(item){
                item.am_status = 1;
              });

              saModel.update( { sa_id : 4 } , { sa_arr : bbody } )
                  .then(function (cbdata2) {
                    
                    res.json({ result : true });
                  });
              
          });
  }

  //-end notice

  function checkInDlist(req, res)
  {
    var parambody = req.body.dobj;
    var all_dm = [];
    var flaga = 0;

    console.log("new dm : " + JSON.stringify(parambody) );

    dmModel.find()
          .then(function (cbdata) {
            all_dm = cbdata;

            all_dm.forEach(function(item1){

                parambody.forEach(function(item2){
                    console.log("_id : " + item2._id);
                    console.log("itmes : " + item2);

                      if(item2._id != undefined)
                      {
                        if((item1.dm_domain_name == item2.dm_domain_name) && (item1._id != item2._id))
                          flaga = 1;
                      }
                      else
                      {
                        if(item1.dm_domain_name == item2.dm_domain_name)
                          flaga = 1;
                      }
                  
                  });

                });

                if(flaga == 0)
                  res.json({ result : true });
                else if(flaga == 1)
                  res.json({ result : false});

          });

  }

  function getAllalarm(req, res)
  {
    saModel.find( { sa_id : 3 } )
          .then(function (cbdata) {
            res.json({ obj : cbdata[0].sa_arr });
          });
  }

  //-start page 3 (poker graph)

  function CalcAll(darr , inc , res)
  {
      //console.log("from , to : " + sfrom.toLocaleString() + ", "+sto.toLocaleString());
      var sindex = 0;
      var num_player = [];
      var riyoun = [];
      async.eachSeries(darr,function(item,callback) 
                {
                  //console.log("sindex : " + sindex);
                    
                      console.log("a : " + (new Date(item.getTime()-inc)).toLocaleString() + ' , ' + (new Date(item.getTime()+inc)).toLocaleString());
                                var prvtime = new Date().getTime();

                                pprModel.aggregate([
                                    {
                                      $match : { round_start_time : { $gte : new Date(item.getTime()-inc) , $lt : new Date(item.getTime()+inc) } }
                                    },
                                    {
                                      $group : {_id : "$player_id", total : { $sum : 1 } , feed : { $sum : "$round_fee" }}
                                    }
                                    ],function (err,docs) {
                                      //console.log("docs : " + JSON.stringify(docs));
                                      if(docs.length != 0)
                                      {
                                          var sumer = 0;
                                          for(var i=0; i<docs.length; i++)
                                            sumer += docs[i].feed;

                                          console.log("num of player : " + docs.length + ' , ' + " all feed : " + sumer);
                                          
                                          num_player.push(docs.length);
                                          riyoun.push(sumer);
                                          
                                      }
                                      else{
                                        num_player.push(0);
                                        riyoun.push(0);
                                      }

                                        console.log("sindex : " + sindex);

                                          if(sindex == 23)
                                            {
                                              console.log("done......");

                                              console.log("bottombar : " + darr.length);
                                                  console.log("num_player : " + num_player.length);
                                                  console.log("riyoun : " + riyoun.length);

                                              res.json( { bottombar : darr , num_player : num_player , riyoun : riyoun } );
                                            }


                                          sindex++;

                                          var cttime = new Date().getTime();
                                          console.log("periods8~~ : " + (cttime - prvtime));
                                          
                                          callback(err)
                                });
                   
                    

                                
                },
                function(err) {
                      if (err) throw err;

                      if(sindex == 23 ){
                          

                      }
                });
           
  }

  function calcDuration(start , end , res)
  {

     var fromd = new Date(start);
      var tod = new Date(end);

      var oneDay = 24*60*60*1000;
      var diffDays = Math.round(Math.abs((fromd.getTime() - tod.getTime())/24));

      var bottomBar = [];
      var temp = new Date(fromd.getTime() + (diffDays/2));
      for(var i=0; i<24; i++)
      {
        bottomBar.push(temp);
        var temp = new Date(temp.getTime() + diffDays);
      }
      //end calc bottom bar.
      console.log("dates : " + bottomBar.length);

      CalcAll( bottomBar , diffDays/2  , res);

  }

  function PrepareToCalc(dr , res)
  {
    //ppeModel
    if(dr.from != undefined)
    {
      //- calc bottombar
      console.log("all are exist : " + dr.from);

      calcDuration(dr.from , dr.to , res);
      //console.log("diff : " + new Date(fromd.getTime() + diffDays) );

    } 
    else
    {
      console.log("from is null");

      pprModel.aggregate([
                          {
                             $group:
                               {
                                 _id: null,
                                 mindate: { $min: "$round_start_time" }
                               }
                           }
                         ],function (err,docs) {
                              console.log("sexy round_start_time : " + JSON.stringify(docs));
                              if(docs.length != 0)
                              {
                                console.log(docs[0].mindate);
                                if(new Date(docs[0].mindate) < new Date(dr.to) )
                                  calcDuration(docs[0].mindate , dr.to , res);
                                else
                                  res.json({ bottombar : [] , num_player : [] , riyoun : [] });
                              }
                              else
                                res.json({ bottombar : [] , num_player : [] , riyoun : [] });

                          });
    }
  }

  function CalcPokerHistory(req, res)
  {
    var duration = req.body;
    console.log(JSON.stringify(duration));

    PrepareToCalc(duration, res);
  }
  //-end page 3 (poker graph)

  //-start for get all status on the top bar

  function getForNumoflogined(fresult, res , req)
  {
    //lrfModel.find()
    var end = new Date();
    var start = new Date();
    start.setHours(0,0,0,0);

    lrfModel.find({u_registerdate : { $gte : start, $lt : end } })
            .then(function(cbdata){
              cbdata.forEach(function(item){
                if(item.u_uppercode == req.session.u_metaid)
                {
                  if(item.u_groupid == "1")
                    fresult.today_lg_visitor++;
                  else
                    fresult.today_lg_shop++;
                }
              });
              res.json(fresult);
              console.log("3nd step : " + JSON.stringify(fresult));

            });
  }

  function getForReport(fresult , res , req)
  {

    mModel.find({m_flag : 2})
            .then(function(cbdata){
              cbdata.forEach(function(item){
                if(item.m_receiverid == req.session.u_metaid)
                {
                  if(item.m_imp_servicer == undefined)
                    fresult.report_req++;
                  else
                    fresult.report_done++;
                }
              });
              console.log("2nd step : " + JSON.stringify(fresult));
              getForNumoflogined(fresult , res , req);

            });
  }

  function getAllStatus(req, res)
  {
    var topInfo = {v_save_req : 0 , s_save_req : 0 , v_pay_req : 0 , s_pay_req : 0 ,
                   v_save_done : 0 , s_save_done : 0 , v_pay_done : 0 , s_pay_done : 0 , 
                   report_req : 0 , report_done : 0 , today_lg_visitor : 0 , today_lg_shop : 0 };
    console.log("ready to go~~~~");
    spModel.aggregate([
      {
         $lookup:{
        from: "user_info",
        localField: "sp_requested_id",
        foreignField: "u_id",
        as: "uinfo"     
        }
      },
      {
         $unwind: "$uinfo"
      }], function (err, docs) {
        console.log("req session ~~~ : ", req.session.u_metaid);
        
        docs.forEach(function(item){
          if(item.uinfo.u_uppercode == req.session.u_metaid)
          {
            if(item.uinfo.u_groupid == "1")
            {
              if(item.sp_requested_type == 1)
              {
                if(item.sp_implemented_result == "3")
                  topInfo.v_save_req++;
                else
                  topInfo.v_save_done++;
              }
              else if(item.sp_requested_type == 2)
              {
                if(item.sp_implemented_result == "3")
                  topInfo.v_pay_req++;
                else
                  topInfo.v_pay_done++;
              }
            }
            else if(item.uinfo.u_groupid == "2")
            {
              if(item.sp_requested_type == 1)
              {
                if(item.sp_implemented_result == "3")
                  topInfo.s_save_req++;
                else
                  topInfo.s_save_done++;
              }
              else if(item.sp_requested_type == 2)
              {
                if(item.sp_implemented_result == "3")
                  topInfo.s_pay_req++;
                else
                  topInfo.s_pay_done++;
              }
            }

          }

          if(req.session.u_uppercode == "1") // for bonsa
          {

            if(((item.uinfo.u_groupid == "1")&&(item.uinfo.u_uppertype == "3"))&&(item.uinfo.u_uppercode != req.session.u_metaid)) // whole user & market
            {
              if(item.sp_requested_type == 1)
              { 
                console.log("1 : " + item.sp_implemented_result);
                if(item.sp_implemented_result == "3")
                  topInfo.v_save_req++;
                else
                  topInfo.v_save_done++;
              }
              else if(item.sp_requested_type == 2)
              {
                if(item.sp_implemented_result == "3")
                  topInfo.v_pay_req++;
                else
                  topInfo.v_pay_done++;
              }
            }
          }

        });
        console.log("haha~~~ : " + JSON.stringify(topInfo));

        getForReport(topInfo , res , req);

      });

  }

  function getASparam(req , res)
  {
    var tempas = as_param;
    as_param = {spval : ""};
    res.json({obj : tempas});
  }

  function setASparam(req , res)
  {
    var asb = req.body.obj;
    as_param = asb;
    res.json({});
  }

  //-end for get all status on the top bar

  //start test for transfer

  var openConnections = [];
 
// simple route to register the clients
app.get('/stats', function(req, res) {
 
    // set timeout as high as possible
    req.socket.setTimeout(1000);
 
    // send headers for event-stream connection
    // see spec for more information
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
    });
    res.write('\n');
 
    // push this res object to our global variable
    openConnections.push(res);
 
    // When the request is closed, e.g. the browser window
    // is closed. We search through the open connections
    // array and remove this connection.
    req.on("close", function() {
        var toRemove;
        for (var j =0 ; j < openConnections.length ; j++) {
            if (openConnections[j] == res) {
                toRemove =j;
                break;
            }
        }
        openConnections.splice(j,1);
        console.log(openConnections.length);
    });
});
 
setInterval(function() {
    // we walk through each connection
    openConnections.forEach(function(resp) {
        var d = new Date();
        resp.write('id: ' + d.getMilliseconds() + '\n');
        resp.write('data:' + createMsg() +   '\n\n'); // Note the extra newline
    });
 
}, 1000);
 
function createMsg() {
    msg = {};
 
    msg.flag = allstatus_flag;
    msg.shcdata = shopcbdata;

    msg.proval = gprogressval;
 
    return JSON.stringify(msg);
}

//-- start manage sounds flag

function getSoundsflag(req, res)
{
  var tempss = req.session.status_sound;
  req.session.status_sound = 0;

  res.json({ obj : tempss });
  
}

function setSoundsflag(req, res)
{
  var pss = req.body.obj;
  req.session.status_sound = pss;
  res.json({});
}

function getSoundsjson(req, res)
{
  console.log("In get testdata : " + req.session.testdata);
  console.log("In get updated json : " + JSON.stringify(req.session.sounds_json));
  res.json({ obj : req.session.sounds_json });
}

function setSoundsjson(req, res)
{
  var pp = req.body;
  req.session.sounds_json = pp;
  req.session.testdata = 10;

  console.log("In set testdata : " + req.session.testdata);
  console.log("In set updated json : " + JSON.stringify(req.session.sounds_json));
  res.json({});
}

//-- end manage sounds flag


//-- start page 3(filter by player)

function get_visitor_for_game(req, res)
{
  console.log("get player : " + req.session.game_playername);
  var templayer = req.session.game_playername;
  req.session.game_playername = "";
  res.json({ "user" : templayer });
  
}


function do_GR2(fidata ,res)
{
  res.json({ obj :  fidata });
  //console.log("fidata : " + fidata.length);
}

function do_GR1(finaldata , soarr ,res)
{
            var teno = {};

            if(soarr.length>0)
            {
              var item1 = soarr.shift();
              if(item1.event_type == '입장')
              {
                
                teno['playerid'] = item1.ppeinfo[0].u_nickname;
                teno['enter_time'] = item1.event_time;
                teno['enter_money'] = item1.event_money;
                
                for(var k=0; k<soarr.length; k++)
                {
                  if((item1.player_id == soarr[k].player_id)&&(soarr[k].event_type == '퇴장'))
                  {
                    teno['out_time'] = soarr[k].event_time;
                    teno['out_money'] = soarr[k].event_money;
                    teno['money_change'] = Math.abs(teno.enter_money - teno.out_money);
                    soarr.splice(k , 1);
                    break;
                  }
                }
                //console.log("start : " + testa + "/ end : " + testt);
                finaldata.push(teno);
                do_GR1(finaldata , soarr , res);

              }
              else if(item1.event_type == '퇴장')
              {
                teno['playerid'] = item1.ppeinfo[0].u_nickname;
                teno['out_time'] = item1.event_time;
                teno['out_money'] = item1.event_money;

                finaldata.push(teno);
                do_GR1(finaldata , soarr , res);
              }

            }
            //console.log("slen : " + soarr.length);
            else if(soarr.length == 0)
            {
              console.log("slen : " + soarr.length);
              do_GR2(finaldata , res);
            }

}

function getGResultByVisitorNum(req, res)
{
  var grbody = req.body;
  if(grbody.event_time != undefined)
  {
    if(grbody.event_time['$lt'] != undefined)
    grbody.event_time['$lt'] = new Date(grbody.event_time['$lt']);

    if(grbody.event_time['$gte'] != undefined)
    grbody.event_time['$gte'] = new Date(grbody.event_time['$gte']);
  }

  console.log("grbody : " + JSON.stringify(grbody))

  ppeModel.aggregate([{$lookup:{
                              from: "user_info",
                              localField: "player_id",
                              foreignField: "u_id",
                              as: "ppeinfo"
                              }},
                              { $match : grbody } ,
                              {
                                $count : "wholeppe"
                              }],function (err,docs) {
            if(docs.length !=0)
            {
            console.log("ppe datas num : " + docs[0].wholeppe );
            res.json({ "ppenum" : docs[0].wholeppe});
            }
            else
            {
              res.json({ "ppenum" : 0});
            }


        });
}

function getGResultByVisitor(req, res)
{

  var grbody = req.body.w_param;
  if(grbody.event_time != undefined)
  {
    if(grbody.event_time['$lt'] != undefined)
    grbody.event_time['$lt'] = new Date(grbody.event_time['$lt']);

    if(grbody.event_time['$gte'] != undefined)
    grbody.event_time['$gte'] = new Date(grbody.event_time['$gte']);
  }

  console.log("grbody : " + JSON.stringify(grbody))

  ppeModel.aggregate([  
                        { $sort : { "event_time" : -1} },
                        {$lookup:{
                              from: "user_info",
                              localField: "player_id",
                              foreignField: "u_id",
                              as: "ppeinfo"
                              }},{ $match : grbody },
                              {
                                $skip : parseInt((req.body.p_param.currentpage-1)*req.body.p_param.numperpage)
                              },
                              {
                                $limit : parseInt(req.body.p_param.numperpage)
                              }],function (err,docs) {

            res.json({ obj :  docs });

        });
}

//-- end page 3(filter by player)

//-- start page 3

function getWholeMatchedGHData(req, res)
{
  var wghbody = req.body.fp;
  var wghadbody = req.body.fpr;

    if(wghbody.ep_datetime != undefined)
    {
        if(wghbody.ep_datetime['$lt'] != undefined)
            wghbody.ep_datetime['$lt'] = new Date(wghbody.ep_datetime['$lt']);
        if(wghbody.ep_datetime['$gte'] != undefined)
            wghbody.ep_datetime['$gte'] = new Date(wghbody.ep_datetime['$gte']);
    }

    console.log("w g h : " + JSON.stringify(wghbody));

    var filarr = [];
    filarr.push(wghbody);
    filarr.push(wghadbody);

    console.log("wgh bojong fil : " + JSON.stringify(filarr));

    var prvtime = new Date().getTime();

    epModel.aggregate([
        {
            $lookup:{
                from: "user_info",
                localField: "ep_loginid",
                foreignField: "u_id",
                as: "uinfo"
            }
        },
        {
            $match : { $and : filarr }
        },
        { $count : "pagenum"}
        ],function(err, docs){
        console.log("err : " + err);
        var cttime = new Date().getTime();
        console.log("periods4~~ : " + (cttime - prvtime));

        if(docs.length != 0) {
            console.log("wgh length : " + docs[0].pagenum);
            res.json({"wghnum": docs[0].pagenum});
        }
        else
            res.json({"wghnum": 0});
    });


}

function getWholeGameHistory(req, res)
{
  var wghbody = req.body.fp;
  var addfil = req.body.adfil;

  if(wghbody.ep_datetime != undefined)
  {
    if(wghbody.ep_datetime['$lt'] != undefined)
    wghbody.ep_datetime['$lt'] = new Date(wghbody.ep_datetime['$lt']);
    if(wghbody.ep_datetime['$gte'] != undefined)
    wghbody.ep_datetime['$gte'] = new Date(wghbody.ep_datetime['$gte']);
  }

console.log("w g h : " + JSON.stringify(wghbody));
  console.log("pv : "+ ((req.body.pv.currentpage-1)*req.body.pv.numperpage) + " - " + req.body.pv.numperpage);
  console.log("adfil : " + JSON.stringify(addfil));
  epModel.aggregate([
                      { $sort : { "ep_datetime" : -1} },
                      {
                         $lookup:{
                        from: "user_info",
                        localField: "ep_loginid",
                        foreignField: "u_id",
                        as: "uinfo"     
                        }
                      },
                      { 
                        $match : wghbody 
                      },
                      {
                          $match : addfil
                      },
                      {
                        $skip : parseInt((req.body.pv.currentpage-1)*req.body.pv.numperpage)
                      },
                      {
                        $limit : parseInt(req.body.pv.numperpage)
                      }

                    ],function(err, docs){
                        console.log("err : " + err);
                        console.log("resutl per page : " + docs.length);
                        res.json({ result : docs });
                    });

}

//-- end page 3

//-- start payment page

function setPaymentMethod(req, res)
{
  var pm_objbody = req.body.pmobj;
  console.log(JSON.stringify(pm_objbody));

  lrfModel.update({u_id : req.session.u_metaid} , { "u_pm" : pm_objbody})
            .then(function(pmdata){
              res.json({ result : true });
            });
}

function loadPaymentMethod(req, res)
{
  lrfModel.find({u_id : req.session.u_metaid})
            .then(function(pmdata){
              res.json({ result : pmdata[0].u_pm });
            });
}

//-- end payment page

//- start pageRW
function getMaxSPid(req, res)
{


    spModel.find().sort({sp_id:-1}).limit(1)
                .then(function (cbdata){
                  console.log("come come");
                  if(cbdata.length == 0)
                  {
                    res.json({ maxid : 0 });
                  }
                  else{
                  console.log("fff : "+cbdata[0].sp_id+1);
                  res.json({ maxid : cbdata[0].sp_id+1 });
                      }
                },
                function (err)
                {
                  console.log("none!!!");
                });
}

function doRequestRW(req, res)
{
  var rwbody = req.body.obj;
  console.log("spdata : " + JSON.stringify(rwbody));

  rwbody['sp_requested_id'] = req.session.u_id;
  spModel.create(rwbody)
          .then(function(rwdata){
              console.log("myuperid : " + req.session.u_uppercode);
            lrfModel.find({ u_id : req.session.u_uppercode })
                      .then(function(chdata){
                        console.log("to.. TRANSFER_SHOPSHOP");
                        clientsocket.emit('TRANSFER_SHOPSHOP' , {"receiverid":chdata[0].u_id, "receiverdeep" : chdata[0].u_deep , "result" : 1});
                        res.json({result : true});
                      });
            
          });
}

function loadAllMySPRequest(req, res)
{
  var sprbody = {};
  sprbody["sp_requested_type"] = req.body.sp_requested_type;

  var mypvparam = req.body.pvparam;

  sprbody['sp_requested_id'] = req.session.u_id;

  spModel.aggregate([
                      { $sort : { "sp_datetime" : -1} },
                      { 
                        $match : sprbody 
                      },
                      {
                        $skip : parseInt((mypvparam.currentpage-1)*mypvparam.numperpage)
                      },
                      {
                        $limit : parseInt(mypvparam.numperpage)
                      }

                    ],function(err, docs){
                        res.json({obj : docs});
                    });
}

function loadAllMySPRequestNum(req, res)
{
  var sprbody = {};
  sprbody["sp_requested_type"] = req.body.sp_requested_type;

  sprbody['sp_requested_id'] = req.session.u_id;



  spModel.aggregate([
                      { 
                        $match : sprbody 
                      },
                      {
                        $count : "pagenum"
                      }

                    ],function(err, docs){

                      console.log("sp requeste~~~~ : " + JSON.stringify(docs));

                      if(docs.length != 0)
                        res.json({obj : docs[0].pagenum});
                      else
                        res.json({obj : 0});
                    });
}

function cancelReq(req, res)
{
  var crwbody = req.body.obj;
  spModel.update({"sp_id" : crwbody.sp_id} , {"sp_implemented_result" : '2'})
          .then(function(crwdata){
            console.log("myuperid : " + req.session.u_uppercode);
            lrfModel.find({ u_id : req.session.u_uppercode })
                      .then(function(chdata){
                        console.log("to.. TRANSFER_SHOPSHOP");
                        clientsocket.emit('TRANSFER_SHOPSHOP' , {"receiverid":chdata[0].u_id, "receiverdeep" : chdata[0].u_deep , "result" : 1});
                        res.json({result : true});
                      });
          }); 
}

function showShopRWmsm(req, res)
{
  console.log("diffid : " + refermsm.diffid);
  
  spModel.find({sp_id : refermsm.diffid})
          .then(function(ssrwmsm){
            var reqdate = new Date(ssrwmsm[0].sp_datetime);
            var sptypetext;
            var imptypetext;
            if(ssrwmsm[0].sp_requested_type == 1)
              sptypetext = "충전요청";
            else if(ssrwmsm[0].sp_requested_type == 2)
              sptypetext = "환전요청";

            if(ssrwmsm[0].sp_implemented_result == "1")
              imptypetext = "승인";
            else if(ssrwmsm[0].sp_implemented_result == "2")
              imptypetext = "취소";

            var msmstr = reqdate.toLocaleString() + "에 "+sptypetext+"한 " + "금액 : "+ ssrwmsm[0].sp_real_money + " " + ssrwmsm[0].sp_money_code + 
                        "에 해당한 게임금액 : "+ ssrwmsm[0].sp_money + "이 "+imptypetext+"되였습니다.";
            res.json({msmbody : msmstr});
          });
}

function loadShopBalance(req, res)
{
  console.log("enter....");
  var param1 = {};
  var param2 = {};
  if(req.session.u_deep == 1) // if its bonsa
  {
    console.log("it's bonsa balance param");
    /*var comarr = [];
    for(var i=100; i<=200; i++)
      comarr.push(i.toString());

    param1 = {'uinfo1.u_uppercode' : '1'};
    param2 = {  
                  $or : [
                      {'uinfo2.u_uppercode' : '1'} ,
                      {'uinfo2.u_groupid' : { $in : comarr}} 
                        ] , 
                  sp_implemented_result : "1"
             };*/
    res.json({ lsbalance : 0 , deeper : req.session.u_deep , lsbflag : "yes" });
  }
  else // if its others not bonsa
  {
    console.log("it's others balance param");

    param1 = { ep_loginid : req.session.u_metaid };
    param2 = { sp_servicer_id : req.session.u_metaid , sp_implemented_result : "1" };
  }
  if(req.session.u_deep != 1)
  {
    epModel.aggregate([
    {
      $match : param1
    },
    {
      $group : {

      _id : "$ep_loginid",
      plusval : {
            "$sum" : {
              "$cond" : {
                if : { $gte : [ "$ep_type", 100 ] } , then: "$ep_money" , else: 0
                        }
                      }
                },
      minusval : {
            "$sum" : {
              "$cond" : {
                if : { $lt : [ "$ep_type", 100 ] } , then: "$ep_money" , else: 0
                        }
                      }
                }
              }
    }], function(err, docs){

      var fa = 0;
      var sa = 0;
      if(docs != undefined)
      {
        if(docs.length>0)
        {
          if(docs[0].minusval != undefined)
            sa = docs[0].minusval;
          if(docs[0].plusval != undefined)
            fa = docs[0].plusval;
        }
      }
      var aab = fa - sa;
                spModel.aggregate([
              {
                $match : { sp_requested_id : req.session.u_metaid , sp_implemented_result : '3' , sp_requested_type : 2 }
              },
              {
                $group : {

                      _id : "$sp_requested_id",
                      sumval : { $sum: "$sp_money" }
                    }
              }], function(mederr, meddocs){
                console.log("original : " + aab);

                var ta = 0;
                if(meddocs != undefined)
                {
                  if(meddocs.length > 0)
                  {
                    if(meddocs[0].sumval != undefined)
                     ta = meddocs[0].sumval;
                  }
                }
                aab = aab - ta;
                  
                    spModel.aggregate([
                        {
                          $match : param2
                        },
                        {
                          $group : {

                          _id : null,
                          plusval : {
                                "$sum" : {
                                  "$cond" : {
                                    if : { $eq : [ "$sp_requested_type", 2 ] } , then: "$sp_money" , else: 0
                                            }
                                          }
                                    },
                          minusval : {
                                "$sum" : {
                                  "$cond" : {
                                    if : { $eq : [ "$sp_requested_type", 1 ] } , then: "$sp_money" , else: 0
                                            }
                                          }
                                    }
                                  }
                        }], function(err, spdocs){

                          var fa1 = 0;
                          var sa1 = 0;
                          if(spdocs != undefined)
                          {
                            if(spdocs.length>0)
                            {
                              if(spdocs[0].minusval != undefined)
                                sa1 = spdocs[0].minusval;
                              if(spdocs[0].plusval != undefined)
                                fa1 = spdocs[0].plusval;
                            }
                          }
                          var aab1 = fa1 - sa1;
                          aab = aab + aab1;

                          console.log("spdocs : " + aab1);
                          res.json({ lsbalance : aab , deeper : req.session.u_deep , lsbflag : "no" });

                        });
              });

    });
  }
}

function getMyUpperCardInfo(req, res)
{
  lrfModel.find({ u_id : req.session.u_uppercode })
            .then(function(mucdata){
              if(mucdata[0].u_pm != undefined)
              {
                console.log("A")
                res.json({ cardsinfo : mucdata[0].u_pm });
              }
              else
              {
                console.log("B")
                res.json({ cardsinfo : {} });
              }
            });
}

//- end pageRW

//- start some part of page 1
 
  /*function getTable1DetailCalc(req, res)
 {
  if(parseInt(req.session.u_groupid) >= 100) // if this is manager
    {
      lrfModel.find({"u_uppercode" : "1"})
                .then(function(gzcdata){
                  getTable1DetailCalcClone(req, res, gzcdata[0].u_id);
                });
    }
  else{
    getTable1DetailCalcClone(req, res, req.session.u_id);
  }
 }*/

 function getTable1DetailCalc(req, res)
 {
    var ttype = req.body.type;
    var mykey = req.body.keyword;
    var filtering = {};
    switch(parseInt(ttype))
    {
      case 1:
        filtering = {  "u_id": { $ne: req.session.u_metaid }  , "paths": { $all: req.session.paths } , "u_groupid" : "2" };
        break;
      case 2:
        filtering = {  "u_uppercode": { $ne: req.session.u_metaid }  , "paths": { $all: req.session.paths } , "u_groupid" : "1" };
        break;
      case 3:
        filtering = {  "paths": req.session.paths  , "u_groupid" : "1" };
        break;
    }
    if(mykey != "")
          filtering["u_nickname"] = {'$regex': '.*'+mykey+'.*'};

    if(ttype == 1){
    lrfModel.aggregate([
                                {
                                    $match : filtering
                                },
                                { $count : "pagenum"}
                                ],function (err,kaldocs) {
                                    
                                    if(kaldocs.length == 0)
                                      res.json({"wpagenum": 0});
                                    else
                                      res.json({"wpagenum": kaldocs[0].pagenum});

                                });
  }
  if(ttype == 2)
  {
    lrfModel.aggregate([
                                {
                                    $match : filtering
                                },
                                { $count : "pagenum"}
                                ],function (err,kaldocs) {

                                  if(kaldocs.length == 0)
                                    res.json({"wpagenum": 0});
                                  else
                                    res.json({"wpagenum": kaldocs[0].pagenum});

                                });
  }
  if(ttype == 3)
  {
    lrfModel.aggregate([
                                {
                                    $match : filtering
                                },
                                { $count : "pagenum"}
                                ],function (err,kaldocs) {

                                  if(kaldocs.length == 0)
                                    res.json({"wpagenum": 0});
                                  else
                                    res.json({"wpagenum": kaldocs[0].pagenum});

                                });
  }

 }

 /*function getTable1Detail(req, res)
 {
  if(parseInt(req.session.u_groupid) >= 100) // if this is manager
    {
      lrfModel.find({"u_uppercode" : "1"})
                .then(function(gzcdata){
                  getTable1DetailClone(req, res, gzcdata[0].u_id);
                });
    }
  else{
    getTable1DetailClone(req, res, req.session.u_id);
  }
 }*/

 function getTable1Detail(req, res)
 {
    var ttype = req.body.type;
    var mykey = req.body.keyword;

    var gt_num = req.body.pagenum;
    var gt_amount = req.body.pageamount;

    var filtering = {};
    switch(parseInt(ttype))
    {
      case 1:
        filtering = {  "u_id": { $ne: req.session.u_metaid }  , "paths": { $all: req.session.paths } , "u_groupid" : "2" };
        break;
      case 2:
        filtering = {  "u_uppercode": { $ne: req.session.u_metaid }  , "paths": { $all: req.session.paths } , "u_groupid" : "1" };
        break;
      case 3:
        filtering = {  "paths": req.session.paths  , "u_groupid" : "1" };
        break;
    }
    if(mykey != "")
          filtering["u_nickname"] = {'$regex': '.*'+mykey+'.*'};

    if(ttype == 1){
    lrfModel.aggregate([
                                {
                                    $match : filtering
                                },
                                {
                                  $skip : parseInt((gt_num-1)*gt_amount)
                                },
                                {
                                  $limit : parseInt(gt_amount)
                                },
                                { 
                                  $project: { "u_nickname" : 1 , "paths" : 1 } 
                                }
                                ],function (err,kaldocs) {
                                    
                                    var rauval = kaldocs;

                                    async.eachSeries(rauval,function(item,callback) 
                                    {

                                        lrfModel.find({"paths": item.paths , "u_groupid" : "1"}).count()
                                                .then(function(first){
                                                  item['direct_mem'] = first;

                                                  lrfModel.find({"paths": { $all: item.paths } , "u_groupid" : "1"}).count()
                                                    .then(function(second){
                                                      item['allunder_mem'] = second;
                                                      callback();
                                                    });
                                                  
                                              });

                                    },
                                    function(err) {
                                        if (err) throw err;

                                        //console.log("****end*****//yogiya~" + rauval.length);

                                        res.json({"result" : rauval});
                                      });

                                  });
  }
  if(ttype == 2)
  {
    lrfModel.aggregate([
                                {
                                    $match : filtering
                                },
                                {
                                  $skip : parseInt((gt_num-1)*gt_amount)
                                },
                                {
                                  $limit : parseInt(gt_amount)
                                },
                                { 
                                  $project: { "u_nickname" : 1 , "u_level" : 1 } 
                                }
                                ],function (err,kaldocs) {

                                  //console.log("****end*****//yogiyaaaa~" + JSON.stringify(kaldocs));

                                  res.json({"result" : kaldocs});

                                });
  }
  if(ttype == 3)
  {
    lrfModel.aggregate([
                                {
                                    $match : filtering
                                },
                                {
                                  $skip : parseInt((gt_num-1)*gt_amount)
                                },
                                {
                                  $limit : parseInt(gt_amount)
                                },
                                { 
                                  $project: { "u_nickname" : 1 , "u_level" : 1 } 
                                }
                                ],function (err,kaldocs) {

                                  //console.log("****end*****//yogiya~" + kaldocs.length);

                                  res.json({"result" : kaldocs});

                                });
  }

 }

 function getTable2DetailCalc(req, res)
 {
    var maintype = req.body.maintype;
    var subtype = req.body.subtype;
    var mykey = req.body.keyword;

    var subfiltering = {};
    var mainfiltering = [];
    var ssfil = {};
    console.log("keyword : " + mykey);

    if(mykey != "")
          ssfil["u_nickname"] = {'$regex': '.*'+mykey+'.*'};

    switch(parseInt(maintype))
    {
      case 1: //shop
        ssfil['paths'] = { $all: req.session.paths };
        ssfil['u_groupid'] = "2";

        mainfiltering = [
                    {
                    $match : ssfil
                    },
                    { 
                        "$redact": {
                            "$cond": [
                                { "$gt": [ { "$size": "$paths" }, req.session.paths.length ] },
                                "$$KEEP",
                                "$$PRUNE"
                            ]
                        }
                    },
                    { 
                      $count : "pagenum"
                    }
                   ];
        break;
      case 2: //member
        ssfil['paths'] = req.session.paths;
        ssfil['u_groupid'] = "1";

        mainfiltering = [
                    {
                    $match : ssfil
                    },
                    { 
                      $count : "pagenum"
                    }
                   ];
        break;
    }
        //console.log("mainfiltering : " + JSON.stringify(mainfiltering));


    lrfModel.aggregate(mainfiltering,function (err,kaldocs) {
                      console.log("table2ssd step : " + JSON.stringify(kaldocs));
                      if(kaldocs.length == 0)
                        res.json({"keitanum": 0});
                      else
                        res.json({"keitanum": kaldocs[0].pagenum});

      });

 }

/*function getTable2Detail(req, res)
 {
  if(parseInt(req.session.u_groupid) >= 100) // if this is manager
    {
      lrfModel.find({"u_uppercode" : "1"})
                .then(function(gzcdata){
                  getTable2DetailClone(req, res, gzcdata[0].u_id);
                });
    }
  else{
    getTable2DetailClone(req, res, req.session.u_id);
  }
 }*/

function getTable2Detail(req, res)
 {
    var maintype = req.body.maintype;
    var subtype = req.body.subtype;
    var mykey = req.body.keyword;

    var subfiltering = {};
    var mainfiltering = [];
    var ssfil = {};

    var gt_num = req.body.pagenum;
    var gt_amount = req.body.pageamount;

    console.log("keyword : " + mykey);

    if(mykey != "")
          ssfil["u_nickname"] = {'$regex': '.*'+mykey+'.*'};

    switch(parseInt(maintype))
    {
      case 1: //shop
        ssfil['paths'] = { $all: req.session.paths };
        ssfil['u_groupid'] = "2";

        mainfiltering = [
                    {
                    $match : ssfil
                    },
                    { 
                        "$redact": {
                            "$cond": [
                                { "$gt": [ { "$size": "$paths" }, req.session.paths.length ] },
                                "$$KEEP",
                                "$$PRUNE"
                            ]
                        }
                    },
                    {
                      $skip : parseInt((gt_num-1)*gt_amount)
                    },
                    {
                      $limit : parseInt(gt_amount)
                    },
                    { 
                      $project: { "u_nickname" : 1 , "u_id" : 1 } 
                    }
                   ];
        break;
      case 2: //member
        ssfil['paths'] = req.session.paths;
        ssfil['u_groupid'] = "1";

        mainfiltering = [
                    {
                    $match : ssfil
                    },
                    {
                      $skip : parseInt((gt_num-1)*gt_amount)
                    },
                    {
                      $limit : parseInt(gt_amount)
                    },
                    { 
                      $project: { "u_nickname" : 1 , "u_id" : 1 } 
                    }
                   ];
        break;
    }

        console.log("mainfiltering : " + JSON.stringify(mainfiltering));


        lrfModel.aggregate(mainfiltering,function (err,kaldocs) {

          console.log("my kal : ~~~ " + kaldocs.length);
          var start_date;
          var end_date;

          switch(parseInt(subtype))
          {
            case 1:
              start_date = new Date();
              end_date = new Date();
              start_date.setHours(0,0,0,0);
              subfiltering = { $and: [ {$gte: [ "$ep_datetime", start_date ]} , {$lt: [ "$ep_datetime", end_date ]} ] };
              break;
            case 2:
              yesterday = (new Date()).toDateFromDays(-1);
              end_date = yesterday.setHours(23,59,59,999);
              start_date = yesterday.setHours(0,0,0,0);
              subfiltering = { $and: [ {$gte: [ "$ep_datetime", start_date ]} , {$lt: [ "$ep_datetime", end_date ]} ] };
              break;
            case 3:
              end_date = new Date();
              oneweek = end_date.toDateFromDays(-7);
              start_date = oneweek.setHours(0,0,0,0);
              subfiltering = { $and: [ {$gte: [ "$ep_datetime", start_date ]} , {$lt: [ "$ep_datetime", end_date ]} ] };
              break;
            case 4:
              end_date = new Date();
              twoweek = end_date.toDateFromDays(-14);
              start_date = twoweek.setHours(0,0,0,0);
              subfiltering = { $and: [ {$gte: [ "$ep_datetime", start_date ]} , {$lt: [ "$ep_datetime", end_date ]} ] };
              break;
            case 5:
              end_date = new Date();
              onemonth = end_date.toDateFromDays(-31);
              start_date = onemonth.setHours(0,0,0,0);
              subfiltering = { $and: [ {$gte: [ "$ep_datetime", start_date ]} , {$lt: [ "$ep_datetime", end_date ]} ] };
              break;
            case 6:
              end_date = new Date();
              threemonth = end_date.toDateFromDays(-93);
              start_date = threemonth.setHours(0,0,0,0);
              subfiltering = { $and: [ {$gte: [ "$ep_datetime", start_date ]} , {$lt: [ "$ep_datetime", end_date ]} ] };
              break;
            case 7:
              end_date = new Date();
              subfiltering = { $and: [ {$lt: [ "$ep_datetime", end_date ]} ] };
              break;
          }

          var tempkal = kaldocs;

          async.eachSeries(kaldocs,function(item,callback) 
                            {

                                epModel.aggregate([
                                {
                                    $lookup:{
                                              from: "user_info",
                                              localField: "ep_loginid",
                                              foreignField: "u_id",
                                              as: "umyinfo"
                                            }
                                },
                                {
                                      $unwind: "$umyinfo"
                                },
                                {
                                    $match : { ep_loginid : req.session.u_metaid , ep_type : 126 , "ep_others.ep_ag_arr" : item.u_id }
                                },
                                {
                                    $group : {

                                    _id : "$ep_loginid" , 

                                    pvalue : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: subfiltering, then: "$ep_money", else: 0
                                                        }
                                                    }
                                            }

                                    }
                                }
                                ],function (err,rakdocs) {
                                    //console.log("rakdocs : " + JSON.stringify(rakdocs));
                                    if(rakdocs.length == 0)
                                      item['moneys'] = 0;
                                    else
                                      item['moneys'] = rakdocs[0].pvalue;

                                    callback();
                                });

                            },
                            function(err) {
                                if (err) throw err;

                                //console.log("tempkal~~~###$$$ : " + JSON.stringify(tempkal[0]));
                                res.json({result : tempkal});

                        });

        });
 }

 /*function getZhonghe(req, res)
 {
  if(parseInt(req.session.u_groupid) >= 100) // if this is manager
    {
      lrfModel.find({"u_uppercode" : "1"})
                .then(function(gzcdata){
                  getZhongheclone(req, res, gzcdata[0].u_id);
                });
    }
  else{
    getZhongheclone(req, res, req.session.u_id);
  }
 }*/

 function getZhonghe(req, res)
 {
  var fil_obj1,fil_obj2,fil_obj3;
  fil_obj1 = fil_obj2 = fil_obj3 = {};
  var table1 = {};
  var table2 = {};
  var table3 = [];
    //console.log("mypaths : " + req.session.paths);

    var today_end_date = new Date();
    var today_start_date = new Date();
    today_start_date.setHours(0,0,0,0);

    var yesterday = (new Date()).toDateFromDays(-1);
    var yesterday_end_date = yesterday.setHours(23,59,59,999);
    var yesterday_start_date = yesterday.setHours(0,0,0,0);

    var oneweek_end_date = new Date();
    var oneweek = oneweek_end_date.toDateFromDays(-7);
    var oneweek_start_date = oneweek.setHours(0,0,0,0);

    var twoweek_end_date = new Date();
    var twoweek = twoweek_end_date.toDateFromDays(-14);
    var twoweek_start_date = twoweek.setHours(0,0,0,0);

    var onemonth_end_date = new Date();
    var onemonth = onemonth_end_date.toDateFromDays(-31);
    var onemonth_start_date = onemonth.setHours(0,0,0,0);

    var threemonth_end_date = new Date();
    var threemonth = threemonth_end_date.toDateFromDays(-93);
    var threemonth_start_date = threemonth.setHours(0,0,0,0);

    var whole_end_date = new Date();

    var gameParams = [{gameid : 1 , winflag : 103 , failflag : 3}, // Holdem Poker
                        {gameid : 2 , winflag : 104 , failflag : 4}, // Majang
                        {gameid : 3 , winflag : 105 , failflag : 5}, // Deou Di zhu
                        {gameid : 4 , winflag : 106 , failflag : 6}] // Sadari

      var prvtime = new Date().getTime();

      lrfModel.find({  "u_id": { $ne: req.session.u_metaid }  , "paths": { $all: req.session.paths } , "u_groupid" : "2"  }).count().then(function(myd){
        //console.log("myddd~~~~~~ :L " + myddd);
        table1["childshop_num"] = myd;
        lrfModel.find({  "u_uppercode": { $ne: req.session.u_metaid }  , "paths": { $all: req.session.paths } , "u_groupid" : "1"  }).count().then(function(mydd){
            table1["member_num"] = mydd;
            lrfModel.find({  "paths": req.session.paths  , "u_groupid" : "1" }).count().then(function(myddd){
              table1["d_member_num"] = myddd;
              //console.log("table1@@ : " + JSON.stringify(table1));

              epModel.aggregate([
                                {
                                    $match : { ep_loginid : req.session.u_metaid , ep_type : 126 ,  "ep_others.ep_ag_arr": { $size: 1 } }
                                },
                                {
                                    $group : {

                                    _id : "$ep_loginid" , 

                                    todayfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", today_start_date ]} , {$lt: [ "$ep_datetime", today_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    yesterdayfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", yesterday_start_date ]} , {$lt: [ "$ep_datetime", yesterday_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    oneweekfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", oneweek_start_date ]} , {$lt: [ "$ep_datetime", oneweek_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    twoweekfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", twoweek_start_date ]} , {$lt: [ "$ep_datetime", twoweek_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    onemonthfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", onemonth_start_date ]} , {$lt: [ "$ep_datetime", onemonth_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    threemonthfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", threemonth_start_date ]} , {$lt: [ "$ep_datetime", threemonth_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    allfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$lt: [ "$ep_datetime", whole_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                }

                                    }
                                }
                                ],function (err,kaldocs) {
                                  if(kaldocs.length != 0)
                                    table2["directmem_profit"] = kaldocs[0];
                                    
                                    epModel.aggregate([
                                      {
                                          $match : { ep_loginid : req.session.u_metaid , ep_type : 126 }
                                      },
                                      {
                                          $group : {

                                          _id : "$ep_loginid" , 

                                          todayfeed : {
                                                          "$sum": {
                                                              "$cond": {
                                                                if: { $and: [ {$gte: [ "$ep_datetime", today_start_date ]} , {$lt: [ "$ep_datetime", today_end_date ]} ] }, then: "$ep_money", else: 0
                                                              }
                                                          }
                                                      },
                                          yesterdayfeed : {
                                                          "$sum": {
                                                              "$cond": {
                                                                if: { $and: [ {$gte: [ "$ep_datetime", yesterday_start_date ]} , {$lt: [ "$ep_datetime", yesterday_end_date ]} ] }, then: "$ep_money", else: 0
                                                              }
                                                          }
                                                      },
                                          oneweekfeed : {
                                                          "$sum": {
                                                              "$cond": {
                                                                if: { $and: [ {$gte: [ "$ep_datetime", oneweek_start_date ]} , {$lt: [ "$ep_datetime", oneweek_end_date ]} ] }, then: "$ep_money", else: 0
                                                              }
                                                          }
                                                      },
                                          twoweekfeed : {
                                                          "$sum": {
                                                              "$cond": {
                                                                if: { $and: [ {$gte: [ "$ep_datetime", twoweek_start_date ]} , {$lt: [ "$ep_datetime", twoweek_end_date ]} ] }, then: "$ep_money", else: 0
                                                              }
                                                          }
                                                      },
                                          onemonthfeed : {
                                                          "$sum": {
                                                              "$cond": {
                                                                if: { $and: [ {$gte: [ "$ep_datetime", onemonth_start_date ]} , {$lt: [ "$ep_datetime", onemonth_end_date ]} ] }, then: "$ep_money", else: 0
                                                              }
                                                          }
                                                      },
                                          threemonthfeed : {
                                                          "$sum": {
                                                              "$cond": {
                                                                if: { $and: [ {$gte: [ "$ep_datetime", threemonth_start_date ]} , {$lt: [ "$ep_datetime", threemonth_end_date ]} ] }, then: "$ep_money", else: 0
                                                              }
                                                          }
                                                      },
                                          allfeed : {
                                                          "$sum": {
                                                              "$cond": {
                                                                if: { $and: [ {$lt: [ "$ep_datetime", whole_end_date ]} ] }, then: "$ep_money", else: 0
                                                              }
                                                          }
                                                      }

                                          }
                                      }
                                      ],function (err,wangdocs) {
                                        if(wangdocs.length != 0)
                                          table2["whole_profit"] = wangdocs[0];
										  
										  if(table2.whole_profit == undefined)
                                            table2["whole_profit"] = { "todayfeed" : 0 , "yesterdayfeed" : 0 ,"oneweekfeed" : 0 ,"twoweekfeed" : 0 ,"onemonthfeed" : 0 ,"threemonthfeed" : 0 ,"allfeed" : 0 };
                                          if(table2.directmem_profit == undefined)
                                            table2["directmem_profit"] = { "todayfeed" : 0 , "yesterdayfeed" : 0 ,"oneweekfeed" : 0 ,"twoweekfeed" : 0 ,"onemonthfeed" : 0 ,"threemonthfeed" : 0 ,"allfeed" : 0 };

                                          var shop_profit = {};
                                          shop_profit['_id'] = req.session.u_metaid;
                                          shop_profit['todayfeed'] = table2.whole_profit.todayfeed - table2.directmem_profit.todayfeed;
                                          shop_profit['yesterdayfeed'] = table2.whole_profit.yesterdayfeed - table2.directmem_profit.yesterdayfeed;
                                          shop_profit['oneweekfeed'] = table2.whole_profit.oneweekfeed - table2.directmem_profit.oneweekfeed;
                                          shop_profit['twoweekfeed'] = table2.whole_profit.twoweekfeed - table2.directmem_profit.twoweekfeed;
                                          shop_profit['onemonthfeed'] = table2.whole_profit.onemonthfeed - table2.directmem_profit.onemonthfeed;
                                          shop_profit['threemonthfeed'] = table2.whole_profit.threemonthfeed - table2.directmem_profit.threemonthfeed;
                                          shop_profit['allfeed'] = table2.whole_profit.allfeed - table2.directmem_profit.allfeed;

                                          table2["shop_profit"] = shop_profit;

                                          //console.log("table2@@@@ : " + JSON.stringify(table2));

                                            async.eachSeries(gameParams,function(item,callback) 
                                                    {
                                                        

                                                        epModel.aggregate([
                                                        {
                                                            $match : { ep_loginid : req.session.u_metaid , ep_type : 126 , "ep_others.game_id" : item.gameid }
                                                        },
                                                        {
                                                            $group : {

                                                            _id : "$ep_loginid" , 

                                                            todayfeed : {
                                                                            "$sum": {
                                                                                "$cond": {
                                                                                  if: { $and: [ {$gte: [ "$ep_datetime", today_start_date ]} , {$lt: [ "$ep_datetime", today_end_date ]} ] }, then: "$ep_money", else: 0
                                                                                }
                                                                            }
                                                                        },
                                                            yesterdayfeed : {
                                                                            "$sum": {
                                                                                "$cond": {
                                                                                  if: { $and: [ {$gte: [ "$ep_datetime", yesterday_start_date ]} , {$lt: [ "$ep_datetime", yesterday_end_date ]} ] }, then: "$ep_money", else: 0
                                                                                }
                                                                            }
                                                                        },
                                                            oneweekfeed : {
                                                                            "$sum": {
                                                                                "$cond": {
                                                                                  if: { $and: [ {$gte: [ "$ep_datetime", oneweek_start_date ]} , {$lt: [ "$ep_datetime", oneweek_end_date ]} ] }, then: "$ep_money", else: 0
                                                                                }
                                                                            }
                                                                        },
                                                            twoweekfeed : {
                                                                            "$sum": {
                                                                                "$cond": {
                                                                                  if: { $and: [ {$gte: [ "$ep_datetime", twoweek_start_date ]} , {$lt: [ "$ep_datetime", twoweek_end_date ]} ] }, then: "$ep_money", else: 0
                                                                                }
                                                                            }
                                                                        },
                                                            onemonthfeed : {
                                                                            "$sum": {
                                                                                "$cond": {
                                                                                  if: { $and: [ {$gte: [ "$ep_datetime", onemonth_start_date ]} , {$lt: [ "$ep_datetime", onemonth_end_date ]} ] }, then: "$ep_money", else: 0
                                                                                }
                                                                            }
                                                                        },
                                                            threemonthfeed : {
                                                                            "$sum": {
                                                                                "$cond": {
                                                                                  if: { $and: [ {$gte: [ "$ep_datetime", threemonth_start_date ]} , {$lt: [ "$ep_datetime", threemonth_end_date ]} ] }, then: "$ep_money", else: 0
                                                                                }
                                                                            }
                                                                        },
                                                            allfeed : {
                                                                            "$sum": {
                                                                                "$cond": {
                                                                                  if: { $and: [ {$lt: [ "$ep_datetime", whole_end_date ]} ] }, then: "$ep_money", else: 0
                                                                                }
                                                                            }
                                                                        }

                                                            }
                                                        }
                                                        ],function (err,gamedocs) {
                                                            
                                                            var tempel = gamedocs[0];

                                                            table3.push(tempel);

                                                            callback();
                                                        });

                                                    },
                                                    function(err) {
                                                        if (err) throw err;

                                                        //console.log("table3~~~~~~~~~~~~" + JSON.stringify(table3)); 
                                                        var cttime = new Date().getTime();
                                                        console.log("periods~~ : " + (cttime - prvtime));

                                                        res.json({ "table1" : table1 , "table2" : table2 , "table3" : table3 });


                                            });
                                          
                                      });
                                    
                                });
              
              });

          });
      });
            
 }


/* Define new prototype methods on Date object. */
      // Returns Date as a String in YYYY-MM-DD format.
      Date.prototype.toISODateString = function () {
        return this.toISOString().substr(0,10);
      };

      // Returns new Date object offset `n` days from current Date object.
      Date.prototype.toDateFromDays = function (n) {
        n = parseInt(n) || 0;
        var newDate = new Date(this.getTime());
        newDate.setDate(this.getDate() + n);
        return newDate;
      };

  function s_allfeed(str)
  {
    return str.match(/([^\?]*)\allfeed":(\d*)/);
  }

  function s_today(str)
  {
    return str.match(/([^\?]*)\odayfeed":(\d*)/);
  }

  function s_yesterday(str)
  {
    return str.match(/([^\?]*)\esterdayfeed":(\d*)/);
  }

  function s_oneweek(str)
  {
    return str.match(/([^\?]*)\oneweekfeed":(\d*)/);
  }

  function s_twoweek(str)
  {
    return str.match(/([^\?]*)\woweekfeed":(\d*)/);
  }

  function s_onemonth(str)
  {
    return str.match(/([^\?]*)\onemonthfeed":(\d*)/);
  }

  function s_threemonth(str)
  {
    return str.match(/([^\?]*)\hreemonthfeed":(\d*)/);
  }
  function s_myid()
  {
    return str.match(/([^\?]*)\_id":(\d*)/);
  }

function doPBAforvisitor(v_arr , res , currentuser)
{

    var memPBAs = [];

    var today_end_date = new Date();
    var today_start_date = new Date();
    today_start_date.setHours(0,0,0,0);

    var yesterday = (new Date()).toDateFromDays(-1);
    var yesterday_end_date = yesterday.setHours(23,59,59,999);
    var yesterday_start_date = yesterday.setHours(0,0,0,0);

    var oneweek_end_date = new Date();
    var oneweek = oneweek_end_date.toDateFromDays(-7);
    var oneweek_start_date = oneweek.setHours(0,0,0,0);

    var twoweek_end_date = new Date();
    var twoweek = twoweek_end_date.toDateFromDays(-14);
    var twoweek_start_date = twoweek.setHours(0,0,0,0);

    var onemonth_end_date = new Date();
    var onemonth = onemonth_end_date.toDateFromDays(-31);
    var onemonth_start_date = onemonth.setHours(0,0,0,0);

    var threemonth_end_date = new Date();
    var threemonth = threemonth_end_date.toDateFromDays(-93);
    var threemonth_start_date = threemonth.setHours(0,0,0,0);

    var whole_end_date = new Date();


    //console.log("st : " + start_date.toString() + " - ed : " + end_date.toString());


    async.eachSeries(v_arr,function(item,callback) 
                            {
                                console.log("v_arr item : " + item.u_id);

                                epModel.aggregate([
                                {
                                    $match : { ep_loginid : currentuser , ep_type : 126 , $and : [ {"ep_others.ep_ag_arr" : item.u_id} , { "ep_others.ep_ag_arr": { $size: 1 } } ] }
                                },
                                {
                                    $group : {

                                    _id : "$ep_loginid" , 
                                    todayfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", today_start_date ]} , {$lt: [ "$ep_datetime", today_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    yesterdayfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", yesterday_start_date ]} , {$lt: [ "$ep_datetime", yesterday_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    oneweekfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", oneweek_start_date ]} , {$lt: [ "$ep_datetime", oneweek_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    twoweekfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", twoweek_start_date ]} , {$lt: [ "$ep_datetime", twoweek_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    onemonthfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", onemonth_start_date ]} , {$lt: [ "$ep_datetime", onemonth_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    threemonthfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", threemonth_start_date ]} , {$lt: [ "$ep_datetime", threemonth_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    allfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$lt: [ "$ep_datetime", whole_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                }

                                    }
                                }
                                ],function (err,docs) {
                                    var str_res = JSON.stringify(docs);
                                    console.log("PBA : " + str_res);
                                    
                                    var ssitem = {};
                                    if(s_allfeed(str_res) != null)
                                       ssitem['s_allfeed'] = s_allfeed(str_res)[2];
                                    else
                                       ssitem['s_allfeed'] = 0;

                                      console.log("s_allfeed : " + ssitem.s_allfeed);

                                    if(s_today(str_res) != null)
                                       ssitem['s_today'] = s_today(str_res)[2];
                                    else
                                       ssitem['s_today'] = 0;

                                      console.log("s_today : " + ssitem.s_today);

                                    if(s_yesterday(str_res) != null)
                                       ssitem['s_yesterday'] = s_yesterday(str_res)[2];
                                    else
                                       ssitem['s_yesterday'] = 0;

                                      console.log("s_yesterday : " + ssitem.s_yesterday);

                                    if(s_oneweek(str_res) != null)
                                       ssitem['s_oneweek'] = s_oneweek(str_res)[2];
                                    else
                                       ssitem['s_oneweek'] = 0;

                                      console.log("s_oneweek : " + ssitem.s_oneweek);

                                    if(s_twoweek(str_res) != null)
                                       ssitem['s_twoweek'] = s_twoweek(str_res)[2];
                                    else
                                       ssitem['s_twoweek'] = 0;

                                      console.log("s_twoweek : " + ssitem.s_twoweek);

                                    if(s_onemonth(str_res) != null)
                                       ssitem['s_onemonth'] = s_onemonth(str_res)[2];
                                    else
                                       ssitem['s_onemonth'] = 0;

                                      console.log("s_onemonth : " + ssitem.s_onemonth);

                                    if(s_threemonth(str_res) != null)
                                       ssitem['s_threemonth'] = s_threemonth(str_res)[2];
                                    else
                                       ssitem['s_threemonth'] = 0;
                                     
                                      console.log("s_threemonth : " + ssitem.s_threemonth);

                                    memPBAs.push(ssitem);

                                    callback();
                                });

                            },
                            function(err) {
                                if (err) throw err;

                                console.log("****end*****//yogiya~" + memPBAs.length);

                                res.json({ "memPBAs" : memPBAs });

                                //res.json({ "guan" : "hong" });
                                //doShopBonasProfit(ssbody , res);


                    });
}

function doPBAforshop(s_arr , res , currentuser)
{
    var shopPBAs = [];

    var today_end_date = new Date();
    var today_start_date = new Date();
    today_start_date.setHours(0,0,0,0);

    var yesterday = (new Date()).toDateFromDays(-1);
    var yesterday_end_date = yesterday.setHours(23,59,59,999);
    var yesterday_start_date = yesterday.setHours(0,0,0,0);

    var oneweek_end_date = new Date();
    var oneweek = oneweek_end_date.toDateFromDays(-7);
    var oneweek_start_date = oneweek.setHours(0,0,0,0);

    var twoweek_end_date = new Date();
    var twoweek = twoweek_end_date.toDateFromDays(-14);
    var twoweek_start_date = twoweek.setHours(0,0,0,0);

    var onemonth_end_date = new Date();
    var onemonth = onemonth_end_date.toDateFromDays(-31);
    var onemonth_start_date = onemonth.setHours(0,0,0,0);

    var threemonth_end_date = new Date();
    var threemonth = threemonth_end_date.toDateFromDays(-93);
    var threemonth_start_date = threemonth.setHours(0,0,0,0);

    var whole_end_date = new Date();


    //console.log("st : " + start_date.toString() + " - ed : " + end_date.toString());


    async.eachSeries(s_arr,function(item,callback) 
                            {
                                console.log("s_arr item : " + item.u_id);

                                epModel.aggregate([
                                {
                                    $lookup:{
                                              from: "user_info",
                                              localField: "ep_loginid",
                                              foreignField: "u_id",
                                              as: "umyinfo"
                                            }
                                },
                                {
                                      $unwind: "$umyinfo"
                                },
                                {
                                    $match : { ep_loginid : currentuser , ep_type : 126 , "ep_others.ep_ag_arr" : item.u_id }
                                },
                                {
                                    $group : {

                                    _id : "$ep_loginid" , 

                                    todayfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", today_start_date ]} , {$lt: [ "$ep_datetime", today_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    yesterdayfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", yesterday_start_date ]} , {$lt: [ "$ep_datetime", yesterday_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    oneweekfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", oneweek_start_date ]} , {$lt: [ "$ep_datetime", oneweek_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    twoweekfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", twoweek_start_date ]} , {$lt: [ "$ep_datetime", twoweek_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    onemonthfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", onemonth_start_date ]} , {$lt: [ "$ep_datetime", onemonth_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    threemonthfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", threemonth_start_date ]} , {$lt: [ "$ep_datetime", threemonth_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    allfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$lt: [ "$ep_datetime", whole_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                }

                                    }
                                }
                                ],function (err,docs) {
                                    var str_res = JSON.stringify(docs);
                                    console.log("PBA : " + str_res);
                                    
                                    var ssitem = {};
                                    if(s_allfeed(str_res) != null)
                                       ssitem['s_allfeed'] = s_allfeed(str_res)[2];
                                    else
                                       ssitem['s_allfeed'] = 0;

                                      console.log("s_allfeed : " + ssitem.s_allfeed);

                                    if(s_today(str_res) != null)
                                       ssitem['s_today'] = s_today(str_res)[2];
                                    else
                                       ssitem['s_today'] = 0;

                                      console.log("s_today : " + ssitem.s_today);

                                    if(s_yesterday(str_res) != null)
                                       ssitem['s_yesterday'] = s_yesterday(str_res)[2];
                                    else
                                       ssitem['s_yesterday'] = 0;

                                      console.log("s_yesterday : " + ssitem.s_yesterday);

                                    if(s_oneweek(str_res) != null)
                                       ssitem['s_oneweek'] = s_oneweek(str_res)[2];
                                    else
                                       ssitem['s_oneweek'] = 0;

                                      console.log("s_oneweek : " + ssitem.s_oneweek);

                                    if(s_twoweek(str_res) != null)
                                       ssitem['s_twoweek'] = s_twoweek(str_res)[2];
                                    else
                                       ssitem['s_twoweek'] = 0;

                                      console.log("s_twoweek : " + ssitem.s_twoweek);

                                    if(s_onemonth(str_res) != null)
                                       ssitem['s_onemonth'] = s_onemonth(str_res)[2];
                                    else
                                       ssitem['s_onemonth'] = 0;

                                      console.log("s_onemonth : " + ssitem.s_onemonth);

                                    if(s_threemonth(str_res) != null)
                                       ssitem['s_threemonth'] = s_threemonth(str_res)[2];
                                    else
                                       ssitem['s_threemonth'] = 0;

                                     console.log("s_threemonth : " + ssitem.s_threemonth);

                                    ssitem['s_myid'] = item.u_id;
                                     
                                     console.log("s_myid : " + ssitem.s_myid);
                                      

                                    //if(ssitem['s_myuppercode'] == sess.u_id) 
                                      shopPBAs.push(ssitem);

                                    callback();
                                });

                            },
                            function(err) {
                                if (err) throw err;

                                console.log("hallo");
                                var finald_pbadata = [];

                                lrfModel.find({"u_uppercode" : currentuser})
                                          .then(function(lrfdata){
                                            console.log("lrfdata : " + lrfdata.length);

                                            lrfdata.forEach(function(lrfitem){
                                              shopPBAs.forEach(function(spbad){

                                                if(lrfitem.u_id == spbad.s_myid)
                                                  finald_pbadata.push(spbad);
                                              });
                                              
                                            });

                                            console.log("final spba : " + finald_pbadata.length);

                                              res.json({ "shopPBAs" : finald_pbadata });

                                            //doPBAforvisitor(v_arr , finald_pbadata , res , currentuser);

                                          });
                                

                                //doShopBonasProfit(ssbody , res);


                    });
}
function getProfitByAllV(req, res)
{
  var s_arr = req.body.s_arr;
  var currentuser = req.session.u_metaid;

  console.log("s_arr : " + s_arr.length);
  doPBAforshop(s_arr , res , currentuser);

}

function getProfitByAllV_m(req, res)
{
    var v_arr = req.body.v_arr;
    var currentuser = req.session.u_metaid;

    console.log("v_arr : " + v_arr.length);
    doPBAforvisitor( v_arr , res , currentuser);

}


function doProfitByGame(g_arr , res , currentuser)
{
    var gameParr = [];

    var today_end_date = new Date();
    var today_start_date = new Date();
    today_start_date.setHours(0,0,0,0);

    var yesterday = (new Date()).toDateFromDays(-1);
    var yesterday_end_date = yesterday.setHours(23,59,59,999);
    var yesterday_start_date = yesterday.setHours(0,0,0,0);

    var oneweek_end_date = new Date();
    var oneweek = oneweek_end_date.toDateFromDays(-7);
    var oneweek_start_date = oneweek.setHours(0,0,0,0);

    var twoweek_end_date = new Date();
    var twoweek = twoweek_end_date.toDateFromDays(-14);
    var twoweek_start_date = twoweek.setHours(0,0,0,0);

    var onemonth_end_date = new Date();
    var onemonth = onemonth_end_date.toDateFromDays(-31);
    var onemonth_start_date = onemonth.setHours(0,0,0,0);

    var threemonth_end_date = new Date();
    var threemonth = threemonth_end_date.toDateFromDays(-93);
    var threemonth_start_date = threemonth.setHours(0,0,0,0);

    var whole_end_date = new Date();


    //console.log("st : " + start_date.toString() + " - ed : " + end_date.toString());


    async.eachSeries(g_arr,function(item,callback) 
                            {
                                console.log("g_arr item : " + item.gameid);

                                epModel.aggregate([
                                {
                                    $match : { ep_loginid : currentuser , ep_type : 126 , "ep_others.game_id" : item.gameid }
                                },
                                {
                                    $group : {

                                    _id : "$ep_loginid" , 

                                    todayfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", today_start_date ]} , {$lt: [ "$ep_datetime", today_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    yesterdayfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", yesterday_start_date ]} , {$lt: [ "$ep_datetime", yesterday_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    oneweekfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", oneweek_start_date ]} , {$lt: [ "$ep_datetime", oneweek_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    twoweekfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", twoweek_start_date ]} , {$lt: [ "$ep_datetime", twoweek_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    onemonthfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", onemonth_start_date ]} , {$lt: [ "$ep_datetime", onemonth_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    threemonthfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$gte: [ "$ep_datetime", threemonth_start_date ]} , {$lt: [ "$ep_datetime", threemonth_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                },
                                    allfeed : {
                                                    "$sum": {
                                                        "$cond": {
                                                          if: { $and: [ {$lt: [ "$ep_datetime", whole_end_date ]} ] }, then: "$ep_money", else: 0
                                                        }
                                                    }
                                                }

                                    }
                                }
                                ],function (err,docs) {
                                    var str_res = JSON.stringify(docs);
                                    console.log("gameobj : " + str_res);
                                    
                                    var ssitem = {};
                                    if(s_allfeed(str_res) != null)
                                       ssitem['s_allfeed'] = s_allfeed(str_res)[2];
                                    else
                                       ssitem['s_allfeed'] = 0;

                                      console.log("s_allfeed : " + ssitem.s_allfeed);

                                    if(s_today(str_res) != null)
                                       ssitem['s_today'] = s_today(str_res)[2];
                                    else
                                       ssitem['s_today'] = 0;

                                      console.log("s_today : " + ssitem.s_today);

                                    if(s_yesterday(str_res) != null)
                                       ssitem['s_yesterday'] = s_yesterday(str_res)[2];
                                    else
                                       ssitem['s_yesterday'] = 0;

                                      console.log("s_yesterday : " + ssitem.s_yesterday);

                                    if(s_oneweek(str_res) != null)
                                       ssitem['s_oneweek'] = s_oneweek(str_res)[2];
                                    else
                                       ssitem['s_oneweek'] = 0;

                                      console.log("s_oneweek : " + ssitem.s_oneweek);

                                    if(s_twoweek(str_res) != null)
                                       ssitem['s_twoweek'] = s_twoweek(str_res)[2];
                                    else
                                       ssitem['s_twoweek'] = 0;

                                      console.log("s_twoweek : " + ssitem.s_twoweek);

                                    if(s_onemonth(str_res) != null)
                                       ssitem['s_onemonth'] = s_onemonth(str_res)[2];
                                    else
                                       ssitem['s_onemonth'] = 0;

                                      console.log("s_onemonth : " + ssitem.s_onemonth);

                                    if(s_threemonth(str_res) != null)
                                       ssitem['s_threemonth'] = s_threemonth(str_res)[2];
                                    else
                                       ssitem['s_threemonth'] = 0;

                                     console.log("s_threemonth : " + ssitem.s_threemonth);

                                      ssitem['s_gameid'] = item.gameid;

                                      gameParr.push(ssitem);

                                    callback();
                                });

                            },
                            function(err) {
                                if (err) throw err;

                                console.log("hallo");

                                res.json({ gp_result : gameParr });


                    });
}

function getProfitByGame(req, res)
{
  var g_arr = req.body.obj;

  var currentuser = req.session.u_metaid;

  doProfitByGame(g_arr , res , currentuser);

}


// end some part of page 1

// start page17 help

function getMaxHNid(req, res)
{

    hnModel.find().sort({hn_id:-1}).limit(1)
                .then(function (cbdata){
                  if(cbdata.length == 0)
                  {
                    res.json({ result : 0 });
                  }
                  else
                  {
                  console.log("fff : "+cbdata[0].hn_id+1);
                  res.json({ result : cbdata[0].hn_id+1 });
                  }
                },
                function (err)
                {

                });
}

function addHelps(req, res)
{
  var helpvar = req.body.obj;
  hnModel.create(helpvar)
          .then(function(cbdata){
            res.json({result :true});
          });
}

function findHelpDatasNum(req, res)
{
  var fildatas = req.body.obj;
  hnModel.find(fildatas).count()
          .then(function(cbdata){
            res.json({"totalamount" : cbdata})
          });
}

function findHelpDatas(req, res)
{
  var fildatas = req.body.obj;
  var pvdatas = req.body.pvobj;

  hnModel.aggregate([
                      { $sort : { "hn_date" : -1} },
                      { 
                        $match : fildatas 
                      },
                      {
                        $skip : parseInt((pvdatas.currentpage-1)*pvdatas.numperpage)
                      },
                      {
                        $limit : parseInt(pvdatas.numperpage)
                      }

                    ],function(err, cbdata){

                        res.json({result : cbdata});
                    });
}

function updateHelpDatas(req, res)
{
  var uhdata = req.body;
  hnModel.update({"hn_id" : uhdata.id} , uhdata.obj)
          .then(function(cbdata){
            res.json({result : true});
          });
}

function delHelpDatas(req, res)
{
  var dhdata = req.body;
  hnModel.remove({"hn_id" : dhdata.hn_id})
          .then(function(cbdata){
            res.json({result : true});
          });
}

// end page17 help

// start page 16
  
  function get_firstsite_user(req, res)
  {
    lrfModel.find({u_deep : 1})
              .then(function(cbdata){
                res.json({ result : cbdata })
              });
  }

  function checkByacid(req, res)
  {
    lrfModel.find({u_ac_metaid : req.body.targetid})
            .then(function(cbdata){
              res.json({ result : cbdata[0].u_nickname });
            });
  }

// end page 16

// start check for emit signal to correct target 

function CheckForEmitTarget(req, res)
{

  var cfep = req.body.cfeparam;
  console.log("signaltype : " + cfep.signaltype + " / " + "result : " + cfep.result);
  console.log("this : myid : " + req.session.u_metaid  + "   receiverid : " + cfep.receiverid);
  allstatus_flag = 0;
  if(req.session.u_metaid == cfep.receiverid)
  {
    if(cfep.signaltype == 1) // from shop
    {
      if(cfep.result == 2) // its for sp done
        refermsm = cfep;
    }

    res.json({ result : true , flag : cfep.result });
  }
  else
    res.json({ result : false });
}

// end check for emit signal to correct target 

//start get allow data
  
  function get_allow_data(req, res)
  {
    console.log("will get acmodel~~~!!@@#");
    acModel.aggregate([{
                        $lookup:{
                              from: "user_info",
                              localField: "ac_id",
                              foreignField: "u_ac_metaid",
                              as: "uinfo"
                                }
                      },
                      {
                        $lookup:{
                              from: "group_info",
                              localField: "ac_id",
                              foreignField: "u_ac_metaid",
                              as: "ginfo"
                                }
                      }
                      ],function (err,docs) {

                        console.log("num of found : " + docs.length);

                        g_allow_data = docs;

                        res.json({ allowinfo : docs });

                      });
              
  }

  function get_global_acdata(req, res)
  {
    res.json({ acdata : g_allow_data , mycid : req.session.u_id , mygroup : req.session.u_groupid , mydeeper : req.session.u_deep });
  }

  function updateAllowers(req, res)
  {
    var comedata = req.body.obj;
    acModel.update({ ac_id : comedata.founderid } , comedata.up_obj)
            .then(function(cbdata){

                acModel.aggregate([{
                        $lookup:{
                              from: "user_info",
                              localField: "ac_id",
                              foreignField: "u_ac_metaid",
                              as: "uinfo"
                                }
                      },
                      {
                        $lookup:{
                              from: "group_info",
                              localField: "ac_id",
                              foreignField: "u_ac_metaid",
                              as: "ginfo"
                                }
                      }
                      ],function (err,docs) {

                        console.log("num of found : " + docs.length);

                        g_allow_data = docs;

                        res.json({ });

                      }); 
            });
  }

//end get allow data

//start page18

function addImplementer(imper , wmoney)
{
  bpModel.create({bp_imper : imper , bp_impamount : wmoney , bp_impdate : new Date()})
          .then(function(bpdata){

          });
}

function updateProcessed(targetruid)
{
  epModel.update({ep_processed: 0 , ep_loginid : targetruid}, {$set: {ep_processed: 1}}, {multi: true})
          .then(function(updataer){

          });
}

function updateFlag(targetruid , rugameid)
{

  epModel.update({ep_flag: 2 , "ep_others.ep_ag_arr" : targetruid , "ep_others.game_id" : rugameid }, {$set: {ep_flag: 0}}, {multi: true})
          .then(function(updataer){

          });
}

function  myRemove(karr , removelen) {
  var resultsarr = [];
  for(var i=0; i<karr.length; i++)
  {
    if(i >= removelen)
      resultsarr.push(karr[i]);
  }
  return resultsarr;
}

function domainImp(req , res, bettingames , allmembers , maxepid)
{ 
  console.log("start~~~~");
  var cmaxepid = maxepid;
  var starttime = 0;
  var wholetime = allmembers.length;

  var thiswholemoney = 0;

   async.eachSeries(allmembers,function(alitem,callback) 
      {

        lrfModel.aggregate([ 
              { "$graphLookup": { 
                  "from": "user_info", 
                  "startWith": "$u_uppercode", 
                  "connectFromField": "u_uppercode", 
                  "connectToField": "u_id", 
                  "as": "ancestors"
              }}, 
              { "$match": { "u_id": alitem.ruid } }, 
              { "$addFields": { 
                  "ancestors": { 
                      "$reverseArray": { 
                          "$map": { 
                              "input": "$ancestors", 
                              "as": "t", 
                              "in": {mu_id : "$$t.u_id" , mu_t1 : "$$t.u_ticheng1" , mu_t2 : "$$t.u_ticheng2"}
                          } 
                      } 
                  }
              }}
          ],function (err , pathdatas) {
              
              //console.log("pathsdata : " + JSON.stringify(pathdatas));

              var startpath = [];

              var mancestor = pathdatas[0].ancestors;
              for(var i=0; i<mancestor.length; i++)
                startpath.push(mancestor[i].mu_id);

              startpath.splice(0, 0, alitem.ruid);
              var startpath = startpath.reverse();
              //console.log(alitem + " :-> " + startpath); 
              
              async.eachSeries(bettingames,function(blitem,callback1) 
                {

                  epModel.aggregate([
                  {
                    $match : {ep_processed : 0 , ep_loginid : alitem.ruid }
                  },
                  {
                   $group : {

                              _id : "$ep_loginid" , 

                              failsum : {
                                      "$sum": {
                                              $cond: [ { $eq: [ "$ep_type", blitem.failflag ] }, "$ep_money", 0 ]
                                           
                                              }
                                           },
                              winsum : {
                                      "$sum": {
                                           $cond: [ { $eq: [ "$ep_type", blitem.winflag ] }, "$ep_money", 0 ]
                                           }
                                         }
                            }
                  }],function (err , firstdocs) {

                    var mfailsum = 0;
                    var mwinsum = 0;
                    if(firstdocs.length != 0)
                    {
                      if(firstdocs[0].failsum != undefined)
                        mfailsum = firstdocs[0].failsum;
                      if(firstdocs[0].winsum != undefined)
                        mwinsum = firstdocs[0].winsum;
                    }

                    var WholeAAmount = mfailsum - mwinsum;

                    thiswholemoney += WholeAAmount;
                    

                    //console.log("WholeAAmount~~ : " + WholeAAmount);



                    epModel.aggregate([
                      {
                        $match : {"ep_others.ep_ag_arr" : alitem.ruid , ep_flag : 2 , "ep_others.game_id" : blitem.gameid }
                      },
                      {
                       $group : {

                                  _id : "$ep_loginid" , 
                                  mwholefsum : { "$sum": "$ep_money" }

                                }
                      }],function (err , seconddocs) {

                          var WholeBAmount = 0;
                          if(seconddocs.length != 0)
                          {
                            if(seconddocs[0].mwholefsum != undefined)
                              WholeBAmount = seconddocs[0].mwholefsum;
                          }

                          var buho = 0;
                          if((WholeAAmount + WholeBAmount) < 0)
                            buho = 1;

                          var finalInputsData = [];

                          if(buho == 1)
                          {   
                              var temppaths = myRemove(startpath , 1);
                              var loginedid = startpath[0];

                              cmaxepid++;

                              if(WholeAAmount != 0)
                              {
                              finalInputsData.push({ ep_id : cmaxepid , ep_datetime : new Date() ,
                                                     ep_loginid : loginedid , ep_money : WholeAAmount ,
                                                     ep_type : 126 , ep_metaid : -100 , 
                                                     ep_others : {game_id : blitem.gameid , game_title : "gyolsan" , 
                                                                  table_name : "earn_pay_history" , 
                                                                  ep_ag_arr : temppaths} , 
                                                     ep_flag : 2
                                                    });
                              }
                          }
                          else if(buho == 0)
                          {
                              var theRest = 0; // give the profit money to the child shop to divide 
                              var theMine = 0; // 

                              //console.log("startpath : " + JSON.stringify(startpath));

                              var j=0;
                              while(j<(startpath.length-1))
                              {
                                var temppaths = myRemove(startpath , j+1);
                                var loginedid = startpath[j];

                                //console.log("temppaths : " + temppaths);
                                //console.log("loginedid : " + loginedid);

                                cmaxepid++;

                                //%
                                var percenter = 0;
                                for(var k=0; k<mancestor.length; k++)
                                {
                                  if(mancestor[k].mu_id == startpath[j]) // find for tichen1, tichneg2
                                  {
                                    if(alitem.rutype == "2") // find for user register type
                                      percenter = mancestor[k].mu_t1;
                                    else if(alitem.rutype == "3")
                                      percenter = mancestor[k].mu_t2;
                                  }
                                }
                                //console.log("percenter : " + percenter);
                                //%

                                //the divided money and the given money for the child 
                                
                                //console.log("WholeBAmount : " + WholeBAmount);
                                if(startpath.length>2)
                                {
                                  if(j == 0) // its bonsa
                                  {
                                    theMine = Math.abs(WholeBAmount) + ((WholeAAmount - Math.abs(WholeBAmount))*(1-percenter));
                                    theRest = (WholeAAmount - Math.abs(WholeBAmount))*percenter;
                                  }  
                                  else if(j == (startpath.length-2))
                                  {
                                    theMine = theRest;
                                  }
                                  else
                                  {
                                    theMine = theRest*(1-percenter);
                                    theRest = theRest*percenter;
                                  }
                                }
                                else if(startpath.length<=2)
                                {
                                  theMine = WholeAAmount;
                                  theRest = 0;
                                }
                                //console.log("theMine : " + theMine + "/" + "theRest : " + theRest);

                                if(theMine != 0)
                                {
                                      finalInputsData.push({ ep_id : cmaxepid , ep_datetime : new Date() ,
                                                       ep_loginid : loginedid , 
                                                       ep_money : theMine ,
                                                       ep_type : 126 , ep_metaid : -100 , 
                                                       ep_others : {game_id : blitem.gameid , game_title : "gyolsan" , 
                                                                    table_name : "earn_pay_history" , 
                                                                    ep_ag_arr : temppaths} , 
                                                       ep_flag : 0
                                                      });
                                }

                                j++;
                              }

                              

                          }

                          if(finalInputsData.length != 0)
                          {
                            console.log("per each datas : " + JSON.stringify(finalInputsData));
                            epModel.insertMany(finalInputsData)
                                    .then(function(manydatas){

                                      if(buho == 0)
                                        updateFlag(alitem.ruid , blitem.gameid);

                                    });
                          }

                          callback1();

                        });
                  });

                },
                function(err) {
                      if (err) throw err;

                      starttime++;
                      gprogressval = ((starttime/wholetime)*100).toFixed(0);
                      //console.log("progressbar : " + gprogressval);

                      updateProcessed(alitem.ruid);
                      callback();
                });

        });

      },
      function(err) {
            if (err) throw err;

            addImplementer(req.session.u_nickname , thiswholemoney);

              res.json({result : "success"});
      });
}

function doCheckOut(req , res)
{
  var bettingames = [
    {gameid : 4 , winflag : 106 , failflag : 6}
  ];

  var allmembers = [];

  lrfModel.aggregate(
    [{ 
        $match : { "u_groupid" : "1" } 
      },
      {
        $group:
         {
           _id: "$u_groupid",
           items: { $push:  {ruid : "$u_id" , rutype : "$u_uppertype"} }
         }
      }
    ],function(err , dcdocs){
      //console.log("all members :~ " + JSON.stringify(dcdocs));
      allmembers = dcdocs[0].items;

      epModel.find().sort({ep_id:-1}).limit(1)
                .then(function (cbdata){
                  if(cbdata.length == 0)
                  {
                    res.json({ result : 0 });
                  }
                  else
                  {
                    console.log("fff : "+cbdata[0].ep_id);
                    domainImp(req , res , bettingames , allmembers , cbdata[0].ep_id);
                  }
                },
                function (err)
                {

                });
      

    });
  
  //res.json({ result : true});
}

function getProgressval(req , res) 
{
  res.json({psval : gprogressval});
}
function setProgressval(req, res)
{
  gprogressval = req.body.val;
  res.json({});
}

function getWholeBPNum(req, res)
{
  bpModel.find({}).count()
          .then(function(gwbpndata){
            res.json({"matched_len" : gwbpndata});
          })
}

function getBPhistory(req, res)
{
  var pvdatas = req.body;
  console.log("pvdatas : " + JSON.stringify(pvdatas));

  bpModel.aggregate([
                      { $sort : { "bp_impdate" : -1} },
                      {
                        $skip : parseInt((pvdatas.currentpage-1)*pvdatas.numperpage)
                      },
                      {
                        $limit : parseInt(pvdatas.numperpage)
                      }

                    ],function(err, cbdata){

                  
                        res.json({result : cbdata});
                    });
}

function expectationImp(req, res)
{
  epModel.aggregate([
                  {
                    $match : {ep_processed : 0}
                  },
                  {
                   $group : {

                              _id : "$ep_processed" , 

                              failsum : {
                                      "$sum": {
                                              $cond: [ { $lt: [ "$ep_type", 100 ] }, "$ep_money", 0 ]
                                           
                                              }
                                           },
                              winsum : {
                                      "$sum": {
                                           $cond: [ { $gte: [ "$ep_type", 100 ] }, "$ep_money", 0 ]
                                           }
                                         }
                            }
                  }],function (err , firstdocs) {
                    var mfailsum = 0;
                    var mwinsum = 0;
                    if(firstdocs.length != 0)
                    {
                      if(firstdocs[0].failsum != undefined)
                        mfailsum = firstdocs[0].failsum;
                      if(firstdocs[0].winsum != undefined)
                        mwinsum = firstdocs[0].winsum;
                    }

                    var ewholeMoney = mfailsum - mwinsum;

                    bpModel.find().sort({bp_impdate:-1}).limit(1)
                          .then(function (cbdata){
                            if(cbdata.length == 0)
                            {
                              res.json({ result : "0" , mymoney : ewholeMoney });
                            }
                            else
                            {
                            console.log("fff : "+cbdata[0].bp_impdate);
                            res.json({ result : cbdata[0].bp_impdate , mymoney : ewholeMoney });
                            }
                          },
                          function (err)
                          {

                          });

              });
}

//end page18

//start load shop list

function getRealChildshop(req, res)
{
  var targetobj = req.session.u_metaid;
  var targetpath = req.session.paths;

  lrfModel.aggregate([
      {
        $match : {
           u_groupid : "2" , paths : { $all: targetpath } , "u_id": { $ne: targetobj }
                  }
      },
      {
        $project : {
          "u_uppercode" : 1 , 
          "u_nickname" : 1 ,
          "u_id" : 1 , 
          "u_deep" : 1 
        }
      }
    ], function(err, grcdsdata){

      //console.log("myshopss~~~~ : " + grcdsdata.length);
      res.json({obj : grcdsdata});

    });
}

function getObjects(obj, key, val, newVal) {
          var newValue = newVal;
          if(newValue.length == 0)
            return obj;
          else{
            var objects = [];
            for (var i in obj) {
                if (!obj.hasOwnProperty(i)) continue;
                if (typeof obj[i] == 'object') {
                    objects = objects.concat(getObjects(obj[i], key, val, newValue));
                } else if (i == key && obj[key] == val) {
                    obj["items"] = newVal;
                }
            }
            return obj;
          }
      }

function getRealChildshopWithHierachied(req, res)
{
  var targetobj = req.session.u_metaid;
  var targetpath = req.session.paths;


  lrfModel.aggregate([
      {
        $match : {
           u_groupid : "2" , paths : { $all: targetpath } 
                  }
      },
      {
        $group : {

          _id : "$u_uppercode" ,
          items : { $push:  { text: "$u_nickname", id: "$u_id" } }
        }
      }
    ], function(err, grcdsdata){

      console.log("err : " + err);
      console.log("myshopss~~~~ : " + JSON.stringify(grcdsdata));
      console.log("@@@ : " + req.body.flag);

      var mfinaltree = {};
      grcdsdata.forEach(function(grdsitem){
        if(grdsitem._id == targetobj)
          mfinaltree = grdsitem;
      });

      var tempgrd = grcdsdata;
      while(tempgrd.length != 0)
      {
        var ivalue = tempgrd.pop();
        if(ivalue._id != targetobj)
          mfinaltree = getObjects(mfinaltree , 'id' , ivalue._id , ivalue.items);
      }
      if(req.body.flag == 0) // shop
        mfinaltree['text'] = req.session.u_nickname;
      else if(req.body.flag == 1) // manager
        mfinaltree['text'] = req.body.myheader;

      mfinaltree['id'] = req.session.u_metaid;

      console.log("mfinaltree~ : " + JSON.stringify(mfinaltree));

      res.json(mfinaltree);
    });
}


function getRealAllChildshop(req, res)
{
  var targetobj = req.session.u_metaid;
  var targetpath = req.session.paths;

  lrfModel.aggregate([
      {
        $match : {
           u_groupid : "2"
                  }
      },
      {
        $project : {
          "u_uppercode" : 1 , 
          "u_nickname" : 1 ,
          "u_id" : 1 , 
          "u_deep" : 1 
        }
      }
    ], function(err, grcdsdata){

      //console.log("myshopss~~~~ : " + grcdsdata.length);
      res.json(grcdsdata);

    });
}

//end load shop list

  //end test for transfer
clientsocket.emit('SUB_SERVER_START' , {"deep" : 1});
app.listen(10001);
